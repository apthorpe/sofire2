\chapter{Output Description}

Output quantities for the one-cell code are listed in Table~\ref{tbl:c1_output};
two-cell output is described in Table~\ref{tbl:c2_output}.

\section{SOFIRE~II, One-Cell}

These transient result quantities listed in Table~\ref{tbl:c1_output} are printed
in the code's tabular output and are also saved as a comma-separated value (CSV)
file to simplify visualization and analysis.

\begin{table}[htb]
    \caption{One-cell output description\label{tbl:c1_output}}
    \begin{center}
        \begin{tabularx}{\textwidth}{llc}
        \toprule
Variable        & Description                & Unit       \\
        \midrule
\texttt{TIME}   & Time                       & \si{\hour} \\
\texttt{XM}     & Sodium burning rate        & \si{\lbm\per\hour\per\square\foot} \\
\texttt{TS}     & Sodium surface temperature & \si{\rankine} \\
\texttt{TGASC}  & Gas temperature            & \si{\rankine} \\
\texttt{TF1}    & Floor temperature, node 1  & \si{\rankine} \\
\midrule
\texttt{TF2}    & Floor temperature, node 2  & \si{\rankine} \\
\texttt{TF3}    & Floor temperature, node 3  & \si{\rankine} \\
\texttt{TF4}    & Floor temperature, node 4  & \si{\rankine} \\
\texttt{TWC1}   & Wall temperature, node 1   & \si{\rankine} \\
\texttt{TWC2}   & Wall temperature, node 2   & \si{\rankine} \\
\midrule
\texttt{TWC3}   & Wall temperature, node 3   & \si{\rankine} \\
\texttt{TWC4}   & Wall temperature, node 4   & \si{\rankine} \\
\texttt{QCONV1} & Convective heat transfer, sodium to gas & \si{\BTU\per\hour} \\
\texttt{QCONV2} & Convective heat transfer, gas to wall   & \si{\BTU\per\hour} \\
\texttt{PGASC}  & Gas pressure               & \si{\psf} absolute \\
\midrule
\texttt{QROD}   & Radiation heat transfer, sodium to wall & \si{\BTU\per\hour} \\
\texttt{QRAD}   & Radiation heat transfer, sodium to gas  & \si{\BTU\per\hour} \\
\texttt{TS1}    & Sodium pool temperature, node 1  & \si{\rankine} \\
\texttt{SUM2}   & Mass of sodium burned      & \si{\lbm} \\
\texttt{OX}     & Oxygen mass remaining in cell & \si{\lbm} \\
\midrule
\texttt{RHOC}   & Gas density                & \si{\lbm\per\cubic\foot} \\
\texttt{SUM1}   & Mass of oxygen consumed    & \si{\lbm} \\
\texttt{W}      & Mass of gas in cell        & \si{\lbm} \\
\texttt{F40}    & Gas leakage volumetric flowrate & \si{\cubic\foot\per\hour} \\
\texttt{C}      & Oxygen concentration corrected for leakage  & weight-\% \ce{O2} \\
\midrule
\texttt{W1}     & Mass of gas exhausted from cell & \si{\lbm} \\
\texttt{W2}     & Mass of gas leaking into/out of cell & \si{\lbm} \\
\texttt{QRAD1}  & Radiation heat transfer, floor liner to floor node 2 & \si{\BTU\per\hour} \\
\texttt{QRAD2}  & Radiation heat transfer, wall liner to wall node 2 & \si{\BTU\per\hour} \\
\texttt{OXR}    & Mass remaining of sodium oxide released to gas & \si{\lbm} \\
\midrule
\texttt{DTMIN1} & Time step based on heat transfer rate to gas & \si{\hour} \\
\texttt{DTMIN2} & Time step based on heat transfer rate from sodium surface & \si{\hour} \\
\texttt{DT}     & Minimum time step & \si{\hour} \\
\texttt{DTMIN}  & Stabilized time step & \si{\hour} \\
\texttt{TS1}    & Sodium pool temperature, node 1 & \si{\rankine} \\
\midrule
\texttt{TS2}    & Sodium pool temperature, node 2 & \si{\rankine} \\
\texttt{TS3}    & Sodium pool temperature, node 3 & \si{\rankine} \\
        \bottomrule
        \end{tabularx}
    \end{center}
\end{table}

\clearpage

\section{SOFIRE~II, Two-Cell}

These transient result quantities listed in Table~\ref{tbl:c2_output} are printed
in the code's tabular output.

\begin{table}[htb]
    \caption{Two-cell output description\label{tbl:c2_output}}
    \begin{center}
        \begin{tabularx}{\textwidth}{llc}
        \toprule
Variable        & Description                & Unit       \\
        \midrule
\texttt{TIME}   & Time                       & \si{\hour} \\
\texttt{XMC}    & Sodium burning rate        & \si{\lbm\per\hour\per\square\foot} \\
\texttt{TSC}    & Sodium surface temperature & \si{\rankine} \\
\texttt{TFC1}   & Primary cell floor temperature, node 1  & \si{\rankine} \\
\texttt{TFC2}   & Primary cell floor temperature, node 2  & \si{\rankine} \\
\midrule
\texttt{TFC3}   & Primary cell floor temperature, node 3  & \si{\rankine} \\
\texttt{TFC4}   & Primary cell floor temperature, node 4  & \si{\rankine} \\
\texttt{TWC1}   & Primary cell wall temperature, node 1   & \si{\rankine} \\
\texttt{TWC2}   & Primary cell wall temperature, node 2   & \si{\rankine} \\
\texttt{TWC3}   & Primary cell wall temperature, node 3   & \si{\rankine} \\
\midrule
\texttt{TWC4}   & Primary cell wall temperature, node 4   & \si{\rankine} \\
\texttt{TGASC}  & Primary cell gas temperature            & \si{\rankine} \\
\texttt{TWS1}   & Secondary cell wall temperature, node 1 & \si{\rankine} \\
\texttt{TWS2}   & Secondary cell wall temperature, node 2 & \si{\rankine} \\
\texttt{TWS3}   & Secondary cell wall temperature, node 3 & \si{\rankine} \\
\midrule
\texttt{TWS4}   & Secondary cell wall temperature, node 4 & \si{\rankine} \\
\texttt{TGASS}  & Secondary cell gas temperature          & \si{\rankine} \\
\texttt{PGASC}  & Primary cell gas pressure               & \si{\psf} absolute \\
\texttt{QCONV1} & Convective heat transfer, sodium to primary cell gas & \si{\BTU\per\hour} \\
\texttt{QCONV2} & Convective heat transfer, gas to primary cell wall  & \si{\BTU\per\hour} \\
\midrule
\texttt{QCONV3} & Convective heat transfer, gas to secondary cell wall  & \si{\BTU\per\hour} \\
\texttt{QRADC}  & Radiation heat transfer, sodium to primary cell wall & \si{\BTU\per\hour} \\
\texttt{W}      & Mass of gas transferred to primary cell        & \si{\lbm} \\
\texttt{PGASS}  & Secondary cell gas pressure               & \si{\psf} absolute \\
\texttt{SUMS}   & Mass of oxygen consumed    & \si{\lbm} \\
\midrule
\multicolumn{3}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htb]
    \begin{center}
        \begin{tabularx}{\textwidth}{llc}
\multicolumn{3}{c}{\textbf{Table~\ref{tbl:c2_output} Two-cell output description -- \textit{Continued from previous page}}} \\
\toprule
Variable        & Description                & Unit       \\
\midrule
\texttt{SUMC}   & Mass of sodium burned      & \si{\lbm} \\
\texttt{WC}     & Mass of gas in primary cell        & \si{\lbm} \\
\texttt{WS}     & Mass of gas in secondary cell        & \si{\lbm} \\
\texttt{QRODC}  & Radiation heat transfer, sodium to primary cell gas & \si{\BTU\per\hour} \\
\texttt{QRAD1}  & Radiation heat transfer, primary cell wall liner to wall node 2 & \si{\BTU\per\hour} \\
\midrule
\texttt{QRAD2}  & Radiation heat transfer, primary cell floor liner to floor node 2 & \si{\BTU\per\hour} \\
\texttt{QRAD4}  & Radiation heat transfer, secondary cell wall liner to wall node 2 & \si{\BTU\per\hour} \\
\texttt{CC}     & Oxygen concentration in primary gas & weight fraction \ce{O2} \\
\texttt{CS}     & Oxygen concentration in secondary gas & weight fraction \ce{O2} \\
\texttt{TSC1}   & Sodium pool temperature, node 1  & \si{\rankine} \\
\midrule
\texttt{ALPHA}  & Gas thermal diffusivity                & \si{\square\foot\per\hour} \\
\texttt{V10}    & Gas velocity in opening between cells & \si{\lbm} \\
\texttt{W1}     & Mass of gas transferred to primary cell & \si{\lbm} \\
\texttt{DTMIN1} & Time step based on heat transfer rate from sodium surface & \si{\hour} \\
\texttt{DTMIN2} & Time step based on heat transfer rate to primary cell gas & \si{\hour} \\
\midrule
\texttt{DTMIN}  & Stabilized time step & \si{\hour} \\
\texttt{TSC2}   & Sodium pool temperature, node 2 & \si{\rankine} \\
\texttt{TSC3}   & Sodium pool temperature, node 3 & \si{\rankine} \\
\texttt{TSC4}   & Sodium pool temperature, node 4 & \si{\rankine} \\
\texttt{WIN}    & Mass of gas inleakage to secondary cell & \si{\lbm} \\
\midrule
\texttt{WPA}    & \emph{Not used}               & - \\
\texttt{WEXT}   & Mass of gas exhausted from secondary cell & \si{\lbm} \\
\texttt{F3}     & Secondary gas exhaust volumetric flowrate & \si{\cubic\foot\per\minute} \\
\texttt{PA}     & Ambient gas pressure               & \si{\psf} absolute \\
        \bottomrule
        \end{tabularx}
    \end{center}
\end{table}

% \clearpage
