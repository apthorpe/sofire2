program c1_property_data_z

    ! Test Zofu real asserts.

    use zofu
    use proplib, only: populate_mdb

    implicit none

    type(unit_test_type) :: test

    continue

    call populate_mdb()

    call test%init()

    call test%run(test_ar_g_cp_janaf)
    call test%run(test_ar_g_mu_yaws)

    call test%summary()

    ! Set non-zero exit code to indicate test failure to CMake/CTest
    if (test%assertions%failed > 0) then
        stop 1
    end if

contains

! Cp(Ar(g)) from 200K to 6000K, Chase 1985 (JANAF, 3rd Ed.)
subroutine test_ar_g_cp_janaf(test)
    use proplib, only: WP, RGAS, mdb, gid_Ar, ftoa
    implicit none
    class(unit_test_type), intent(in out) :: test
    real(kind=WP) :: T, expected, actual, reltol
    character(len=80) :: msg
    integer :: i
1   format('Cp(Ar(condensed), T=', F8.1, ') +-', F4.1, '% kJ/kmol')
    continue

    T = 100.0_WP
    expected = 20.786_WP
    reltol = 0.005_WP
    do i = 1, 59
        T = T + 100.0_WP
        actual = mdb%gas(gid_Ar)%cp(T) * RGAS
        write(msg, 1) T, reltol * 100.0_WP
        call test%assert(actual, expected, msg, reltol)
    end do

    return
end subroutine test_ar_g_cp_janaf

! Viscosity (mu) for Ar(g) from 200K to 3200K, Yaws 2009
! (Transport Properties of Hydrocarbons and Chemicals)
subroutine test_ar_g_mu_yaws(test)
    use proplib, only: WP, mdb, gid_Ar, ftoa
    implicit none
    class(unit_test_type), intent(in out) :: test
    real(kind=WP) :: T, expected, actual, reltol
    character(len=80) :: msg
    integer :: i
1   format('mu(Ar(condensed), T=', F8.1, ') +-', F4.1, '% kJ/kmol')
    continue

    T = 150.0_WP
    reltol = 0.08_WP
    do i = 1, 63
        T = T + 50.0_WP
        ! Viscosity
        actual = mdb%gas(gid_Ar)%mu(T)
        expected = 6.1898_WP + T * (7.8295E-1_WP                        &
            + T * (-2.1921E-4_WP + T * 2.8331E-8_WP))
        write(msg, 1) T, reltol
        call test%assert(actual, expected, msg, reltol)
    end do

    return
end subroutine test_ar_g_mu_yaws

end program c1_property_data_z
