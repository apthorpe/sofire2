!> @file c1_sofire_c1.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_sofire_c1 module.
program c1_sofire_c1
    use sofire2_param, only: WP
    use m_constant, only: ZERO!, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_sofire_c1
    use toast
    implicit none

    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_sofire_c1")

    !!! Test exposed functions

    ! set_initial_conditions(scenario, result_curr, cell_curr,            &
    !     crust_curr, dt_manager, hs_wall, hs_floor, hs_na_pool,          &
    !     hs_na_crust, qcond_path, qconv_path, qrad_path, mf_dry_air,     &
    !     legacy_rgas)

    ! calculate_single_timestep(scenario, result_curr,                    &
    !     result_prev, cell_curr, cell_prev, crust_curr, dt_manager,      &
    !     hs_wall, hs_floor, hs_na_pool, hs_na_crust, qcond_path,         &
    !     qconv_path, qrad_path, PGASC_adjusted, TGASC_adjusted,          &
    !     mf_dry_air, prtct, errmsg, EXX_error)

    ! write_new_model_state(test_cell, crust)

    ! q_radiation_r(emiss, area, tsrc, tdest)

    ! calculate_burning_rate(M_SODIUM, ANA, APOOL, RHO_OX, T1, EXX,       &
    !     DTMIN, XM, OXLB)

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_sofire_c1.json")

    call test%checkfailure()
end program c1_sofire_c1
