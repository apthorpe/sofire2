!> @file c1_property_data.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_material module. Some reference
!! thermodynamic data taken from @cite Haberman1980, some reference
!! transport correlations taken from @cite Yaws2009
program c1_property_data
    use sofire2_param, only: WP
    use m_constant, only: ZERO, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_material
    use toast
    implicit none

!    gid_N2, gid_O2, aid_Na2O, aid_Na2O2

    ! Taken from Haberman1980
    real(kind=WP), dimension(*), parameter :: cp_h20_hj = (/            &
        1.851_WP, 1.852_WP, 1.869_WP, 1.890_WP, 1.913_WP,               &
        1.939_WP, 1.967_WP, 1.997_WP, 2.029_WP, 2.061_WP,               &
        2.095_WP, 2.129_WP, 2.164_WP, 2.198_WP, 2.233_WP,               &
        2.266_WP, 2.299_WP, 2.331_WP, 2.369_WP, 2.407_WP,               &
        2.440_WP, 2.473_WP, 2.504_WP, 2.535_WP, 2.565_WP,               &
        2.593_WP, 2.621_WP, 2.648_WP, 2.673_WP, 2.698_WP,               &
        2.721_WP, 2.744_WP /)

    ! Taken from Haberman1980
    real(kind=WP), dimension(*), parameter :: cp_co2_hj = (/            &
        0.761_WP, 0.817_WP, 0.869_WP, 0.916_WP, 0.958_WP,               &
        0.995_WP, 1.029_WP, 1.060_WP, 1.088_WP, 1.113_WP,               &
        1.137_WP, 1.158_WP, 1.177_WP, 1.195_WP, 1.212_WP,               &
        1.227_WP, 1.240_WP, 1.253_WP, 1.264_WP, 1.275_WP,               &
        1.285_WP, 1.294_WP, 1.302_WP, 1.309_WP, 1.317_WP,               &
        1.323_WP, 1.329_WP, 1.335_WP, 1.340_WP, 1.345_WP,               &
        1.350_WP, 1.355_WP /)

    ! Cp(CO2) in J/mol-K from JANAF, from 200 K to 2000 K in 100 K
    real(kind=WP), dimension(*), parameter :: cp_co2_j = (/             &
        32.359_WP, 37.221_WP, 41.325_WP, 44.627_WP, 47.321_WP,          &
        49.564_WP, 51.434_WP, 52.999_WP, 54.308_WP, 55.409_WP,          &
        56.342_WP, 57.137_WP, 57.802_WP, 58.379_WP, 58.886_WP,          &
        59.317_WP, 59.701_WP, 60.049_WP, 60.350_WP /)

    ! Note: Entry for 100 C (69.4) was calculated by linear
    ! interpolation from https://www.ohio.edu/mechanical/thermo/property_tables/combustion/oxygen_enth.html
    ! Original tabular entry was 63.5, yielding a suspiciously large
    ! deviation against CEA2 (9%). Remaining values are within 2%
    real(kind=WP), dimension(*), parameter :: h_o2_hj = (/              &
         -69.3_WP,  -22.9_WP,   23.0_WP,   69.4_WP,  116.4_WP,          &
         164.2_WP,  212.7_WP,  262.1_WP,  312.2_WP,  363.0_WP,          &
         414.6_WP,  466.7_WP,  519.4_WP,  572.6_WP,  626.3_WP,          &
         680.4_WP,  734.7_WP,  789.7_WP,  844.9_WP,  900.3_WP,          &
         956.1_WP, 1012.1_WP /)

    ! Cp(Na(condensed))) in J/mol-K from JANAF, from 200 K to 500 K in
    ! 50 K increments. Below 371 K are properties of Na(cr), 371 K and
    ! above are for Na(L)
    real(kind=WP), dimension(*), parameter :: cp_na_C_j = (/            &
         25.987_WP,  27.008_WP,  28.204_WP,  30.142_WP,  31.510_WP,     &
         31.004_WP, 30.552_WP /)

    ! Entropy in J/mol-K from JANAF, from 200 K to 900 K in 100 K
    ! increments. 900 K was selected as the upper cutoff because
    ! JANAF data assumes alpha-phase solid above 948 K while CEA2
    ! assumes liquid phase, resulting in ~10% deviation in entropy
    ! values
    real(kind=WP), dimension(*), parameter :: s_na2o2_j = (/            &
         61.785_WP,  95.354_WP, 122.268_WP, 144.748_WP, 164.093_WP,     &
        181.108_WP, 203.601_WP, 216.981_WP /)

    ! Cp(Na2O2) in J/mol-K from JANAF, from 200 K to 900 K in 100 K
    ! increments. See above.
    real(kind=WP), dimension(*), parameter :: cp_na2o2_j = (/           &
        75.479_WP,  89.408_WP,  97.721_WP,  103.767_WP,  108.445_WP,    &
       112.332_WP, 113.596_WP, 113.596_WP /)

    ! Cp(Na2O) (alpha,beta,gamma) from 200 K to 1400 K; JANAF 3e
    real(kind=WP), dimension(*), parameter :: cp_na2o_j = (/            &
        59.551_WP,  69.245_WP, 75.776_WP, 81.496_WP, 85.709_WP,         &
        88.851_WP,  91.291_WP, 93.266_WP, 94.918_WP, 96.353_WP,         &
        97.629_WP,  98.788_WP, 99.860_WP /)

    ! Cp(N2) (g) from 200 K to 2100 K; JANAF 3e
    real(kind=WP), dimension(*), parameter :: cp_n2_j = (/              &
        29.107_WP,  29.125_WP, 29.249_WP, 29.580_WP, 30.110_WP,         &
        30.754_WP,  31.433_WP, 32.090_WP, 32.697_WP, 33.241_WP,         &
        33.723_WP,  34.147_WP, 34.518_WP, 34.843_WP, 35.128_WP,         &
        35.378_WP,  35.600_WP, 35.796_WP, 35.971_WP, 36.126_WP /)

    ! Cp(O2) (g) from 200 K to 2100 K; JANAF 3e
    real(kind=WP), dimension(*), parameter :: cp_o2_j = (/              &
        29.126_WP,  29.385_WP, 30.106_WP, 31.091_WP, 32.090_WP,         &
        32.981_WP,  33.733_WP, 34.355_WP, 34.870_WP, 35.300_WP,         &
        35.667_WP,  35.988_WP, 36.277_WP, 36.544_WP, 36.796_WP,         &
        37.040_WP,  37.277_WP, 37.510_WP, 37.741_WP, 37.969_WP /)

    type(material_database) :: mdb

    real(kind=WP) :: T
    real(kind=WP) :: cp_offset
    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_property_data")

!    call mdb%init()
    call mdb%populate()

    ! *** Ar ***

    ! Cp(Ar(g)) from 200K to 6000K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    refval = 20.786_WP
    do i = 1, 59
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(Ar)/Cp_ref at ', T, ' K = ',                            &
        !     mdb%gas(gid_Ar)%cp(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ', T, &
        !      ' K = Cp(Ar) = ', mdb%gas(gid_Ar)%cp(T) * cp_offset,       &
        !      ' vs Cp_ref = ', refval, ' kJ/kmol-K'
        call test%assertequal(mdb%gas(gid_Ar)%cp(T) * cp_offset,        &
            refval, rel_tol=0.005_WP,                                   &
            message="Cp(Ar(condensed)) +-0.5% kJ/kmol")
    end do

    ! mu for Ar(g) from 200K to 3200K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    T = 150.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 63
        T = T + 50.0_WP
        ! Viscosity
        refval = 6.1898_WP + T * (7.8295E-1_WP                          &
            + T * (-2.1921E-4_WP + T * 2.8331E-8_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'mu(N2)/mu_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_Ar)%mu(T) / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(N2) = ', mdb%gas(gid_Ar)%mu(T),                  &
        !     ' vs mu_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_Ar)%mu(T),                    &
            refval, rel_tol=0.08_WP,                                    &
            message="mu(Ar(g)) +-8%")
    end do

    ! k for Ar(g) from 200K to 2000K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    T = 150.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 37
        T = T + 50.0_WP
        ! Conductity
        refval = -3.0142E-4_WP + T * (6.8128E-5_WP                      &
            + T * (-3.1822E-8_WP + T * 6.8540E-12_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'k(N2)/k_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_Ar)%k(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = k(N2) = ', mdb%gas(gid_Ar)%k(T) * cp_offset,       &
        !     ' vs k_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_Ar)%k(T) * cp_offset,         &
            refval, rel_tol=0.095_WP,                                   &
            message="k(Ar(g)) +-9.5%")
    end do

    call test%assertequal(mdb%gas(gid_Ar)%cp_t_lo(), 200.0_WP,          &
        rel_tol=0.01_WP,                                                &
        message="cp(Ar(g)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%gas(gid_Ar)%cp_t_hi(), 20000.0_WP,        &
        rel_tol=0.01_WP,                                                &
        message="cp(Ar(g)) T-upper = 20000 K +-1%")

    call test%assertequal(mdb%gas(gid_Ar)%mw, 39.948_WP,                &
        rel_tol=0.001_WP,                                               &
        message="MW(Ar) is 39.948 g/mol (JANAF)")

    ! *** CO2 ***

    ! Cp(CO2(g)) from -50C to 1500C, App. B, Table B-15
    ! "Engineering Thermodynamics" Haberman, William and John, James,
    ! 1980, Allyn and Bacon
    T = -100.0_WP
    cp_offset = RGAS / mdb%gas(gid_CO2)%mw
    do i = 1, size(cp_co2_hj)
        T = T + 50.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'Cp(CO2)/Cp_ref at ', T,    &
        !      ' C = ', mdb%gas(gid_CO2)%cp(T + T0K) * cp_offset / cp_co2_hj(i), ''!' kJ/kg-K'
        call test%assertequal(mdb%gas(gid_CO2)%cp(T + T0K) * cp_offset, &
            cp_co2_hj(i), rel_tol=0.005_WP,                             &
            message="Cp(CO2(g)) +-0.5% kJ/kg-K")
    end do

    ! Cp(CO2(g)) from 200 K to 2000 K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(cp_co2_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'Cp(CO2)/Cp_ref at ', T,    &
        !      ' C = ', mdb%gas(gid_CO2)%cp(T) * cp_offset / cp_co2_j(i), ''!' kJ/kg-K'
        call test%assertequal(mdb%gas(gid_CO2)%cp(T) * cp_offset,       &
            cp_co2_j(i), rel_tol=0.005_WP,                              &
            message="Cp(CO2(g)) +-0.5% kJ/kg-K")
    end do

    call test%assertequal(mdb%gas(gid_CO2)%mw, 44.0098_WP,              &
        rel_tol=0.001_WP,                                               &
        message="MW(CO2) is 44.0098 g/mol (JANAF)")

    call test%assertequal(mdb%gas(gid_CO2)%dHform, -393.522E3_WP,       &
        rel_tol=0.001_WP,                                               &
        message="dHform(CO2) at 298.15K is -393.522 kJ/mol (JANAF)")

    ! *** Na ***

    ! Cp(Na(g)) from 200 K to 1100 K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    refval = 20.786_WP
    cp_offset = RGAS
    do i = 1, 10
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(Na(g)))/Cp_ref at ', T, ' K = ',                        &
        !     mdb%gas(gid_Na)%cp(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ', T, &
        !         ' K = Cp(Na(g))) = ', mdb%gas(gid_Na)%cp(T) * cp_offset,  &
        !         ' vs Cp_ref = ', refval, ' kJ/kmol-K'
        call test%assertequal(mdb%gas(gid_Na)%cp(T) * cp_offset,        &
            refval, rel_tol=0.06_WP,                                    &
            message="Cp(Na(g)) +-6% kJ/kmol")
    end do

    ! mu for Na(g) from 1200K to 1500K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    ! While this appears to be a poor correlation, sodium vapor viscosity
    ! measurements vary over a wide range. Figure 1.9 of TID-26008
    ! "Sodium-NaK Engineering Handbook. Volume 1. Sodium Chemistry
    ! and Physical Properties" presents a range of values consistent with
    ! the results from CEA2
    T = 1150.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 7
        T = T + 50.0_WP
        ! Viscosity
        refval = 13.0_WP + T * (3.87E-1_WP                              &
            + T * (-2.2333E-4_WP + T * 6.6667E-8_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'mu(Na)/mu_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_Na)%mu(T) / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(Na) = ', mdb%gas(gid_Na)%mu(T),                  &
        !     ' vs mu_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_Na)%mu(T),                    &
            refval, rel_tol=0.21_WP,                                    &
            message="mu(Na(g)) +-21%")
    end do

    ! k for Na(g) from 1200K to 1450K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals). See above
    ! for commentary on data variability. Section 1-5.5(f) of TID-26008
    ! notes the difficulty of measuring the thermal conductivity of
    ! sodium vapor
    T = 1150.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 6
        T = T + 50.0_WP
        ! Conductity
        refval = 1.5488_WP + T * (-3.2414E-3_WP                         &
            + T * (2.3230E-6_WP + T * (-5.5333E-10_WP)))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'k(Na(g))/k_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_Na)%k(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = k(Na(g)) = ', mdb%gas(gid_Na)%k(T) * cp_offset,       &
        !     ' vs k_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_Na)%k(T) * cp_offset,         &
            refval, rel_tol=0.4_WP,                                     &
            message="k(Na(g)) +-40%")
    end do

    call test%assertequal(mdb%gas(gid_Na)%cp_t_lo(), 200.0_WP,          &
        rel_tol=0.01_WP,                                                &
        message="cp(Na(g)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%gas(gid_Na)%cp_t_hi(), 20000.0_WP,        &
        rel_tol=0.01_WP,                                                &
        message="cp(Na(g)) T-upper = 20000 K +-1%")

    call test%assertequal(mdb%gas(gid_Na)%mw, 22.98977_WP,              &
        rel_tol=0.001_WP,                                               &
        message="MW(Na) is 22.98977 g/mol (JANAF)")

    call test%assertequal(mdb%gas(gid_Na)%dHform, 107.3E3_WP,           &
        rel_tol=0.005_WP,                                               &
        message="dHform(Na) at 298.15K is 107.3 kJ/mol (JANAF)")

    ! Cp(Na(condensed)) from 200 K to 500 K, Chase 1985 (JANAF, 3rd Ed.)
    T = 150.0_WP
    refval = 20.786_WP
    cp_offset = RGAS
    do i = 1, size(cp_na_C_j)
        T = T + 50.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(Na(condensed)))/Cp_ref at ', T, ' K = ',                &
        !     mdb%aerosol(aid_Na)%cp(T) * cp_offset / cp_na_C_j(i), ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ', T, &
        !         ' K = Cp(Na(condensed))) = ', mdb%aerosol(aid_Na)%cp(T) * cp_offset,  &
        !         ' vs Cp_ref = ', cp_na_C_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%aerosol(aid_Na)%cp(T) * cp_offset,    &
            cp_na_C_j(i), rel_tol=0.01_WP,                              &
            message="Cp(Na(condensed)) +-1% kJ/kmol")
    end do

    call test%assertequal(mdb%aerosol(aid_Na)%mw, 22.98977_WP,          &
        rel_tol=0.001_WP,                                               &
        message="MW(Na) is 22.98977 g/mol (JANAF)")

    ! *** Na2O ***

    ! Cp(Na2O(condensed)) from 200K to 1400K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(cp_na2o_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(Na2O)/Cp_ref at ', T, ' K = ',                          &
        !     mdb%aerosol(aid_Na2O)%cp(T) * cp_offset / cp_na2o_j(i), ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ', T,   &
        !      ' K = Cp(Na2O) = ', mdb%aerosol(aid_Na2O)%cp(T) * cp_offset,   &
        !      ' vs Cp_ref = ', cp_na2o_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%aerosol(aid_Na2O)%cp(T) * cp_offset,  &
            cp_na2o_j(i), rel_tol=0.06_WP,                              &
            message="Cp(Na2O(condensed)) +-6% kJ/kmol")
    end do

    call test%assertequal(mdb%aerosol(aid_Na2O)%cp_t_lo(), 200.0_WP,    &
        rel_tol=0.01_WP,                                                &
        message="cp(Na2O(condensed)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%aerosol(aid_Na2O)%cp_t_hi(), 6000.0_WP,   &
        rel_tol=0.01_WP,                                                &
        message="cp(Na2O(condensed)) T-upper = 6000 K +-1%")

    ! Heat released per pound of Na consumed for Na2O: 3876.4 vs 3900.0
    call test%assertequal(-mdb%aerosol(aid_Na2O)%dHform                 &
        / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G),           &
        3900.0_WP, rel_tol=0.01_WP,                                     &
        message="hForm(Na2O)/mNa is about 3900 +-1% BTU/lbm-Na")
    ! write(unit=6, fmt="(A, F12.4)") "hForm(Na2O)/mNa is about 3900 BTU/lbm-Na vs ", &
    !     -mdb%aerosol(aid_Na2O)%dHform                                   &
    !     / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G)
    call test%assertequal(mdb%aerosol(aid_Na2O)%dHform, -417.98E3_WP,   &
        abs_tol=4.2E3_WP,                                               &
        message="hForm(Na2O) is -417.98kJ/mol (JANAF)")
    ! write(unit=6, fmt="(A, ES16.6)") "hForm(Na2O) expected to be -417.98kJ/mol vs ", &
    !     mdb%aerosol(aid_Na2O)%dHform

    call test%assertequal(mdb%aerosol(aid_Na2O)%mw, 61.97894_WP,        &
        rel_tol=0.001_WP,                                               &
        message="MW(Na2O) is 61.67894 g/mol (JANAF)")

    ! *** Na2O2 ***

    ! S(Na2O2(condensed)) from 200K to 900K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(s_na2o2_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'S(Na2O2)/S_ref at ', T, &
        !      ' C = ', mdb%aerosol(aid_Na2O2)%s(T) * cp_offset / s_na2o2_j(i), ''!' kJ/kg-K'
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ', T,   &
        !      ' K = S(Na2O2) = ', mdb%aerosol(aid_Na2O2)%s(T) * cp_offset,   &
        !      ' vs S_ref = ', s_na2o2_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%aerosol(aid_Na2O2)%s(T) * cp_offset,  &
            s_na2o2_j(i), rel_tol=0.01_WP,                              &
            message="S(Na2O2(condensed)) +-1% kJ/kmol (<1000K)")
    end do

    ! Cp(Na2O2(condensed)) from 200K to 900K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(cp_na2o2_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(Na2O2)/Cp_ref at ', T, ' C = ',                         &
        !     mdb%aerosol(aid_Na2O2)%cp(T) * cp_offset / cp_na2o2_j(i), ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = Cp(Na2O2) = ',                                     &
        !     mdb%aerosol(aid_Na2O2)%cp(T) * cp_offset,                   &
        !         ' vs Cp_ref = ', cp_na2o2_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%aerosol(aid_Na2O2)%cp(T) * cp_offset, &
            cp_na2o2_j(i), rel_tol=0.01_WP,                             &
            message="Cp(Na2O2(condensed)) +-6% kJ/kmol")
    end do

    call test%assertequal(mdb%aerosol(aid_Na2O2)%cp_t_lo(), 200.0_WP,   &
        rel_tol=0.01_WP, message="cp(Na2O2(condensed)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%aerosol(aid_Na2O2)%cp_t_hi(), 6000.0_WP,  &
        rel_tol=0.01_WP, message="cp(Na2O2(condensed)) T-upper = 6000 K +-1%")

    ! Heat released per pound of Na consumed for Na2O2: 4787.4 vs 4500.0
    ! Note that JANAF also gives about 4800 BTU/lbm-Na; suspect SOFIRE II data is low
    call test%assertequal(-mdb%aerosol(aid_Na2O2)%dHform                &
        / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G),           &
        4500.0_WP, rel_tol=0.08_WP,                                     &
        message="hForm(Na2O2)/mNa is 4500 +-8% BTU/lbm-Na")
    ! write(unit=6, fmt="(A, F12.4)") "hForm(Na2O2)/mNa is about 4500 BTU/lbm-Na vs ", &
    !     -mdb%aerosol(aid_Na2O2)%dHform                                  &
    !     / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G)
    call test%assertequal(mdb%aerosol(aid_Na2O2)%dHform, -513.21E3_WP,  &
        abs_tol=5.0E3_WP,                                               &
        message="hForm(Na2O2) is -513.21kJ/mol (JANAF)")
    ! write(unit=6, fmt="(A, ES16.6)") "hForm(Na2O2) expected to be -513.2kJ/mol vs ", &
    !     mdb%aerosol(aid_Na2O2)%dHform

    call test%assertequal(mdb%aerosol(aid_Na2O2)%mw, 77.97834_WP,       &
        rel_tol=0.001_WP,                                               &
        message="MW(Na2O2) is 77.97834 g/mol (JANAF)")

    ! *** N2 ***

    ! Cp(N2(g)) from 200K to 2100K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(cp_n2_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(N2)/Cp_ref at ', T, ' C = ',                            &
        !     mdb%gas(gid_N2)%cp(T) * cp_offset / cp_n2_j(i), ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = Cp(N2) = ',                                     &
        !     mdb%gas(gid_N2)%cp(T) * cp_offset,                   &
        !         ' vs Cp_ref = ', cp_n2_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%gas(gid_N2)%cp(T) * cp_offset,        &
            cp_n2_j(i), rel_tol=0.01_WP,                                &
            message="Cp(N2(g)) +-1% kJ/kmol")
    end do

    ! k and mu for N2(g) from 250K to 1900K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    T = 200.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 26
        T = T + 50.0_WP
        ! Conductivity
        refval = -2.2678E-4_WP + T * (1.0275E-4_WP                       &
            + T * (-6.0151E-8_WP + T * 2.2332E-11_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'k(N2)/k_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_N2)%k(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = k(N2) = ', mdb%gas(gid_N2)%k(T) * cp_offset,       &
        !     ' vs k_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_N2)%k(T) * cp_offset,         &
            refval, rel_tol=0.06_WP,                                    &
            message="k(N2(g)) +-6%")
    end do

    T = 200.0_WP
    cp_offset = 1.0_WP
    do i = 1, 35
        T = T + 50.0_WP
        ! Viscosity
        refval = 4.4656_WP + T * (6.3814E-1_WP                          &
            + T * (-2.6956E-4_WP + T * 5.4113E-8_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'mu(N2)/mu_ref at ', &
        !     T, ' K = ', mdb%gas(gid_N2)%mu(T) / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(N2) = ', mdb%gas(gid_N2)%mu(T),                 &
        !     ' vs mu_ref = ', refval, ' micropoise'
        call test%assertequal(mdb%gas(gid_N2)%mu(T),                    &
            refval, rel_tol=0.05_WP,                                    &
            message="mu(N2(g)) +-5%")
    end do

    call test%assertequal(mdb%gas(gid_N2)%mu_t_lo(), 200.0_WP,          &
        rel_tol=0.01_WP, message="mu(N2(g)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%gas(gid_N2)%mu_t_hi(), 15000.0_WP,        &
        rel_tol=0.01_WP, message="mu(N2(g)) T-upper = 15000 K +-1%")

    call test%assertequal(mdb%gas(gid_N2)%mw, 28.0134_WP,               &
        rel_tol=0.001_WP,                                               &
        message="MW(N2) is 28.0134 g/mol (JANAF)")

    ! *** O2 ***

    ! h(O2(g)) from -50C to 1000C, App. B, Table B-17
    ! "Engineering Thermodynamics" Haberman, William and John, James,
    ! 1980, Allyn and Bacon. Note that h(100C) was interpolated as
    ! 69.4 kJ/kg from a different source (see above) vs 63.5 kJ/kg
    ! as given in the table. Lower value gave ~9% deviation with CEA2
    ! correlation; remaining values are within 3%
    T = -100.0_WP
    cp_offset = RGAS / mdb%gas(gid_O2)%mw
    do i = 1, size(h_o2_hj)
        T = T + 50.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'h(O2)/h_ref at ', T, &
        !      ' C = ', mdb%gas(gid_O2)%h(T + T0K) * cp_offset / h_o2_hj(i), ''!' kJ/kg-K'
        call test%assertequal(mdb%gas(gid_O2)%h(T + T0K) * cp_offset,   &
            h_o2_hj(i), rel_tol=0.03_WP,                                &
            message="h(O2(g)) +-3.0% kJ/kg")
    end do
    ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'h(O2) at ', 25.0_WP,    &
    !     ' C = ', mdb%gas(gid_O2)%h(25.0_WP + T0K) * cp_offset, ' kJ/kg-K'
    call test%assertequal(mdb%gas(gid_O2)%h(25.0_WP + T0K) * cp_offset, &
        ZERO, abs_tol=0.05_WP, message="h(O2(g)) at 25C should be zero")

    ! Cp(O2(g)) from 200K to 2100K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(cp_o2_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)')                      &
        !     'Cp(O2)/Cp_ref at ', T, ' C = ',                            &
        !     mdb%gas(gid_O2)%cp(T) * cp_offset / cp_o2_j(i), ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = Cp(O2) = ',                                     &
        !     mdb%gas(gid_O2)%cp(T) * cp_offset,                   &
        !         ' vs Cp_ref = ', cp_o2_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%gas(gid_O2)%cp(T) * cp_offset,        &
            cp_o2_j(i), rel_tol=0.01_WP,                                &
            message="Cp(O2(g)) +-1% kJ/kmol")
    end do

    ! k and mu for O2(g) from 200K to 2000K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    ! Note that CEA2 returns k as W/cm-K * 1E6 or W/m-K * 1E4. That is,
    ! divide k(CEA2) by 10000 to get conductivity in units of W/m-K
    ! Both correlations return viscosity in micropoise
    T = 150.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 37
        T = T + 50.0_WP
        ! Conductivity
        refval = 1.5475E-4_WP + T * (9.4153E-5_WP                       &
            + T * (-2.7529E-8_WP + T * 5.2069E-12_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'k(O2)/k_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_O2)%k(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(N2) = ', mdb%gas(gid_O2)%k(T) * cp_offset,      &
        !     ' vs mu_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_O2)%k(T) * cp_offset,         &
            refval, rel_tol=0.03_WP,                                    &
            message="k(O2(g)) +-3%")

        ! Viscosity
        refval = -4.9433_WP + T * (8.0673E-1_WP                         &
            + T * (-4.0416E-4_WP + T * 1.0111E-7_WP))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'mu(O2)/mu_ref at ', &
        !     T, ' K = ', mdb%gas(gid_O2)%mu(T) / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(N2) = ', mdb%gas(gid_O2)%mu(T),                 &
        !     ' vs mu_ref = ', refval, ' uP'
        call test%assertequal(mdb%gas(gid_O2)%mu(T),                    &
            refval, rel_tol=0.05_WP,                                    &
            message="mu(O2(g)) +-5%")
    end do

    call test%assertequal(mdb%gas(gid_O2)%k_t_lo(), 200.0_WP,           &
        rel_tol=0.01_WP, message="k(O2(g)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%gas(gid_O2)%k_t_hi(), 15000.0_WP,         &
        rel_tol=0.01_WP, message="k(O2(g)) T-upper = 15000 K +-1%")

    call test%assertequal(mdb%gas(gid_O2)%mw, 31.9988_WP,               &
        rel_tol=0.001_WP,                                               &
        message="MW(O2) is 31.9988 g/mol (JANAF)")

    ! ! Print summary and reset
    ! call printsummary(test)
    ! call test%reset()

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_property_data.json")

    call test%checkfailure()
end program c1_property_data
