!> @file m_burnchem.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Burn chemistry routines
module m_burnchem
    use sofire2_param, only: WP
    implicit none
    private

    public :: k_from_s
    public :: s_from_kna2o
    public :: yna2o_from_k
    public :: k_from_yna2o
    public :: qburn_mNa_from_k

contains

!> @brief Derive fraction of consumed \f$\ce{O2}\f$ resulting in \f$\ce{Na2O}\f$
!! as a reaction product \f$k\f$ from stoichiometric combustion ratio
!! (input parameter/variable: `ANA`; nomenclature: \f$S\f$)
!!
!! @note This assumes reaction products are limited to \f$\ce{Na2O}\f$
!! and \f$\ce{Na2O2}\f$. By definition the mole fraction of \f$\ce{Na2O2}\f$
!! is one minus the mole fraction of \f$\ce{Na2O}\f$.
pure real(kind=WP) function k_from_s(S) result(k_na2o)
    use m_constant, only: ZERO, ONE, TWO, FOUR
    use m_material, only: material_db, gid_Na, gid_O2
    implicit none
    !> Stoichiometric combustion ratio between \f$\ce{Na2O}\f$
    !! and \f$\ce{Na2O}\f$, dimensionless
    real(kind=WP), intent(in) :: S

    ! Formula weight of constituents
    real(kind=WP) :: mw_o2
    real(kind=WP) :: mw_na

    ! Na/O2 mass ratios of combustion products
    real(kind=WP) :: rfo_na2o
    real(kind=WP) :: rfo_na2o2

    continue

    mw_o2 = material_db%gas(gid_O2)%mw
    mw_na = material_db%gas(gid_Na)%mw

    rfo_na2o = FOUR * mw_na / mw_o2
    rfo_na2o2 = TWO * mw_na / mw_o2

    if (S < rfo_na2o2) then
        ! Something is dreadfully wrong
        k_na2o = ZERO
    else if (S > rfo_na2o) then
        ! Something is dreadfully wrong
        k_na2o = ONE
    else
        k_na2o = (S - rfo_na2o2) / (rfo_na2o - rfo_na2o2)
    end if

    return
end function k_from_s

!> Derive stoichiometric combustion ratio (input parameter/variable:
!! `ANA`; nomenclature: \f$S\f$) from fraction of consumed
!! \f$\ce{O2}\f$ resulting in \f$\ce{Na2O}\f$ as a reaction product,
!! \f$k\f$
!!
!! @note This assumes reaction products are limited to \f$\ce{Na2O}\f$
!! and \f$\ce{Na2O2}\f$.
pure real(kind=WP) function s_from_kna2o(k_monoxide) result(S)
    use m_constant, only: ZERO, ONE, TWO, FOUR
    use m_material, only: material_db, gid_Na, gid_O2
    implicit none

    !> Stoichiometric combustion ratio between \f$\ce{Na2O}\f$
    !! and \f$\ce{Na2O}\f$, dimensionless
    real(kind=WP), intent(in) :: k_monoxide

    ! Formula weight of constituents
    real(kind=WP) :: mw_o2
    real(kind=WP) :: mw_na

    ! Na/O2 mass ratios of combustion products
    real(kind=WP) :: rfo_na2o
    real(kind=WP) :: rfo_na2o2

    real(kind=WP) :: k_na2o
    real(kind=WP) :: k_na2o2

    continue

    k_na2o = min(ONE, max(ZERO, k_monoxide))
    k_na2o2 = ONE - k_na2o

    mw_o2 = material_db%gas(gid_O2)%mw
    mw_na = material_db%gas(gid_Na)%mw

    rfo_na2o = FOUR * mw_na / mw_o2
    rfo_na2o2 = TWO * mw_na / mw_o2

    S = k_na2o * rfo_na2o + k_na2o2 * rfo_na2o2

    return
end function s_from_kna2o

!> Derive fraction of total consumed oxygen used in the formation of
!! sodium monoxide, \f$k\f$, from fraction of \f$\ce{Na2O}\f$ produced
!! as reaction product
!!
!! @note This assumes reaction products are limited to \f$\ce{Na2O}\f$
!! and \f$\ce{Na2O2}\f$. By definition the mole fraction of \f$\ce{Na2O2}\f$
!! is one minus the mole fraction of \f$\ce{Na2O}\f$.
pure real(kind=WP) function k_from_yna2o(y_monoxide) result(k)
    use m_constant, only: ZERO, HALF, ONE
    use m_material, only: material_db, gid_O2, aid_Na2O, aid_Na2O2
    implicit none

    !> Stoichiometric combustion ratio between \f$\ce{Na2O}\f$
    !! and \f$\ce{Na2O}\f$, dimensionless
    real(kind=WP), intent(in) :: y_monoxide

    ! Formula weight of constituents
    real(kind=WP) :: mw_o2
    ! real(kind=WP) :: mw_na
    real(kind=WP) :: mw_na2o
    real(kind=WP) :: mw_na2o2

    ! Na/O2 mass ratios of combustion products
    real(kind=WP) :: fo_na2o
    real(kind=WP) :: fo_na2o2

    real(kind=WP) :: y_na2o
    real(kind=WP) :: y_na2o2

    continue

    y_na2o = min(ONE, max(ZERO, y_monoxide))
    y_na2o2 = ONE - y_na2o

    mw_o2 = material_db%gas(gid_O2)%mw
    ! mw_na = material_db%gas(gid_Na)%mw
    mw_na2o = material_db%aerosol(aid_Na2O)%mw
    mw_na2o2 = material_db%aerosol(aid_Na2O2)%mw

    fo_na2o = HALF * mw_o2 / mw_na2o
    fo_na2o2 = mw_o2 / mw_na2o2

    k = fo_na2o * y_na2o / (fo_na2o * y_na2o + fo_na2o2 * y_na2o2)

    return
end function k_from_yna2o

!> Derive fraction of \f$\ce{Na2O}\f$ produced as reaction product,
!! \f$Y\f$, from fraction of total consumed oxygen used in the formation
!! of sodium monoxide, \f$k\f$
!!
!! @note This assumes reaction products are limited to \f$\ce{Na2O}\f$
!! and \f$\ce{Na2O2}\f$. By definition the mole fraction of \f$\ce{Na2O2}\f$
!! is one minus the mole fraction of \f$\ce{Na2O}\f$.
pure real(kind=WP) function yna2o_from_k(k) result(y_na2o)
    use m_constant, only: ZERO, HALF, ONE
    use m_material, only: material_db, gid_O2, aid_Na2O, aid_Na2O2
    implicit none

    !> Fraction of total consumed oxygen used in the formation of sodium
    !! monoxide, dimensionless
    real(kind=WP), intent(in) :: k

    ! Formula weight of constituents
    real(kind=WP) :: mw_o2
    real(kind=WP) :: mw_na2o
    real(kind=WP) :: mw_na2o2

    ! Na/O2 mass ratios of combustion products
    real(kind=WP) :: fo_na2o
    real(kind=WP) :: fo_na2o2

    real(kind=WP) :: k_na2o
    real(kind=WP) :: k_na2o2

    continue

    k_na2o = min(ONE, max(ZERO, k))
    k_na2o2 = ONE - k_na2o

    mw_o2 = material_db%gas(gid_O2)%mw
    mw_na2o = material_db%aerosol(aid_Na2O)%mw
    mw_na2o2 = material_db%aerosol(aid_Na2O2)%mw

    fo_na2o = HALF * mw_o2 / mw_na2o
    fo_na2o2 = mw_o2 / mw_na2o2

    y_na2o = k * fo_na2o2 / ((ONE - k) * fo_na2o + k * fo_na2o2)

    return
end function yna2o_from_k

!> Derive heat released per unit mass of \f$\ce{Na}\f$ consumed from
!! \f$k\f$, the fraction of total consumed oxygen used in the formation
!! of sodium monoxide.
!!
!! @note This assumes reaction products are limited to \f$\ce{Na2O}\f$
!! and \f$\ce{Na2O2}\f$. By definition the mole fraction of \f$\ce{Na2O2}\f$
!! is one minus the mole fraction of \f$\ce{Na2O}\f$.
pure real(kind=WP) function qburn_mNa_from_k(k_monoxide, use_legacy)         &
    result(qburn_mNa)
    use m_constant, only: ZERO, HALF, ONE, TWO, FOUR, THOUSAND,         &
        JOULE_BTU, LBM_G
    use m_material, only: material_db, gid_Na, gid_O2, aid_Na2O,        &
        aid_Na2O2
    implicit none

    !> Stoichiometric combustion ratio between \f$\ce{Na2O}\f$
    !! and \f$\ce{Na2O}\f$, dimensionless
    real(kind=WP), intent(in) :: k_monoxide

    !> Flag to select legacy or property database heat of
    !! formation. Optional; default is .true.
    logical, intent(in), optional :: use_legacy

    ! Formula weight of constituents
    real(kind=WP) :: mw_o2
    real(kind=WP) :: mw_na

    ! Na/O2 mass ratios of combustion products
    real(kind=WP) :: rfo_na2o
    real(kind=WP) :: rfo_na2o2

    ! Heat of formation of reaction products (from original code)
    ! Units are BTU/lbm-Na consumed
    real(kind=WP) :: dHform_na2o
    real(kind=WP) :: dHform_na2o2

    ! Mole fraction of reaction products
    real(kind=WP) :: k_na2o
    real(kind=WP) :: k_na2o2

    logical :: use_old_hform

    continue

    use_old_hform = .true.
    if (present(use_legacy)) then
        use_old_hform = use_legacy
    end if

    ! Clamp values of k_monoxide to [0 .. 1]
    k_na2o = min(ONE, max(ZERO, k_monoxide))
    k_na2o2 = ONE - k_na2o

    mw_o2 = material_db%gas(gid_O2)%mw
    mw_na = material_db%gas(gid_Na)%mw

    ! Heat of formation of reaction products (BTU/lbm-Na consumed)
    if (use_old_hform) then
        ! Values from original code
        dHform_na2o  = 3900.0_WP
        dHform_na2o2 = 4500.0_WP
    else
        ! Values derived from internal property database (taken from CEA2)
        dHform_na2o  = -material_db%aerosol(aid_Na2O)%dHform / (TWO * mw_na * JOULE_BTU * LBM_G)
        dHform_na2o2 = -material_db%aerosol(aid_Na2O2)%dHform / (TWO * mw_na * JOULE_BTU * LBM_G)
    end if

    ! block
    !     real(kind=WP) :: rcs_monoxide
    !     real(kind=WP) :: rcs_peroxide
    !     rcs_monoxide = (-material_db%aerosol(aid_Na2O)%dHform  / (TWO * mw_na * JOULE_BTU * LBM_G)) / dHform_na2o
    !     rcs_peroxide = (-material_db%aerosol(aid_Na2O2)%dHform / (TWO * mw_na * JOULE_BTU * LBM_G)) / dHform_na2o2
    !     write(6, '("C/S dHform(Na2O),  BTU/lbm-Na: ", ES16.8)') rcs_monoxide
    !     write(6, '("C/S dHform(Na2O2), BTU/lbm-Na: ", ES16.8)') rcs_peroxide
    ! end block

!     block
!         real(kind=WP) :: rmp_cea
!         real(kind=WP) :: rmp_sofire2

! 1       format("R(m/p): CEA = ", ES12.4, ", SOFIRE2 = ", ES12.4)

!         continue

!         rmp_sofire2 = dHform_na2o / dHform_na2o2
!         rmp_cea = (-material_db%aerosol(aid_Na2O)%dHform / (TWO * mw_na * JOULE_BTU)) &
!             / (-material_db%aerosol(aid_Na2O2)%dHform / (TWO * mw_na * JOULE_BTU))
!         write(unit=6, fmt=1) rmp_cea, rmp_sofire2
!         rmp_cea = (-material_db%aerosol(aid_Na2O)%dHform / (HALF * mw_o2 * JOULE_BTU)) &
!             / (-material_db%aerosol(aid_Na2O2)%dHform / (mw_o2 * JOULE_BTU))
!         write(unit=6, fmt=1) rmp_cea, rmp_sofire2
!     end block

    rfo_na2o = FOUR * mw_na / mw_o2
    rfo_na2o2 = TWO * mw_na / mw_o2

    qburn_mNa = (k_na2o * rfo_na2o * dHform_na2o                        &
              + k_na2o2 * rfo_na2o2 * dHform_na2o2)                     &
              / s_from_kna2o(k_na2o)

    return
end function qburn_mNa_from_k

end module m_burnchem