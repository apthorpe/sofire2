!> @file m_material.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Classes, functions, and data structures involving material properties
!!
!! Transport and thermodynamic property information is expected to be imported
!! from the data files `trans.inp` and `thermo.inp` supplied by or compatible
!! with those provided with CEA2; see @cite NASA-RP1311-v1 and
!! @cite NASA-RP1311-v2 for details on data file contents and format
module m_material
    use sofire2_param, only: WP
    use m_constant, only: ZERO
    implicit none

    private

    public :: kgair
    public :: muair
    public :: get_gas_id
    public :: get_aerosol_id
    public :: is_gas_id
    public :: is_aerosol_id
    public :: new_thermoprop_dataframe

! Standard atmospheric composition taken from @cite USStdAtmos1976
! Name      MW          NFair
! N2        28.0134     7.8084E-1
! O2        31.9988     2.09476E-1
! Ar        39.948      9.34E-3
! CO2       44.00995    3.14E-4
! Ne        20.183      1.818E-05
! He        4.0026      5.24E-06
! Kr        83.8        1.14E-06
! Xe        131.3       8.70E-08
! CH4       16.04303    2.00E-06
! H2        2.01594     5.00E-07

    !> Phase ID for unknown/undefined phase
    integer, parameter, public :: phase_NULL = -1

    !> Phase ID for gas phase
    integer, parameter, public :: phase_GAS = 0

    !> Phase ID for condensed (e.g. aerosol) phase
    integer, parameter, public :: phase_CONDENSED = 1

    !> Material ID for undefined material
    integer, parameter, public :: mid_NULL   = 0

    ! ID for each tracked gas species

    !> Gas ID for undefined material
    integer, parameter, public :: gid_NULL   = mid_NULL

    !> Gas ID (index) for \f$\ce{Ar}\f$, gas only
    integer, parameter, public :: gid_Ar     = 1
    !> Gas ID (index) for \f$\ce{CH4}\f$, gas only
    integer, parameter, public :: gid_CH4    = 2
    !> Gas ID (index) for \f$\ce{CO}\f$, gas only
    integer, parameter, public :: gid_CO     = 3
    !> Gas ID (index) for \f$\ce{CO2}\f$, gas only
    integer, parameter, public :: gid_CO2    = 4
    !> Gas ID (index) for \f$\ce{H2}\f$, gas only
    integer, parameter, public :: gid_H2     = 5
    !> Gas ID (index) for \f$\ce{H2O}\f$. Vapor only; see `aid_H2O`
    !! for aerosol ID
    integer, parameter, public :: gid_H2O    = 6
    !> Gas ID (index) for \f$\ce{He}\f$, gas only
    integer, parameter, public :: gid_He     = 7
    !> Gas ID (index) for \f$\ce{Kr}\f$, gas only
    integer, parameter, public :: gid_Kr     = 8
    !> Gas ID (index) for \f$\ce{N2}\f$, gas only
    integer, parameter, public :: gid_N2     = 9
    !> Gas ID (index) for \f$\ce{Na}\f$. Vapor only; see `aid_Na` for
    !! aerosol ID
    integer, parameter, public :: gid_Na     = 10
    !> Gas ID (index) for \f$\ce{Ne}\f$, gas only
    integer, parameter, public :: gid_Ne     = 11
    !> Gas ID (index) for \f$\ce{O2}\f$, gas only
    integer, parameter, public :: gid_O2     = 12
    !> Gas ID (index) for \f$\ce{Xe}\f$, gas only
    integer, parameter, public :: gid_Xe     = 13

    !> Names of gas species considered in SOFIRE 2. Names are consistent
    !! with those listed in CEA2 property libraries @cite NASA-RP1311-v2
    character(len=6), dimension(*), parameter :: gas_name = (/          &
        'Ar    ', 'CH4   ', 'CO    ', 'CO2   ', 'H2    ', 'H2O   ',     &
        'He    ', 'Kr    ', 'N2    ', 'Na    ', 'Ne    ', 'O2    ',     &
        'Xe    ' /)

    !> Number of gas species in SOFIRE 2 model
    integer, parameter, public :: n_gas = size(gas_name)

    !> IDs of condensed (solid and liquid) species considered in
    !! SOFIRE 2.
    integer, dimension(n_gas), parameter, public :: gas_id = (/         &
    gid_Ar, gid_CH4, gid_CO, gid_CO2, gid_H2, gid_H2O, gid_He, gid_Kr,  &
    gid_N2, gid_Na, gid_Ne, gid_O2, gid_Xe /)

    ! ID for each tracked aerosol species

    !> Aerosol ID for undefined material
    integer, parameter, public :: aid_NULL   = mid_NULL

    !> Aerosol ID (index) for \f$\ce{H2O}\f$. Liquid only; see `gid_H2O`
    !! for gas ID
    integer, parameter, public :: aid_H2O    = 1
    !> Aerosol ID (index) for \f$\ce{Na}\f$. Solid and liquid only; see
    !! `gid_Na` for gas ID
    integer, parameter, public :: aid_Na     = 2
    !> Aerosol ID (index) for \f$\ce{NaO2}\f$, solid and liquid only
    integer, parameter, public :: aid_NaO2   = 3
    !> Aerosol ID (index) for \f$\ce{NaOH}\f$, solid and liquid only
    integer, parameter, public :: aid_NaOH   = 4
    !> Aerosol ID (index) for \f$\ce{Na2CO3}\f$, solid and liquid only
    integer, parameter, public :: aid_Na2CO3 = 5
    !> Aerosol ID (index) for \f$\ce{Na2O}\f$, solid and liquid only
    integer, parameter, public :: aid_Na2O   = 6
    !> Aerosol ID (index) for \f$\ce{Na2O2}\f$, solid and liquid only
    integer, parameter, public :: aid_Na2O2  = 7

    !> Names of condensed (solid and liquid) species considered in
    !! SOFIRE 2. Names are consistent with those listed in CEA2 property
    !! libraries @cite NASA-RP1311-v2
    character(len=6), dimension(*), parameter, public :: aer_name = (/  &
        'H2O   ', 'Na    ', 'NaO2  ', 'NaOH  ', 'Na2CO3',               &
        'Na2O  ', 'Na2O2 ' /)

    !> Number of condensed species in SOFIRE 2 model
    integer, parameter, public :: n_aer = size(aer_name)

    !> IDs of condensed (solid and liquid) species considered in
    !! SOFIRE 2.
    integer, dimension(n_aer), parameter, public :: aer_id = (/         &
        aid_H2O, aid_Na, aid_NaO2, aid_NaOH, aid_Na2CO3,                &
        aid_Na2O, aid_Na2O2 /)

    !> Single temperature range \f$C{p}\f$ correlation
    type, public :: cp_correlation
        !> Lower temperature limit of interval, \f$\si{\kelvin}\f$
        real(kind=WP) :: t_lo = ZERO
        !> Upper temperature limit of interval, \f$\si{\kelvin}\f$
        real(kind=WP) :: t_hi = ZERO
        !> Enthalpy reference \f$H^{0}(\SI{273.15}{\kelvin}) - H^{0}(\SI{0}{\kelvin})\f$, \f$\si{\joule\per\kgmol}\f$
        real(kind=WP) :: h_std_0 = ZERO
        !> Correlation coefficients, \f$a\f$
        real(kind=WP), dimension(7) :: a = ZERO
        !> Integration constants, \f$b\f$
        real(kind=WP), dimension(2) :: b = ZERO
    contains
        !> Initialize attributes
        procedure :: init => cp_correlation_init
        !> Evaluate constant \f$C_{p}\f$, \f$\si{\joule\per\kilo\gram\per\kelvin}\f$
        procedure :: cp_constant => cp_correlation_cp_constant
        !> Evaluate specific heat correlation \f$C_{p}(t)\f$ at temperature \f$t\f$, \f$\si{\kelvin}\f$
        procedure :: cp_at => cp_correlation_cp
        !> Evaluate enthalpy correlation at temperature \f$t\f$, \f$\si{\kelvin}\f$
        procedure :: h_at => cp_correlation_enthalpy
        !> Evaluate entropy correlation at temperature \f$t\f$, \f$\si{\kelvin}\f$
        procedure :: s_at => cp_correlation_entropy
    end type cp_correlation

    !> Single temperature range correlation for viscosity,
    !! thermal conductivity, and interaction parameter
    type, public :: mu_correlation
        !> Lower temperature limit of interval, \f$\si{\kelvin}\f$
        real(kind=WP) :: t_lo = ZERO
        !> Upper temperature limit of interval, \f$\si{\kelvin}\f$
        real(kind=WP) :: t_hi = ZERO
        !> Correlation coefficients, \f$a\f$
        real(kind=WP), dimension(4) :: a = ZERO
    contains
        !> Initialize attributes
        procedure :: init => mu_correlation_init
        !> Evaluate correlation at temperature \f$t\f$, \f$\si{\kelvin}\f$
        procedure :: at_t => mu_correlation_value
    end type mu_correlation

    !> Thermodynamic property dataframe. Sufficient to model condensed
    !! species and can be used as a base class for gasspecies.
    type, public :: thermoprop_dataframe
        !> Material ID
        integer :: id
        !> Material name
        character(len=32) :: name = 'UNINITIALIZED MATERIAL          '
        !> Formula weight, \f$\us{\lbm\per\lbmol}\f$
        real(kind=WP) :: mw = ZERO
        !> Heat of formation, \f$\si{\joule\per\kgmol}\f$
        real(kind=WP) :: dHform = ZERO
        !> Enthalpy reference \f$H^{0}(\SI{273.15}{\kelvin}) - H^{0}(\SI{0}{\kelvin})\f$, \f$\si{\joule\per\kgmol}\f$
        real(kind=WP) :: h_std_0 = ZERO
        !> Phase indicator
        integer :: phase_id = phase_NULL
        !> Temperature intervals for specific heat \f$C_{p}\f$ calculation
        integer :: n_cp = -1
        !> List of temperature-interval specific heat correlation objects
        type(cp_correlation), dimension(:), allocatable :: cp_corr
    contains
        !> Initialize composition attributes
        procedure :: init => thermoprop_dataframe_init
        !> Detect if supplied name matches material name
        procedure :: matches_name => thermoprop_dataframe_matches_name
        !> Temperature-dependent specific heat
        procedure :: cp => thermoprop_dataframe_cp
        !> Temperature-dependent enthalpy
        procedure :: h => thermoprop_dataframe_enthalpy
        !> Temperature-dependent entropy
        procedure :: s => thermoprop_dataframe_entropy
        !> Lower temperature limit for specific heat correlation
        procedure :: cp_t_lo => thermoprop_dataframe_cp_t_lo
        !> Upper temperature limit for specific heat correlation
        procedure :: cp_t_hi => thermoprop_dataframe_cp_t_hi
    end type thermoprop_dataframe

    !> Material dataframe
    type, extends(thermoprop_dataframe), public :: material_dataframe
        ! !> Material ID - INHERITED
        ! integer :: id
        ! !> Material name - INHERITED
        ! character(len=32) :: name = 'UNINITIALIZED MATERIAL          '
        ! !> Formula weight, \f$\us{\lbm\per\lbmol}\f$ - INHERITED
        ! real(kind=WP) :: mw = ZERO
        ! !> Heat of formation, \f$\si{\joule\per\kgmol}\f$ - INHERITED
        ! real(kind=WP) :: dHform = ZERO
        ! !> Phase indicator - INHERITED
        ! integer :: phase_id = phase_NULL
        ! !> Temperature intervals for specific heat \f$C_{p}\f$ calculation - INHERITED
        ! integer :: n_cp = -1
        !> Temperature intervals for dynamic viscosity \f$\mu\f$ calculation
        integer :: n_mu = -1
        !> Temperature intervals for thermal conductivity \f$k\f$ calculation
        integer :: n_k = -1
        ! !> List of temperature-interval specific heat correlation objects - INHERITED
        ! type(cp_correlation), dimension(:), allocatable :: cp_corr
        !> List of viscosity-form temperature-interval dynamic viscosity correlation objects
        type(mu_correlation), dimension(:), allocatable :: mu_corr
        !> List of viscosity-form temperature-interval thermal conductivity correlation objects
        type(mu_correlation), dimension(:), allocatable :: k_corr

    contains
        !> Initialize composition attributes
        procedure :: init => material_dataframe_init
        ! !> Temperature-dependent specific heat - INHERITED
        ! procedure :: cp => material_dataframe_cp
        !> Temperature-dependent enthalpy - INHERITED
        ! procedure :: h => material_dataframe_enthalpy
        ! !> Temperature-dependent entropy - INHERITED
        ! procedure :: s => material_dataframe_entropy
        !> Temperature-dependent dynamic viscosity
        procedure :: mu => material_dataframe_mu
        !> Temperature-dependent thermal conductivity
        procedure :: k => material_dataframe_k
        !> Lower temperature limit for dynamic viscosity correlation
        procedure :: mu_t_lo => material_dataframe_mu_t_lo
        !> Upper temperature limit for dynamic viscosity correlation
        procedure :: mu_t_hi => material_dataframe_mu_t_hi
        !> Lower temperature limit for thermal conductivity correlation
        procedure :: k_t_lo => material_dataframe_k_t_lo
        !> Upper temperature limit for thermal conductivity correlation
        procedure :: k_t_hi => material_dataframe_k_t_hi
    end type material_dataframe

    !> Material property database
    type, public :: material_database
        !> Transport property data file name
        character(len=80) :: transfn = 'trans.inp'
        !> Thermodynamic property data file name
        character(len=80) :: thermofn = 'thermo.inp'
        !> List of material objects for property calculation
        type(material_dataframe), dimension(n_gas) :: gas
        !> List of material objects for property calculation
        type(thermoprop_dataframe), dimension(n_aer) :: aerosol

    contains
        !> Initialize attributes
        procedure :: init => material_database_init
        !> Lookup gid by gas name
        procedure :: id_from_gas_name =>                                &
            material_database_id_from_gas_name
        !> Lookup gid by aerosol name
        procedure :: id_from_aerosol_name =>                            &
            material_database_id_from_aerosol_name
        !> Populated database from CEA2 data files
        procedure :: populate => material_database_populate
        !> Match thermodynamic property record obtained from data file
        !! to gas or condensed (aerosol) species being tracked by the
        !! application and import property data to the appropriate
        !! material database entry.
        procedure :: import_thermo_record =>                            &
            material_database_import_thermo_record
        !> Read thermo record and record it as gas property data
        procedure, nopass :: read_raw_thermo_record =>                  &
            material_database_read_raw_thermo_record
    end type material_database

    !> Global material database
    type(material_database), public :: material_db

contains

!> Returns the temperature-dependent thermal conductivity of air in units of
!! \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
!!
!! @note The source of this correlation is unknown but it compares well
!! with the quadratic relation given by \cite Kannuluik1951 over the latter's
!! range of validity, \f$\SIrange{0}{218}{\celsius}\f$
elemental real(kind=WP) function kgair(temp) result(kg)
    use m_constant, only: T0R
    implicit none
    !> Temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: temp

    continue

    kg = 0.014_WP + 1.92E-05_WP * (temp - T0R)

    return
end function kgair

!> Returns the temperature-dependent absolute (dynamic) viscosity of air
!! in units of \f$\us{\lbm\foot\per\hour}\f$
!!
!! @note The source of this correlation is unknown but it compares
!! reasonably well with tabular data
elemental real(kind=WP) function muair(temp) result(mu)
    use m_constant, only: SEC_HR
    implicit none

    !> Temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: temp

    continue

    mu = (0.0188_WP + 4.94E-05_WP * temp) / SEC_HR

    return
end function muair

!> Return gid of gas associated with name. Returns `gid_NULL` if name
!! cannot be found
pure integer function id_of_name(name, matl_names, null_id) result(id)
    use m_textio, only: munch
    implicit none

    !> Material name
    character(len=*), intent(in) :: name

    !> List of material names
    character(len=6), dimension(:), intent(in) :: matl_names

    !> ID to return if name not found
    integer, intent(in) :: null_id

    character(len=6) :: tmp_name
    integer :: i

    continue

    id = null_id
    tmp_name = munch(name)

    do i = 1, size(matl_names)
        if (tmp_name == matl_names(i)) then
            id = i
            exit
        end if
    end do

    return
end function id_of_name

!> Return gid of gas associated with name. Returns `gid_NULL` if name
!! cannot be found
pure integer function get_gas_id(name) result(gid)
    implicit none

    !> Material name
    character(len=*), intent(in) :: name

    continue

    gid = id_of_name(name, gas_name, gid_NULL)

    return
end function get_gas_id

!> Return aid of aerosol associated with name. Returns `aid_NULL` if name
!! cannot be found
pure integer function get_aerosol_id(name) result(aid)
    implicit none

    !> Material name
    character(len=*), intent(in) :: name

    continue

    aid = id_of_name(name, aer_name, aid_NULL)

    return
end function get_aerosol_id

!> Returns .true. if id is a valid gas ID
elemental logical function is_gas_id(id) result (is_gas)
    implicit none
    !> Material ID
    integer, intent(in) :: id
    continue
    is_gas = any(gas_id == id)
    return
end function is_gas_id

!> Returns .true. if id is a valid aerosol ID
elemental logical function is_aerosol_id(id) result (is_aerosol)
    implicit none
    !> Material ID
    integer, intent(in) :: id
    continue
    is_aerosol = any(aer_id == id)
    return
end function is_aerosol_id

!> Initialize attributes
subroutine cp_correlation_init(this)
    implicit none

    !> Object reference
    class(cp_correlation), intent(inout) :: this

    continue

    this%t_lo = ZERO
    this%t_hi = ZERO
    this%h_std_0 = ZERO
    this%a = ZERO
    this%b = ZERO

    return
end subroutine cp_correlation_init

!> Return \f$C_{p}\f$ when \f$C_{p}\f$ is constant
!! (some condensed species)
pure real(kind=WP) function cp_correlation_cp_constant(this) result(cp)
    implicit none

    !> Object reference
    class(cp_correlation), intent(in) :: this

    continue

    cp = this%a(1)

    return
end function cp_correlation_cp_constant

!> Return \f$C_{p}\f$ as a function of temperature
pure real(kind=WP) function cp_correlation_cp(this, t) result(cp)
    implicit none

    !> Object reference
    class(cp_correlation), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    continue

    cp = (this%a(1) / t + this%a(2)) / t + this%a(3)                &
        + t * (this%a(4) + t * (this%a(5)                           &
        + t * (this%a(6) + t * this%a(7))))

    return
end function cp_correlation_cp

!> Return specific enthalpy as a function of temperature
pure real(kind=WP) function cp_correlation_enthalpy(this, t) result(h)
    use m_constant, only: FIFTH, QUARTER, THIRD, HALF
    implicit none

    !> Object reference
    class(cp_correlation), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    continue
    h = -this%a(1) / t                                                  &
        + this%a(2) * log(t)                                            &
        + t * (this%a(3)                                                &
        + t * (HALF * this%a(4)                                         &
        + t * (THIRD * this%a(5)                                        &
        + t * (QUARTER * this%a(6)                                      &
        + t * FIFTH * this%a(7)))))                                     &
        + this%b(1)
    return
end function cp_correlation_enthalpy

!> Return specific entropy as a function of temperature
pure real(kind=WP) function cp_correlation_entropy(this, t) result(s)
    use m_constant, only: QUARTER, THIRD, HALF
    implicit none
    !> Object reference
    class(cp_correlation), intent(in) :: this
    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t
    continue
    s = -(HALF * this%a(1) / t + this%a(2)) / t                         &
        + this%a(3) * log(t)                                            &
        + t * (this%a(4)                                                &
        + t * (HALF * this%a(5)                                         &
        + t * (THIRD * this%a(6)                                        &
        + t * QUARTER * this%a(7))))                                    &
        + this%b(2)
    return
end function cp_correlation_entropy

!> Initialize attributes
subroutine mu_correlation_init(this)
    implicit none

    !> Object reference
    class(mu_correlation), intent(inout) :: this

    continue

    this%t_lo = ZERO
    this%t_hi = ZERO
    this%a = ZERO

    return
end subroutine mu_correlation_init

!> Return correlation at a given temperature
pure real(kind=WP) function mu_correlation_value(this, t) result(val)
    implicit none
    !> Object reference
    class(mu_correlation), intent(in) :: this
    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t
    continue
    val = exp(this%a(1) * log(t)                                        &
        + (this%a(2) + this%a(3) / t) / t + this%a(4))
    return
end function mu_correlation_value

!> Construct thermoprop_dataframe object with given properties and
!! assigned number of `cp_correlation` slots.
type(thermoprop_dataframe) function new_thermoprop_dataframe(           &
    name, mw, dHform, phase_id, n_cp) result (tdf)
    implicit none

    ! !> Material ID (gid for gases, aid for aerosols)
    ! integer, intent(in) :: id

    !> Material name
    character(len=*), intent(in) :: name

    !> Formula weight, \f$\us{\lbm\per\lbmol}\f$
    real(kind=WP), intent(in) :: mw

    !> Heat of formation, \f$\si{\joule\per\kgmol}\f$
    real(kind=WP), intent(in) :: dHform

    ! !> Enthalpy reference \f$H^{0}(\SI{273.15}{\kelvin}) - H^{0}(\SI{0}{\kelvin})\f$, \f$\si{\joule\per\kgmol}\f$
    ! real(kind=WP), intent(in) :: h_std_0

    !> Phase indicator
    integer, intent(in) :: phase_id

    !> Temperature intervals for specific heat \f$C_{p}\f$ calculation
    integer, intent(in) :: n_cp

    integer :: i

    continue

    call tdf%init()

    ! tdf%id = id
    tdf%name = name
    tdf%mw = mw
    tdf%dHform = dHform
    ! tdf%h_std_0 = h_std_0
    tdf%phase_id = phase_id
    tdf%n_cp = n_cp

    if (n_cp > 0) then
        allocate(tdf%cp_corr(n_cp))
        do i = 1, n_cp
            call tdf%cp_corr(i)%init()
        end do
    else if (n_cp == 0) then
        ! Need single entry for constant Cp
        allocate(tdf%cp_corr(1))
        call tdf%cp_corr(1)%init()
    end if

    return
end function new_thermoprop_dataframe

!> Initialize attributes
subroutine thermoprop_dataframe_init(this)
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(inout) :: this

    continue

    this %id = mid_NULL
    this%name = 'UNINITIALIZED MATERIAL          '

    this%mw = ZERO
    this%dHform = ZERO
    this%h_std_0 = ZERO

    this%phase_id = phase_NULL

    this%n_cp = -1
    if (allocated(this%cp_corr)) then
        deallocate(this%cp_corr)
    end if

    return
end subroutine thermoprop_dataframe_init

!> Return true if name matches name of material, false otherwise
pure logical function thermoprop_dataframe_matches_name(this, name)     &
    result(match)
    use m_textio, only: munch
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(in) :: this

    !> Material name to match
    character(len=32), intent(in) :: name

    continue

    match = (munch(this%name) == munch(name))

    return
end function thermoprop_dataframe_matches_name

!> Return temperature-dependent specific heat based on the correct
!! temperature interval
pure real(kind=WP) function thermoprop_dataframe_cp(this, t) result(cp)
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    integer :: i

    continue

    cp = -404.0_WP
    if (this%n_cp > 0) then
        do i = 1, this%n_cp
            if (t >= this%cp_corr(i)%t_lo                               &
                .and. t <= this%cp_corr(i)%t_hi) then
                cp = this%cp_corr(i)%cp_at(t)
                exit
            end if
        end do
    else if (this%n_cp == 0) then
        cp  = this%cp_corr(1)%cp_constant()
    end if

    return
end function thermoprop_dataframe_cp

!> Return temperature-dependent specific heat based on the correct
!! temperature interval
pure real(kind=WP) function thermoprop_dataframe_enthalpy(this, t)  &
    result(h)
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    integer :: i

    continue

    h = -404.0_WP
    if (this%n_cp > 0) then
        do i = 1, this%n_cp
            if (t >= this%cp_corr(i)%t_lo                               &
                .and. t <= this%cp_corr(i)%t_hi) then
                h = this%cp_corr(i)%h_at(t)
                exit
            end if
        end do
    end if

    return
end function thermoprop_dataframe_enthalpy

!> Return temperature-dependent specific heat based on the correct
!! temperature interval
pure real(kind=WP) function thermoprop_dataframe_entropy(this, t)       &
    result(s)
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    integer :: i

    continue

    s = -404.0_WP
    if (this%n_cp > 0) then
        do i = 1, this%n_cp
            if (t >= this%cp_corr(i)%t_lo                               &
                .and. t <= this%cp_corr(i)%t_hi) then
                s = this%cp_corr(i)%s_at(t)
                exit
            end if
        end do
    end if

    return
end function thermoprop_dataframe_entropy

!> Lower temperature limit for specific heat correlation
pure real(kind=WP) function thermoprop_dataframe_cp_t_lo(this)          &
    result(t_lo)
    use m_constant, only: BIG1P9
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(in) :: this

    continue

    if (this%n_cp > 0) then
        t_lo = minval(this%cp_corr(:)%t_lo)
    else
        t_lo = BIG1P9
    end if

    return
end function thermoprop_dataframe_cp_t_lo

!> Upper temperature limit for specific heat correlation
pure real(kind=WP) function thermoprop_dataframe_cp_t_hi(this)          &
    result(t_hi)
    implicit none

    !> Object reference
    class(thermoprop_dataframe), intent(in) :: this

    continue

    if (this%n_cp > 0) then
        t_hi = maxval(this%cp_corr(:)%t_hi)
    else
        t_hi = ZERO
    end if

    return
end function thermoprop_dataframe_cp_t_hi

!> Initialize attributes
subroutine material_dataframe_init(this)
    implicit none

    !> Object reference
    class(material_dataframe), intent(inout) :: this

    continue

    this %id = mid_NULL
    this%name = 'UNINITIALIZED MATERIAL          '

    this%mw = ZERO
    this%dHform = ZERO

    this%phase_id = phase_NULL

    this%n_cp = -1
    if (allocated(this%cp_corr)) then
        deallocate(this%cp_corr)
    end if

    this%n_mu = -1
    if (allocated(this%mu_corr)) then
        deallocate(this%mu_corr)
    end if

    this%n_k = -1
    if (allocated(this%k_corr)) then
        deallocate(this%k_corr)
    end if

    return
end subroutine material_dataframe_init

!> Return temperature-dependent dynamic viscosity based on the correct
!! temperature interval
pure real(kind=WP) function material_dataframe_mu(this, t) result(mu)
    implicit none

    !> Object reference
    class(material_dataframe), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    integer :: i

    continue

    mu = -404.0_WP
    if (this%n_mu > 0) then
        do i = 1, this%n_mu
            if (t >= this%mu_corr(i)%t_lo                               &
                .and. t <= this%mu_corr(i)%t_hi) then
                mu = this%mu_corr(i)%at_t(t)
                exit
            end if
        end do
    end if

    return
end function material_dataframe_mu

!> Return temperature-dependent thermal conductivity based on the correct
!! temperature interval
pure real(kind=WP) function material_dataframe_k(this, t) result(k)
    implicit none

    !> Object reference
    class(material_dataframe), intent(in) :: this

    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: t

    integer :: i

    continue

    k = -404.0_WP
    if (this%n_k > 0) then
        do i = 1, this%n_k
            if (t >= this%k_corr(i)%t_lo                                &
                .and. t <= this%k_corr(i)%t_hi) then
                k = this%k_corr(i)%at_t(t)
                exit
            end if
        end do
    end if

    return
end function material_dataframe_k

!> Lower temperature limit for dynamic viscosity correlation
pure real(kind=WP) function material_dataframe_mu_t_lo(this)            &
    result(t_lo)
    use m_constant, only: BIG1P9
    implicit none

    !> Object reference
    class(material_dataframe), intent(in) :: this

    continue

    if (this%n_mu > 0) then
        t_lo = minval(this%mu_corr(:)%t_lo)
    else
        t_lo = BIG1P9
    end if

    return
end function material_dataframe_mu_t_lo

!> Upper temperature limit for dynamic viscosity correlation
pure real(kind=WP) function material_dataframe_mu_t_hi(this)            &
    result(t_hi)
    implicit none

    !> Object reference
    class(material_dataframe), intent(in) :: this

    continue

    if (this%n_mu > 0) then
        t_hi = maxval(this%mu_corr(:)%t_hi)
    else
        t_hi = ZERO
    end if

    return
end function material_dataframe_mu_t_hi

!> Lower temperature limit for thermal conductivity correlation
pure real(kind=WP) function material_dataframe_k_t_lo(this)             &
    result(t_lo)
    use m_constant, only: BIG1P9
    implicit none

    !> Object reference
    class(material_dataframe), intent(in) :: this

    continue

    if (this%n_mu > 0) then
        t_lo = minval(this%k_corr(:)%t_lo)
    else
        t_lo = BIG1P9
    end if

    return
end function material_dataframe_k_t_lo

!> Upper temperature limit for thermal conductivity correlation
pure real(kind=WP) function material_dataframe_k_t_hi(this)             &
    result(t_hi)
    implicit none

    !> Object reference
    class(material_dataframe), intent(in) :: this

    continue

    if (this%n_k > 0) then
        t_hi = maxval(this%k_corr(:)%t_hi)
    else
        t_hi = ZERO
    end if

    return
end function material_dataframe_k_t_hi

!> Initialize attributes
subroutine material_database_init(this)
    implicit none

    !> Object reference
    class(material_database), intent(inout) :: this

    integer :: i

    continue

    do i = 1, n_gas
        call this%gas(i)%init()
    end do

    do i = 1, n_aer
        call this%aerosol(i)%init()
    end do

    return
end subroutine material_database_init

!> Return gid of gas associated with name. Returns `gid_NULL` if name
!! cannot be found
pure integer function material_database_id_from_gas_name(this, name)    &
    result(gid)
    implicit none

    !> Object reference
    class(material_database), intent(in) :: this

    !> Gas name
    character(len=*), intent(in) :: name

    integer :: i

    continue

    gid = gid_NULL

    do i = 1, n_gas
        if (this%gas(i)%matches_name(name)) then
            gid = this%gas(i)%id
            exit
        end if
    end do

    return
end function material_database_id_from_gas_name

!> Return aid of aerosol associated with name. Returns `aid_NULL`
!! if name cannot be found
pure integer function material_database_id_from_aerosol_name(this,      &
    name) result(aid)
    implicit none

    !> Object reference
    class(material_database), intent(in) :: this

    !> Aerosol name
    character(len=*), intent(in) :: name

    integer :: i

    continue

    aid = aid_NULL

    do i = 1, n_aer
        if (this%aerosol(i)%matches_name(name)) then
            aid = this%aerosol(i)%id
            exit
        end if
    end do

    return
end function material_database_id_from_aerosol_name

!> Populate database from CEA2 data files
subroutine material_database_populate(this, verbose)
    use iso_fortran_env, only: iostat_end, iostat_eor,                  &
        stderr => error_unit
    use m_textio, only: munch
    implicit none

    ! Thermo library section parsing states
    !> Initial state
    integer, parameter :: THS_NONE = -1
    !> Parsed thermodynamic library entry record state
    integer, parameter :: THS_THERMO = 0
    !> Parsed expected gas property temperature intervals
    integer, parameter :: THS_INTERVAL = 1
    !> Last parsed product properties state
    integer, parameter :: THS_PRODUCTS = 2
    !> Completed parsing product properties state
    integer, parameter :: THS_END_PRODUCTS = 3
    !> Last parsed reactant properties state
    integer, parameter :: THS_REACTANTS = 4
    !> Completed parsing product properties state
    integer, parameter :: THS_END_REACTANTS = 5
    !> Completed parsing thermodynamic library file state
    integer, parameter :: THS_DONE = 6

    !> Object reference
    class(material_database), intent(inout) :: this

    !> Verbosity flag. Optional; default is .false.
    logical, intent(in), optional :: verbose

    logical :: is_verbose

    logical :: err
    integer :: rct
    character(len=80) :: card

    character(len=80) :: msg
    integer :: iostatus

    integer :: tr_unit
    integer :: th_unit

    integer :: cardct
    integer :: monoct
    integer :: binct
    integer :: gasct

    integer :: record_type
    integer :: vct
    integer :: cct

    character(len=15), dimension(2) :: tr_species
    character(len=1) :: vpresent
    integer :: vlim
    character(len=1) :: cpresent
    integer :: clim
    character(len=40) :: tr_comment

    character(len=1) :: interval_type
    real(kind=WP) :: tlo
    real(kind=WP) :: thi
    real(kind=WP), dimension(4) :: a

    integer :: id_tmp
    character(len=32) :: name_tmp

    integer :: i
    type(mu_correlation), dimension(3) :: vtmp
    type(mu_correlation), dimension(3) :: ctmp

    integer :: ths_state

    type(thermoprop_dataframe) :: tmp_tdf

    logical :: placed_gas
    logical :: placed_aerosol
    integer :: thpct
    integer :: thrct
    integer :: gasthct
    integer :: aerthct

    real(kind=WP), dimension(4) :: tg_bounds
    character(len=10) :: th_date

10  format(A80)
! 20  format(I06, ':', A80)
30  format(A)
! 40  format(A, ", ", I0, " of ", I0)
100 format(A15, 1X, A15, 3X, A1, I1, A1, I1, 2X, A40)
110 format(1X, A1, 2F9.2, 4E15.8)
500 format('Tried opening ', A, ' on unit ', I0, ' for read but got ',  &
    I0, ': ', A)
505 format('Cannot read card, error ', I0, ': ', A)
510 format('Cannot parse ', A, ' card, error ', I0, ': ', A)

    continue

    if (present(verbose)) then
        is_verbose = verbose
    else
        is_verbose = .false.
    end if

    iostatus = 0
    msg = "OK"
    open(newunit=tr_unit, file=this%transfn, action='READ',             &
        status='OLD', form='FORMATTED', iostat=iostatus, iomsg=msg)
    if (iostatus /= 0) then
        write(stderr, fmt=500) munch(this%transfn), tr_unit, iostatus,  &
            munch(msg)
    end if

    cardct = 0
    monoct = 0
    binct = 0
    gasct = 0
    record_type = 0
    vct = 0
    vlim = 0
    cct = 0
    clim = 0
    do while (iostatus == 0)
        cardct = cardct + 1
        read(unit=tr_unit, fmt=10, iostat=iostatus, iomsg=msg) card
        if (iostatus /= 0) then
            write(unit=stderr, fmt=505) iostatus, munch(msg)
            exit
        else
            ! write(unit=stderr, fmt=20) cardct, card
        end if

        if (card(1:4) == "tran") then
            ! write(unit=stderr, fmt=30) "- Beginning of transport file"

            ! call gas_tmp%init()

            cycle
        else if (card(1:3) == "end") then
            ! write(unit=stderr, fmt=30) "- End of transport file"

            if (record_type == 1) then
                !!! Finalize previous record
                name_tmp = munch(tr_species(1))
                ! Look up id by name from gas_names
                id_tmp = get_gas_id(name_tmp)
                ! write(unit=stderr, fmt="(A, ' = ', A, ' -> ', I0)") tr_species(1), name_tmp, id_tmp
                if (id_tmp /= gid_NULL) then
                    ! Add transport data to this%gas
                    gasct = gasct + 1
                    ! write(unit=stderr, fmt='("$$ Finalizing gas record ", I0, ": ", A)') id_tmp, name_tmp
                    this%gas(id_tmp)%id = id_tmp
                    this%gas(id_tmp)%name = name_tmp
                    this%gas(id_tmp)%n_mu = vlim
                    if (vlim > 0) then
                        this%gas(id_tmp)%mu_corr = vtmp(1:vlim)
                    end if
                    this%gas(id_tmp)%n_k = clim
                    if (clim > 0) then
                        this%gas(id_tmp)%k_corr = ctmp(1:clim)
                    end if
                end if
            end if

            exit
        else if (card(1:1) /= " ") then
            if (vct /= vlim) then
                write(unit=stderr, fmt=30) '!! Viscosity interval '     &
                    // 'count of previous record does not match '       &
                    // 'expected count'
            ! else
            !     if (vlim > 0) then
            !         write(unit=stderr, fmt=30) '-- Found all expected viscosity intervals'
            !     end if
            end if

            if (cct /= clim) then
                write(unit=stderr, fmt=30) '!! Conductivity interval '  &
                    // 'count of previous record does not match '       &
                    // 'expected count'
            ! else
            !     if (clim > 0) then
            !         write(unit=stderr, fmt=30) '-- Found all expected conductivity intervals'
            !     end if
            end if

            if (record_type == 1) then
                !!! Finalize previous record
                name_tmp = munch(tr_species(1))
                ! Look up id by name from gas_names
                id_tmp = get_gas_id(name_tmp)
                ! write(unit=stderr, fmt="(A, ' = ', A, ' -> ', I0)") tr_species(1), name_tmp, id_tmp
                if (id_tmp /= gid_NULL) then
                    ! Add transport data to this%gas
                    gasct = gasct + 1
                    ! write(unit=stderr, fmt='("$$ Finalizing gas record ", I0, ": ", A)') id_tmp, name_tmp
                    this%gas(id_tmp)%id = id_tmp
                    this%gas(id_tmp)%name = name_tmp
                    this%gas(id_tmp)%n_mu = vlim
                    if (vlim > 0) then
                        this%gas(id_tmp)%mu_corr = vtmp(1:vlim)
                    end if
                    this%gas(id_tmp)%n_k = clim
                    if (clim > 0) then
                        this%gas(id_tmp)%k_corr = ctmp(1:clim)
                    end if
                end if
            end if

            ! write(unit=stderr, fmt=30) "- Record head"

            ! call gas_tmp%init()
            do i = 1, 3
                call vtmp(i)%init()
                call ctmp(i)%init()
            end do

            vlim = 0
            vct = 0
            clim = 0
            cct = 0
            read(card, fmt=100, iostat=iostatus, iomsg=msg)             &
                tr_species(1), tr_species(2), vpresent, vlim, cpresent, &
                clim, tr_comment
            if (iostatus /= 0) then
                write(stderr, fmt=510) 'trans record head', iostatus,   &
                    munch(msg)
            end if

            if (len(munch(tr_species(2))) == 0) then
                ! write(unit=stderr, fmt=30) '-- Transport property record for ' &
                !     // munch(tr_species(1))
                record_type = 1
                monoct = monoct + 1
            else
                ! write(unit=stderr, fmt=30) '-- Interactivity record between ' &
                !     // munch(tr_species(1)) // ' and '                  &
                !     // munch(tr_species(2))
                record_type = 2
                binct = binct + 1
            end if

            ! if (vpresent == 'V') then
            !     write(unit=stderr, fmt='(A, I0, A)') '-- Expecting ', vlim, ' viscosity intervals'
            ! end if
            ! if (cpresent == 'C') then
            !     write(unit=stderr, fmt='(A, I0, A)') '-- Expecting ', clim, ' conductivity intervals'
            ! end if
            ! if (len(munch(tr_comment)) > 0) then
            !     write(unit=stderr, fmt=30) '-- Transport comment: ' // munch(tr_comment)
            ! end if
        else if (card(1:2) == " V") then
            vct = vct + 1
            if (record_type == 1) then
                ! write(unit=stderr, fmt=40) "- Viscosity interval", vct, vlim
                if (vct == vlim) then
                    ! write(unit=stderr, fmt=30) "-- Last interval"
                else if (vct > vlim) then
                    write(unit=stderr, fmt=30) "!! Unexpected interval"
                end if

                read(card, fmt=110, iostat=iostatus, iomsg=msg)         &
                    interval_type, tlo, thi, a

                if (iostatus /= 0) then
                    write(stderr, fmt=510) 'viscosity interval',        &
                        iostatus, munch(msg)
                end if

                ! if (tlo < 200.0_WP) then
                !     write(unit=stderr, fmt=30) "~~ Lower limit temperature is below that needed for this problem domain"
                ! else if (tlo > 5000.0_WP) then
                !     write(unit=stderr, fmt=30) "?? Lower limit temperature is above that needed for this problem domain"
                ! end if

                ! if (thi < 200.0_WP) then
                !     write(unit=stderr, fmt=30) "?? Upper limit temperature is below that needed for this problem domain"
                ! else if (thi > 5000.0_WP) then
                !     write(unit=stderr, fmt=30) "~~ Upper limit temperature is above that needed for this problem domain"
                ! end if

                if (thi <= tlo) then
                    write(unit=stderr, fmt=30) "!! Upper limit "        &
                        // "temperature is at or below lower limit "    &
                        // "temperature"
                end if

                ! write(unit=stderr, fmt="('-- Correlation coefficients:', 4(1X, ES16.9))") a

                vtmp(vct) = mu_correlation(tlo, thi, a)

            ! else if (record_type == 2) then
            !     write(unit=stderr, fmt=30) "-- Ignoring viscosity interval of interactivity record"
            end if
        else if (card(1:2) == " C") then
            cct = cct + 1
            if (record_type == 1) then
                ! write(unit=stderr, fmt=40) "- Conductivity interval", cct, clim
                if (cct == clim) then
                    ! write(unit=stderr, fmt=30) "-- Last interval"
                else if (cct > clim) then
                    write(unit=stderr, fmt=30) "!! Unexpected interval"
                end if

                read(card, fmt=110, iostat=iostatus, iomsg=msg)         &
                    interval_type, tlo, thi, a

                if (iostatus /= 0) then
                    write(stderr, fmt=510) 'conductivity interval',     &
                        iostatus, munch(msg)
                end if

                ! if (tlo < 200.0_WP) then
                !     write(unit=stderr, fmt=30) "~~ Lower limit temperature is below that needed for this problem domain"
                ! else if (tlo > 5000.0_WP) then
                !     write(unit=stderr, fmt=30) "?? Lower limit temperature is above that needed for this problem domain"
                ! end if

                ! if (thi < 200.0_WP) then
                !     write(unit=stderr, fmt=30) "?? Upper limit temperature is below that needed for this problem domain"
                ! else if (thi > 5000.0_WP) then
                !     write(unit=stderr, fmt=30) "~~ Upper limit temperature is above that needed for this problem domain"
                ! end if

                if (thi <= tlo) then
                    write(unit=stderr, fmt=30) "!! Upper limit "        &
                        // "temperature is at or below lower limit "    &
                        // "temperature"
                end if

                ! write(unit=stderr, fmt="('-- Correlation coefficients:', 4(1X, ES16.9))") a

                ctmp(cct) = mu_correlation(tlo, thi, a)

            ! else if (record_type == 2) then
                ! write(unit=stderr, fmt=30) "-- Ignoring conductivity interval of interactivity record"
            end if
        else
            write(unit=stderr, fmt=30) "? ***** Unknown card *****"
        end if
    end do

    if (is_verbose) then
        write(unit=stderr, fmt=*)
        write(unit=stderr, fmt=30) 'Reading from trans.inp:'
        write(unit=stderr, fmt="('* ', I0, ' lines read, ', "           &
            // "I0, ' gas records, ', "                                 &
            // "I0, ' species records, ', "                             &
            // "I0, ' interation records')")                            &
            cardct, gasct, monoct, binct
        ! do i = 1, n_gas
        !     write(unit=stderr, fmt="(I0, ': ', A, ' mu(500K) = ', ES16.9, ', k(500K) = ', ES16.9)") &
        !         this%gas(i)%id, this%gas(i)%name, this%gas(i)%mu(500.0_WP), this%gas(i)%k(500.0_WP)
        ! end do
    end if

    close(tr_unit)

    msg = "OK"
    open(newunit=th_unit, file=this%thermofn, action='READ',            &
        status='OLD', form='FORMATTED', iostat=iostatus, iomsg=msg)
    if (iostatus /= 0) then
        write(stderr, fmt=500) munch(this%thermofn), th_unit,   &
            iostatus, munch(msg)
    end if

    thpct = 0
    thrct = 0
    gasthct = 0
    aerthct = 0
    ths_state = THS_NONE
    cardct = 0
    do while (iostatus == 0 .and. ths_state /= THS_DONE)
        placed_gas = .false.
        placed_aerosol = .false.
        cardct = cardct + 1
        read(unit=th_unit, fmt=10, iostat=iostatus, iomsg=msg) card
        if (iostatus /= 0) then
            write(unit=stderr, fmt=505) iostatus, munch(msg)
            exit
        end if

        ! Echo with card number
        ! write(unit=stderr, fmt=20) cardct, card

        ! if state is (any)
        if (len_trim(card) == 0) then
            ! write(unit=stderr, fmt=30) '- Blank'
            ! No state change
            cycle
        end if

        ! if state is (any)
        if (card(1:1) == "!" .or. card(1:1) == "#") then
            ! write(unit=stderr, fmt=30) '- Comment'
            ! No state change
            cycle
        end if

        ! if state is NONE
        if (ths_state == THS_NONE) then
            if (card(1:6) == "thermo") then
                ! write(unit=stderr, fmt=30) '- Begin thermo database'
                ths_state = THS_THERMO
                cycle
            end if
        end if

        ! if state is THS_THERMO
        ! read( 4F10.3, A10) -- temperature interval bounds (200, 1000, 6000, 20000) followed by a text date
        ! state is THS_INTERVAL
        if (ths_state == THS_THERMO) then
            read(card, fmt='(4F10.3, A10)') tg_bounds, th_date
            ! write(unit=stderr, fmt="('- Thermo gas temp intervals', "   &
            !     // "4(2X, F10.2), ' Date: ', A10)") tg_bounds, th_date
            ths_state = THS_INTERVAL
            cycle
        end if

        if (ths_state == THS_INTERVAL                                   &
            .or. ths_state == THS_PRODUCTS) then
            if (card(1:12) == "END PRODUCTS") then
                ! write(unit=stderr, fmt=30) '- End products section'
                ths_state = THS_END_PRODUCTS
                ! thp_state = THP_NONE
                cycle
            else
                ! write(unit=stderr, fmt=30) '- Product thermo data'
                ths_state = THS_PRODUCTS

                call this%read_raw_thermo_record(tmp_tdf, th_unit,      &
                    card, rct, err, iostatus, msg)
                if (err) then
                    write(unit=stderr, fmt="('Error reading product "   &
                        // "thermo record: (', I0, ') ', A)")           &
                        iostatus, msg
                    exit
                else
                    thpct = thpct + 1
                    call this%import_thermo_record(tmp_tdf, placed_gas, &
                        placed_aerosol)
                    if (placed_gas) then
                        gasthct = gasthct + 1
                    end if
                    if (placed_aerosol) then
                        aerthct = aerthct + 1
                    end if
                    ! write(unit=stderr, fmt="('-- Read ', I0, "          &
                    !     // "' additional lines of a product thermo "    &
                    !     // "record: ', A)") rct,                        &
                    !     munch(tmp_tdf%name)
                    cardct = cardct + rct
                end if

                cycle
            end if
        end if

        if (ths_state == THS_END_PRODUCTS                               &
            .or. ths_state == THS_REACTANTS) then
            if (card(1:13) == "END REACTANTS") then
                ! write(unit=stderr, fmt=30) '- End reactants section (DONE)'
                ths_state = THS_DONE
                cycle
            else
                ! write(unit=stderr, fmt=30) '- Reactant thermo data'
                ths_state = THS_REACTANTS
                call this%read_raw_thermo_record(tmp_tdf, th_unit,      &
                    card, rct, err, iostatus, msg)
                if (err) then
                    write(unit=stderr, fmt="('Error reading reactant "  &
                        // "thermo record: (', I0, ') ', A)")           &
                        iostatus, msg
                    exit
                else
                    thrct = thrct + 1
                    call this%import_thermo_record(tmp_tdf, placed_gas, &
                        placed_aerosol)
                    if (placed_gas) then
                        gasthct = gasthct + 1
                    end if
                    if (placed_aerosol) then
                        aerthct = aerthct + 1
                    end if
                    ! write(unit=stderr, fmt="('-- Read ', I0, "          &
                    !     // "' additional lines of a reactant thermo "   &
                    !     // "record: ', A)") rct,                        &
                    !     munch(tmp_tdf%name)
                    cardct = cardct + rct
                end if
                cycle
            end if
        end if
    end do

    if (is_verbose) then
        write(unit=stderr, fmt=*)
        write(unit=stderr, fmt=30) 'Reading from thermo.inp:'
        write(unit=stderr, fmt="('* ', I0, ' lines read, ', "           &
            // "I0, ' products found, ', "                              &
            // "I0, ' reactants found, ', "                             &
            // "I0, ' gas species imported, ', "                        &
            // "I0, ' aerosol species imported')")                      &
            cardct, thpct, thrct, gasthct, aerthct

        write(unit=stderr, fmt=*)
        write(unit=stderr, fmt=30) 'Gas Example Property Data:'
        do i = 1, n_gas
            write(unit=stderr, fmt="(I2, ': ', A6, "                    &
                // "', mu(500K) = ', ES16.9, "                          &
                // "', k(500K) = ', ES16.9, "                           &
                // "', Cp(500K) = ', ES16.9, ', n_cp = ', I2)")         &
                this%gas(i)%id, trim(this%gas(i)%name),                 &
                this%gas(i)%mu(500.0_WP), this%gas(i)%k(500.0_WP),      &
                this%gas(i)%cp(500.0_WP), this%gas(i)%n_cp
        end do

        write(unit=stderr, fmt=*)
        write(unit=stderr, fmt=30) 'Aerosol Example Property Data:'
        do i = 1, n_aer
            write(unit=stderr, fmt="(I2, ': ', A6, "                    &
                // "', Cp(500K) = ', ES16.9, ', n_cp = ', I2)")         &
                this%aerosol(i)%id, trim(this%aerosol(i)%name),         &
                this%aerosol(i)%cp(500.0_WP), this%aerosol(i)%n_cp
        end do
    end if

    close(th_unit)

    return
end subroutine material_database_populate

!> @brief Match thermodynamic property record obtained from data file
!! to gas or condensed (aerosol) species being tracked by the
!! application and import property data to the appropriate material
!! database entry.
!!
!! The diagnostic arguments `placed_gas` and `placed_aerosol` are set
!! to `.true.` if the supplied property record is imported into the
!! application material database. Only one of these may be true (the
!! properties are either added to a gas entry or an aerosol,
!! not both); both may be false if the supplied property record is
!! ignored.
!!
!! @note By default the entry for water ice (`H2O(L)`) is specifically
!! ignored because its presence is not considered physically realistic
!! in the application code. This may be changed by passing the optional
!! argument `include_ice` as `.true.`
subroutine material_database_import_thermo_record(this, raw_tdf,        &
    placed_gas, placed_aerosol, include_ice)
    use iso_fortran_env, only: iostat_end, iostat_eor,                  &
        stderr => error_unit
    use m_textio, only: munch
    implicit none

    !> Object reference
    class(material_database), intent(inout) :: this

    !> Lightly-processed `thermoprop_dataframe`. Note that id and
    !! material name may need cleanup and further processing before use
    type(thermoprop_dataframe), intent(in) :: raw_tdf

    !> Flag indicating a matching gas species was found.
    !! Supports diagnostic reporting in the calling routine.
    logical, intent(out) :: placed_gas

    !> Flag indicating a matching aerosol species was found.
    !! Supports diagnostic reporting in the calling routine.
    logical, intent(out) :: placed_aerosol

    !> Flag indicating that water ice (`H2O(cr)`) properties should
    !! be added as properties of aerosol \f$\ce{H2O}\f$. This argument
    !! is optional; if missing the default value is `.false.` and
    !! behavior is to ignore water ice properties.
    logical, intent(in), optional :: include_ice

    character(len=32) :: scratch
    character(len=15) :: tmp_matl
    integer :: l_matl
    character(len=6) :: tmp_gas
    integer :: l_gas

    integer :: i
    integer :: id

    logical :: ignore_ice

    continue

    if (present(include_ice)) then
        ignore_ice = (.not. include_ice)
    else
        ignore_ice = .true.
    end if

    placed_gas = .false.
    placed_aerosol = .false.

    scratch = munch(raw_tdf%name)
    tmp_matl = scratch(1:15)
    l_matl = len_trim(tmp_matl)

    if (raw_tdf%phase_id < 0) then
        ! Uninitialized
        write(unit=stderr,                                              &
            fmt="('--- Raw thermo property object is uninitialized')")
    else if (raw_tdf%phase_id == phase_GAS) then
        ! Gas
        do i = 1, n_gas
            tmp_gas = gas_name(i)
            id = gas_id(i)
            l_gas = len(munch(tmp_gas))

            if (l_gas == l_matl) then
                if (tmp_gas(1:l_gas) == tmp_matl(1:l_matl)) then
                    ! Assign thermo props of raw_tdf to this%gas(id)
                    this%gas(id)%mw = raw_tdf%mw
                    this%gas(id)%dHform = raw_tdf%dHform
                    this%gas(id)%phase_id = raw_tdf%phase_id
                    this%gas(id)%n_cp = raw_tdf%n_cp
                    this%gas(id)%cp_corr = raw_tdf%cp_corr
                    if (this%gas(id)%n_cp >= 0) then
                        this%gas(id)%h_std_0 =                          &
                            this%gas(id)%cp_corr(1)%h_std_0
                    end if
                    placed_gas = (raw_tdf%phase_id == phase_GAS)
                    placed_aerosol = (raw_tdf%phase_id > 0)
                    ! write(unit=stderr,                                  &
                    !     fmt="('--- Set thermo properties for gas ', "   &
                    !     // "I0, ': ', A)") id, tmp_matl
                    exit
                end if
            end if
        end do
    else
        ! Condensed species

        ! Brittle, but there's really no good way around this...
        if (tmp_matl == 'H2O(cr)' .and. ignore_ice) then
            ! write(unit=stderr, fmt="('--- Specifically skipping ice '," &
            !     // "'(', A, ')')") trim(tmp_matl)
            return
        end if

        do i = 1, n_aer
            tmp_gas = aer_name(i)
            id = aer_id(i)
            l_gas = len(munch(tmp_gas))
            if (l_gas <= l_matl) then
                if (tmp_gas(1:l_gas) == tmp_matl(1:l_gas)) then
                    ! write(unit=stderr, fmt="('--- Prefix match for "    &
                    !     // "aerosol ', I0, ': ', A, ' in ', A)")        &
                    !     id, tmp_gas, tmp_matl
                    if (tmp_matl(l_gas+1:l_gas+3) == '(a)'              &
                        .or. tmp_matl(l_gas+1:l_gas+3) == '(b)'         &
                        .or. tmp_matl(l_gas+1:l_gas+3) == '(c)'         &
                        .or. tmp_matl(l_gas+1:l_gas+3) == '(L)'         &
                        .or. tmp_matl(l_gas+1:l_gas+4) == '(cr)') then
                        ! write(unit=stderr,                              &
                        !     fmt="('--- Species and phase match for "    &
                        !     // "aerosol ', I0, ': ', A, ' in ', A)")    &
                        !     id, tmp_gas, tmp_matl
                        placed_gas = (raw_tdf%phase_id == phase_GAS)
                        placed_aerosol = (raw_tdf%phase_id > 0)
                        if (this%aerosol(id)%n_cp < 0) then
                            ! Assign thermo props of raw_tdf to this%aerosol(id)
                            this%aerosol(id)%name = tmp_gas
                            this%aerosol(id)%id = id
                            this%aerosol(id)%mw = raw_tdf%mw
                            this%aerosol(id)%dHform = raw_tdf%dHform
                            this%aerosol(id)%phase_id = raw_tdf%phase_id
                            this%aerosol(id)%n_cp = raw_tdf%n_cp
                            this%aerosol(id)%cp_corr = raw_tdf%cp_corr
                            if (this%aerosol(id)%n_cp >= 0) then
                                this%aerosol(id)%h_std_0 =              &
                                    this%aerosol(id)%cp_corr(1)%h_std_0
                            end if
                            ! write(unit=stderr,                          &
                            !     fmt="('>>> Insert thermo properties '," &
                            !     // "'for aerosol ', I0, ': ', A)")      &
                            !     id, tmp_matl
                        else
                            ! write(unit=stderr,                          &
                            !     fmt="('+++ Append cp_corr to aerosol ',"&
                            !     // " I0, ': ', A )") id, tmp_matl
                            ! write(unit=stderr, fmt="('+++ n_cp is ', "  &
                            !     // "I0)") this%aerosol(id)%n_cp
                            if (this%aerosol(id)%n_cp > 0               &
                                .and. raw_tdf%n_cp > 0) then
                                this%aerosol(id)%cp_corr = (/           &
                                    this%aerosol(id)%cp_corr,           &
                                    raw_tdf%cp_corr /)
                                this%aerosol(id)%n_cp =                 &
                                    size(this%aerosol(id)%cp_corr)
                                ! write(unit=stderr, fmt="('+++ n_cp ', " &
                                !     // "'increased to ', I0)")          &
                                !     this%aerosol(id)%n_cp
                            end if
                        end if
                        exit
                    else if (tmp_matl(l_gas+1:l_gas+1) == '(') then
                        ! FYI; this case doesn't currently happen in practice
                        write(unit=stderr,                              &
                            fmt="('--- Skipping unwanted phase for "    &
                            // "aerosol ', I0, ': ', A, ' in ', A)")    &
                            id, tmp_gas, tmp_matl
                        exit
                    end if
                end if
            end if
        end do
    end if

    return
end subroutine material_database_import_thermo_record

!> @brief Parse out thermodynamic property data (\f$C_{p}\f$, etc.)
!! and return via `thermoprop_dataframe` object.
!!
!! The first card in the property has been read prior to this routine
!! being called and is passed in via `card_1`. Subsequent cards are
!! read from `th_unit` and parsed as per the thermodynamic property
!! file format documented in Appendix A of @cite NASA-RP1311-v2.
!! Read and parsing errors are detected via the `iostatus` attribute
!! of `read()` and the routine exits on the first error encountered,
!! setting `err` to `.true.` and passing the status code and error
!! message to the calling routine via `iostatus` and `msg` respectively.
!! Errors are not expected to be recoverable and there is no provision
!! for resuming interruped processing of thermodynamic property records.
!! The number of additional cards read from `th_unit` are returned in
!! `rct` to allow the calling routing to maintain a proper count of
!! cards read. A number of record elements are parsed but are not
!! stored in the `thermoprop_dataframe` object. Maximal/strict parsing
!! is attempted to give the greatest chance of detecting errors in the
!! thermodynamic property file to avoid later inaccuracies in
!! caculation.
!!
!! @note The Record 1 format provided in Table A.1 in
!! @cite NASA-RP1311-v2 is inconsistent with the format found in
!! `thermo.inp`. The read format `(A15, 3X, A62)` is used instead.
!!
!! @note The object reference was removed and the method was declared
!! as `nopass` to resolve a compiler warning about unused argument
subroutine material_database_read_raw_thermo_record(raw_tdf, th_unit,   &
    card_1, rct, err, iostatus, msg)
    use iso_fortran_env, only: iostat_end, iostat_eor,                  &
        stderr => error_unit
    use m_textio, only: munch
    implicit none

    ! !> Object reference
    ! class(material_database), intent(inout) :: this

    !> Lightly-processed `thermoprop_dataframe`. Note that id and
    !! material name may need cleanup and further processing before use
    type(thermoprop_dataframe), intent(out) :: raw_tdf

    !> File input unit
    integer, intent(in) :: th_unit

    !> First card of property record, previously read from file by
    !! calling routine
    character(len=80), intent(in) :: card_1

    !> Additional number of cards read; supports diagnostic accounting
    integer, intent(out) :: rct

    !> Error status; set to .true. if error occurs, .false. otherwise
    logical, intent(out) :: err

    !> I/O status code. This is only meaningful if err is set to .true.
    integer, intent(out) :: iostatus

    !> I/O status message. This is only meaningful if err is set to
    !! .true.
    character(len=80), intent(out) :: msg

    character(len=80) :: card

    integer :: i
    integer :: ict

    ! Card 1
    character(len=15) :: raw_matl
    character(len=62) :: comment

    ! Card 2
    integer :: cpct
    character(len=6) :: idcode
    character(len=2), dimension(5) :: el_id
    real(kind=WP), dimension(5) :: el_count
    integer :: phase_id
    real(kind=WP) :: mw
    real(kind=WP) :: dHform

    ! Card 3
    real(kind=WP) :: t_hi
    real(kind=WP) :: t_lo
    integer :: nexps
    real(kind=WP), dimension(8) :: exps
    real(kind=WP) :: h_std_0

    ! Card 4-5
    real(kind=WP), dimension(7) :: a
    real(kind=WP), dimension(2) :: b

100 format(A80)
110 format(A15, 3X, A62)
120 format(1X, I1, 1X, A6, 1X, 5(A2, F6.2), 1X, I1, F13.5, F13.5)
130 format(1X, 2F10.3, 1X, I1, 8F5.1, 3X, F15.3)
140 format(5E16.8)
150 format(2E16.8, 16X, 2E16.8)

    continue

    call raw_tdf%init()
    rct = 0

    ! Premature loop exit indicates read error. err is initialized to
    ! .true. and reset to .false. on normal termination of loop R0
    err = .true.
R0: do
        ! Card 1
        read(card_1, fmt=110, iostat=iostatus, iomsg=msg) raw_matl,     &
            comment
        if (iostatus /= 0) exit R0

        ! Card 2
        read(unit=th_unit, fmt=100, iostat=iostatus, iomsg=msg) card
        if (iostatus == 0) then
            rct = rct + 1
        else
            exit R0
        end if

        read(card, fmt=120, iostat=iostatus, iomsg=msg) cpct, idcode,   &
            (el_id(i), el_count(i), i=1,5), phase_id, mw, dHform
        if (iostatus /= 0) exit R0

        raw_tdf = new_thermoprop_dataframe(munch(raw_matl), mw, dHform, &
            phase_id, cpct)

        if (cpct == 0) then
            ! Card 3.0
            read(unit=th_unit, fmt=100, iostat=iostatus, iomsg=msg) card
            if (iostatus == 0) then
                rct = rct + 1
            else
                exit R0
            end if

            read(card, fmt=130, iostat=iostatus, iomsg=msg) a(1),       &
                a(2), nexps, exps(1:8), h_std_0
            if (iostatus /= 0) exit R0
            a(2) = ZERO
            nexps = 0
            exps = ZERO

            raw_tdf%cp_corr(1)%t_lo = ZERO
            raw_tdf%cp_corr(1)%t_hi = ZERO
            raw_tdf%cp_corr(1)%h_std_0 = h_std_0
            raw_tdf%cp_corr(1)%a = a
            raw_tdf%cp_corr(1)%b = ZERO
        else
            do ict = 1, cpct
                ! Card 3.1
                read(unit=th_unit, fmt=100, iostat=iostatus, iomsg=msg) &
                    card
                if (iostatus == 0) then
                    rct = rct + 1
                else
                    exit R0
                end if

                read(card, fmt=130, iostat=iostatus, iomsg=msg)         &
                    t_lo, t_hi, nexps, exps(1:8), h_std_0
                if (iostatus /= 0) exit R0

                ! Card 4
                read(unit=th_unit, fmt=100, iostat=iostatus, iomsg=msg) &
                    card
                if (iostatus == 0) then
                    rct = rct + 1
                else
                    exit R0
                end if

                read(card, fmt=140, iostat=iostatus, iomsg=msg) a(1:5)
                if (iostatus /= 0) exit R0

                ! Card 5
                read(unit=th_unit, fmt=100, iostat=iostatus, iomsg=msg) &
                    card
                if (iostatus == 0) then
                    rct = rct + 1
                else
                    exit R0
                end if

                read(card, fmt=150, iostat=iostatus, iomsg=msg)         &
                    a(6:7), b(1:2)
                if (iostatus /= 0) exit R0

                raw_tdf%cp_corr(ict)%t_lo = t_lo
                raw_tdf%cp_corr(ict)%t_hi = t_hi
                raw_tdf%cp_corr(ict)%h_std_0 = h_std_0
                raw_tdf%cp_corr(ict)%a = a
                raw_tdf%cp_corr(ict)%b = b
            end do
        end if
        err = .false.
        exit R0
    end do R0

    return
end subroutine material_database_read_raw_thermo_record

end module m_material