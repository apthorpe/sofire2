!> @file c1_output.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_output module.
program c1_output
    use sofire2_param, only: WP
    use m_constant, only: ZERO!, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_output
    use toast
    implicit none

    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_output")

    !!! Test exposed functions

    ! csvfn = get_csv_file(case_id)
    ! iunit = get_csv_unit(filename)
    ! close_csv_unit(iunit)

    !!! Test classes

    ! type, public :: csv_writer
    !     !> CSV data file name
    !     character(len=64) :: filename = 'sofire2_results.csv'

    !     !> Output unit number
    !     integer :: unit = stdout
    ! contains
    !     procedure :: open_file => open_csv_file
    !     procedure :: write_header => write_csv_header
    !     procedure :: write_data => write_csv_data
    !     procedure :: close_file => close_csv_file
    !     final :: destroy_csv_writer
    ! end type csv_writer

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_output.json")

    call test%checkfailure()
end program c1_output
