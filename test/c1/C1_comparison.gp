#!/usr/bin/gnuplot
# vim: set expandtab: set syntax=gnuplot: set nowrap: set noai: set tw=132

set macro

##################################################################
# Load convenience symbols, functions, and variables
load '_sym.gp'
load '_general_functions.gp'

##################################################################
# Set code version and plot timestamp
load '_code_version.gp'
tsfmt = code_version . " %Y%m%d%H%M%S"

casename = 'C1'
outbase = 'C1_comparison'

##################################################################
# Select output terminal

## Uncomment for png output
#L set terminal pngcairo enhanced mono size 9in, 7in dashed linewidth 1 rounded
#P set terminal pngcairo enhanced mono size 7in, 9in dashed linewidth 1 rounded
# ext = 'png'

## Uncomment for pdf output
# set terminal pdfcairo enhanced mono size 11in, 8.5in dashed linewidth 3 rounded
# set terminal pdfcairo enhanced mono size 5in, 7in dashed linewidth 3 rounded
# set terminal pdfcairo enhanced mono size 5in, 5in dashed linewidth 4 rounded
set terminal pdfcairo enhanced mono size 5in, 7in dashed linewidth 2 rounded
ext = 'pdf'

##################################################################
# Select font face
fface = "Cabin"
load('_' . ext . '_font_sizes.gp')

set font nfont
set title font hfont # or tfont?
set ylabel font nfont
set xlabel font nfont
set key font kfont

##################################################################
# Set plot files and variable names
load 'plotvar.gp'

# For CSV files
set datafile separator ','

set key autotitle columnhead # use the first line as key

set rmargin 5

set grid back
set tics back

set timestamp tsfmt rotate font tinyfont

set style data lines

set xlabel 'Time, hour' font nfont
set xrange [0.0 : 10.0]
set xtics 1.0
set mxtics 10

pltnum = 0
########################################################################

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Gas Temperature"

set ylabel 'Temperature, ' . uDEGF font nfont
set yrange [140.0 : 380.0]
set ytics 20.0
set mytics 4
set key right top

plot plotfile1 using (column(cT)):(RtoF(column(cTGASC))) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(RtoF(column(cTGASC))) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(RtoF(column(cTGASC))) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Wall Surface Temperature"

set ylabel 'Temperature, ' . uDEGF font nfont
set yrange [138.0 : 152.0]
set ytics 2.0
set mytics 4

plot plotfile1 using (column(cT)):(RtoF(column(cTWC1))) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(RtoF(column(cTWC1))) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(RtoF(column(cTWC1))) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Sodium Burning Rate"

set ylabel 'Burning Rate, lbm/hr-ft^2' font nfont
set logscale y
set yrange [1.0E-10 : 1.0E0]
set ytics 10
set mytics 10
set format y "10^{%T}"

plot plotfile1 using (column(cT)):(column(cXM)) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(column(cXM)) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(column(cXM)) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Gas Pressure"

set ylabel 'Pressure, psig' font nfont
unset logscale y
set yrange [-2.0 : 6.0]
set ytics 2.0
set mytics 10
set format y "%0.0f"

plot plotfile1 using (column(cT)):(column(cPGASC)/144.0 - 14.7) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(column(cPGASC)/144.0 - 14.7) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(column(cPGASC)/144.0 - 14.7) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Floor Temperature"

set ylabel 'Temperature, ' . uDEGF font nfont
set yrange [100.0 : 600.0]
set ytics 100.0
set mytics 10

plot plotfile1 using (column(cT)):(RtoF(column(cTF1))) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(RtoF(column(cTF1))) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(RtoF(column(cTF1))) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Floor Temperature (Concrete, Node 2)"

set ylabel 'Temperature, ' . uDEGF font nfont
set yrange [130.0 : 170.0]
set ytics 5.0
set mytics 5
set key right bottom

plot plotfile1 using (column(cT)):(RtoF(column(cTF2))) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(RtoF(column(cTF2))) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(RtoF(column(cTF2))) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Wall Temperature (Concrete, Node 2)"

set ylabel 'Temperature, ' . uDEGF font nfont
set yrange [138.0 : 152.0]
set ytics 2.0
set mytics 4
set key right top

plot plotfile1 using (column(cT)):(RtoF(column(cTWC2))) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(RtoF(column(cTWC2))) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(RtoF(column(cTWC2))) title "1973, SP" with linespoints pointtype 11 pointnumber 12

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Sodium Surface Temperature"

set ylabel 'Temperature, ' . uDEGF font nfont
set yrange [0.0 : 1000.0]
set ytics 200.0
set mytics 10

plot plotfile1 using (column(cT)):(RtoF(column(cTS))) title "2020, DP" with linespoints pointtype 4  pointnumber 12, \
     plotfile2 using (column(cT)):(RtoF(column(cTS))) title "2020, SP" with linespoints pointtype 10 pointnumber 12, \
     plotfile3 using (column(cT)):(RtoF(column(cTS))) title "1973, SP" with linespoints pointtype 11 pointnumber 12
