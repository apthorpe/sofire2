# Add to CMakeLists.txt:

# Place after find_package(FLIBS) and include(BuildFLIBS) to define the
# add_flibs_unit_test() function used below
include(FLIBShelper)

# Use something like the following to build and schedule unit tests using
# ftnunit:

# Demo of ftnunit unit test
add_flibs_unit_test(
    TARGET ut_c1_ftnunit
    SOURCES "${TEST_SRC_BASE}/c1/c1_ftnunit.f90" "${SOFIRE2_1C_DEP_SOURCES}"
    DEPENDENCIES pretest_setup FLIBS_external
    RUN_DIRECTORY ${SOFIRE2_1C_TEST_DIR}
)
