\chapter{Model and Software Validation}

A number of authors have provided analysis and criticism of the models
and correlations used in the original version of SOFIRE~II and their
implementation; see \cite{Hilliard1978}, \cite{Newman1983},
\cite{Sienicki2012}, \textit{et al.} Using these works as a basis in
combination with more detailed source code analysis, a number of
specific criticisms of the original 1973 code are elaborated below.
A number of issues have been resolved as a result of this analysis.

\section{SOFIRE~II -- One-Cell}

The heat flow topology of the one-cell model is shown in
Figure~\ref{fig:c1_heat_flow}. Conduction heat transfer is represented
by solid arrows, convection by dashed arrows, and radiation by dotted
arrows. Note that heat conduction between floor nodes 1 and 2 (QCOND2)
and between wall nodes 1 and 2 (QCOND6) takes into account a thin air
gap separating the liner from the structure wall.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c1_heat_flow}
    \end{center}
    \caption{Heat flows in one-cell model}
    \label{fig:c1_heat_flow}
\end{figure}

\subsection{Air Thermal Conductivity}

\texttt{AK1} and \texttt{AK2} appear to be linear temperature-dependent correlations for
gas (air) thermal conductivity of the form:

\begin{equation}
    \mathtt{AK}(T) = \num{0.014} + \num{1.92E-05} T
\end{equation}
where $T$ is in $\us{\fahrenheit}$ and \texttt{AK} is in $\us{\BTU\per\hour\per\foot\per\fahrenheit}$.

No reference is provided for this correlation in \cite{Beiriger1973} but
\cite{Kannuluik1951} gives a similar quadratic relation which is comparable
up to $\SI{218}{\celsius}$, the upper limit of the Kannuluik and Carman correlation:

\begin{equation}
    k_{g,air}(t) = \num{5.75E-5} \left(1 + \num{3.17E-2} t - \num{2.1E-6}t^{2}\right)
\end{equation}
where $t$ is in $\si{\celsius}$ and $k_{g,air}$ is in $\si{\calorie\per\second\per\centi\meter\per\celsius}$.

The original correlation has been extracted from the main program and isolated as function
\texttt{kgair()} in the module \texttt{m\_sofire\_c1}.

Looking further at the code, \texttt{AK1} is evaluated at a temperature between
the sodium pool and bulk gas. \texttt{AK2} is evaluated at a temperature
between the cell wall liner (node 1) and the bulk gas.

\subsection{Low Precision in $\frac{1}{3}$ Exponent}
\label{ss:vv_third}

\texttt{EX1} and \texttt{EX2} appear to implement a portion of the heat transfer correlations
given in Equations~\ref{eq:h_conv_wall} and \ref{eq:h_conv_floor}, specifically the portions of the form:

\begin{equation}
    \left( g \text{Pr} \frac{\beta}{\nu^{2}} \Delta T\right)^{\frac{1}{3}}
\end{equation}

\texttt{EX1} is implemented as

\begin{Verbatim}
    EX1 = (GIN * B1 / D1 * abs(TS - TGASC))**THIRD
\end{Verbatim}

where $\text{GIN} = g$, $\text{B1} = \beta$, and $\text{D1} = \nu^{2}$, making
\texttt{EX1} represent $\frac{\text{Gr}^{\frac{1}{3}}}{L}$ with the characteristic
length $L$ cancelling out later.

The exponent provided in the text is $\frac{1}{3}$ which was implemented
in the original code as \texttt{0.3333}. This truncated value of the exponent may have
been reasonable for single precision calculation in the original computing environment
but for the purpose of clarity and correspondence with the technical basis, this value
should be replaced with a numerical constant of the full precision allowed by the
current code. The constant \texttt{THIRD} is defined as \texttt{1.0\_WP / 3.0\_WP}
where \texttt{WP} indicates the `working precision' of the program (parametrically
set to single- or double precision). Double precision operation is recommended; see
the discussion of the effects of precision on code fidelity and performance in
Chapter~\ref{c:c1_comparison}.

Changing the value of this exponent is expected to materially change the results
of the code, but deviations are expected to be small and more appropriately
implement the original physical models. Regression tests have been modified
to reflect the expected change in results.

\subsection{Low Precision of Rankine Conversion Factor}

The original code used \US{460}{\rankine} for \US{0}{\fahrenheit} instead of the more
precise value of \US{459.67}{\rankine}. As noted in Subsection~\ref{ss:vv_third}, code
results are expected to change from this modification, deviation is small and
better reflects the original intent of the code. Regression tests have been updated as
a result.

\subsection{Unreferenced Stefan-Boltzmann Constant}

The original code defined the Stefan-Boltzmann constant
as $\US{1.714E-9}{\BTU\per\hour\per\square\foot\per\rankine^4}$ but provided
no reference for this value. The constant was redefined
as $\SI{5.670374419E-8}{\watt\per\square\meter\per\kelvin^4}$ (an
approximation of the CODATA 2018 value) which converts
to $\US{1.7123E-9}{\BTU\per\hour\per\square\foot\per\rankine^4}$
As noted in Subsection~\ref{ss:vv_third}, code results are expected to change from
this modification, deviation is small and better reflects the original intent of the
code. Regression tests have been updated as a result.

\subsection{Unreferenced Low-Precision Value of Specific Molar Gas Constant for Air}

The original code defined the specific molar gas constant for air \texttt{RIN}
as \US{53.3}{\foot\lbf\per\rankine\per\lbm} but provided
no reference for this value. The constant was redefined
as the universal molar gas constant (approximated from the CODATA 2018 value)
divided by a representative formula weight for air
(\SI{28.9647}{\lbm\per\lbmol} from \cite{USStdAtmos1976}).

There are still issues with this approach. The formula weight in \cite{USStdAtmos1976}
is representative of dry air at sea level. However sample case C1 uses this value to
estimate the mass of a depleted oxygen environment: dry air but with 4.4~wt-\% \ce{O2}
(\textit{vs} 20.9~wt-\% \ce{O2}). This results in overestimating gas mass by about 2\%
if the ideal gas law is used with the formula weight of standard air. Initially,
revising \texttt{RIN} to a better-referenced value should increase accuracy slightly
for scenarios considering combustion with standard dry air. However, the use of a
specific gas constant based on a fixed gas composition degrades accuracy of the
calculation as combustion progresses and the cell gas atmosphere composition deviates
from the initial standard conditions. A better resolution is to track individual gas
species so that cell gas mass reflects its actual composition.

As noted in Subsection~\ref{ss:vv_third}, code results are expected to change from
this modification, deviation is small and better reflects the original intent of the
code. Regression tests have been updated as a result.

\subsection{Unreferenced Material Property Data in Original User Report and Example Input}

A general issue with the SOFIRE~II code and documentation is the paucity of references
for physical and material property data. Examples include
stoichiometric combustion ratio $S$ (p.15 of \cite{Beiriger1973}, p.\pageref{loc:combustion_ratio}
of this document), heat of formation $\Delta H$ for combustion products
(p.15 of \cite{Beiriger1973}, p.\pageref{loc:hform} of this document), and coefficients on the
equation relating the fraction of \ce{Na2O} produced $Y$ with the fraction of \ce{O2}
consumed in producing \ce{Na2O} $k$ (p.32 of \cite{Beiriger1973}, p.\pageref{eq:Y_to_k},
Eq.~\ref{eq:Y_to_k} in the current document).

The value of $S$, $\Delta H$, and the coefficients relating $k$ to $Y$ were all
confirmed using formula weight and heat of formation data from the CEA2 code;
see \cite{NASA-RP1311-v1} and \cite{NASA-RP1311-v2}. Values used in SOFIRE~II
are within a few percent of the CEA2 property data found in the files
\texttt{thermo.inp} and \texttt{trans.inp}.

SOFIRE~II could be easily modified to import material properties and correlations from
the CEA2 data files, providing accurate, referenced values for material properties.
This is especially important if more detailed combustion and mass tracking models are desired.

\subsection{Unreferenced Low-Precision Value of Standard Gravitational Acceleration}

The original code defined the standard gravitational acceleration \texttt{GIN}
as \US{32.2}{\foot\per\square\second} but provided no reference for this (traditional)
value. The constant was replaced with the CODATA 2018 value scaled to US customary units.

As noted in Subsection~\ref{ss:vv_third}, code results are expected to change from
this modification, deviation is small and better reflects the original intent of the
code. Regression tests have been updated as a result.

\subsection{Forced Flow Rate \texttt{F3} Appears to be in Units of Cubic Feet Per Minute}
\label{ss:vv_f3}

Dimensional analysis of the line

\begin{Verbatim}
    W1 = F3 * RHOC * 60.0 * DTMIN
\end{Verbatim}

appears to follow

\begin{equation}
    \us{\lbm} = \mathtt{F3}\us{\cubic\foot} / t_{1} \us{\lbm\per\cubic\foot} 60 t_{1} / t_{2} \us{hour}
\end{equation}

which implies $t_{2}$ has units of \us{\hour} (canceling out \texttt{DTMIN}),
leaving the conversion factor \num{60} to have units \us{\minute\per\hour}.
Dimensionally, therefore, \texttt{F3} must be in units of
\us{\cubic\foot\per\minute} instead of the documented units of
\us{\cubic\foot\per\hour}.

Note that in the two-cell code, \texttt{F3} represents an internal one-sided
exchange flow and the conversion factor \US{60}{\minute\per\hour} appears twice,
to convert to and then from \us{\cubic\foot\per\minute}.

Further review is warranted, ideally of documented cases which make use of
\texttt{F3}. If this is can be confirmed as a documentation error, it should
be corrected.

\subsection{Gas Leakage Rate \texttt{F40} Appears to be in Units of Cubic Feet Per Minute}

Similar to the analysis in Section~\ref{ss:vv_f3}, dimensional analysis of the lines

\begin{Verbatim}
    W2 = F40 * RHOx * 60.0 * DTMIN
\end{Verbatim}

where \texttt{x} is either \texttt{A} or \texttt{C} (indicating ambient or cell
gas density, respectively) exhibits the same issues as in the case of forced flow
rate \texttt{F3}.

This may also indicate the pressure leakage factor \texttt{CON} is in units of
\us{\cubic\foot\per\min} per $\sqrt{\us{in} \text{(water column)}}$ (undefined
in the original documentation).

As in Section~\ref{ss:vv_f3}, further review is warranted. If this is can be
confirmed as a documentation error, it should be corrected.

\subsection{Ambiguous Convective Heat Transfer Relation - Sodium Pool}

\texttt{QCONV1} represents the heat transferred from the sodium pool to the
cell atmosphere by turbulent natural convection. This implements
Equation~\ref{eq:q_conv_floor} and depends on the intermediate quantities \texttt{B1},
\texttt{D1}, \texttt{AK1}, and \texttt{EX1} as well as several input and state variables
-- sodium pool area \texttt{A1}, cell gas density \texttt{RHOC}, gravitational acceleration
\texttt{GIN}, temperature of the sodium pool and cell gas (\texttt{TS} and \texttt{TGASC}
respectively).

The relationship between Eq.~\ref{eq:q_conv_floor} and the composition of \texttt{QCONV1}
is not straightforward; it is not immediately obvious which variables and parameters
correspond to each of the terms in Eq.~\ref{eq:q_conv_floor}. It is believed all terms are
accounted for except for the Prandtl number; a discussion of the intermediate quantities
and implementation follows.

\texttt{B1} may be $\beta$; for an ideal gas, $\beta = \frac{1}{T_{\infty}}$. If that's the case,

\begin{equation}
    T_{\infty} = \frac{T_{sodium} + T_{gas}}{2}
\end{equation}

and one can ask why $T_{\infty}$ is not taken as the bulk gas temperature $T_{gas}$ instead
(horizontal plate properties taken at $T_{film} = \bar{T}$
including $\beta$; $\beta$ taken at $T_{gas}$ for vertical plate?).

The code defines \texttt{D1} as

\begin{Verbatim}
D1 = ((4.94E-05_WP * T1 + 0.0188_WP) / (RHOC * 3600.0_WP))**2
\end{Verbatim}

If \texttt{D1} represents $\frac{\text{Pr}}{\nu^{2}}$ what does the factor
3600 represent? \si{\second\per\hour}? $\rho_{g}$ is \texttt{RHOC} which might
imply the correlation \texttt{4.94E-05\_WP * T1 + 0.0188\_WP} represents dynamic
viscosity of air divided by $\sqrt{\text{Pr}}$ (possible but seems unlikely.)

Regardless, the term \texttt{4.94E-05\_WP * T1 + 0.0188\_WP} in \texttt{D1}
resembles the form of the property correlation for the thermal conductivity
of air (units of \us{\lbm\foot\per\hour}?).

If we isolate what we suspect is the absolute/dynamic viscosity of air as a separate
function \texttt{muair()}, we can rewrite \texttt{D1} as

\begin{Verbatim}
    D1 = (muair(T1) / RHOC)**2
\end{Verbatim}

and a unit test can be created to compare the results of \texttt{muair()}
with tabulated data over a range of temperatures to confirm our assumptions
about the correlation and its units.

\texttt{EX1} appears to be

\begin{equation}
    \left( g \text{Pr} \frac{\beta}{\nu^{2}} \Delta T\right)^{\frac{1}{3}}
\end{equation}

as described in Subsection~\ref{ss:vv_third}. \texttt{EX1} is implemented in code as

\begin{Verbatim}
    EX1 = (GIN * B1 / D1 * abs(TS - TGASC))**THIRD
\end{Verbatim}

where $\text{GIN} = g$, $\text{B1} = \beta$, and $\text{D1} = \nu^{2}$.
\texttt{abs(TS - TGASC)} should always be non-negative in the timeframe of interest;
\texttt{abs()} is likely used to prevent \texttt{TS - TGASC} from spuriously
becoming negative due to numerical `noise' in long-running scenarios where
\texttt{TS} and \texttt{TGASC} equilibrate. It may be reasonable to replace
\texttt{abs(TS - TGASC)} with \texttt{max(TS - TGASC, DTEMPMIN)} or add logic
to ensure this heat transfer correlation is only applied in the case of
a cold gas and a hot plate. Note that in cases with low \texttt{TS - TGASC},
the flow regime will transition from turbulent to laminar and a different
heat transfer correlation should be used, \textit{e.g.}
$\bar{\text{Nu}_{L}} = \num{0.54} {\text{Ra}_{L}}^{\frac{1}{4}}$ for
$\num{1E4} \leq \text{Ra}_{L} \leq \num{1E7}$.
Regardless, more thought is required
before changing \texttt{abs()} to \texttt{max()} or otherwise reformulating
this expression.

Finally, the heat transfer relation \texttt{QCONV1} is defined as

\begin{Verbatim}
    QCONV1 = 0.14_WP * A1 * AK1 * (TS - TGASC) * EX1
\end{Verbatim}

which implements

\begin{align}
    q_{conv,1} & = h_{conv,1} A_{s} \Delta T \\
             & = \left(\num{0.14} k_{g,air} \left( g \text{Pr} \frac{\beta}{\nu^{2}} \Delta T\right)^{\frac{1}{3}}\right) A_{s} \Delta T \\
\end{align}

The choice of heat transfer correlation is reasonable, though the leading coefficient might be updated to \num{0.15}
(see \cite{Incropera2007}; $\bar{\text{Nu}_{L}} = \num{0.15} {\text{Ra}_{L}}^{\frac{1}{3}}$
for $\num{1E7} \leq \text{Ra}_{L} \leq \num{1E11}$). This change would increase heat transfer by about 7\%.

The final unresolved question is why Pr does not seem to appear in the expressions for \texttt{QCONV1}, \texttt{EX1}, or \texttt{D1}.

\subsection{Convective Mass Transfer Analogy}

Reference~\cite{Sienicki2012} criticizes the practice of replacing the Prandtl number
with the Schmidt number in the Sherwood number correlation, making a physicaly
analogy between heat and mass transfer in the case of turbulent natural convection.
The authors note (correctly) that in general this analogy only holds if
$\text{Pr} \approx \text{Sc}$ which holds in the specific case of air.
This is reinforced by \cite{Incropera2007} which notes that for a first approximation,
this technique is reasonable provided that
$\text{Le} = \frac{\text{Pr}}{\text{Sc}} \approx 1$ (as above) and
$\Delta \rho = \rho_{surface} - \rho_{\infty}$ considers both differences
in concentration and temperature.

This suggests the mass transfer model is adequate but that some assurance
should be provided in the code to ensure that the assumptions and conditions
regarding density, Prandtl, and Schmidt number are fulfilled.

\subsection{Inappropriate Convective Heat Transfer Relation - Gas to Cell Liner (Node 1)}

\texttt{QCONV2} is defined similar to \texttt{QCONV1}; the main difference is the
leading coeffiecient on the heat transfer correlation is now \num{0.27} instead
of \num{0.14}:

\begin{Verbatim}
    QCONV2 = 0.27_WP * A2 * AK2 * (TGASC - TWC1) * EX2
\end{Verbatim}

where \texttt{A2} is the cell wall area, \texttt{AK2} is $k_{g,air}$ evaluated at
$\bar{T} = \frac{\mathtt{TGASC} + \mathtt{TWC1}}{2}$, and \texttt{EX2} appears to be

\begin{equation}
    \left( g \text{Pr} \frac{\beta}{\nu^{2}} \Delta T\right)^{\frac{1}{3}}
\end{equation}

as described in Subsection~\ref{ss:vv_third}.

\begin{align}
    q_{conv,2} & = h_{conv,2} A_{wall} \Delta T \\
             & = \left(\num{0.27} k_{g,air} \left( g \text{Pr} \frac{\beta}{\nu^{2}} \Delta T\right)^{\frac{1}{3}}\right) A_{wall} \Delta T \\
\end{align}

This does not seem to match any of the correlations for external free convection
heat transfer to a vertical plate given in \cite{Incropera2007}; the closest has
a leading coeffient of \num{0.1} instead of \num{0.27}. Further study is needed,
especially in the original reference, \cite{McAdams1954}.

\subsection{Unreferenced and Inconsistent Pressure Constant}

At the beginning of the scenario, \texttt{PATM} is set to ambient pressure \texttt{PA}
multiplied by a factor of \num{0.1926}:

\begin{Verbatim}
    PATM = PA * 0.1926_WP
\end{Verbatim}

In each timestep, \texttt{PIN} is set to current gas pressure \texttt{PGASC} multiplied
by approximately the same factor \num{0.19261}:

\begin{Verbatim}
    PIN = PGASC * 0.19261_WP
\end{Verbatim}

These appear to be unit conversions from \us{\lbf\per\square\foot} to inches(water column).

The constants have replaced with \texttt{INWC\_PSF} $\approx \US{0.192222}{\inch (w.c.) \square\foot\per\lbf}$ in
\texttt{m\_constant.f90}

\subsection{Unreferenced Sodium Oxide Release Fraction Coefficient}

The total mass of sodium oxides (composition undefined) released to the cell
gas (\texttt{OXR}) is given by

\begin{Verbatim}
    OXR = SUM2 * 0.2696
\end{Verbatim}

where \texttt{SUM2} is the total mass of sodium burned. No reference or basis
for the constant \num{0.2696} is provided.

\subsection{Unreferenced Diffusion Coefficient Correlation}

Equation~\ref{eq:t2} provides an expression for the gas transport coefficient
$H_{G}$ which depends on the Schmidt number and the diffusion coefficient $D$.
It is believed that the diffusion coefficient is represented by \texttt{DIFF}
as follows:

\begin{Verbatim}
    DIFF = 241.57 / (132.0 + T1 / 1.8) * (T1 / 493.2)**2.5
    HF = 0.075 * DIFF * EX1
\end{Verbatim}

where \texttt{T1} is the film temperature above the sodium pool ($\frac{T_{pool} + T_{gas}}{2}$)
and \texttt{EX1} is as described in Section~\ref{ss:vv_third}.
\texttt{HF} appears to represent $H_{G}$ although the expected
correlation coefficient is \num{0.14} instead of \num{0.075}.

The diffusion coefficient is referenced to the 3rd~edition of
\emph{Perry's Chemical Engineers' Handbook}
(1950) \cite{Perry_3e} but no correlation resembling the expression for \texttt{DIFF}
could be found in the later, 8th~edition (2006) \cite{Perry_8e}. As \cite{Perry_3e} and
\cite{Perry_8e} are general engineering references, a more specific reference such as
\cite{Poling2000} or \cite{TransportPhenomena_2e} may be more appropriate today.
In any case, these works are expected to contain more relevant, specific, or historical
correlations than a general engineering reference.

Reference~\cite{Hilliard1978} provides a much more detailed technical basis for the
calculation of \texttt{DIFF} and \texttt{HF} than does \cite{Beiriger1973}, noting that:

\begin{quote}
[SOFIRE~II] calculates the sodium burning rate by a method similar to that discussed
in the previous section, except that the oxygen diffusivity is calculated by the
Sutherland Equation instead of using the more exact Chapman-Enskog Kinetic Theory.
\end{quote}

and specifying page 539 of \cite{Perry_3e} as the source of the Sutherland Equation.

Newman \cite{Newman1983} has similar criticisms of SOFIRE~II's mass transfer model:

\begin{quote}
The SOFIRE~II code can be made to produce pessimistic predictions for
sodium releases, but requires improvement in a more rigorous choice of
product ratios, more accurate mass transfer and heat transfer analogies
and a greater knowledge of the emissivity of sodium smokes which can
also be used as a variable.
\end{quote}

Further research is necessary to determine the source and applicability of the
relation provided for \texttt{DIFF} however most prior reviews of SOFIRE~II
echo the conclusion that the Chapman-Enskog treatment is superior to that currently
used in the code. Future versions of SOFIRE should replace the Sutherland Equation
in the mass transport calculations with a more applicable and accurate method such
as Chapman-Enskog.

It is not clear there is much value in determining the specific source of the
equations used to calculate \texttt{DIFF}. It may be more useful to estimate the
range of variation of \texttt{DIFF} with \texttt{T1} and compare with the expected
range of $D$ when calculated using Chapman-Enskog theory over the same temperature
range. This would provide a coarse estimate of the effect of changing the underlying
physical model.

Before migrating to a more detailed and accurate method of calculating diffusion
coefficient, more detailed tracking of liquid, gas, and aerosol species is necessary.
This would also support more detailed chemistry and combustion models and would
simplify mass accounting.

\subsection{Rectilinear Geometry Assumed for All Heat Sinks }

All heat sinks (sodium surface and pool nodes, wall and floor nodes) are
treated as flat (rectilinear) slabs. This may not be appropriate for
cylindrical shells such as the cell walls.

\subsection{Heat Flow Validation}
\label{ss:vv_energy_balance}

The one-cell model heat flows shown in Figure~\ref{fig:c1_heat_flow}
are described more fully in Table~\ref{tbl:c1_heat_flow}. In the cases
where heat flows are explicitly tracked in the code, the name given
in the table corresponds to that variable in the code (typically the
radiation and convection terms).

\begin{table}[htb]
    \caption{One-cell model heat flow descriptions\label{tbl:c1_heat_flow}}
    \begin{center}
        \begin{tabularx}{\textwidth}{llXc}
        \toprule
Name & Mode        & Description                & Explicitly tracked? \\
\midrule
QCONV1  & convection & sodium pool `crust' to cell gas & Yes \\
QCONV2  & convection & cell gas to wall liner (node 1) & Yes \\
\midrule
QROD    & radiation  & sodium pool `crust' to wall liner (node 1) & Yes \\
QRAD1   & radiation  & wall liner (node 1) across gap to wall node 2 & Yes \\
QRAD2   & radiation  & floor liner (node 1) across gap to floor node 2 & Yes \\
QRAD3   & radiation  & sodium pool `crust' to cell gas & Yes \\
\midrule
QCOND1  & conduction & sodium pool to floor liner (node 1) & No \\
QCOND2  & conduction & floor liner (node 1) across air gap to floor node 2 & No \\
QCOND3  & conduction & floor node 2 to node 3  & No \\
QCOND4  & conduction & floor node 3 to node 4  & No \\
QCOND5  & conduction & floor node 4 to ambient & No \\
\midrule
QCOND6  & conduction & wall node 1 to node 2  & No \\
QCOND7  & conduction & wall node 2 to node 3  & No \\
QCOND8  & conduction & wall node 3 to node 4  & No \\
QCOND9  & conduction & wall node 4 to ambient & No \\
\midrule
QCOND10 & conduction & sodium pool `crust' to sodium pool node 1 & No \\
QCOND11 & conduction & sodium pool node 1 to node 2 & No \\
QCOND12 & conduction & sodium pool node 2 to node 3 & No \\
QCOND13 & conduction & sodium pool node 3 to node 4 & No \\
\midrule
QFIRE & external & combustion heat to sodium pool `crust' & No \\
QLEAK & external & heat to/from gas space from pressure-driven in/outleakage, see \texttt{CON} & No \\
QEXHAUST & external & heat to/from gas space from forced gas supply/exhaust, see \texttt{F3} & No \\
QMASS\_S1 & external & heat to/from sodium pool node 1 via sodium source/drain 1 & No \\
QMASS\_S2 & external & heat to/from sodium pool node 2 via sodium source/drain 2 & No \\
QMASS\_S3 & external & heat to/from sodium pool node 3 via sodium source/drain 3 & No \\
QMASS\_S4 & external & heat to/from sodium pool node 4 via sodium source/drain 4 & No \\
\bottomrule
\end{tabularx}
\end{center}
\end{table}

The temperature variables illustrated in Figure~\ref{fig:c1_geometry} were
reviewed and the heat flows associated with each are listed in Table~\ref{tbl:c1_qtemp}.
Note that TA is a fixed (isothermal) boundary condition and is not affected by heat flows.

Since the neither the cell gas space chemical composition, constituent enthalpy,
nor specific heat is tracked in detail nor, the energy in the gas space is not
necessarily conserved when pressure-driven gas leakage or forced supply/exhaust
is enabled. Combined gas/aerosol temperature is scaled by the amount of in- or
outflow that occurs but the specific change in energy
\texttt{(QLEAK + QEXHAUST) * DTMIN} is not known. Similarly, the in/outflow of sodium
from pool nodes 1 through 4 does not adequately track energy or thermophysical
property data and the specific change in energy of each sodium pool node
(\texttt{QMASS\_S1} through \texttt{QMASS\_S4}) is not known.

Until a more rigorous treatment of liquid, gas, and aerosol energy and
thermophysical properties is implemented, the use of the input parameters
\texttt{CON}, \texttt{F3}, \texttt{S1}, \texttt{S2}, \texttt{S3}, and \texttt{S4}
is not recommended.

\begin{table}[htb]
    \caption{Correspondence between temperatures and heat flows in the one-cell model\label{tbl:c1_qtemp}}
    \begin{center}
        \begin{tabularx}{\textwidth}{lXXl}
        \toprule
Temperature & Internal heat inflows & Internal heat outflows & External sources/sinks \\
\midrule
% TA & Fixed (isothermal boundary condition) & & \\
TSS & & QCONV1, QROD, QRAD3, QCOND10 & Combustion \\
TS1 & QCOND10 & QCOND11 & Sodium mass flow S1 \\
TS2 & QCOND11 & QCOND12 & Sodium mass flow S2 \\
TS3 & QCOND12 & QCOND13 & Sodium mass flow S3 \\
TS4 & QCOND13 & QCOND1  & Sodium mass flow S4 \\
TWC1 & QCONV1, QROD & QRAD2, QCOND6 &  \\
TWC2 & QRAD2, QCOND6 & QCOND7 &  \\
TWC3 & QCOND7 & QCOND8 &  \\
TWC4 & QCOND8 & QCOND9 &  \\
TF1 & QCOND1 & QRAD1, QCOND2 &  \\
TF2 & QRAD1, QCOND2 & QCOND3 &  \\
TF3 & QCOND3 & QCOND4 &  \\
TF4 & QCOND4 & QCOND5 &  \\
TGASC & QCONV1, QRAD3 & QCONV2 & QLEAK, QEXHAUST \\
\bottomrule
\end{tabularx}
\end{center}
\end{table}

\clearpage

\subsection{Mass Flow Validation}
\label{ss:vv_mass_balance}

SOFIRE~II does not explicitly track the mass of sodium, oxygen, or combustion
reaction products in any of the nodes. Only the inital mass of sodium and the
mass of sodium and oxygen consumed by combustion are tracked. Cell gas
composition and mass are not explicitly tracked nor is sodium pool mass.
Some attempt is made to scale sodium pool temperatures and gas temperature,
pressure, and oxygen concentration to compensate for external mass sources
and sinks (forced ventilation, leakage, sodium supply/drain) but these are
approximate and effectively speculative. This in turn affects the accuracy
of the heat and temperature estimates provided by SOFIRE~II; see
Section~\ref{ss:vv_energy_balance}.

Combustion is limited by the initial mass of sodium  (\texttt{SOD}), the
cumulative mass of sodium burned (\texttt{SUM2}), and the oxygen concentration \texttt{C}
(fraction of oxygen in the cell atmosphere). The mass and volume of sodium
in the crust and pool nodes is not affected by combustion. The sodium burning
rate is set to zero when $(\texttt{SOD} - \texttt{SUM2}) < \US{0.1}{\lbm}$ or
the oxygen concentration in the gas space (\texttt{C}) drops to zero.

Consider the implementation of forced ventilation (volumetric flowrate \texttt{F3},
integrated exhaust gas mass \texttt{W1}) and pressure-driven leakage
(pressure-dependent volumetric leak rate \texttt{CON}, instantaneous volumetric flow rate
\texttt{F40}, and integrated outleakage gas mass \texttt{W2})
and their effect on oxygen concentration \texttt{C} (mass fraction).

To illustrate this case, the per-timestep calculation of \texttt{C} is simplified to:
\begin{Verbatim}
    PIN = prev%PGASC * INWC_PSF

    curr%W1 = F3 * prev%RHOC * curr%DTMIN * SEC_MIN

    if (PATM < PIN) then
        ! For outleakage (PATM < PIN)
        curr%F40 = CON * sqrt(PIN - PATM)
        curr%W2 = curr%F40 * prev%RHOC * curr%DTMIN * SEC_MIN
        curr%C = (prev%W * prev%C
                  - curr%W2 * prev%C
                  - curr%W1 * prev%C)
                  / (prev%W - curr%W2 - curr%W1)
        prev%TGASC = (prev%W * prev%TGASC
                      - curr%W2 * prev%TGASC
                      - curr%W1 * prev%TGASC)
                      / (prev%W - curr%W1 - curr%W2)
        curr%W = prev%W - curr%W2 - curr%W1
    else
        ! For inleakage (PATM > PIN)
        curr%F40 = CON * sqrt(PATM - PIN)
        curr%W2 = curr%F40 * RHOA * curr%DTMIN * SEC_MIN
        curr%C = (prev%W * prev%C
                  + curr%W2 * CO
                  - curr%W1 * prev%C)
                  / (prev%W + curr%W2 - curr%W1)
        prev%TGASC = (curr%W2 * TA
                      - curr%W1 * prev%TGASC
                      + prev%W * prev%TGASC)
                      / (curr%W2 - curr%W1 + prev%W)
        curr%W = prev%W + curr%W2 - curr%W1
    end if

    curr%RHOC = curr%W / VOLAC

    prev%PGASC = curr%RHOC * RIN * prev%TGASC

    curr%OX = VOLAC * curr%C * curr%RHOC

    ! HF represents the volumetric flow rate of air into
    ! the sodium crust (mass transport)
    curr%XM = ANA * HF * curr%RHOC * curr%C
    OXLB = curr%XM * A1 * curr%DTMIN / ANA
    curr%SUM1 = OXLB + prev%SUM1
    curr%SUM2 = curr%SUM1 * ANA

    curr%RHOC = (curr%W - OXLB) / VOLAC
    curr%C = (curr%OX - OXLB) / (curr%RHOC * VOLAC)
    curr%W = curr%W - OXLB

    curr%OXR = curr%SUM2 * 0.2696

    curr%PGASC = curr%RHOC * RIN * curr%TGASC
\end{Verbatim}

\begin{itemize}
\item Cell gas temperature and pressure are \texttt{TGASC} and
\texttt{PGASC} respectively; their initial values at time $t = 0$ are
\texttt{TGASCI} and \texttt{PGASCI}
\item \texttt{TA} and \texttt{RHOA} are the temperature and density of ambient air, respectively
\item \texttt{prev} represents state variables calculated during the previous timestep;
\texttt{curr} represents state variables calculated for the current timestep.
\texttt{prev} and \texttt{curr} are data structures; for example, \texttt{prev\%TGASC}
is the cell gas temperature as calculated for the immediately previous timestep
(time = $t$) and \texttt{curr\%TGASC} is the cell gas temperature as calculated
for the current timestep (time = $t + \Delta t$)
\item \texttt{C} is initially set to \texttt{CO}, initial mass
fraction of oxygen in cell atmosphere
\item \texttt{W} is initially set to total gas mass using the ideal gas law,
\texttt{PGASCI * VOLAC / (RIN * TGASCI)}
\item The cell gas density \texttt{RHOC} is initially set to
\texttt{W / VOLAC = PGASCI / (RIN * TGASCI)}
\item \texttt{DTMIN} is the timestep in hours
\item \texttt{PIN} is the cell gas pressure \texttt{PGASC}
scaled to inches (water column)
\item \texttt{PATM} is the ambient gas pressure \texttt{PA}
scaled to inches (water column)
\item \texttt{curr\%OX} is the mass of oxygen in the cell gas at beginning
of the current timestep
\item \texttt{OXLB} is the mass of oxygen consumed by the sodium combustion
reaction over the current timestep
\item \texttt{SUM1} and \texttt{SUM2} are the integrated masses of oxygen and sodium
(respectively) consumed over the duration of the scenario
\end{itemize}

In lieu of line-by-line analysis, there are several important points to note about the implementation:

\begin{itemize}
    \item Cell oxygen mass \texttt{OX} is inferred from oxygen concentration \texttt{C} and gas density
    \texttt{RHOC} (cell volume \texttt{VOLAC} is fixed) however it is not tracked from timestep to timestep
    \item Ambient air is assumed to have the same oxygen mass fraction as the initial cell gas (\texttt{CO})
    \item Oxygen mass added or removed from the cell via leakage or forced ventilation is not explicitly
    calculated or tracked. Instead, the oxygen concentration and temperature of the cell gas is scaled
    based on integrated flowrates
    \item Previous timestep conditions should be immutable, however previous timestep cell gas
    temperature is modified to account for gas flow in the current timestep.
    Ideally a separate variable initialized with the beginning-of-timestep cell gas
    temperature would be used instead of changing the value of \texttt{prev\%TGASC}.
    This may seem a minor philosophical variable naming issue but modifying \texttt{prev\%TGASC} violates
    the analyst's expectation that this variable accurately represents conditions during the
    previous timestep and would likely cause difficult-to-detect errors should the code be extended later.
    \item For outflow, modification of the current timestep oxygen concentration and previous timestep
    gas temperature appears to be ineffective, the scaling factor based on integrated mass cancelling out:
    \texttt{curr\%C = (prev\%W * prev\%C - curr\%W2 * prev\%C - curr\%W1 * prev\%C) / (prev\%W - curr\%W2 - curr\%W1) = prev\%C * (prev\%W - curr\%W2 - curr\%W1) / (prev\%W - curr\%W2 - curr\%W1) = prev\%C}. The result is reasonable - no change in temperature is expected for outflow in this case.
    \item If the system is isolated (\texttt{CON} and \texttt{F3} are set to zero), none of this scaling is attempted.
    \item There is no check to ensure that \texttt{OXLB} does not exceed \texttt{OX},
    \textit{i.e.} that the mass of oxygen consumed by combustion in the current timestep
    is limited by the mass of oxygen in the cell.
\end{itemize}

These issues could all be resolved by strict accounting of gas, liquid,
and aerosol species in all nodes (cell gas volume, sodium pool and crust nodes).
Further, strict accounting would allow more detailed calculations of molecular
diffusion using (for example) Chapman-Enskog theory, more flexible combustion models,
\textit{etc.} Considering the code as it currently exists however,
cell leakage and ventilation features should not be used;
\texttt{CON} and \texttt{F3} should be set to zero.

A suggested mass flow topology for the one-cell model is shown in
Figures~\ref{fig:c1_na_mass_flow} and \ref{fig:c1_o_mass_flow}
for sodium and oxygen respectively. The sources and nodes are representative
of the existing model however the internodal mass transfers are either
approximate or nonexistant, as in the case of sodium flow between pool layers.
Of these, the most care was taken with the sodium pool burning rate but it is
still treated in a very simplistic manner as noted in \cite{Hilliard1978},
\cite{Newman1983}, \cite{Sienicki2012}, \textit{et al}.

As noted in Section~\ref{ss:vv_energy_balance}, the use of the input parameters
\texttt{CON}, \texttt{F3}, \texttt{S1}, \texttt{S2}, \texttt{S3}, and \texttt{S4}
is not recommended.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c1_na_mass_flow}
    \end{center}
    \caption{Suggested sodium flows in one-cell model}
    \label{fig:c1_na_mass_flow}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c1_o_mass_flow}
    \end{center}
    \caption{Suggested oxygen mass flows in one-cell model}
    \label{fig:c1_o_mass_flow}
\end{figure}

\clearpage

\section{SOFIRE~II -- Two-Cell}

Valdidation of the two-cell version of SOFIRE~II has been deferred until the
one-cell version has been stabilized.
