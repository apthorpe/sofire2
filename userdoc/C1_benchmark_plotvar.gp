plotfile14 = 'C1B4_results.csv'
plotfile15 = 'C1B5_results.csv'
plotfile16 = 'C1B6_results.csv'
plotfile2 = 'test_4_pressure_benchmark.csv'
plotfile2a = 'test_4_pressure_benchmark_sofire.csv'
plotfile3 = 'test_4_burn_rate_benchmark.csv'
plotfile3a = 'test_4_burn_rate_benchmark_sofire.csv'
plotfile4 = 'test_5_pressure_benchmark.csv'
plotfile4a = 'test_5_pressure_benchmark_sofire.csv'
plotfile5 = 'test_5_burn_rate_benchmark.csv'
plotfile5a = 'test_5_burn_rate_benchmark_monoxide.csv'
plotfile5b = 'test_5_burn_rate_benchmark_peroxide.csv'
plotfile6 = 'test_6_pressure_benchmark.csv'
plotfile6a = 'test_6_pressure_benchmark_sofire.csv'
plotfile7 = 'test_6_pool_temp_benchmark.csv'
plotfile7a = 'test_6_pool_temp_benchmark_noburn.csv'
plotfile7b = 'test_6_pool_temp_benchmark_burn.csv'

c1_T =      1
c1_XM =     2
c1_TS =     3
c1_TGASC =  4
c1_TF1 =    5
c1_TF2 =    6
c1_TF3 =    7
c1_TF4 =    8
c1_TWC1 =   9
c1_TWC2 =   10
c1_TWC3 =   11
c1_TWC4 =   12
c1_QCONV1 = 13
c1_QCONV2 = 14
c1_PGASC =  15
c1_QROD =   16
c1_QRAD3 =  17
c1_TS1 =    18
c1_SUM2 =   19
c1_OX =     20
c1_RHOC =   21
c1_SUM1 =   22
c1_W =      23
c1_F40 =    24
c1_C =      25
c1_W1 =     26
c1_W2 =     27
c1_QRAD1 =  28
c1_QRAD2 =  29
c1_OXR =    30
c1_DTMIN1 = 31
c1_DTMIN2 = 32
c1_DT =     33
c1_DTMIN =  34
c1_TS2 =    35
c1_TS3 =    36
c1_TS4 =    37

c2_TIME =    1
c2_DP =      2

c3_TIME =    1
c3_XM =      2

c4_TIME =    1
c4_DP =      2

c5_TIME =    1
c5_XM =      2

c6_TIME =    1
c6_DP =      2

c6a_TIME =      1
c6a_DP_BURN =   2
c6a_DP_NOBURN = 3

c7_TIME =       1
c7_TS =         2
