!> @file c1_textio.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_textio module.
program c1_textio
    use iso_fortran_env, only: REAL32, REAL64, REAL128,                 &
        INT8, INT16, INT32, INT64, stdout => OUTPUT_UNIT
    use sofire2_param, only: WP
    use m_textio
    use toast
    implicit none

    character (len=80) :: t1
    character (len=80) :: t2
    character (len=:), allocatable :: c1
    character (len=:), allocatable :: c2

    integer(kind=INT8)  :: ii8
    integer(kind=INT16) :: ii16
    integer(kind=INT32) :: ii32
    integer(kind=INT64) :: ii64

    real (kind=REAL32) :: rr32
    real (kind=REAL64) :: rr64
    real (kind=REAL128) :: rr128

    logical :: tf

    type(TestCase) :: test

    continue

    call test%init(name="c1_textio")

    ! Functions to test:
    ! ucase()

    c1 = "HAM"
    c2 = "ham"
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase(ham)")

    c2 = "HaM"
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase(HaM)")

    c2 = "HAM"
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase(HAM)")

    c1 = "0"
    c2 = "0"
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase(0)")

    c1 = " Z0_B4   "
    c2 = " z0_B4   "
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase(z0_B4)")

    c2 = " Z0_b4   "
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase( Z0_b4   )")

    c2 = " z0_b4   "
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase( z0_b4   )")

    c2 = " Z0_B4   "
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase( Z0_B4   )")

    c1 = ""
    c2 = ""
    call ucase(c2)
    call test%asserttrue(c1 == c2, message="ucase()")

    ! lcase()

    c1 = "ham"
    c2 = "ham"
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase(ham)")

    c2 = "HaM"
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase(HaM)")

    c2 = "HAM"
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase(HAM)")

    c1 = "0"
    c2 = "0"
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase(0)")

    c1 = " z0_b4   "
    c2 = " z0_B4   "
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase(z0_B4)")

    c2 = " Z0_b4   "
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase( Z0_b4   )")

    c2 = " z0_b4   "
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase( z0_b4   )")

    c2 = " Z0_B4   "
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase( Z0_B4   )")

    c1 = ""
    c2 = ""
    call lcase(c2)
    call test%asserttrue(c1 == c2, message="lcase()")

    ! munch()

    t1 = "  a moderate amount of text on a single line with plenty of     wh    it esp ace      "
    c1 = "a moderate amount of text on a single line with plenty of     wh    it esp ace"
    call test%asserttrue(munch(t1) == c1, message="munch(fixed_string)")

    c2 = t1
    call test%asserttrue(munch(c2) == c1, message="munch(allocatable_string)")

    c1 = ""
    t1 = "                                                                                "
    call test%asserttrue(munch(t1) == c1, message="munch(fixed_string, empty)")

    c2 = t1
    call test%asserttrue(munch(c2) == c1, message="munch(allocatable_string, empty)")

    ! i8toa()
    ! itoa()
1   format("[", A, "]")

    c1 = "127"
    ii8 = huge(ii8)
    ! write(unit=stdout, fmt=1) itoa(ii8)
    ! write(unit=stdout, fmt=1) i8toa(ii8)
    call test%asserttrue(itoa(ii8) == c1, message="itoa(huge(INT8))")
    call test%asserttrue(i8toa(ii8) == c1, message="i8toa(huge(INT8))")

    c1 = "0"
    ii8 = 0_INT8
    call test%asserttrue(itoa(ii8) == c1, message="itoa(0)")
    call test%asserttrue(i8toa(ii8) == c1, message="i8toa(0)")

    c1 = "-126"
    ii8 = -126_INT8
    call test%asserttrue(itoa(ii8) == c1, message="itoa(-126)")
    call test%asserttrue(i8toa(ii8) == c1, message="i8toa(-126)")

    c1 = "-127"
    ii8 = -huge(ii8)
    call test%asserttrue(itoa(ii8) == c1, message="itoa(-huge(INT8))")
    call test%asserttrue(i8toa(ii8) == c1, message="i8toa(-huge(INT8))")

    ! i16toa()
    ! itoa()

    c1 = "32767"
    ii16 = huge(ii16)
    ! write(unit=stdout, fmt=1) itoa(ii16)
    ! write(unit=stdout, fmt=1) i16toa(ii16)
    call test%asserttrue(itoa(ii16) == c1, message="itoa(huge(INT16))")
    call test%asserttrue(i16toa(ii16) == c1, message="i16toa(huge(INT16))")

    c1 = "-32767"
    ii16 = -huge(ii16)
    ! write(unit=stdout, fmt=1) itoa(ii16)
    ! write(unit=stdout, fmt=1) i16toa(ii16)
    call test%asserttrue(itoa(ii16) == c1, message="itoa(-huge(INT16))")
    call test%asserttrue(i16toa(ii16) == c1, message="i16toa(-huge(INT16))")

    ! i32toa()
    ! itoa()

    c1 = "2147483647"
    ii32 = huge(ii32)
    ! write(unit=stdout, fmt=1) itoa(ii32)
    ! write(unit=stdout, fmt=1) i32toa(ii32)
    call test%asserttrue(itoa(ii32) == c1, message="itoa(huge(INT32))")
    call test%asserttrue(i32toa(ii32) == c1, message="i32toa(huge(INT32))")

    c1 = "-2147483647"
    ii32 = -huge(ii32)
    ! write(unit=stdout, fmt=1) itoa(ii32)
    ! write(unit=stdout, fmt=1) i32toa(ii32)
    call test%asserttrue(itoa(ii32) == c1, message="itoa(-huge(INT32))")
    call test%asserttrue(i32toa(ii32) == c1, message="i32toa(-huge(INT32))")

    ! i64toa()
    ! itoa()

    c1 = "9223372036854775807"
    ii64 = huge(ii64)
    ! write(unit=stdout, fmt=1) itoa(ii64)
    ! write(unit=stdout, fmt=1) i64toa(ii64)
    call test%asserttrue(itoa(ii64) == c1, message="itoa(huge(INT64))")
    call test%asserttrue(i64toa(ii64) == c1, message="i64toa(huge(INT64))")

    c1 = "-9223372036854775807"
    ii64 = -huge(ii64)
    ! write(unit=stdout, fmt=1) itoa(ii64)
    ! write(unit=stdout, fmt=1) i64toa(ii64)
    call test%asserttrue(itoa(ii64) == c1, message="itoa(-huge(INT64))")
    call test%asserttrue(i64toa(ii64) == c1, message="i64toa(-huge(INT64))")

    ! ftoa()
    ! r32toa()
    ! r64toa()
    ! r128toa()

    ! ftoa()
    ! r32toa()
    c1 = '3.4028E+38'
    rr32 = huge(rr32)
    ! write(unit=stdout, fmt=*) rr32
    ! write(unit=stdout, fmt=1) ftoa(rr32)
    ! write(unit=stdout, fmt=1) r32toa(rr32)
    call test%asserttrue(ftoa(rr32) == c1, message="ftoa(huge(REAL32))")
    call test%asserttrue(r32toa(rr32) == c1, message="r32toa(huge(REAL32))")

    c1 = '3.40282347E+38'
    ! write(unit=stdout, fmt=*) rr32
    ! write(unit=stdout, fmt=1) c1
    ! write(unit=stdout, fmt=1) ftoa(rr32, 'ES', 15, 8, 2)
    ! write(unit=stdout, fmt=1) r32toa(rr32, 'ES', 15, 8, 2)
    call test%asserttrue(ftoa(rr32, 'ES', 15, 8, 2) == c1, message="ftoa(huge(REAL32), full)")
    call test%asserttrue(r32toa(rr32, 'ES', 15, 8, 2) == c1, message="r32toa(huge(REAL32), full)")

    c1 = '-3.4028E+38'
    rr32 = -huge(rr32)
    ! write(unit=stdout, fmt=*) rr32
    ! write(unit=stdout, fmt=1) ftoa(rr32)
    ! write(unit=stdout, fmt=1) r32toa(rr32)
    call test%asserttrue(ftoa(rr32) == c1, message="ftoa(-huge(REAL32))")
    call test%asserttrue(r32toa(rr32) == c1, message="r32toa(-huge(REAL32))")

    c1 = '-3.40282347E+38'
    call test%asserttrue(ftoa(rr32, 'ES', 15, 8, 2) == c1, message="ftoa(-huge(REAL32), full)")
    call test%asserttrue(r32toa(rr32, 'ES', 15, 8, 2) == c1, message="r32toa(-huge(REAL32), full)")

    ! ftoa()
    ! r64toa()
    c1 = '1.7977E+308'
    rr64 = huge(rr64)
    ! write(unit=stdout, fmt=*) rr64
    ! write(unit=stdout, fmt=1) ftoa(rr64)
    ! write(unit=stdout, fmt=1) r64toa(rr64)
    ! write(unit=stdout, fmt=1) ftoa(rr64, 'ES', 26, 16, 3)
    ! write(unit=stdout, fmt=1) c1
    ! write(unit=stdout, fmt=1) r64toa(rr64)
    call test%asserttrue(ftoa(rr64) == c1, message="ftoa(huge(REAL64))")
    call test%asserttrue(r64toa(rr64) == c1, message="r64toa(huge(REAL64))")

    c1 = '1.7976931348623157E+308'
    ! write(unit=stdout, fmt=*) ftoa(rr64)
    ! write(unit=stdout, fmt=1) ftoa(rr64)
    ! write(unit=stdout, fmt=1) ftoa(rr64, 'ES', 26, 16, 3)
    ! write(unit=stdout, fmt=1) c1
    ! write(unit=stdout, fmt=1) r64toa(rr64, 'ES', 26, 16)
    call test%asserttrue(ftoa(rr64, 'ES', 26, 16, 3) == c1, message="ftoa(huge(REAL64), full)")
    call test%asserttrue(r64toa(rr64, 'ES', 26, 16, 3) == c1, message="r64toa(huge(REAL64), full)")

    c1 = '-1.7977E+308'
    rr64 = -huge(rr64)
    ! write(unit=stdout, fmt=*) rr64
    ! write(unit=stdout, fmt=1) ftoa(rr64)
    ! write(unit=stdout, fmt=1) r64toa(rr64)
    ! write(unit=stdout, fmt=1) ftoa(rr64, 'ES', 26, 16, 3)
    ! write(unit=stdout, fmt=1) c1
    call test%asserttrue(ftoa(rr64) == c1, message="ftoa(-huge(REAL64))")
    call test%asserttrue(r64toa(rr64) == c1, message="r64toa(-huge(REAL64))")

    c1 = '-1.7976931348623157E+308'
    ! write(unit=stdout, fmt=1) ftoa(rr64, 'ES', 26, 16)
    ! write(unit=stdout, fmt=1) r64toa(rr64, 'ES', 26, 16)
    call test%asserttrue(ftoa(rr64, 'ES', 26, 16, 3) == c1, message="ftoa(-huge(REAL64), full)")
    call test%asserttrue(r64toa(rr64, 'ES', 26, 16, 3) == c1, message="r64toa(-huge(REAL64), full)")

    ! ftoa()
    ! r128toa()
    c1 = '1.18973149535723176508575932662800702E+4932'
    rr128 = huge(rr128)
    ! write(unit=stdout, fmt="(ES47.35E4)") rr128
    ! write(unit=stdout, fmt=*) rr128
    ! write(unit=stdout, fmt=1) ftoa(rr128, 'ES', 47, 35, 4)
    ! write(unit=stdout, fmt=1) r128toa(rr128, 'ES', 47, 35, 4)
    call test%asserttrue(ftoa(rr128, 'ES', 47, 35, 4) == c1, message="ftoa(huge(REAL128), full)")
    call test%asserttrue(r128toa(rr128, 'ES', 47, 35, 4) == c1, message="r128toa(huge(REAL128), full)")

    c1 = '-1.18973149535723176508575932662800702E+4932'
    rr128 = -huge(rr128)
    ! write(unit=stdout, fmt="(ES47.35E4)") rr128
    ! write(unit=stdout, fmt=*) rr128
    ! write(unit=stdout, fmt=1) ftoa(rr128, 'ES', 47, 35, 4)
    ! write(unit=stdout, fmt=1) r128toa(rr128, 'ES', 47, 35, 4)
    call test%asserttrue(ftoa(rr128, 'ES', 47, 35, 4) == c1, message="ftoa(-huge(REAL128), full)")
    call test%asserttrue(r128toa(rr128, 'ES', 47, 35, 4) == c1, message="r128toa(-huge(REAL128), full)")

    ! ltoa()
    c1 = ".true."

    tf = .true.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.true.)")

    tf = .TruE.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.TruE.)")

    tf = .TRUE.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.TRUE.)")

    tf = .tRuE.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.tRuE.)")

    c1 = ".false."

    tf = .false.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.false.)")

    tf = .FaLsE.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.FaLsE.)")

    tf = .FALSE.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.FALSE.)")

    tf = .faLSE.
    call test%asserttrue(ltoa(tf) == c1, message="ltoa(.faLSE.)")

    ! call test%assertequal(this, that,         &
    !     rel_tol=0.01_WP, message="k(O2(g)) T-upper = 15000 K +-1%")

    ! ! Print summary and reset
    ! call printsummary(test)
    ! call test%reset()

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_textio.json")

    call test%checkfailure()
end program c1_textio
