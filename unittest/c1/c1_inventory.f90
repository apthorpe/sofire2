!> @file c1_inventory.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_inventory module.
program c1_inventory
    use iso_fortran_env, only: stderr => error_unit
    use sofire2_param, only: WP
    use m_constant, only: ZERO, THIRD, HALF, ONE, TWO, THREE, FIVE,     &
        RGAS, R_K, LBM_KG
     !JOULE_BTU, LBM_G, T0K, RGAS
    use m_inventory, only: id_mass, id_fraction, material_dist, parcel, &
        inventory, new_id_mass, get_nf_dry_air, as_nf, as_mf,           &
        new_gas_mf, new_aerosol_mf, new_gas_parcel, new_aerosol_parcel, &
        new_gas_inventory, new_aerosol_inventory
    use m_material, only: phase_NULL, phase_GAS, phase_CONDENSED,       &
        aid_NULL, gid_NULL, mid_NULL, n_gas, gas_id, gid_Ar, gid_CH4,   &
        gid_CO2, gid_H2, gid_He, gid_Kr, gid_N2, gid_Ne, gid_O2,        &
        gid_Xe, n_aer, aer_id, aid_Na, aid_Na2O, aid_Na2O2,             &
        is_gas_id, is_aerosol_id, material_db

    use toast

    implicit none

    real(kind=WP) :: T
    real(kind=WP) :: refval
    real(kind=WP) :: tmp_n
    real(kind=WP) :: tmp_cp
    real(kind=WP) :: tmp_u
    real(kind=WP) :: mw
    real(kind=WP) :: fmw1
    real(kind=WP) :: fmw2
    real(kind=WP) :: fmw3
    integer :: i
    integer :: id

    type(TestCase) :: test

    type(id_mass) :: im_test

    type(id_fraction) :: if_test

    type(material_dist) :: md1
    type(material_dist) :: md2
    type(material_dist) :: nd1
    type(material_dist) :: nd2

    type(parcel) :: p1
    type(parcel) :: p2

    type(inventory) :: inv1
    type(inventory) :: inv2

    continue

    call test%init(name="c1_inventory")

    call material_db%populate()

    !!! Test exposed functions

    ! new_id_mass -- DONE
    ! new_gas_parcel -- DONE
    ! new_aerosol_parcel -- DONE
    ! new_gas_mf -- DONE
    ! new_aerosol_mf -- DONE
    ! new_gas_inventory -- DONE
    ! new_aerosol_inventory -- DONE
    ! as_nf -- DONE
    ! as_mf -- DONE
    ! get_nf_dry_air -- DONE

    !!! Test classes

    ! ##################################################################
    ! id_mass
    ! ##################################################################
    ! - id_mass%phase_id == phase_NULL
    ! - id_mass%id == mid_NULL
    ! - id_mass%mass == ZERO
    ! - id_mass%init()
    ! - id_mass%is_gas()
    ! - id_mass%is_aerosol()
    ! - id_mass%moles()

    call test%assertequal(im_test%phase_id, phase_NULL,                 &
        message="Uninitialized id_mass%phase_id == phase_NULL")
    call test%assertequal(im_test%id, mid_NULL,                         &
        message="Uninitialized id_mass%id == mid_NULL")
    call test%assertequal(im_test%mass, ZERO,                           &
        message="Uninitialized id_mass%mass == ZERO")
    call test%assertequal(im_test%moles(), ZERO,                        &
        message="Uninitialized id_mass%moles() == ZERO")

    call test%assertfalse(im_test%is_gas(),                             &
        message="Uninitialized id_mass%is_gas() is false")
    call test%assertfalse(im_test%is_aerosol(),                         &
        message="Uninitialized id_mass%is_aerosol() is false")

    im_test%phase_id = -12
    im_test%id = -32
    im_test%mass = -550.0_WP

    call im_test%init()

    call test%assertequal(im_test%phase_id, phase_NULL,                 &
        message="Initialized id_mass%phase_id == phase_NULL")
    call test%assertequal(im_test%id, mid_NULL,                         &
        message="Initialized id_mass%id == mid_NULL")
    call test%assertequal(im_test%mass, ZERO,                           &
        message="Initialized id_mass%mass == ZERO")
    call test%assertequal(im_test%moles(), ZERO,                        &
        message="Initialized id_mass%moles() == ZERO")

    call test%assertfalse(im_test%is_gas(),                             &
        message="Initialized (Null phase) id_mass%is_gas() is false")
    call test%assertfalse(im_test%is_aerosol(),                         &
        message="Initialized (Null phase) "                             &
        // "id_mass%is_aerosol() is false")

    im_test%phase_id = phase_GAS
    call test%asserttrue(im_test%is_gas(),                              &
        message="Gas: id_mass%is_gas() is true")
    call test%assertfalse(im_test%is_aerosol(),                         &
        message="Gas: id_mass%is_aerosol() is false")

    im_test%phase_id = phase_CONDENSED
    call test%assertfalse(im_test%is_gas(),                             &
        message="Condensed: id_mass%is_gas() is false")
    call test%asserttrue(im_test%is_aerosol(),                          &
        message="Condensed: id_mass%is_aerosol() is true")

    ! Argon check

    call im_test%init()
    im_test%phase_id = phase_GAS
    im_test%id = gid_Ar
    im_test%mass = ONE
    call im_test%dump()
    call im_test%dump(0)
    mw = 39.948_WP
    call test%assertequal(im_test%moles(),                              &
        0.02503254230499649544407730049064_WP,                          &
        rel_tol=0.001_WP,                                               &
        message="Manual Ar(g): 1 lbm = 0.02503 lbmol +-0.1%")

    call im_test%init()
    im_test = new_id_mass(phase_GAS, gid_Ar, ONE)
    call test%assertequal(im_test%moles(),                              &
        0.02503254230499649544407730049064_WP,                          &
        rel_tol=0.001_WP,                                               &
        message="new_id_mass Ar(g): 1 lbm = 0.02503 lbmol +-0.1%")

    ! Invalid mass
    call im_test%init()
    im_test = new_id_mass(phase_GAS, gid_Ar, -ONE)
    call test%assertequal(im_test%mass, ZERO, rel_tol=0.001_WP,         &
        message="new_mass_id Ar(g): negative mass coerced to zero")
    ! write(unit=6, fmt='(A, ES12.4, A, ES12.4)') 'Ar: -1 lbm -> ',       &
    !     im_test%mass, ' lbm is (moles) ', im_test%moles()

    call test%assertequal(im_test%moles(), ZERO, rel_tol=0.001_WP,      &
        message="new_mass_id Ar(g): "                                   &
        // "negative mass coerced to zero moles")

    ! Invalid ID
    call im_test%init()
    im_test = new_id_mass(phase_GAS, gid_Xe + 10, ONE)
    call test%assertequal(im_test%id, gid_NULL,                         &
        message="new_mass_id invalid gas ID: mass coerced to zero")
    call test%assertequal(im_test%mass, ZERO, rel_tol=0.001_WP,         &
        message="new_mass_id invalid gas ID: mass coerced to zero")
    call test%assertequal(im_test%moles(), ZERO, rel_tol=0.001_WP,      &
        message="new_mass_id invalid gas ID: "                          &
        // "negative mass coerced to zero moles")

    ! Na2O2 check

    call im_test%init()
    im_test%phase_id = phase_CONDENSED
    im_test%id = aid_Na2O2
    im_test%mass = ONE
    mw = 77.97834_WP
    call test%assertequal(im_test%moles(),                              &
        0.0128240739672067910140174822906_WP,                           &
        rel_tol=0.001_WP,                                               &
        message="Manual Na2O2(condensed): "                             &
        // "1 lbm = 0.012824 lbmol +-0.1%")

    call im_test%init()
    im_test = new_id_mass(phase_CONDENSED, aid_Na2O2, ONE)
    call test%assertequal(im_test%moles(),                              &
        0.0128240739672067910140174822906_WP,                           &
        rel_tol=0.001_WP,                                               &
        message="new_mass_id Na2O2(condensed): "                        &
        // "1 lbm = 0.012824 lbmol +-0.1%")

    ! Invalid mass
    call im_test%init()
    im_test = new_id_mass(phase_CONDENSED, aid_Na2O2, -ONE)
    call test%assertequal(im_test%mass, ZERO, rel_tol=0.001_WP,         &
        message="new_mass_id Na2O2(condensed): "                        &
        // "negative mass coerced to zero")
    ! write(unit=6, fmt='(A, ES12.4, A, ES12.4)') 'Na2O2: -1 lbm -> ',    &
    !     im_test%mass, ' lbm is (moles) ', im_test%moles()
    call test%assertequal(im_test%moles(), ZERO, rel_tol=0.001_WP,      &
        message="new_mass_id Na2O2(condensed): negative mass coerced "  &
        // "to zero moles")

    ! Invalid ID
    call im_test%init()
    im_test = new_id_mass(phase_CONDENSED, gid_Xe + 10, ONE)
    call test%assertequal(im_test%id, aid_NULL,                         &
        message="new_mass_id invalid aerosol ID: mass coerced to zero")
    call test%assertequal(im_test%mass, ZERO, rel_tol=0.001_WP,         &
        message="new_mass_id invalid aerosol gas ID: "                  &
        // "mass coerced to zero")
    call test%assertequal(im_test%moles(), ZERO, rel_tol=0.001_WP,      &
        message="new_mass_id invalid aerosol ID: "                      &
        // "negative mass coerced to zero moles")

    ! ##################################################################
    ! id_fraction
    ! ##################################################################

    ! id_fraction%phase_id == phase_NULL
    ! id_fraction%id == mid_NULL
    ! id_fraction%f == ZERO
    ! id_fraction%init()
    ! id_fraction%is_gas()
    ! id_fraction%is_aerosol()
    ! id_fraction%dump()

    call test%assertequal(if_test%phase_id, phase_NULL,                 &
        message="Uninitialized id_fraction%phase_id == phase_NULL")
    call test%assertequal(if_test%id, mid_NULL,                         &
        message="Uninitialized id_fraction%id == mid_NULL")
    call test%assertequal(if_test%f, ZERO,                           &
        message="Uninitialized id_fraction%f == ZERO")

    call test%assertfalse(if_test%is_gas(),                             &
        message="Uninitialized id_fraction%is_gas() is false")
    call test%assertfalse(if_test%is_aerosol(),                         &
        message="Uninitialized id_fraction%is_aerosol() is false")

    if_test%phase_id = -21
    if_test%id = 990
    if_test%f = 7.0_WP

    call if_test%init()

    call test%assertequal(if_test%phase_id, phase_NULL,                 &
        message="Initialized id_fraction%phase_id == phase_NULL")
    call test%assertequal(if_test%id, mid_NULL,                         &
        message="Initialized id_fraction%id == mid_NULL")
    call test%assertequal(if_test%f, ZERO,                              &
        message="Initialized id_fraction%f == ZERO")

    call test%assertfalse(if_test%is_gas(),                             &
        message="Initialized (Null phase) id_fraction%is_gas() is false")
    call test%assertfalse(if_test%is_aerosol(),                         &
        message="Initialized (Null phase) "                             &
        // "id_fraction%is_aerosol() is false")

    ! Argon check

    call if_test%init()
    if_test%phase_id = phase_GAS
    if_test%id = gid_Ar
    if_test%f = HALF

    call test%asserttrue(if_test%is_gas(),                              &
        message="Ar(g) id_fraction%is_gas() is true")
    call test%assertfalse(if_test%is_aerosol(),                         &
        message="Ar(g) id_fraction%is_aerosol() is false")

    call if_test%dump()

    ! Na2O2 check

    call if_test%init()
    if_test%phase_id = phase_CONDENSED
    if_test%id = aid_Na2O2
    if_test%f = THIRD

    call test%assertfalse(if_test%is_gas(),                             &
        message="Na2O2 id_fraction%is_gas() is true")
    call test%asserttrue(if_test%is_aerosol(),                          &
        message="Na2O2 id_fraction%is_aerosol() is true")

    call if_test%dump(if_test%id)

    ! ##################################################################
    ! material_dist
    ! ##################################################################

    ! material_dist%name = 'UNINITIALIZED MATERIAL DIST     '
    ! material_dist%dist_type = 'm'
    ! material_dist%phase_id = phase_NULL
    ! material_dist%dist(:)
    ! -- Methods
    ! material_dist%init()
    ! material_dist%normalize()
    ! material_dist%scale_to_fraction()
    ! material_dist%is_gas()
    ! material_dist%is_aerosol()
    ! material_dist%is_mass_fraction()
    ! material_dist%is_mole_fraction()
    ! material_dist%as_nf()
    ! material_dist%as_mf()
    ! material_dist%mw()
    ! material_dist%dump()

    call test%assertequal(md1%name,                                     &
        'UNINITIALIZED MATERIAL DIST     ',                             &
        message="Uninitialized md1%name is default")
    call test%assertequal(md1%dist_type, "m",                           &
        message="Uninitialized md1%dist_type is 'm'")
    call test%assertequal(md1%phase_id, phase_NULL,                     &
        message="Uninitialized md1%phase_id == phase_NULL")
    call test%assertfalse(allocated(md1%dist),                          &
        message="Uninitialized md1%dist is unallocated")

    call test%assertfalse(md1%is_gas(),                                 &
        message="Uninitialized md1%is_gas() is false")
    call test%assertfalse(md1%is_aerosol(),                             &
        message="Uninitialized md1%is_aerosol() is false")

    call test%asserttrue(md1%is_mass_fraction(),                        &
        message="Uninitialized md1%is_mass_fraction() is true")
    call test%assertfalse(md1%is_mole_fraction(),                       &
        message="Uninitialized md1%is_mole_fraction() is false")

    call md1%normalize()

    call test%assertfalse(allocated(md1%dist),                          &
        message="Uninitialized md1%dist is unallocated post-normalize")

    nd1 = md1%as_nf()

    call test%assertequal(nd1%name,                                     &
        'UNINITIALIZED MATERIAL DIST     ',                             &
        message="Uninitialized nd1%name is default")
    call test%assertequal(nd1%dist_type, "n",                           &
        message="Uninitialized nd1%dist_type is 'n'")
    call test%assertequal(nd1%phase_id, phase_NULL,                     &
        message="Uninitialized nd1%phase_id == phase_NULL")
    call test%assertfalse(allocated(nd1%dist),                          &
        message="Uninitialized nd1%dist is unallocated")

    call test%assertfalse(nd1%is_gas(),                                 &
        message="Uninitialized nd1%is_gas() is false")
    call test%assertfalse(nd1%is_aerosol(),                             &
        message="Uninitialized nd1%is_aerosol() is false")

    call test%assertfalse(nd1%is_mass_fraction(),                       &
        message="Uninitialized nd1%is_mass_fraction() is false")
    call test%asserttrue(nd1%is_mole_fraction(),                        &
        message="Uninitialized nd1%is_mole_fraction() is true")

    call md1%dump()
    ! call nd1%dump()
    ! call md2%dump()

    md2 = md1%as_mf()

    call test%assertequal(md2%name,                                     &
        'UNINITIALIZED MATERIAL DIST     ',                             &
        message="Uninitialized md2%name is default")
    call test%assertequal(md2%dist_type, "m",                           &
        message="Uninitialized md2%dist_type is 'm'")
    call test%assertequal(md2%phase_id, phase_NULL,                     &
        message="Uninitialized md2%phase_id == phase_NULL")
    call test%assertfalse(allocated(md2%dist),                          &
        message="Uninitialized md2%dist is unallocated")

    call test%assertfalse(md2%is_gas(),                                 &
        message="Uninitialized md2%is_gas() is false")
    call test%assertfalse(md2%is_aerosol(),                             &
        message="Uninitialized md2%is_aerosol() is false")

    call test%asserttrue(md2%is_mass_fraction(),                        &
        message="Uninitialized md2%is_mass_fraction() is true")
    call test%assertfalse(md2%is_mole_fraction(),                       &
        message="Uninitialized md2%is_mole_fraction() is false")

    ! Set attributes of md1
    md1%name = 'Cornelius Pinotubo III, Jr.     '
    md1%dist_type = "n"
    md1%phase_id = phase_CONDENSED
    allocate(md1%dist(42))
    md1%dist(:)%f = ONE
    md1%dist(:)%phase_id = phase_GAS
    md1%dist(:)%id = -14

    ! Wipe it all clean
    call md1%init()

    call test%assertequal(md1%name,                                     &
        'UNINITIALIZED MATERIAL DIST     ',                             &
        message="Uninitialized md1%name is default")
    call test%assertequal(md1%dist_type, "m",                           &
        message="Uninitialized md1%dist_type is 'm'")
    call test%assertequal(md1%phase_id, phase_NULL,                     &
        message="Uninitialized md1%phase_id == phase_NULL")
    call test%assertfalse(allocated(md1%dist),                          &
        message="Uninitialized md1%dist is unallocated")

    call test%assertfalse(md1%is_gas(),                                 &
        message="Uninitialized md1%is_gas() is false")
    call test%assertfalse(md1%is_aerosol(),                             &
        message="Uninitialized md1%is_aerosol() is false")

    call test%asserttrue(md1%is_mass_fraction(),                        &
        message="Uninitialized md1%is_mass_fraction() is true")
    call test%assertfalse(md1%is_mole_fraction(),                       &
        message="Uninitialized md1%is_mole_fraction() is false")

    call nd1%init()
    nd1%dist_type = "n"
    nd1%phase_id = phase_GAS
    allocate(nd1%dist(3))
    nd1%dist(:)%phase_id = nd1%phase_id
    nd1%dist(1)%id = gid_Ar
    nd1%dist(1)%f = ONE
    nd1%dist(2)%id = gid_O2
    nd1%dist(2)%f = TWO
    nd1%dist(3)%id = gid_N2
    nd1%dist(3)%f = THREE
    call nd1%normalize()

    call test%assertequal(nd1%dist(1)%f, THIRD * HALF,                  &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd1%dist(1)%f = 1/6")
    call test%assertequal(nd1%dist(2)%f, THIRD, abs_tol=1.0E-6_WP,      &
        message="Normalized nd1%dist(2)%f = 2/6")
    call test%assertequal(nd1%dist(3)%f, HALF, abs_tol=1.0E-6_WP,       &
        message="Normalized nd1%dist(3)%f = 3/6")

    fmw1 = nd1%dist(1)%f * material_db%gas(gid_Ar)%mw
    fmw2 = nd1%dist(2)%f * material_db%gas(gid_O2)%mw
    fmw3 = nd1%dist(3)%f * material_db%gas(gid_N2)%mw
    mw = fmw1 + fmw2 + fmw3
    call test%assertequal(nd1%mw(), mw, abs_tol=1.0E-6_WP,              &
        message="Normalized nd1%mw() = Ar:O2:N2 = 1:2:3)")

    call nd2%init()
    nd2 = as_nf(new_gas_mf())
    nd2%dist(gid_N2)%f = THREE
    nd2%dist(gid_O2)%f = TWO
    nd2%dist(gid_Ar)%f = ONE
    call nd2%normalize()
    call test%assertequal(nd2%mw(), nd1%mw(), abs_tol=1.0E-6_WP,        &
        message="Normalized nd2%mw() = nd1%mw() = Ar:O2:N2 = 1:2:3)")

    call test%assertequal(nd1%dist(1)%f, nd2%dist(gid_Ar)%f,            &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd1%dist(1)%f = nd2%dist(gid_Ar)%f")
    call test%assertequal(nd1%dist(2)%f, nd2%dist(gid_O2)%f,            &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd1%dist(2)%f = nd2%dist(gid_O2)%f")
    call test%assertequal(nd1%dist(3)%f, nd2%dist(gid_N2)%f,            &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd1%dist(3)%f = nd2%dist(gid_N2)%f")

    md2 = nd2%as_mf()

    ! call nd2%dump()
    ! call md2%dump()

    call test%assertequal(nd2%dist(gid_Ar)%f,                           &
        md2%dist(gid_Ar)%f * mw / material_db%gas(gid_Ar)%mw,           &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(gid_Ar)%f = md2%dist(gid_Ar)%f "   &
        // "* mw / material_db%gas(gid_Ar)%mw")
    call test%assertequal(nd2%dist(gid_N2)%f,                           &
        md2%dist(gid_N2)%f * mw / material_db%gas(gid_N2)%mw,           &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(gid_N2)%f = md2%dist(gid_N2)%f "   &
        // "* mw / material_db%gas(gid_N2)%mw")
    call test%assertequal(nd2%dist(gid_O2)%f,                           &
        md2%dist(gid_O2)%f * mw / material_db%gas(gid_O2)%mw,           &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(gid_O2)%f = md2%dist(gid_O2)%f "   &
        // "* mw / material_db%gas(gid_O2)%mw")

    call test%assertequal(nd2%mw(), md2%mw(), abs_tol=1.0E-6_WP,        &
        message="Normalized nd2%mw() = md2%mw()")

    !!! Check of new_aerosol_mf()
    call nd2%init()
    nd2 = as_nf(new_aerosol_mf())
    nd2%dist(aid_Na)%f = THREE
    nd2%dist(aid_Na2O)%f = TWO
    nd2%dist(aid_Na2O2)%f = ONE
    call nd2%normalize()
    ! call test%assertequal(nd2%mw(), nd1%mw(), abs_tol=1.0E-6_WP,        &
    !     message="Normalized nd2%mw() = nd1%mw() = Na:Na2O:Na2O2 = 3:2:1)")

    call test%assertequal(nd2%dist(aid_Na)%f, 1.0_WP / 2.0_WP,          &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(aid_Na)%f = 1/2")
    call test%assertequal(nd2%dist(aid_Na2O)%f, 1.0_WP / 3.0_WP,        &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(aid_Na2O)%f = 1/3")
    call test%assertequal(nd2%dist(aid_Na2O2)%f, 1.0_WP / 6.0_WP,       &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(aid_Na2O2)%f = 1/6")

    mw = nd2%mw()
    md2 = nd2%as_mf()

    ! call nd2%dump()
    ! call md2%dump()

    call test%assertequal(nd2%dist(aid_Na)%f,                           &
        md2%dist(aid_Na)%f * mw / material_db%aerosol(aid_Na)%mw,       &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(aid_Na)%f = md2%dist(aid_Na)%f "   &
        // "* mw / material_db%aerosol(aid_Na)%mw")
    call test%assertequal(nd2%dist(aid_Na2O)%f,                         &
        md2%dist(aid_Na2O)%f * mw / material_db%aerosol(aid_Na2O)%mw,   &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(aid_Na2O)%f = md2%dist(aid_Na2O)%f " &
        // "* mw / material_db%aerosol(aid_Na2O)%mw")
    call test%assertequal(nd2%dist(aid_Na2O2)%f,                        &
        md2%dist(aid_Na2O2)%f * mw / material_db%aerosol(aid_Na2O2)%mw, &
        abs_tol=1.0E-6_WP,                                              &
        message="Normalized nd2%dist(aid_Na2O2)%f = md2%dist(aid_Na2O2)%f " &
        // "* mw / material_db%aerosol(aid_Na2O2)%mw")

    ! write(unit=stderr, fmt='(A, ES12.4, A, ES12.4)') 'nd2%mw: ', nd2%mw(), ', md2%mw: ', md2%mw()
    call test%assertequal(nd2%mw(), md2%mw(), abs_tol=1.0E-6_WP,        &
        message="Normalized nd2%mw() = md2%mw()")

    !!!

    md1 = new_gas_mf()
    call test%assertequal(size(md1%dist), n_gas,                        &
        message="Gas mf has n_gas elements")
    call test%asserttrue(all(md1%dist(:)%f == ZERO),                    &
        message="New gas mf has all f set to zero")
    call test%asserttrue(all(md1%dist(:)%phase_id == md1%phase_id),     &
        message="New gas mf has consistent phase_id")
    do i = 1, n_gas
        call test%assertequal(md1%dist(i)%id, gas_id(i),                &
        message="New gas mf has gas IDs in order")
    end do
    call md1%normalize()

    nd1 = md1%as_nf()
    call test%assertequal(size(nd1%dist), n_gas,                        &
        message="Gas nf has n_gas elements")
    call test%asserttrue(all(nd1%dist(:)%f == ZERO),                    &
        message="New gas nf has all f set to zero")
    call test%asserttrue(all(nd1%dist(:)%phase_id == nd1%phase_id),     &
        message="New gas nf has consistent phase_id")
    do i = 1, n_gas
        call test%assertequal(nd1%dist(i)%id, gas_id(i),                &
        message="New gas nf has gas IDs in order")
    end do
    call nd1%normalize()

    md2 = new_aerosol_mf()
    call test%assertequal(size(md2%dist), n_aer,                        &
        message="Aerosol mf has n_aer elements")
    call test%asserttrue(all(md2%dist(:)%f == ZERO),                    &
        message="New aerosol mf has all f set to zero")
    call test%asserttrue(all(md2%dist(:)%phase_id == md2%phase_id),     &
        message="New aerosol mf has consistent phase_id")
    do i = 1, n_aer
        call test%assertequal(md2%dist(i)%id, aer_id(i),                &
        message="New aerosol mf has aerosol IDs in order")
    end do
    call md2%normalize()

    nd2 = md2%as_nf()
    call test%assertequal(size(nd2%dist), n_aer,                        &
        message="Gas nf has n_aer elements")
    call test%asserttrue(all(nd2%dist(:)%f == ZERO),                    &
        message="New aerosol nf has all f set to zero")
    call test%asserttrue(all(nd2%dist(:)%phase_id == nd2%phase_id),     &
        message="New aerosol nf has consistent phase_id")
    do i = 1, n_aer
        call test%assertequal(nd2%dist(i)%id, aer_id(i),                &
        message="New aerosol nf has aerosol IDs in order")
    end do
    call nd2%normalize()

    md1 = nd2%as_mf()
    call test%assertequal(size(md1%dist), n_aer,                        &
        message="Aerosol mf has n_aer elements")
    call test%asserttrue(all(md1%dist(:)%f == ZERO),                    &
        message="New aerosol mf has all f set to zero")
    call test%asserttrue(all(md1%dist(:)%phase_id == md1%phase_id),     &
        message="New aerosol mf has consistent phase_id")
    do i = 1, n_aer
        call test%assertequal(md1%dist(i)%id, aer_id(i),                &
        message="New aerosol mf has aerosol IDs in order")
    end do
    call md1%normalize()

    call md1%init()
    md1 = as_mf(nd2)
    call test%assertequal(size(md1%dist), n_aer,                        &
        message="Aerosol mf has n_aer elements")
    call test%asserttrue(all(md1%dist(:)%f == ZERO),                    &
        message="New aerosol mf has all f set to zero")
    call test%asserttrue(all(md1%dist(:)%phase_id == md1%phase_id),     &
        message="New aerosol mf has consistent phase_id")
    do i = 1, n_aer
        call test%assertequal(md1%dist(i)%id, aer_id(i),                &
        message="New aerosol mf has aerosol IDs in order")
    end do
    call md1%normalize()

    ! material_dist%scale_to_fraction()

    call md1%init()
    md1 = new_gas_mf()
    md1%dist(gid_Ar)%f = ONE
    md1%dist(gid_Xe)%f = TWO
    md1%dist(gid_Ne)%f = THREE
    call md1%normalize()

    ! call md1%dump()
    call md1%scale_to_fraction(gid_Ar, HALF)
    ! call md1%dump()

    call test%assertequal(md1%dist(gid_Ar)%f, HALF, abs_tol=1.0E-6_WP,  &
        message="Scale_to_fraction: md1%dist(gid_Ar)%f = 0.5")
    refval = HALF * TWO / FIVE
    ! refval = 0.2_WP
    ! write(unit=stderr, fmt='(A, ES12.4, A, ES12.4)') 'Xe f = ', md1%dist(gid_Xe)%f, ', refval = ', refval
    call test%assertequal(md1%dist(gid_Xe)%f, refval,                   &
        abs_tol=1.0E-6_WP,                                              &
        message="Scale_to_fraction: md1%dist(gid_Xe)%f = 0.2")
    refval = HALF * THREE / FIVE
    ! refval = 0.3_WP
    ! write(unit=stderr, fmt='(A, ES12.4, A, ES12.4)') 'Ne f = ', md1%dist(gid_Ne)%f, ', refval = ', refval
    call test%assertequal(md1%dist(gid_Ne)%f, refval,                   &
        abs_tol=1.0E-6_WP,                                              &
        message="Scale_to_fraction: md1%dist(gid_Ne)%f = 0.3")

    call md1%init()
    md1 = new_gas_mf()
    md1%dist(gid_Ar)%f = ONE

    call md1%scale_to_fraction(gid_Ar, HALF)

    ! write(unit=stderr, fmt='(A, ES12.4, A, ES12.4)') 'Ar f = ', md1%dist(gid_Ar)%f, ', refval = ', ONE
    call test%assertequal(md1%dist(gid_Ar)%f, ONE, abs_tol=1.0E-6_WP,   &
        message="Scale_to_fraction: single constituent is always 1.0")

    call md1%init()
    md1 = new_gas_mf()
    call md1%scale_to_fraction(gid_Ar, HALF)

    ! write(unit=stderr, fmt='(A, ES12.4, A, ES12.4)') 'Ar f = ', md1%dist(gid_Ar)%f, ', refval = ', ONE
    call test%assertequal(md1%dist(gid_Ar)%f, ONE, abs_tol=1.0E-6_WP,   &
        message="Scale_to_fraction: single constituent is always 1.0 "  &
        // "even for initially empty dist")

    call nd1%init()
    nd1 = get_nf_dry_air()

    ! call nd1%dump()
    call test%assertequal(count(nd1%dist(:)%f > ZERO), 10,              &
        message="nf dry air: 10 constituents")

    call test%assertequal(nd1%dist(gid_N2)%f, 78.084E-2_WP,             &
        rel_tol=1.0E-3_WP, message="nf dry air: f(N2) = 78.084%")
    call test%assertequal(nd1%dist(gid_O2)%f, 20.9476E-2_WP,            &
        rel_tol=1.0E-3_WP, message="nf dry air: f(O2) = 20.9476%")
    ! write(unit=stderr, fmt='(A, ES16.8)') 'f(Ar) = ', nd1%dist(gid_Ar)%f
    call test%assertequal(nd1%dist(gid_Ar)%f, 0.934E-2_WP,              &
        rel_tol=1.0E-3_WP, message="nf dry air: f(Ar) = 0.934%")
    ! write(unit=stderr, fmt='(A, ES16.8)') 'f(CH4) = ', nd1%dist(gid_CH4)%f
    call test%assertequal(nd1%dist(gid_CH4)%f, 2.0E-6_WP,               &
        rel_tol=1.0E-3_WP, message="nf dry air: f(CH4) = 2.0E-6")
    ! write(unit=stderr, fmt='(A, ES16.8)') 'f(CO2) = ', nd1%dist(gid_CO2)%f
    call test%assertequal(nd1%dist(gid_CO2)%f, 3.14E-4_WP,              &
        rel_tol=1.0E-3_WP, message="nf dry air: f(CO2) = 3.14E-4")
    call test%assertequal(nd1%dist(gid_Ne)%f, 1.818E-5_WP,              &
        rel_tol=1.0E-3_WP, message="nf dry air: f(Ne) = 1.818E-5")
    call test%assertequal(nd1%dist(gid_He)%f, 5.24E-6_WP,               &
        rel_tol=1.0E-3_WP, message="nf dry air: f(He) = 5.24E-6")
    call test%assertequal(nd1%dist(gid_Kr)%f, 1.14E-6_WP,               &
        rel_tol=1.0E-3_WP, message="nf dry air: f(Kr) = 1.14E-6")
    call test%assertequal(nd1%dist(gid_Xe)%f, 8.70E-8_WP,               &
        rel_tol=1.0E-3_WP, message="nf dry air: f(Xe) = 8.7E-8")
    call test%assertequal(nd1%dist(gid_H2)%f, 5.00E-07_WP,              &
        rel_tol=1.0E-3_WP, message="nf dry air: f(H2) = 5.0E-7")

    ! ##################################################################
    ! parcel
    ! ##################################################################
    ! parcel%name = 'UNINITIALIZED PARCEL            '
    ! parcel%phase_id = phase_NULL
    ! parcel%species(:)
    !
    ! parcel%init()
    ! parcel%clone_parcel()
    ! parcel%add()
    ! parcel%subtract()
    ! parcel%is_gas()
    ! parcel%is_aerosol()
    ! parcel%mass()
    ! parcel%moles()
    ! parcel%mw()
    ! parcel%energy()

    call nd1%init()
    call nd2%init()
    call md1%init()
    call md2%init()

    call test%assertequal(p1%name,                                      &
        'UNINITIALIZED PARCEL            ',                             &
        message="Uninitialized md1%name is default")
    call test%assertequal(p1%phase_id, phase_NULL,                      &
        message="Uninitialized p1%phase_id == phase_NULL")
    call test%assertfalse(allocated(p1%species),                        &
        message="Uninitialized p1%species is unallocated")
    call test%assertfalse(p1%is_gas(),                                  &
        message="Uninitialized: p1 is not gas")
    call test%assertfalse(p1%is_aerosol(),                              &
        message="Uninitialized: p1 is not aerosol")

    p1%name = 'Thinking of an unrelated thing  '
    allocate(p1%species(14))

    p1%phase_id = phase_GAS

    call test%asserttrue(p1%is_gas(),                                   &
        message="Manual: p1 is gas when phase_id == phase_GAS")
    call test%assertfalse(p1%is_aerosol(),                              &
        message="Manual: p1 is not aerosol when phase_id == phase_GAS")

    p1%phase_id = phase_CONDENSED

    call test%assertfalse(p1%is_gas(),                                  &
        message="Manual: p1 is not gas when phase_id == phase_CONDENSED")
    call test%asserttrue(p1%is_aerosol(),                               &
        message="Manual: p1 is aerosol when phase_id == phase_CONDENSED")

    call p1%init()

    call test%assertequal(p1%name,                                      &
        'UNINITIALIZED PARCEL            ',                             &
        message="Initialized md1%name is default")
    call test%assertequal(p1%phase_id, phase_NULL,                      &
        message="Initialized p1%phase_id == phase_NULL")
    call test%assertfalse(allocated(p1%species),                        &
        message="Initialized p1%species is unallocated")
    call test%assertfalse(p1%is_gas(),                                  &
        message="Initialized: p1 is not gas")
    call test%assertfalse(p1%is_aerosol(),                              &
        message="Initialized: p1 is not aerosol")

    nd1 = get_nf_dry_air()
    md1 = nd1%as_mf()
    mw = md1%mw()

    p1 = new_gas_parcel(name='Ten pounds of dry air           ',        &
         mass=10.0_WP, mf=md1)

    p2 = new_gas_parcel(name='Ten pounds of dry air in moles  ',        &
         moles=10.0_WP / mw, nf=nd1)

    call test%assertequal(p2%mw(), mw, abs_tol=1.0E-6_WP,               &
        message="nf/mf initialized gas parcels: p2%mw() = md1%mw()")
    mw = p2%mw()
    call test%assertequal(size(p1%species), size(p2%species),           &
        message="nf/mf initialized gas parcels: size(p1) = size(p2)")
    call test%assertequal(count(p1%species(:)%mass > ZERO), 10,         &
        message="nf/mf initialized gas parcels: p1 has 10 constituents")
    call test%assertequal(count(p1%species(:)%mass > ZERO),             &
        count(p2%species(:)%mass > ZERO),                               &
        message="nf/mf initialized gas parcels: "                       &
        // "p1, p2 have same constituent count")
    call test%assertequal(sum(p1%species(:)%mass), 10.0_WP,             &
        abs_tol=1.0E-3_WP,                                              &
        message="nf/mf initialized gas parcels: sum(p1%mass) = 10.0")
    call test%assertequal(p1%mass(), 10.0_WP, abs_tol=1.0E-3_WP,        &
        message="nf/mf initialized gas parcels: p1%mass() = 10.0")
    call test%assertequal(p2%mass(), 10.0_WP, abs_tol=1.0E-3_WP,        &
        message="nf/mf initialized gas parcels: p2%mass() = 10.0")
    call test%assertequal(p1%moles(), 10.0_WP / mw, abs_tol=1.0E-3_WP,  &
        message="nf/mf initialized gas parcels: p1%moles() = 10.0 / mw")

    do i = 1, size(p1%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5, A, ES12.5)')         &
        !     'Gas ', p1%species(i)%id, ' p1 mass = ', p1%species(i)%mass, &
        !     ', p2 mass = ', p2%species(i)%mass
        call test%assertequal(p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="nf/mf initialized gas parcels: p1%species(:)%mass) = p2%...")
    end do

    ! Manually compose energy of dry air parcel p1 at 500 K
    T = 500.0_WP
    call p1%dump()
    refval = ZERO
    do i = 1, size(p1%species(:))
        tmp_n = p1%species(i)%moles()
        tmp_cp = RGAS * (material_db%gas(p1%species(i)%id)%cp(T) - ONE)
        tmp_u = tmp_n * tmp_cp * T
        refval = refval + tmp_u
        if (p1%species(i)%mass > ZERO) then
            write(unit=stderr, fmt='(A, I0, A, F6.2, A, ES12.5, A, '    &
                // 'ES12.5, A, ES12.5, A)')                             &
                'Cp[gas=', p1%species(i)%id, '](T = ', T,               &
                ' K) is ', tmp_cp, ' J/mol-K; n = ',                    &
                p1%species(i)%moles(), ' mol, mCpT = ', tmp_u, ' J'
        end if
    end do
    write(unit=stderr, fmt='(A, ES12.5, A)')                            &
        'Total energy =', refval, ' J'
    write(unit=stderr, fmt='(A, F8.2, A, ES12.5, A)') 'Gas energy '     &
        // 'at T = ', T, ' K is ', p1%energy_si(T), ' J'
    call test%assertequal(p1%energy_si(T), refval, rel_tol=1.0E-3_WP,     &
        message="Gas parcel: Energy of 10 kg of dry air at 500 K")

    call p2%init()
    p2 = p1%clone_parcel()

    do i = 1, size(p1%species(:))
        call test%assertequal(p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="cloned gas parcels: p1%species(:)%mass) = p2%...")
    end do

    call p2%add(p1)

    do i = 1, size(p1%species(:))
        call test%assertequal(TWO * p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Added gas parcels: p2%species(:)%mass) = 2 * p1%...")
    end do

    call p2%subtract(p1)

    do i = 1, size(p1%species(:))
        call test%assertequal(p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Subtracted gas parcels: p2%species(:)%mass) = p1%...")
    end do

    ! Aerosol parcel clone/add/subtract

    nd1 = as_nf(new_aerosol_mf())
    nd1%name = 'Na:Na2O:Na2O2 1:2:3             '
    nd1%dist(aid_Na)%f = ONE
    nd1%dist(aid_Na2O)%f = TWO
    nd1%dist(aid_Na2O2)%f = THREE
    call nd1%normalize()
    md1 = nd1%as_mf()
    mw = md1%mw()

    p1 = new_aerosol_parcel(name='Ten pounds of 1:2:3 Na species m',    &
         mass=10.0_WP, mf=md1)

    p2 = new_aerosol_parcel(name='Ten pounds of 1:2:3 Na species n',    &
         moles=10.0_WP / mw, nf=nd1)

    ! write(unit=stderr, fmt='(A, F14.8, A, F14.8, A, F14.8)') 'p1%mw = ', p1%mw(), &
    !     ', p2%mw = ', p2%mw(), ', mw_ref = ', mw

    call test%assertequal(p2%mw(), mw, abs_tol=1.0E-6_WP,               &
        message="nf/mf initialized aerosol parcels: p2%mw() = md1%mw()")
    mw = p2%mw()
    call test%assertequal(size(p1%species), size(p2%species),           &
        message="nf/mf initialized aerosol parcels: size(p1) = size(p2)")
    call test%assertequal(count(p1%species(:)%mass > ZERO), 3,         &
        message="nf/mf initialized aerosol parcels: p1 has 3 constituents")
    call test%assertequal(count(p1%species(:)%mass > ZERO),             &
        count(p2%species(:)%mass > ZERO),                               &
        message="nf/mf initialized aerosol parcels: p1, p2 have same constituent count")
    call test%assertequal(sum(p1%species(:)%mass), 10.0_WP,             &
        abs_tol=1.0E-3_WP,                                              &
        message="nf/mf initialized aerosol parcels: sum(p1%mass) = 10.0")
    call test%assertequal(p1%mass(), 10.0_WP, abs_tol=1.0E-3_WP,        &
        message="nf/mf initialized aerosol parcels: p1%mass() = 10.0")
    call test%assertequal(p2%mass(), 10.0_WP, abs_tol=1.0E-3_WP,        &
        message="nf/mf initialized aerosol parcels: p2%mass() = 10.0")
    call test%assertequal(p1%moles(), 10.0_WP / mw, abs_tol=1.0E-3_WP,  &
        message="nf/mf initialized aerosol parcels: p1%moles() = 10.0 / mw")

    do i = 1, size(p1%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5, A, ES12.5)')         &
        !     'Aerosol ', p1%species(i)%id, ' p1 mass = ', p1%species(i)%mass, &
        !     ', p2 mass = ', p2%species(i)%mass
        call test%assertequal(p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="nf/mf initialized aerosol parcels: p1%species(:)%mass) = p2%...")
    end do

    call p2%init()
    p2 = p1%clone_parcel()

    do i = 1, size(p1%species(:))
        call test%assertequal(p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="cloned aerosol parcels: p1%species(:)%mass) = p2%...")
    end do

    call p2%add(p1)

    do i = 1, size(p1%species(:))
        call test%assertequal(TWO * p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Added aerosol parcels: p2%species(:)%mass) = 2 * p1%...")
    end do

    call p2%subtract(p1)

    do i = 1, size(p1%species(:))
        call test%assertequal(p1%species(i)%mass, p2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Subtracted aerosol parcels: p2%species(:)%mass) = p1%...")
    end do

    call p2%subtract(p1)

    do i = 1, size(p1%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     p1%species(i)%id, ' p2 mass = ', p2%species(i)%mass
        call test%assertequal(p2%species(i)%mass, ZERO,                 &
        abs_tol=1.0E-6_WP,                                              &
        message="Subtracted aerosol parcels: p2%species(:)%mass) = 0")
    end do

    ! Negative aerosol mass
    call p2%subtract(p1)

    do i = 1, size(p2%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     p2%species(i)%id, ' p2 mass = ', p2%species(i)%mass
        call test%assertequal(p2%species(i)%mass, -p1%species(i)%mass,  &
        abs_tol=1.0E-6_WP,                                              &
        message="Subtracted aerosol parcels (negative): "               &
            // "p2%species(:)%mass) = -p1%...")
    end do

    T = 400.0_WP
    ! call p1%dump()
    refval = ZERO
    do i = 1, size(p1%species(:))
        tmp_n = p1%species(i)%moles()
        tmp_cp = RGAS * material_db%aerosol(p1%species(i)%id)%cp(T)
        tmp_u = tmp_n * tmp_cp * T
        refval = refval + tmp_u
        ! if (p1%species(i)%mass > ZERO) then
        !     write(unit=stderr, fmt='(A, I0, A, F6.2, A, ES12.5, A, '    &
        !         // 'ES12.5, A, ES12.5, A)')                             &
        !         'Cp[aerosol=', p1%species(i)%id, '](T = ', T,           &
        !         ' K) is ', tmp_cp, ' J/mol-K; n = ',                    &
        !         p1%species(i)%moles(), ' mol, mCpT = ', tmp_u, ' J'
        ! end if
    end do
    ! write(unit=stderr, fmt='(A, ES12.5, A)')                            &
    !     'Total energy =', refval, ' J'
    ! write(unit=stderr, fmt='(A, F8.2, A, ES12.5, A)') 'Aerosol energy ' &
    !     // 'at T = ', T, ' K is ', p1%energy_si(T), ' J'
    call test%assertequal(p1%energy_si(T), refval, rel_tol=1.0E-3_WP,   &
        message="Aerosol parcel: Energy of 10 kg of 1:2:3 Na at 400 K")

    ! ##################################################################
    ! inventory
    ! ##################################################################
    ! inventory%mf
    ! inventory%nf
    !
    ! inventory%init()
    ! inventory%clone_inventory()
    ! inventory%add()
    ! inventory%subtract()
    ! inventory%update_distributions()
    ! inventory%mw()
    ! inventory%as_mf()
    ! inventory%as_nf()
    ! inventory%add_component_mass()
    ! inventory%remove_component_mass()
    ! inventory%extract_mixture_mass()

    call nd1%init()
    call nd2%init()
    call md1%init()
    call md2%init()
    ! call inv1%init()
    ! call inv1%dump()

    call test%assertequal(inv1%name,                                    &
        'UNINITIALIZED PARCEL            ',                             &
        message="Uninitialized inv%name is default (inherited)")
    call test%assertequal(inv1%phase_id, phase_NULL,                    &
        message="Uninitialized inv1%phase_id == phase_NULL")
    call test%assertfalse(allocated(inv1%species),                      &
        message="Uninitialized inv1%species is unallocated")
    call test%assertfalse(inv1%is_gas(),                                &
        message="Uninitialized: inv1 is not gas")
    call test%assertfalse(inv1%is_aerosol(),                            &
        message="Uninitialized: inv1 is not aerosol")
    call test%assertfalse(allocated(inv1%nf%dist),                      &
        message="Uninitialized: inv1%nf%dist is unallocated")
    call test%assertfalse(allocated(inv1%mf%dist),                      &
        message="Uninitialized: inv1%mf%dist is unallocated")

    inv1%name = 'Thinking of an unrelated thing  '
    allocate(inv1%species(14))

    inv1%phase_id = phase_GAS

    call test%asserttrue(inv1%is_gas(),                                 &
        message="Manual: inv1 is gas when phase_id == phase_GAS")
    call test%assertfalse(inv1%is_aerosol(),                            &
        message="Manual: inv1 is not aerosol when "                     &
        // "phase_id == phase_GAS")

    inv1%phase_id = phase_CONDENSED

    call test%assertfalse(inv1%is_gas(),                                &
        message="Manual: inv1 is not gas when "                         &
        // "phase_id == phase_CONDENSED")
    call test%asserttrue(inv1%is_aerosol(),                             &
        message="Manual: inv1 is aerosol when "                         &
        // "phase_id == phase_CONDENSED")

    inv1%nf = get_nf_dry_air()
    inv1%mf = new_aerosol_mf()

    call test%asserttrue(allocated(inv1%nf%dist),                       &
        message="Manual set: inv1%nf%dist is allocated")

    call test%asserttrue(allocated(inv1%mf%dist),                       &
        message="Manual set: inv1%mf%dist is allocated")

    call inv1%init()

    call test%assertequal(inv1%name,                                    &
        'UNINITIALIZED INVENTORY         ',                             &
        message="Initialized inv1%name is default")
    call test%assertequal(inv1%phase_id, phase_NULL,                    &
        message="Initialized inv1%phase_id == phase_NULL")
    call test%assertfalse(allocated(inv1%species),                      &
        message="Initialized inv1%species is unallocated")
    call test%assertfalse(inv1%is_gas(),                                &
        message="Initialized: inv1 is not gas")
    call test%assertfalse(inv1%is_aerosol(),                            &
        message="Initialized: inv1 is not aerosol")
    call test%assertfalse(allocated(inv1%nf%dist),                      &
        message="Initialized: inv1%nf%dist is unallocated")
    call test%assertfalse(allocated(inv1%mf%dist),                      &
        message="Initialized: inv1%mf%dist is unallocated")

    nd1 = get_nf_dry_air()
    md1 = nd1%as_mf()
    mw = md1%mw()

    inv1 = new_gas_inventory(name='Ten mass units of dry air       ',   &
         mass=10.0_WP, mf=md1)

    inv2 = new_gas_inventory(name='Ten mass units of dry air (by n)',   &
         moles=10.0_WP / mw, nf=nd1)

    call test%assertequal(inv2%mw(), mw, abs_tol=1.0E-6_WP,             &
        message="nf/mf initialized gas inventories: inv2%mw() = md1%mw()")
    mw = inv2%mw()
    call test%assertequal(size(inv1%species), size(inv2%species),       &
        message="nf/mf initialized gas inventories: size(inv1) = size(inv2)")
    call test%assertequal(count(inv1%species(:)%mass > ZERO), 10,       &
        message="nf/mf initialized gas inventories: inv1 has 10 constituents")
    call test%assertequal(count(inv1%species(:)%mass > ZERO),           &
        count(inv2%species(:)%mass > ZERO),                             &
        message="nf/mf initialized gas inventories: "                   &
        // "inv1, inv2 have same constituent count")
    call test%assertequal(sum(inv1%species(:)%mass), 10.0_WP,           &
        abs_tol=1.0E-3_WP,                                              &
        message="nf/mf initialized gas inventories: sum(inv1%mass) = 10.0")
    call test%assertequal(inv1%mass(), 10.0_WP, abs_tol=1.0E-3_WP,      &
        message="nf/mf initialized gas inventories: inv1%mass() = 10.0")
    call test%assertequal(inv2%mass(), 10.0_WP, abs_tol=1.0E-3_WP,      &
        message="nf/mf initialized gas inventories: inv2%mass() = 10.0")
    call test%assertequal(inv1%moles(), 10.0_WP / mw, abs_tol=1.0E-3_WP, &
        message="nf/mf initialized gas inventories: inv1%moles() = 10.0 / mw")

    do i = 1, size(inv1%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5, A, ES12.5)')         &
        !     'Gas ', inv1%species(i)%id, ' inv1 mass = ', inv1%species(i)%mass, &
        !     ', inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv1%species(i)%mass, inv2%species(i)%mass, &
        rel_tol=1.0E-4_WP,                                              &
        message="nf/mf initialized gas inventories: inv1%species(:)%mass) = inv2%...")
    end do

    ! Manually compose energy of dry air inventory inv1 at 308 K
    T = 308.0_WP
    call inv1%dump()
    refval = ZERO
    do i = 1, size(inv1%species(:))
        tmp_n = inv1%species(i)%moles()
        tmp_cp = RGAS * (material_db%gas(inv1%species(i)%id)%cp(T) - ONE)
        tmp_u = tmp_n * tmp_cp * T
        refval = refval + tmp_u
        if (inv1%species(i)%mass > ZERO) then
            write(unit=stderr, fmt='(A, I0, A, F6.2, A, ES12.5, A, '    &
                // 'ES12.5, A, ES12.5, A)')                             &
                'Cp[gas=', inv1%species(i)%id, '](T = ', T,             &
                ' K) is ', tmp_cp, ' J/mol-K; n = ',                    &
                inv1%species(i)%moles(), ' mol, mCpT = ', tmp_u, ' J'
        end if
    end do
    write(unit=stderr, fmt='(A, ES12.5, A)')                            &
        'Total energy =', refval, ' J'
    write(unit=stderr, fmt='(A, F8.2, A, ES12.5, A)') 'Gas energy '     &
        // 'at T = ', T, ' K is ', inv1%energy_si(T), ' J'
    call test%assertequal(inv1%energy_si(T), refval,                    &
        rel_tol=1.0E-3_WP,                                              &
        message="Gas inventory: Energy of 10 kg of dry air at 308 K")

    call inv2%init()
    inv2 = inv1%clone_inventory()

    do i = 1, size(inv1%species(:))
        call test%assertequal(inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="cloned gas inventories: inv1%species(:)%mass) = inv2%...")
    end do

    call inv2%add(inv1)

    do i = 1, size(inv1%species(:))
        call test%assertequal(TWO * inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Added gas inventories: inv2%species(:)%mass) = 2 * inv1%...")
    end do

    call inv2%subtract(inv1)

    do i = 1, size(inv1%species(:))
        call test%assertequal(inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Subtracted gas inventories: inv2%species(:)%mass) = inv1%...")
    end do

    ! Aerosol inventory clone/add/subtract

    nd1 = as_nf(new_aerosol_mf())
    nd1%name = 'Na:Na2O:Na2O2 1:3:2             '
    nd1%dist(aid_Na)%f = ONE
    nd1%dist(aid_Na2O)%f = THREE
    nd1%dist(aid_Na2O2)%f = TWO
    call nd1%normalize()
    md1 = nd1%as_mf()
    mw = md1%mw()

    inv1 = new_aerosol_inventory(name='Ten pounds of 1:3:2 Na species m',    &
            mass=10.0_WP, mf=md1)

    inv2 = new_aerosol_inventory(name='Ten pounds of 1:3:2 Na species n',    &
            moles=10.0_WP / mw, nf=nd1)

    ! write(unit=stderr, fmt='(A, F14.8, A, F14.8, A, F14.8)') 'inv1%mw = ', inv1%mw(), &
    !     ', inv2%mw = ', inv2%mw(), ', mw_ref = ', mw

    call test%assertequal(inv2%mw(), mw, abs_tol=1.0E-6_WP,               &
        message="nf/mf initialized aerosol inventories: inv2%mw() = md1%mw()")
    mw = inv2%mw()
    call test%assertequal(size(inv1%species), size(inv2%species),           &
        message="nf/mf initialized aerosol inventories: size(inv1) = size(inv2)")
    call test%assertequal(count(inv1%species(:)%mass > ZERO), 3,         &
        message="nf/mf initialized aerosol inventories: inv1 has 3 constituents")
    call test%assertequal(count(inv1%species(:)%mass > ZERO),             &
        count(inv2%species(:)%mass > ZERO),                               &
        message="nf/mf initialized aerosol inventories: inv1, inv2 have same constituent count")
    call test%assertequal(sum(inv1%species(:)%mass), 10.0_WP,             &
        abs_tol=1.0E-3_WP,                                              &
        message="nf/mf initialized aerosol inventories: sum(inv1%mass) = 10.0")
    call test%assertequal(inv1%mass(), 10.0_WP, abs_tol=1.0E-3_WP,        &
        message="nf/mf initialized aerosol inventories: inv1%mass() = 10.0")
    call test%assertequal(inv2%mass(), 10.0_WP, abs_tol=1.0E-3_WP,        &
        message="nf/mf initialized aerosol inventories: inv2%mass() = 10.0")
    call test%assertequal(inv1%moles(), 10.0_WP / mw, abs_tol=1.0E-3_WP,  &
        message="nf/mf initialized aerosol inventories: inv1%moles() = 10.0 / mw")

    do i = 1, size(inv1%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5, A, ES12.5)')         &
        !     'Aerosol ', inv1%species(i)%id, ' inv1 mass = ', inv1%species(i)%mass, &
        !     ', inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="nf/mf initialized aerosol inventories: inv1%species(:)%mass) = inv2%...")
    end do

    call inv2%init()
    inv2 = inv1%clone_inventory()

    do i = 1, size(inv1%species(:))
        call test%assertequal(inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="cloned aerosol inventories: inv1%species(:)%mass) = inv2%...")
    end do

    call inv2%add(inv1)

    do i = 1, size(inv1%species(:))
        call test%assertequal(TWO * inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Added aerosol inventories: inv2%species(:)%mass) = 2 * inv1%...")
    end do

    call inv2%subtract(inv1)

    do i = 1, size(inv1%species(:))
        call test%assertequal(inv1%species(i)%mass, inv2%species(i)%mass,   &
        rel_tol=1.0E-4_WP,                                              &
        message="Subtracted aerosol inventories: inv2%species(:)%mass) = inv1%...")
    end do

    call inv2%subtract(inv1)

    do i = 1, size(inv1%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv1%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv2%species(i)%mass, ZERO,                 &
        abs_tol=1.0E-6_WP,                                              &
        message="Subtracted aerosol inventories: inv2%species(:)%mass) = 0")
    end do

    ! Negative aerosol mass
    call inv2%subtract(inv1)

    do i = 1, size(inv2%species(:))
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv2%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv2%species(i)%mass, -inv1%species(i)%mass,  &
        abs_tol=1.0E-6_WP,                                              &
        message="Subtracted aerosol inventories (negative): "               &
            // "inv2%species(:)%mass) = -inv1%...")
    end do

    T = 700.0_WP
    ! call inv1%dump()
    refval = ZERO
    do i = 1, size(inv1%species(:))
        tmp_n = inv1%species(i)%moles()
        tmp_cp = RGAS * material_db%aerosol(inv1%species(i)%id)%cp(T)
        tmp_u = tmp_n * tmp_cp * T
        refval = refval + tmp_u
        ! if (inv1%species(i)%mass > ZERO) then
        !     write(unit=stderr, fmt='(A, I0, A, F6.2, A, ES12.5, A, '    &
        !         // 'ES12.5, A, ES12.5, A)')                             &
        !         'Cp[aerosol=', inv1%species(i)%id, '](T = ', T,           &
        !         ' K) is ', tmp_cp, ' J/mol-K; n = ',                    &
        !         inv1%species(i)%moles(), ' mol, mCpT = ', tmp_u, ' J'
        ! end if
    end do
    ! write(unit=stderr, fmt='(A, ES12.5, A)')                            &
    !     'Total energy =', refval, ' J'
    ! write(unit=stderr, fmt='(A, F8.2, A, ES12.5, A)') 'Aerosol energy ' &
    !     // 'at T = ', T, ' K is ', inv1%energy_si(T), ' J'
    call test%assertequal(inv1%energy_si(T), refval, rel_tol=1.0E-3_WP,   &
        message="Aerosol inventory: Energy of 10 kg of 1:3:2 Na at 700 K")

    ! inventory%update_distributions()
    ! inventory%add_component_mass(id, dmass)
    ! inventory%remove_component_mass(id, dmass)

    nd1 = get_nf_dry_air()
    mw = nd1%mw()
    md1 = nd1%as_mf()

    inv1 = new_gas_inventory(name='Nine mass units of dry air      ',   &
        moles=9.0_WP / mw, nf=nd1)

    inv2 = inv1%clone_inventory()

    inv1%species(gid_O2)%mass = inv1%species(gid_O2)%mass + ONE

    call inv2%add_component_mass(gid_O2, ONE)

    call test%assertequal(inv1%mass(), 10.0_WP, rel_tol=1.0E-4_WP,      &
        message="Add component: inv1%mass() = 9 + 1")
    call test%assertequal(inv2%mass(), 10.0_WP, rel_tol=1.0E-4_WP,      &
        message="Add component: inv2%mass() = 9 + 1")

    do i = 1, size(inv1%nf%dist)
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv1%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv1%nf%dist(i)%f, nd1%dist(i)%f,         &
            rel_tol=1.0E-4_WP,                                          &
            message="Add component: Identical inv1%nf before update")
        call test%assertequal(inv1%mf%dist(i)%f, md1%dist(i)%f,         &
            rel_tol=1.0E-4_WP,                                          &
            message="Add component: Identical inv1%mf before update")
    end do

    call inv1%update_distributions()

    do i = 1, size(inv1%species)
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv1%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv1%nf%dist(i)%f, inv2%nf%dist(i)%f,     &
            rel_tol=1.0E-4_WP,                                          &
            message="Add component: Identical inv1%nf after update")
        call test%assertequal(inv1%mf%dist(i)%f, inv2%mf%dist(i)%f,     &
            rel_tol=1.0E-4_WP,                                          &
            message="Add component: Identical inv1%mf after update")
    end do

    call inv2%remove_component(id=gid_O2, dmass=ONE)

    do i = 1, size(inv1%nf%dist)
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv1%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv2%nf%dist(i)%f, nd1%dist(i)%f,         &
            rel_tol=1.0E-4_WP,                                          &
            message="Subtract component: Return to original inv2%nf")
        call test%assertequal(inv2%mf%dist(i)%f, md1%dist(i)%f,         &
            rel_tol=1.0E-4_WP,                                          &
            message="Subtract component: Return to original inv2%mf")
    end do

    ! inventory%extract_mixture()

    inv2 = inv1%extract_mixture(dmass=5.0_WP)
    do i = 1, size(inv1%species)
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv1%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv1%nf%dist(i)%f, inv2%nf%dist(i)%f,     &
            rel_tol=1.0E-4_WP,                                          &
            message="Extract mixture mass: Identical nf")
        call test%assertequal(inv1%mf%dist(i)%f, inv2%mf%dist(i)%f,     &
            rel_tol=1.0E-4_WP,                                          &
            message="Extract mixture mass: Identical mf")
        call test%assertequal(inv1%species(i)%mass,                     &
            inv2%species(i)%mass, rel_tol=1.0E-4_WP,                    &
            message="Extract mixture mass: Identical masses")
    end do

    inv2 = inv1%extract_mixture(dfraction=THIRD)
    do i = 1, size(inv1%species)
        ! write(unit=stderr, fmt='(A, I0, A, ES12.5)') 'Aerosol ',        &
        !     inv1%species(i)%id, ' inv2 mass = ', inv2%species(i)%mass
        call test%assertequal(inv1%nf%dist(i)%f, inv2%nf%dist(i)%f,     &
            rel_tol=1.0E-4_WP,                                          &
            message="Extract mixture fraction: Identical nf")
        call test%assertequal(inv1%mf%dist(i)%f, inv2%mf%dist(i)%f,     &
            rel_tol=1.0E-4_WP,                                          &
            message="Extract mixture fraction: Identical mf")
        call test%assertequal(inv1%species(i)%mass,                     &
            TWO * inv2%species(i)%mass, rel_tol=1.0E-4_WP,              &
            message="Extract mixture fraction: "                        &
                // "inv1%mass = 2 * inv2%mass")
    end do

    call inv1%dump()

    ! ! Print summary and reset
    ! call printsummary(test)
    ! call test%reset()

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_inventory.json")

    call test%checkfailure()
end program c1_inventory
