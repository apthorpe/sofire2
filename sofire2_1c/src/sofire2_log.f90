!> @file sofire2_log.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Provides syslog-style log facilities as an abstraction layer
!! above the lower-level log management system
!!
!! Log management is implemented by m_multilog from the FLIBS
!! distribution; see http://flibs.sourceforge.net/
module sofire2_log
    use m_multilog, only: log_t

    implicit none

    private

    public :: log_all
    public :: log_panic
    public :: log_alert
    public :: log_critical
    public :: log_error
    public :: log_warning
    public :: log_notice
    public :: log_info
    public :: log_debug

    public :: initialize_logs
    public :: finalize_logs

    !> Diagnostic log object
    type(log_t) :: l_diag

    !> Warning/error log object
    type(log_t) :: l_warn

contains

!> Create and initialize diagnostic logs
subroutine initialize_logs(BASENAME, CODENAME, VERSION, BUILDDATE,      &
    file_manifest)
    use sofire2_manifest, only: manifest_t
    use m_textio, only: munch
    use m_multilog, only: add_log, log_group_configure, log_msg,        &
        LOG_SV_DEBUG => DEBUG,                                          &
        LOG_SV_WARNING => WARNING,                                      &
        LOG_SV_ALL => ALL

    implicit none

    !> @brief Scenario name
    !!
    !! Tag indicating a particular run or case associated with the logs;
    !! this string is used as a prefix to the log file name to simplify
    !! identification
    character (len=*), intent(in) :: BASENAME

    !> Software name
    character (len=*), intent(in) :: CODENAME

    !> Software version
    character (len=*), intent(in) :: VERSION

    !> Software build date
    character (len=*), intent(in) :: BUILDDATE

    !> File manifest for tracking archivable and generated files
    type(manifest_t), intent(inout), optional :: file_manifest

    character (len=:), allocatable :: basename_a
    character (len=:), allocatable :: ofn

    continue

    basename_a = munch(BASENAME)
    if (.not. allocated(basename_a)) then
        basename_a = 'scenario'
    else if (len(basename_a) < 1) then
        basename_a = 'scenario'
    end if

    ofn = basename_a // '_diagnostic.log'
    call l_diag%startup(ofn, append=.false., info_lvl=LOG_SV_DEBUG)
    ! Add ofn to manifest - GENERATED
    if (present(file_manifest)) then
        ! Add ofn to manifest - GENERATED
        call file_manifest%add(filename=ofn, archivable=.false.,        &
            generated=.true.)
    end if

    ofn = basename_a // '_warning.log'
    call l_warn%startup(ofn, append=.false., info_lvl=LOG_SV_WARNING)
    if (present(file_manifest)) then
        ! Add ofn to manifest - GENERATED
        call file_manifest%add(filename=ofn, archivable=.false.,        &
            generated=.true.)
    end if

    call add_log(l_diag)
    call add_log(l_warn)

    call log_group_configure('timestamp', .true.)

    ! By default logs do not echo to stdout. Still, ensure it's disabled.
    call l_diag%configure('writeonstdout', .false.)
    call l_warn%configure('writeonstdout', .false.)

    call log_msg('Starting ' // CODENAME // ', ' // VERSION             &
                 // ', created on ' // BUILDDATE, LOG_SV_ALL)

    call log_msg('Opening all log facilities.', LOG_SV_ALL)

    if (len_trim(BASENAME) > 0) then
        call log_msg('Running scenario ' // basename_a)
    end if

    return
end subroutine initialize_logs

!> Gracefully close open log files
subroutine finalize_logs()
    use m_multilog, only: log_msg, shutdown_log_group,                  &
        LOG_SV_ALL => ALL
    continue

    call log_msg('Closing all log facilities.', LOG_SV_ALL)
    call shutdown_log_group()

    return
end subroutine finalize_logs

!> Write message to all log facilities
subroutine log_all(msg)
    use m_multilog, only: log_msg, LOG_SV_ALL => ALL

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_ALL)

!    write(*,*) 1.0 / 0.0

    return
end subroutine log_all

!> Write message to panic log
subroutine log_panic(msg)
    use m_multilog, only: log_msg, LOG_SV_PANIC => ALL

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_PANIC)

    return
end subroutine log_panic

!> Write message to alert log
subroutine log_alert(msg)
    use m_multilog, only: log_msg, LOG_SV_ALERT => WARNING

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_ALERT)

    return
end subroutine log_alert

!> Write message to critical log
subroutine log_critical(msg)
    use m_multilog, only: log_msg, LOG_SV_CRITICAL => ERROR

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_critical)

    return
end subroutine log_critical

!> Write message to error log
subroutine log_error(msg)
    use m_multilog, only: log_msg, LOG_SV_ERROR => ERROR

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_ERROR)

    return
end subroutine log_error

!> Write message to warning log
subroutine log_warning(msg)
    use m_multilog, only: log_msg, LOG_SV_WARNING => WARNING

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_WARNING)

    return
end subroutine log_warning

!> Write message to notice log
subroutine log_notice(msg)
    use m_multilog, only: log_msg, LOG_SV_NOTICE => INFO

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_NOTICE)

    return
end subroutine log_notice

!> Write message to info log
subroutine log_info(msg)
    use m_multilog, only: log_msg, LOG_SV_INFO => INFO

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_INFO)

    return
end subroutine log_info

!> Write message to debug log
subroutine log_debug(msg)
    use m_multilog, only: log_msg, LOG_SV_DEBUG => DEBUG

    implicit none

    !> Log message
    character (len=*), intent(in) :: msg

    continue

    call log_msg(msg, LOG_SV_DEBUG)

    return
end subroutine log_debug

end module sofire2_log
