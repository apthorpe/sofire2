# Retrieve zofu from external source and include into project
#
# The following output variables are set by the ZOFU subproject:
# - ??? ZOFU_LIB_NAME
# - ??? ZOFU_LIBRARY_DIR
# - ??? ZOFU_MODULE_DIR
set(ZOFU_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/ZOFU-source")

FetchContent_Declare(
    ZOFU_external
    GIT_REPOSITORY         https://github.com/acroucher/zofu.git
    GIT_TAG                v1.1.0
    SOURCE_DIR             "${ZOFU_SOURCE_DIR}"
)

FetchContent_MakeAvailable(ZOFU_external)
FetchContent_GetProperties(ZOFU_external)

set(ZOFU_FOUND "${ZOFU_external_POPULATED}")
# __END__