#!/usr/bin/gnuplot
# vim: set expandtab: set syntax=gnuplot: set nowrap: set noai: set tw=132

set macro

##################################################################
# Load convenience symbols, functions, and variables
load '_sym.gp'
load '_general_functions.gp'

##################################################################
# Set code version and plot timestamp
load '_code_version.gp'
tsfmt = code_version . " %Y%m%d%H%M%S"

casename = 'C1 Benchmark'
outbase = 'C1_benchmark'

##################################################################
# Select output terminal

## Uncomment for png output
#L set terminal pngcairo enhanced mono size 9in, 7in dashed linewidth 1 rounded
#P set terminal pngcairo enhanced mono size 7in, 9in dashed linewidth 1 rounded
# ext = 'png'

## Uncomment for pdf output
# set terminal pdfcairo enhanced mono size 11in, 8.5in dashed linewidth 3 rounded
# set terminal pdfcairo enhanced mono size 5in, 7in dashed linewidth 3 rounded
# set terminal pdfcairo enhanced mono size 5in, 5in dashed linewidth 4 rounded
set terminal pdfcairo enhanced mono size 5in, 7in dashed linewidth 2 rounded
ext = 'pdf'

##################################################################
# Select font face
fface = "Cabin"
load('_' . ext . '_font_sizes.gp')

set font nfont
set title font hfont # or tfont?
set ylabel font nfont
set xlabel font nfont
set key font kfont

##################################################################
# Set plot files and variable names
load 'C1_benchmark_plotvar.gp'

# For CSV files
set datafile separator ','

set key autotitle columnhead # use the first line as key

set rmargin 5

set grid back
set tics back

set timestamp tsfmt rotate font tinyfont

set style data lines

set xlabel 'Time, hour' font nfont
set xrange [0.0 : 1.2]
set xtics 0.2
set mxtics 4

pltnum = 0
########################################################################

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Test 4 - Gas Pressure"

set ylabel 'Change in Vessel Pressure, psig' font nfont
unset logscale y
set yrange [0.0 : 3.0]
set ytics 0.5
set mytics 5
set format y "%0.1f"

plot plotfile2 using (column(c2_TIME)):(column(c2_DP)) title "Experimental Data - Test 4" with points pointtype 6, \
     plotfile14 using (column(c1_T)):((column(c1_PGASC) - 2548.0)/144.0) title "SOFIRE 2" with lines, \
     plotfile2a using (column(c2_TIME)):(column(c2_DP)) title "Expected" with lines linetype 6

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Test 4 - Sodium Burning Rate"

set ylabel 'Sodium Burning Rate, lbm/hr-ft^2' font nfont
unset logscale y
set yrange [0.0 : 10.0]
set ytics 2.0
set mytics 4
set format y "%0.0f"

plot plotfile3 using (column(c3_TIME)):(column(c3_XM)) title "Experimental Data - Test 4" with points pointtype 6, \
     plotfile14 using (column(c1_T)):(column(c1_XM)) title "SOFIRE 2" with lines, \
     plotfile3a using (column(c3_TIME)):(column(c3_XM)) title "Expected" with lines linetype 6

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Test 5 - Gas Pressure"

set xlabel 'Time, minutes' font nfont
set xrange [0.0 : 120.0]
set xtics 20.0
set mxtics 4

set ylabel 'Change in System Pressure, psig' font nfont
unset logscale y
set yrange [0.0 : 2.0]
set ytics 0.5
set mytics 5
set format y "%0.1f"

plot plotfile4 using (column(c4_TIME)):(column(c4_DP)) title "Experimental Data - Test 5" with points pointtype 6, \
     plotfile15 using (60.0 * column(c1_T)):((column(c1_PGASC) - 2577.0)/144.0) title "SOFIRE 2" with lines, \
     plotfile4a using (column(c4_TIME)):(column(c4_DP)) title "Expected" with lines linetype 6

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Test 5 - Sodium Burning Rate"

set ylabel 'Sodium Burning Rate, lbm/hr-ft^2' font nfont
unset logscale y
set yrange [0.0 : 6.0]
set ytics 2.0
set mytics 4
set format y "%0.0f"

plot plotfile5 using (column(c5_TIME)):(column(c5_XM)) title "Experimental Data - Test 5" with points pointtype 6, \
     plotfile15 using (60.0 * column(c1_T)):(column(c1_XM)) title "SOFIRE 2" with lines, \
     plotfile5a using (column(c5_TIME)):(column(c5_XM)) title "Expected, 100% Na_2O" with lines linetype 6, \
     plotfile5b using (column(c5_TIME)):(column(c5_XM)) title "Expected, 100% Na_2O_2" with lines linetype 8

set xlabel 'Time, minutes' font nfont
set xrange [0.0 : 65.0]
set xtics 10.0
set mxtics 10

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Test 6 - Gas Pressure"

set ylabel 'System Pressure Rise, psig' font nfont
unset logscale y
set yrange [0.0 : 0.8]
set ytics 0.2
set mytics 4
set format y "%0.1f"

plot plotfile6 using (column(c6_TIME)):(column(c6_DP)) title "Experimental" with points pointtype 8, \
     plotfile16 using (60.0 * column(c1_T)):((column(c1_PGASC) - 2634.6)/144.0) title "SOFIRE 2" with lines linetype 1, \
     plotfile6a using (column(c6a_TIME)):(column(c6a_DP_BURN)) title "Expected, With Burn" with lines linetype 6, \
     plotfile6a using (column(c6a_TIME)):(column(c6a_DP_NOBURN)) title "Expected, Without Burn" with lines linetype 8

pltnum = pltnum + 1
set output outfile(pltnum, outbase, ext)

set title casename . ": Test 6 - Sodium Surface Temperature"

set xlabel 'Time, minutes' font nfont
set xrange [0.0 : 60.0]
set xtics 10.0
set mxtics 10

set ylabel 'Sodium Pool Temperature, ' . uDEGF font nfont
unset logscale y
set yrange [300.0 : 900.0]
set ytics 100.0
set mytics 10

plot plotfile7 using (column(c7_TIME)):(column(c7_TS)) title "Experimental" with points pointtype 2, \
     plotfile16 using (60.0 * column(c1_T)):(RtoF(column(c1_TS))) title "SOFIRE 2" with lines, \
     plotfile7b using (column(c7_TIME)):(column(c7_TS)) title "Expected, With Burn" with lines linetype 6, \
     plotfile7a using (column(c7_TIME)):(column(c7_TS)) title "Expected, Without Burn" with lines linetype 8
