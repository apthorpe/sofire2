!> @file c1_manifest.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for sofire2_manifest module.
program c1_manifest
    use iso_fortran_env, only: stdout => OUTPUT_UNIT,                   &
        stderr => ERROR_UNIT
    use sofire2_manifest, only: manifest_t, new_manifest_node
    use m_textio, only: munch
    use m_format, only: fmt_a
    use toast
    implicit none

    character (len=80) :: t1
    character (len=80) :: t2
    character (len=:), allocatable :: c1
    character (len=:), allocatable :: c2

    logical :: tf1
    logical :: tf2

    type(manifest_t) :: manifest

    type(TestCase) :: test

    continue

    call test%init(name="c1_manifest")

    call manifest%init()

    ! Demonstrate empty dump (should not crash)
    call manifest%dump()

    write(unit=stdout, fmt=fmt_a) '1. Adding case.inp A_'
    call manifest%add('case.inp', archivable=.true.,                    &
        generated=.false.)
    write(unit=stdout, fmt=fmt_a) '2. Adding thermo.inp A_'
    call manifest%add('thermo.inp', archivable=.true.)
    write(unit=stdout, fmt=fmt_a) '3. Adding trans.inp A_'
    call manifest%add('trans.inp')
    write(unit=stdout, fmt=fmt_a) '4. Adding case_diagnostic.log _G'
    call manifest%add('case_diagnostic.log', archivable=.false.,        &
        generated=.true.)
    write(unit=stdout, fmt=fmt_a) '5. Adding case_warning.log _G'
    call manifest%add('case_warning.log', archivable=.false.,           &
        generated=.true.)
    write(unit=stdout, fmt=fmt_a) '6. Adding case.csv AG'
    call manifest%add('case.csv', archivable=.true.,                    &
        generated=.true.)
    ! This file should be ignored (not added); both archivable and
    ! generated are false.
    write(unit=stdout, fmt=fmt_a) '7. Ignoring case_ci_instructions.txt __'
    call manifest%add('case_ci_instructions.txt', archivable=.false.,   &
        generated=.false.)

    ! Demonstrate dump
    call manifest%dump()

    ! Write generated files to stderr, write archivable files to stdout
    write(unit=stdout, fmt=fmt_a) '*** List of files to save'
    write(unit=stderr, fmt=fmt_a) '*** List of files to delete'
    call manifest%write_to_unit(archivable_unit=stdout,                 &
        generated_unit=stderr)

    ! tf = .faLSE.
    ! call test%asserttrue(ltoa(tf) == c1, message="ltoa(.faLSE.)")

    ! call test%assertequal(this, that,         &
    !     rel_tol=0.01_WP, message="k(O2(g)) T-upper = 15000 K +-1%")

    ! ! Print summary and reset
    ! call printsummary(test)
    ! call test%reset()

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_manifest.json")

    call test%checkfailure()
end program c1_manifest
