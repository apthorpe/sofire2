This EasyBuild configuration was contributed by Lars Viklund and
demonstrates how SOFIRE may be built and managed in an HPC environment.
See https://github.com/easybuilders/easybuild for details on EasyBuild