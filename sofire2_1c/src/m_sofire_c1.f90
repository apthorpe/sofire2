!> @file m_sofire_c1.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Library routines which implement the body of the one-cell
!! SOFIRE II application
module m_sofire_c1
    use sofire2_param, only: WP

    implicit none

    private

    public :: set_initial_conditions
    public :: calculate_single_timestep
    public :: write_new_model_state
    public :: q_radiation_r
    public :: calculate_burning_rate

contains

!> Calculate mass and distribution of combustion products by species
!! and suspended/deposited status
subroutine distribute_combustion_products(ANA, OXLB, burn_cell, crust)
!    use iso_fortran_env, only: stderr => error_unit
    use sofire2_param, only: faer_naox
    use m_constant, only: ZERO, HALF, ONE, TWO
    use m_material, only: material_db, gid_O2, aid_Na, aid_Na2O,        &
        aid_Na2O2
    use m_inventory, only: parcel
    use m_cell, only: cell
    use m_burnchem, only: k_from_s, yna2o_from_k
    implicit none

    !> Stoichiometric combustion ratio, total mass of all oxide species
    !! produced per mass of oxygen consumed, dimensionless
    real(kind=WP), intent(in) :: ANA

    !> Mass of oxygen consumed by combustion, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: OXLB

    !> Cell object
    class(cell), intent(inout) :: burn_cell

    !> Sodium pool surface ("crust") chemical inventory
    class(parcel), intent(inout) :: crust

    ! Formula weight of constituents
    real(kind=WP) :: mw_o2
    real(kind=WP) :: mw_na
    real(kind=WP) :: mw_na2o
    real(kind=WP) :: mw_na2o2
    ! real(kind=WP) :: mw_naox

    ! mass of all produced oxide species
    real(kind=WP) :: m_naox
    ! ! aggregate moles of all produced oxide species
    ! real(kind=WP) :: n_naox

    ! mole fraction of Na2O produced
    real(kind=WP) :: y_na2o
    ! mole fraction of Na2O2 produced
    real(kind=WP) :: y_na2o2

    ! fraction of oxides in aerosol form
    real(kind=WP) :: fsusp

    ! Moles of O2 consumed
    real(kind=WP) :: n_o2
    ! Moles of Na consumed
    real(kind=WP) :: n_na
    ! Moles of Na2O produced
    real(kind=WP) :: n_na2o
    ! Moles of Na2O2 produced
    real(kind=WP) :: n_na2o2

    ! mass of Na consumed
    real(kind=WP) :: m_na
    ! mass of Na2O produced
    real(kind=WP) :: m_na2o
    ! mass of Na2O2 produced
    real(kind=WP) :: m_na2o2

    ! Suspended mass of Na2O produced based on SOFIRE II correlation
    real(kind=WP) :: ma_naox_def

    ! ! suspended mass of all oxides produced
    ! real(kind=WP) :: ma_naox
    ! suspended mass of Na2O produced
    real(kind=WP) :: ma_na2o
    ! suspended mass of Na2O2 produced
    real(kind=WP) :: ma_na2o2

    ! deposited mass of Na2O produced
    real(kind=WP) :: md_na2o
    ! deposited mass of Na2O2 produced
    real(kind=WP) :: md_na2o2

    continue

    ! if (OXLB <= ZERO) then
    !     return
    ! end if

    ! Constants:
    ! Formula weight of O2
    mw_o2 = material_db%gas(gid_O2)%mw
    ! Formula weight of Na
    mw_na = material_db%aerosol(aid_Na)%mw
    ! Formula weight of Na2O
    mw_na2o = material_db%aerosol(aid_Na2O)%mw
    ! Formula weight of Na2O2
    mw_na2o2 = material_db%aerosol(aid_Na2O2)%mw

    ! Input:
    ! Mass of consumed O2: OXLB
    ! Mass of consumed Na per mass of consumed O2: ANA

    ! Moles of O2 consumed
    n_o2 = OXLB / mw_o2

    ! Mass of consumed Na
    m_na = ANA * OXLB

    ! Moles of Na consumed
    n_na = m_na / mw_na

    ! Mole fraction of Na2O produced
    y_na2o = yna2o_from_k(k_from_s(ANA))

    ! Mole fraction of Na2O2 produced
    y_na2o2 = ONE - y_na2o

    ! Moles of Na2O produced
    n_na2o = HALF * n_na * y_na2o

    ! Mass of Na2O produced
    m_na2o = n_na2o * mw_na2o

    ! Moles of Na2O2 produced
    n_na2o2 = HALF * n_na * y_na2o2

    ! Mass of Na2O2 produced
    m_na2o2 = n_na2o2 * mw_na2o2

    ! ! Aggregate formula weight of produced oxides - not used
    ! mw_naox = y_na2o * mw_na2o + y_na2o2 * mw_na2o2

    ! ! Aggregate moles of all produced oxide species - not used
    ! n_naox = n_na2o + n_na2o2

    ! Aggregate mass of all produced oxide species
    m_naox = m_na2o + m_na2o2

    ! Total suspended mass of oxides produced
    ma_naox_def = m_na * faer_naox

    ! Fraction of oxides generated in aerosols form
    if (m_naox > ZERO) then
        fsusp = ma_naox_def / m_naox
    else
        fsusp = ZERO
    endif

    ! Suspended mass of Na2O produced
    ma_na2o = fsusp * m_na2o

    ! Suspended mass of Na2O2 produced
    ma_na2o2 = fsusp * m_na2o2

    ! Deposited mass of Na2O produced
    md_na2o = (ONE - fsusp) * m_na2o

    ! Deposited mass of Na2O2 produced
    md_na2o2 = (ONE - fsusp) * m_na2o2

    ! --------------- Diagnostics ---------------

    ! write(6, '("Mass defect: m_na + m_o2 - m_na2o - m_na2o2: ", ES16.8)') &
    !     (m_na + OXLB) - (m_na2o + m_na2o2)
    ! write(6, '("Mole defect, Na: n_na - 1/2 (n_na2o + n_na2o2): ", ES16.8)') &
    !     n_na - HALF * (n_na2o + n_na2o2)
    ! write(6, '("Mole defect, O2: n_o2 - (1/2 n_na2o + n_na2o2): ", ES16.8)') &
    !     n_o2 - (HALF * n_na2o + n_na2o2)

    ! ! write(6, '("fsusp: ", ES16.8)') fsusp
    ! write(6, '("m_na: ", ES16.6, ", ma_naox_def: ", ES16.8, ", ma_na2o: ", ES16.8, ", faer_naox: ", ES16.8, ", fsusp: ", ES16.8)') &
    ! m_na, ma_naox_def, ma_na2o, faer_naox, fsusp

    ! --------------- Update cell and crust composition ---------------

    ! ! Remove reactant O2 from cell gas - handled upstream
    ! call burn_cell%gas%remove_component_mass(gid_O2, OXLB)

    ! Add aerosol product mass to cell aerosol inventory:
    burn_cell%aerosol%species(aid_NA2O)%mass =                          &
        burn_cell%aerosol%species(aid_NA2O)%mass + ma_na2o
    burn_cell%aerosol%species(aid_NA2O2)%mass =                         &
        burn_cell%aerosol%species(aid_NA2O2)%mass + ma_na2o2

    ! Update mass and mole fractions in aerosol
    call burn_cell%aerosol%update_distributions()
    ! burn_cell%aerosol%mf = burn_cell%aerosol%as_mf()
    ! burn_cell%aerosol%nf = burn_cell%aerosol%mf%as_nf()

    ! ! Remove reactant Na from crust - handled upstream
    ! crust%species(aid_NA)%mass = crust%species(aid_NA)%mass + m_na

    ! Add deposited product mass to cell aerosol inventory:
    crust%species(aid_NA2O)%mass = crust%species(aid_NA2O)%mass         &
        + md_na2o
    crust%species(aid_NA2O2)%mass = crust%species(aid_NA2O2)%mass       &
        + md_na2o2

    return
end subroutine distribute_combustion_products

!> Set beginning-of-timestep values for system state at beginning of run
subroutine set_initial_conditions(scenario, result_curr, cell_curr,     &
    crust_curr, dt_manager, hs_wall, hs_floor, hs_na_pool, hs_na_crust, &
    qcond_path, qconv_path, qrad_path, mf_dry_air, legacy_rgas)
    use iso_fortran_env, only: stderr => error_unit
    use sofire2_param, only: n_hs_wall, n_hs_floor, n_hs_na_pool,       &
        n_qcond_path, n_qconv_path, n_qrad_path
    use m_constant, only: ZERO, RGAS_US, FTLBF_BTU, MW_AIR
    use m_dataframe, only: result_dataframe, scenario_dataframe,        &
        timestep_dataframe, heatsink_dataframe, qcond_dataframe,        &
        qconv_dataframe, qrad_dataframe
    use m_material, only: gid_O2, aid_Na
    use m_inventory, only: material_dist, parcel
    use m_cell, only: cell

    implicit none

    !> Scenario object
    type(scenario_dataframe), intent(inout) :: scenario

    !> Current problem state
    type(result_dataframe), intent(inout) :: result_curr

    !> Current cell state
    type(cell), intent(inout) :: cell_curr

    !> Current chemical inventory in sodium pool surface (crust)
    type(parcel), intent(inout) :: crust_curr

    !> Timestep manager
    type(timestep_dataframe), intent(inout) :: dt_manager

    !> List of wall heatsink objects
    type(heatsink_dataframe), dimension(n_hs_wall), intent(inout) ::    &
        hs_wall

    !> List of floor heatsink objects
    type(heatsink_dataframe), dimension(n_hs_floor), intent(inout) ::   &
        hs_floor

    !> List of sodium pool heatsink objects
    type(heatsink_dataframe), dimension(n_hs_na_pool), intent(inout) :: &
        hs_na_pool

    !> Sodium pool surface (crust) object
    type(heatsink_dataframe), intent(inout) :: hs_na_crust

    !> List of conduction heat transfer path objects
    type(qcond_dataframe), dimension(n_qcond_path), intent(inout) ::    &
        qcond_path

    !> List of convection heat transfer path objects
    type(qconv_dataframe), dimension(n_qconv_path), intent(inout) ::    &
        qconv_path

    !> List of radiation heat transfer path objects
    type(qrad_dataframe), dimension(n_qrad_path), intent(inout) ::      &
        qrad_path

    !> Mass distribution of dry air
    type(material_dist), intent(in) :: mf_dry_air

    !> Flag indicating legacy gas constant (for dry air, full O2
    !! concentration) should be used, optional
    logical, intent(in), optional :: legacy_rgas

    logical :: use_legacy_rgas
    real(kind=WP) :: rgas_curr

    continue

    use_legacy_rgas = .true.
    if (present(legacy_rgas)) then
        use_legacy_rgas = legacy_rgas
    end if

    ! New cell and species model
    ! Crust 'parcel' (aerosol/condensed)
    crust_curr%species(aid_NA)%mass = scenario%A1 * scenario%ZNAS   &
        * scenario%RHS

    ! Gas cell (primary)
    cell_curr%name = 'Primary burn cell'
    cell_curr%p = scenario%PGASCI
    cell_curr%v = scenario%VOLAC
    cell_curr%t = scenario%TGASCI

    ! call mf_dry_air%dump()

    cell_curr%gas%mf = mf_dry_air

    cell_curr%gas%name = 'Cell, dry air, initial 4.4w% O2'

    ! call cell_curr%gas%mf%dump()
    call cell_curr%gas%mf%scale_to_fraction(gid_O2, scenario%CO)
    ! call cell_curr%gas%mf%dump()

!    call cell_curr%gas%mf%normalize()

    ! call cell_curr%gas%mf%dump()

    cell_curr%gas%nf = cell_curr%gas%mf%as_nf()

    ! write(unit=stderr, fmt='("Actual vs default gas MW: ", '        &
    !     // 'ES12.5, " vs ", ES12.5, ", ratio = ", ES12.5)')         &
    !     cell_curr%gas%nf%mw(), MW_AIR,                              &
    !     cell_curr%gas%nf%mw() / MW_AIR

    ! call cell_curr%gas%nf%dump()

    call cell_curr%set_ideal_gas_mass()
    ! Note: For compatibility and testing:
    ! W = cell_curr%mass()
    !   = cell_curr%gas%mass() + cell_curr%aerosol%mass()
    ! TGASC = cell_curr%t
    ! PGASC = cell_curr%p
    ! C = cell_curr%gas%mf%dist(gid_O2)%f
    !   = cell_curr%mf_o2()
    ! RHOC = cell_curr%mixture_density()

    ! Set heat sink properties from scenario input
    call scenario%populate_heatnet(hs_na_crust, hs_na_pool,         &
        hs_floor, hs_wall, qcond_path, qconv_path, qrad_path)

    !> /todo Replace scenario with initial gas space temperature
    call result_curr%init_temperatures(scenario, hs_na_crust,       &
        hs_na_pool, hs_floor, hs_wall)

    result_curr%SUM1 = ZERO ! Preinitialized
    result_curr%SUM2 = ZERO ! Preinitialized
    result_curr%F40 = ZERO ! Preinitialized

    result_curr%XM = scenario%XMI
    result_curr%PGASC = scenario%PGASCI

    if (use_legacy_rgas) then
        rgas_curr = RGAS_US * FTLBF_BTU / MW_AIR
    else
        rgas_curr = RGAS_US * FTLBF_BTU / cell_curr%gas%mw()
    end if

    result_curr%W1 = ZERO
    result_curr%W2 = ZERO
    result_curr%W = scenario%PGASCI * scenario%VOLAC                &
        / (rgas_curr * scenario%TGASCI)

    result_curr%RHOC = result_curr%W / scenario%VOLAC
    result_curr%C = scenario%CO
    result_curr%OX = result_curr%W * result_curr%C

    call result_curr%update_qconv(qconv_path, result_curr%TS,       &
        result_curr%TGASC, result_curr%TWC1)
    call result_curr%update_yconv()

    call result_curr%update_qrad(qrad_path,                         &
        result_curr%TF1, result_curr%TF2,                           &
        result_curr%TWC1, result_curr%TWC2,                         &
        result_curr%TS, result_curr%TGASC)
    call result_curr%update_yrad()

    result_curr%DT = ZERO
    result_curr%DTMIN = ZERO
    result_curr%DTMIN1 = ZERO
    result_curr%DTMIN2 = ZERO

    call dt_manager%update(                                         &
        scenario, result_curr%RHOC,                                 &
        hs_wall, hs_floor, hs_na_crust, hs_na_pool, qcond_path,     &
        result_curr%YCONV1, result_curr%YCONV2,                     &
        result_curr%YROD, result_curr%YRAD1,                        &
        result_curr%YRAD2, result_curr%YRAD3)

    result_curr%T = scenario%TI
    return
end subroutine set_initial_conditions

!> Set end-of-timestep values for system state based on previous state
subroutine calculate_single_timestep(scenario, result_curr,             &
    result_prev, cell_curr, cell_prev, crust_curr, dt_manager, hs_wall, &
    hs_floor, hs_na_pool, hs_na_crust, qcond_path, qconv_path,          &
    qrad_path, PGASC_adjusted, TGASC_adjusted, mf_dry_air, prtct,       &
    errmsg, EXX_error)
    use sofire2_param, only: n_hs_wall, n_hs_floor, n_hs_na_pool,       &
        n_qcond_path, n_qconv_path, n_qrad_path
    use m_constant, only: ZERO, ONE, INWC_PSF, MIN_HR
    use m_dataframe, only: result_dataframe, scenario_dataframe,        &
        timestep_dataframe, heatsink_dataframe, qcond_dataframe,        &
        qconv_dataframe, qrad_dataframe
    use m_material, only: gid_O2, aid_NA
    use m_inventory, only: material_dist, parcel,                       &
        new_gas_parcel, new_aerosol_parcel
    use m_cell, only: cell

    implicit none

    !> Scenario object
    type(scenario_dataframe), intent(in) :: scenario

    !> Current problem state
    type(result_dataframe), intent(inout) :: result_curr

    !> Previous problem state
    type(result_dataframe), intent(in) :: result_prev

    !> Current cell state
    type(cell), intent(inout) :: cell_curr

    !> Previous cell state
    type(cell), intent(in) :: cell_prev

    !> Current chemical inventory in sodium pool surface (crust)
    type(parcel), intent(inout) :: crust_curr

    !> Timestep manager
    type(timestep_dataframe), intent(inout) :: dt_manager

    !> List of wall heatsink objects
    type(heatsink_dataframe), dimension(n_hs_wall), intent(inout) ::    &
        hs_wall

    !> List of floor heatsink objects
    type(heatsink_dataframe), dimension(n_hs_floor), intent(inout) ::   &
        hs_floor

    !> List of sodium pool heatsink objects
    type(heatsink_dataframe), dimension(n_hs_na_pool), intent(inout) :: &
        hs_na_pool

    !> Sodium pool surface (crust) object
    type(heatsink_dataframe), intent(inout) :: hs_na_crust

    !> List of conduction heat transfer path objects
    type(qcond_dataframe), dimension(n_qcond_path), intent(inout) ::    &
        qcond_path

    !> List of convection heat transfer path objects
    type(qconv_dataframe), dimension(n_qconv_path), intent(inout) ::    &
        qconv_path

    !> List of radiation heat transfer path objects
    type(qrad_dataframe), dimension(n_qrad_path), intent(inout) ::      &
        qrad_path

    !> Adjusted previous timestep gas pressure
    real(kind=WP), intent(out) :: PGASC_adjusted

    !> Adjusted previous timestep gas temperature
    real(kind=WP), intent(out) :: TGASC_adjusted

    !> Mass distribution of dry air
    type(material_dist), intent(in) :: mf_dry_air

    !> Iteration ID (for diagnostics). May be safely set to zero if not known/needed
    integer, intent(in) :: prtct

    !> Error message
    character(len=400), intent(out) :: errmsg

    !> Error condition
    logical, intent(out) :: EXX_error

    ! Forced flow
    real(kind=WP) :: v_forced
    real(kind=WP) :: vf_forced
    real(kind=WP) :: mg_forced

    ! Replacement for result_*%W1
    type(parcel) :: w1a_curr
    type(parcel) :: w1g_curr

    ! Leakage
    real(kind=WP) :: dp_leak
    real(kind=WP) :: v_leak
    real(kind=WP) :: vf_leak
    real(kind=WP) :: mg_leak
    real(kind=WP) :: ma_leak

    ! Replacement for result_*%W2
    type(parcel) :: w2a_curr
    type(parcel) :: w2g_curr

    ! Reactant-limited mass of oxygen consumed (temporary; compensates
    ! for inventory differences in legacy and detailed mass model)
    real(kind=WP) :: OXLB_cell_model

    continue

    EXX_error = .false.
    errmsg = 'OK'

    TGASC_adjusted = result_prev%TGASC
    PGASC_adjusted = result_prev%PGASC

    call result_curr%init()

    ! Forced flow
    w1a_curr = new_aerosol_parcel()
    w1g_curr = new_gas_parcel()

    ! Leakage
    w2a_curr = new_aerosol_parcel()
    w2g_curr = new_gas_parcel()

    !*** BEGIN - Refactor into component of result_dataframe and
    !*** advance_timestep()
    call dt_manager%update(scenario, result_prev%RHOC, hs_wall,         &
        hs_floor, hs_na_crust, hs_na_pool, qcond_path,                  &
        result_prev%YCONV1, result_prev%YCONV2, result_prev%YROD,       &
        result_prev%YRAD1, result_prev%YRAD2, result_prev%YRAD3)

    result_curr%DTMIN1 = dt_manager%DELS
    result_curr%DTMIN2 = dt_manager%DELGC

    result_curr%DT = dt_manager%get_dt()

    result_curr%DTMIN = result_curr%DT * scenario%X

    result_curr%T = result_prev%T + result_curr%DTMIN
    !*** END - Refactor into component of result_dataframe

    !*** Parallel calculation to W1; needs testing
    ! Forced flow - F3 is positive for outflow,
    ! negative for inflow
    v_forced = scenario%F3 * MIN_HR * result_curr%DTMIN
    vf_forced = v_forced / cell_prev%v
    if (vf_forced < ZERO) then
        ! Forced inflow
        mg_forced = v_forced * scenario%RHOA
        w1g_curr%species(:)%mass = mg_forced * mf_dry_air%dist(:)%f
        w1a_curr%species(:)%mass = ZERO
        !> @todo Modify TGASC_adjusted and PGASC_adjusted
    else if (vf_forced < ONE) then
        ! By definition of F3, this is outflow so removed
        ! fraction of cell volume (vf_forced) must be < 1
        w1g_curr%species(:)%mass = cell_prev%gas%species(:)%mass * vf_forced
        w1a_curr%species(:)%mass = cell_prev%aerosol%species(:)%mass    &
            * vf_forced
    else
        ! Ignore - cannot remove more volume from a cell than is
        ! actually in the cell, especially not in a single
        ! timestep
    end if

    !*** Parallel calculation to W2; needs testing
    ! Leakage
    dp_leak = result_prev%PGASC - scenario%PA
    v_leak = scenario%CON * MIN_HR * result_curr%DTMIN                  &
        * sqrt(abs(dp_leak) * INWC_PSF)
    vf_leak = v_leak / cell_prev%v
    if (dp_leak > ZERO) then
        ! Outleakage
        mg_leak = v_leak * cell_prev%gas_density()
        w2g_curr%species(:)%mass = mg_leak * cell_prev%gas%mf%dist(:)%f
        ma_leak = v_leak * cell_prev%aerosol_density()
        w2a_curr%species(:)%mass =                                      &
            ma_leak * cell_prev%aerosol%mf%dist(:)%f
    else
        ! Inleakage
        mg_leak = v_leak * scenario%RHOA
        w2g_curr%species(:)%mass = mg_leak * mf_dry_air%dist(:)%f

        ! No ambient aerosols
        w2a_curr%species(:)%mass = ZERO
        !> @todo Modify TGASC_adjusted and PGASC_adjusted
    end if
    !> @todo Adjust cell_curr inventory, PGASC_adjusted, and
    !! TGASC_adjusted

    ! Update gas flows W1 and W2, cell gas and aerosol mass,
    ! O2 concentration and beginning-of-timestep gas pressure
    ! (PGASC_adjusted) and temperature (TGASC_adjusted)
    call result_curr%update_gas_flows(scenario, result_prev,            &
        PGASC_adjusted, TGASC_adjusted)

    ! Update convection heat transfer terms
    call result_curr%update_qconv(qconv_path, result_prev%TS,           &
        TGASC_adjusted, result_prev%TWC1)

    ! Update radiation heat transfer terms
    call result_curr%update_qrad(qrad_path, result_prev%TF1,            &
        result_prev%TF2, result_prev%TWC1, result_prev%TWC2,            &
        result_prev%TS, TGASC_adjusted)

    ! Update burning rate and reactant mass

    call result_curr%update_reactant_mass(PGASC_adjusted,               &
        TGASC_adjusted, scenario, result_prev, prtct, errmsg, EXX_error)

    if (EXX_error) then
        return
    end if

    !> @todo Adjust cell_curr inventory, PGASC_adjusted, and
    !! TGASC_adjusted, XM, QC from combustion and combine with
    !! "Remove Na and O2 consumed by fire" fragment

    ! Remove Na and O2 consumed by fire
    ! OXLB_cell_model is OXLB, reduced to mass of O2 in cell on
    ! oxygen starvation (rich conditions), or reduced to mass
    ! of Na in crust divided by stoiciometric combustion ratio
    ! m-Na/m-O2 on fuel starvation (lean conditions)
    OXLB_cell_model = min(                                              &
        result_curr%OXLB,                                               &
        cell_curr%gas%species(gid_O2)%mass,                             &
        crust_curr%species(aid_NA)%mass / scenario%ANA)

    if (OXLB_cell_model > ZERO                                          &
        .and. cell_curr%gas%species(gid_O2)%mass > ZERO                 &
        .and. crust_curr%species(aid_NA)%mass > ZERO) then
        call cell_curr%gas%remove_component(id=gid_O2,                  &
            dmass=OXLB_cell_model)
        crust_curr%species(aid_NA)%mass =                               &
            crust_curr%species(aid_NA)%mass                             &
            - OXLB_cell_model * scenario%ANA
    end if

    ! Code behavior has changed to consider pool heat sink mass changes to
    ! be isothermal and to adjust pool depth (slab thickness).
    !
    ! Mass added or removed from the pool has the same temperature as the
    ! current pool bulk temperature. Pool depth is adjusted to maintain
    ! consistent density.
    call hs_na_pool(1)%add_isothermal_mass(scenario%S1 * result_curr%DTMIN)
    call hs_na_pool(2)%add_isothermal_mass(scenario%S2 * result_curr%DTMIN)
    call hs_na_pool(3)%add_isothermal_mass(scenario%S3 * result_curr%DTMIN)
    call hs_na_pool(4)%add_isothermal_mass(scenario%S4 * result_curr%DTMIN)

    call result_curr%update_temperatures(scenario, result_prev,         &
        hs_na_crust, hs_na_pool, hs_floor, hs_wall, qcond_path,         &
        TGASC_adjusted)

    call result_curr%update_yconv()
    call result_curr%update_yrad()

    call result_curr%update_final_gas_composition(scenario)

    call distribute_combustion_products(scenario%ANA, OXLB_cell_model,  &
        cell_curr, crust_curr)

    return
end subroutine calculate_single_timestep

!> Write selected contents of cell and sodium pool surface (crust) to
!! stdout
subroutine write_new_model_state(test_cell, crust)
    use iso_fortran_env, only: stdout => output_unit
    use m_material, only: gid_O2, aid_Na, aid_Na2O, aid_Na2O2
    use m_inventory, only: parcel, material_dist
    use m_cell, only: cell
    implicit none

    !> Cell object
    type(cell), intent(in) :: test_cell

    !> Crust 'parcel' object
    type(parcel), intent(in) :: crust

    continue

    write(stdout, '("m_cell        = ", ES16.8)')                &
        test_cell%mass()
    write(stdout, '("mg_cell       = ", ES16.8)')                &
        test_cell%gas%mass()
    write(stdout, '("ma_cell       = ", ES16.8)')                &
        test_cell%aerosol%mass()
    write(stdout, '("rho_cell      = ", ES16.8)')                &
        test_cell%mixture_density()
    write(stdout, '("mfg(O2)       = ", ES16.8)')                &
        test_cell%gas%mf%dist(gid_O2)%f
    write(stdout, '("nfg(O2)       = ", ES16.8)')                &
        test_cell%gas%nf%dist(gid_O2)%f
    write(stdout, '("mg(O2)        = ", ES16.8)')                &
        test_cell%gas%species(gid_O2)%mass
    write(stdout, '("ma(Na2O)      = ", ES16.8)')                &
        test_cell%aerosol%species(aid_NA2O)%mass
    write(stdout, '("ma(Na2O2)     = ", ES16.8)')                &
        test_cell%aerosol%species(aid_NA2O2)%mass

    write(stdout, '("mcrust(Na)    = ", ES16.8)')                &
        crust%species(aid_NA)%mass
    write(stdout, '("mcrust(Na2O)  = ", ES16.8)')                &
        crust%species(aid_NA2O)%mass
    write(stdout, '("mcrust(Na2O2) = ", ES16.8)')                &
        crust%species(aid_NA2O2)%mass
    write(stdout, *)

    return
end subroutine write_new_model_state

!> Radiative heat transferred from emitter to receiver in
!! units of \f$\us{\BTU\per\hour}\f$. The value is positive
!! for an emitter which is hotter than the receiver.
elemental real(kind=WP) function q_radiation_r(emiss, area, tsrc, tdest)  &
    result(q)
    use m_constant, only: SIGMA_SB_US
    implicit none

    !> Emissivity, dimensionless
    real(kind=WP), intent(in) :: emiss

    !> Emitter surface area, \f$\us{\square\foot}\f$
    real(kind=WP), intent(in) :: area

    !> Emitter surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: tsrc

    !> Receiver surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: tdest

    continue

    q = area * emiss * SIGMA_SB_US * (tsrc**4 - tdest**4)

    return
end function q_radiation_r

!> Calculate burning rate and other combustion results
subroutine calculate_burning_rate(M_SODIUM, ANA, APOOL, RHO_OX, T1,     &
    EXX, DTMIN, XM, OXLB)
    use sofire2_param, only: min_sodium_mass
    use m_constant, only: ZERO, TENTH, THIRD
    implicit none

    !> Mass of sodium available to burn, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: M_SODIUM

    !> Stoichiometric sodium combustion ratio \f$\us{\lbm\-\ce{Na}\per\lbm-\ce{O2}}\f$
    real(kind=WP), intent(in) :: ANA

    !> Burning pool surface area, \f$\us{\square\foot}\f$
    real(kind=WP), intent(in) :: APOOL

    !> Oxygen density in cell (mass of oxygen in cell divided by total cell
    !! gas volume), \f$\us{\lbm\per\cubic\foot}-\ce{O2}\f$
    real(kind=WP), intent(in) :: RHO_OX

    !> Film temperature between sodium pool surface and cell gas, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: T1

    !> TBD, Not well defined turbulent natural convection parameter
    real(kind=WP), intent(in) :: EXX

    !> Timestep, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: DTMIN

    !> Sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
    real(kind=WP), intent(out) :: XM

    !> Mass of oxygen burned during timestep, \f$\us{\lbm}\f$
    real(kind=WP), intent(out) :: OXLB

    real(kind=WP) :: DIFF
    real(kind=WP) :: HF

    continue

    XM = ZERO
    OXLB = ZERO

    ! If more than 0.1 lbm unburned sodium remains
    if (M_SODIUM > min_sodium_mass) then
        DIFF = 241.57_WP / (132.0_WP + T1 / 1.8_WP)                     &
            * (T1 / 493.2_WP)**2.5_WP
        ! HF = 0.075_WP * DIFF * EX1
        HF = 0.075_WP * DIFF * EXX**THIRD
        XM = ANA * HF * RHO_OX
        OXLB = XM * APOOL * DTMIN / ANA
    end if

    return
end subroutine calculate_burning_rate

end module m_sofire_c1