##################################################################
# Default LaTeX font sizes
# Set typeface before including, e.g. fface = "Cabin"
##################################################################
# Default
# hfont = fface
# tfont = fface
# nfont = fface
# kfont = fface
# tinyfont = fface
# png
# hfont    = fface . ",18"
# tfont    = fface . ",16"
# nfont    = fface . ",12"
# kfont    = fface . ",8"
# tinyfont = fface . ",6"
# PDF
# hfont = fface . ",24"
# tfont = fface . ",22"
# nfont = fface . ",16"
# kfont = fface . ",10"
# tinyfont = fface . ",6"
# LaTeX
hfont    = ",18"
tfont    = ",16"
nfont    = ",12"
kfont    = ",8"
tinyfont = ",6"
##################################################################

