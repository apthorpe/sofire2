!> @file m_dataframe.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Data structures for tracking scenario (input) and transient
!! results (output) data
module m_dataframe
    use sofire2_param, only: WP
    use m_constant, only: ZERO, BIG1P9, TINY1M6
    implicit none

    private

    !> Data structure containing all tracked transient results
    type, public :: result_dataframe
        ! Original tracked transient elements, COMMON /COMA/
        !> Scenario time, hours
        real(kind=WP) :: T = ZERO
        !> Mass of sodium oxide released to gas, \f$\us{\lbm}\f$
        real(kind=WP) :: OXR = ZERO
        !> Cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
        real(kind=WP) :: PGASC = ZERO
        !> Liner floor temperature, center of first (top) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF1 = ZERO
        !> Cell floor temperature, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF2 = ZERO
        !> Cell floor temperature, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF3 = ZERO
        !> Cell floor temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TF4 = ZERO
        !> Cell gas temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TGASC = ZERO
        !> Sodium surface temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TS = ZERO
        !> Sodium pool temperature, center of first (top) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS1 = ZERO
        !> Sodium pool temperature, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS2 = ZERO
        !> Sodium pool temperature, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS3 = ZERO
        !> Sodium pool temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TS4 = ZERO
        !> Liner wall temperature, center of first (inside) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC1 = ZERO
        !> Cell wall temperature, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC2 = ZERO
        !> Cell wall temperature, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC3 = ZERO
        !> Cell wall temperature, center of fourth (outside) node, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC4 = ZERO
        !> Sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
        real(kind=WP) :: XM = ZERO

        ! Previously untracked transient elements
        !> Convective heat transfer rate from sodium to gas, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QCONV1 = ZERO
        !> Convective heat transfer rate from gas to wall liner (node 1), \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QCONV2 = ZERO
        !> Radiative heat transfer rate from wall liner (node 1) to node 2, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QRAD1 = ZERO
        !> Radiative heat transfer rate from floor liner (node 1) to node 2, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QRAD2 = ZERO
        !> Radiative heat transfer rate from sodium pool to gas, \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QRAD3 = ZERO
        !> Radiative heat transfer rate from sodium pool to wall liner (node 1), \f$\us{\BTU\per\hour}\f$
        real(kind=WP) :: QROD = ZERO
        !> Mass of oxygen remaining in cell, \f$\us{\lbm}\f$
        real(kind=WP) :: OX = ZERO
        !> Mass of oxygen consumed by combustion, \f$\us{\lbm}\f$
        real(kind=WP) :: OXLB = ZERO
        !> Gas density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHOC = ZERO
        !> Mass of oxygen consumed, \f$\us{\lbm}\f$
        real(kind=WP) :: SUM1 = ZERO
        !> Mass of sodium burned, \f$\us{\lbm}\f$
        real(kind=WP) :: SUM2 = ZERO
        !> Mass of gas in cell, \f$\us{\lbm}\f$
        real(kind=WP) :: W = ZERO
        !> Volumetric gas leakage rate, \f$\us{\cubic\foot\per\hour}\f$
        real(kind=WP) :: F40 = ZERO
        !> Oxygen concentration corrected for leakage, weight-percent
        real(kind=WP) :: C = ZERO
        !> Gas leakage from cell due to exhaust, \f$\us{\lbm}\f$
        real(kind=WP) :: W1 = ZERO
        !> Gas inflow/outflow mass from cell due to pressure, \f$\us{\lbm}\f$
        real(kind=WP) :: W2 = ZERO
        !> Timestep based on gas mass, \f$\si{\hour}\f$
        real(kind=WP) :: DTMIN1 = ZERO
        !> Timestep based on sodium pool surface, \f$\si{\hour}\f$
        real(kind=WP) :: DTMIN2 = ZERO
        !> Minimum timestep, \f$\si{\hour}\f$
        real(kind=WP) :: DT = ZERO
        !> Stabilized timestep, \f$\si{\hour}\f$
        real(kind=WP) :: DTMIN = ZERO

        !> Floor liner temperature change over timestep, center of first (top) node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTF1 = ZERO
        !> Cell floor temperature change over timestep, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTF2 = ZERO
        !> Cell floor temperature change over timestep, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTF3 = ZERO
        !> Cell floor temperature change over timestep, center of fourth (bottom) node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTF4 = ZERO
        !> Cell gas temperature change over timestep, \f$\us{\rankine}\f$
        real(kind=WP) :: DTGASC = ZERO
        !> Sodium surface temperature change over timestep, \f$\us{\rankine}\f$
        real(kind=WP) :: DTS = ZERO
        !> Sodium pool temperature change over timestep, center of first (top) node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTS1 = ZERO
        !> Sodium pool temperature change over timestep, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTS2 = ZERO
        !> Sodium pool temperature change over timestep, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTS3 = ZERO
        !> Sodium pool temperature change over timestep, center of fourth (bottom) node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTS4 = ZERO
        !> Wall liner temperature change over timestep, center of first (inside) node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTWC1 = ZERO
        !> Cell wall temperature change over timestep, center of second node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTWC2 = ZERO
        !> Cell wall temperature change over timestep, center of third node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTWC3 = ZERO
        !> Cell wall temperature change over timestep, center of fourth (outside) node, \f$\us{\rankine}\f$
        real(kind=WP) :: DTWC4 = ZERO

        !> Convective heat transfer 'admittance' from sodium surface to gas, \f$\us{\rankine\per\BTU}\f$
        real(kind=WP) :: YCONV1 = ZERO
        !> Convective heat transfer 'admittance' from gas to wall liner, \f$\us{\rankine\per\BTU}\f$
        real(kind=WP) :: YCONV2 = ZERO

        !> Radiative heat transfer 'admittance' from wall liner (node 1) to node 2, \f$\us{\rankine\per\BTU}\f$
        real(kind=WP) :: YRAD1 = ZERO
        !> Radiative heat transfer 'admittance' from floor liner (node 1) to node 2, \f$\us{\rankine\per\BTU}\f$
        real(kind=WP) :: YRAD2 = ZERO
        !> Radiative heat transfer 'admittance' from sodium pool to gas, \f$\us{\rankine\per\BTU}\f$
        real(kind=WP) :: YRAD3 = ZERO
        !> Radiative heat transfer 'admittance' from sodium pool to wall liner (node 1), \f$\us{\rankine\per\BTU}\f$
        real(kind=WP) :: YROD = ZERO

        ! Summary results
        !> Summary value, scenario time, \f$\si{\hour}\f$
        real(kind=WP) :: ST = ZERO
        !> Summary value, cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ gauge
        real(kind=WP) :: SPGASC = ZERO
        !> Summary value, liner floor temperature, center of first (top) node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STF1 = ZERO
        !> Summary value, cell floor temperature, center of second node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STF2 = ZERO
        !> Summary value, cell gas temperature, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STGASC = ZERO
        !> Summary value, sodium pool temperature, center of first (top) node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STS1 = ZERO
        !> Summary value, liner wall temperature, center of first (inside) node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STWC1 = ZERO
        !> Summary value, cell wall temperature, center of second node, \f$\us{\fahrenheit}\f$
        real(kind=WP) :: STWC2 = ZERO
        !> Summary value, sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
        real(kind=WP) :: SXM = ZERO

    contains
        !> Initialize results
        procedure :: init => result_dataframe_init
        !> Write initial rates in the original tabular output format
        procedure :: write_legacy_initial_rates => result_dataframe_write_legacy_initial_rates
        !> Write legacy tabular output to stdout
        procedure :: write_legacy_output => result_dataframe_write_legacy_output
        !> Write header to CSV output file
        procedure, nopass :: write_csv_header => result_dataframe_write_csv_header
        !> Write data to CSV output file
        procedure :: write_csv_data => result_dataframe_write_csv_data
        !> Initialize temperatures from initial heat sink and gas conditions
        procedure :: init_temperatures => result_dataframe_init_temperatures
        !> Update element temperature differences and temperatures
        procedure :: update_temperatures => result_dataframe_update_temperatures
        !> Update convective heat transfer
        procedure :: update_qconv => result_dataframe_update_qconv
        !> Update convective 'admittance'
        procedure :: update_yconv => result_dataframe_update_yconv
        !> Update radiative heat transfer
        procedure :: update_qrad => result_dataframe_update_qrad
        !> Update radiative 'admittance'
        procedure :: update_yrad => result_dataframe_update_yrad
        !> Calculate passive pressure-driven leakage and forced outflow and
        !! update the resulting cell mass, density, O2 mass and concentration,
        !! and beginning-of-timestep gas pressure and temperature
        procedure :: update_gas_flows => result_dataframe_update_gas_flows
        !> Update burning rate and mass of oxygen and sodium consumed
        procedure :: update_reactant_mass => result_dataframe_update_reactant_mass
        !> Update cell gas mass, density, oxygen concentration, aerosol mass,
        !! and pressure
        procedure :: update_final_gas_composition => result_dataframe_update_final_gas_composition
        !> Update legacy summary info fields
        procedure :: update_summary_info => result_dataframe_update_summary_info
        !> Write header to summary info CSV file
        procedure, nopass :: write_summary_csv_header => result_dataframe_write_summary_csv_header
        !> Write data to summary info CSV file
        procedure :: write_summary_csv_data => result_dataframe_write_summary_csv_data
    end type result_dataframe

    !> Data structure containing all scenario initial conditions
    type, public :: scenario_dataframe
        !> Scenario title
        character(len=72) :: TITLE = ''

        !> Number of numerical parameters associated with scenario
        integer :: NPARAMS = 87

        !> Initial sodium surface temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TSI = ZERO
        !> Initial sodium temperature, node 1, \f$\us{\rankine}\f$
        real(kind=WP) :: TS1I = ZERO
        !> Initial sodium temperature, node 2, \f$\us{\rankine}\f$
        real(kind=WP) :: TS2I = ZERO
        !> Initial sodium temperature, node 3, \f$\us{\rankine}\f$
        real(kind=WP) :: TS3I = ZERO
        !> Initial sodium temperature, node 4, \f$\us{\rankine}\f$
        real(kind=WP) :: TS4I = ZERO
        !> Initial cell floor temperature, node 1, \f$\us{\rankine}\f$
        real(kind=WP) :: TF1I = ZERO
        !> Initial cell floor temperature, node 2, \f$\us{\rankine}\f$
        real(kind=WP) :: TF2I = ZERO
        !> Initial cell floor temperature, node 3, \f$\us{\rankine}\f$
        real(kind=WP) :: TF3I = ZERO
        !> Initial cell floor temperature, node 4, \f$\us{\rankine}\f$
        real(kind=WP) :: TF4I = ZERO
        !> Initial cell wall temperature, node 1, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC1I = ZERO
        !> Initial cell wall temperature, node 2, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC2I = ZERO
        !> Initial cell wall temperature, node 3, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC3I = ZERO
        !> Initial cell wall temperature, node 4, \f$\us{\rankine}\f$
        real(kind=WP) :: TWC4I = ZERO
        !> Initial cell gas temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TGASCI = ZERO
        !> Initial cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
        real(kind=WP) :: PGASCI = ZERO
        !> Initial time, \f$\si{\hour}\f$
        real(kind=WP) :: TI = ZERO
        !> Ambient temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: TA = ZERO
        !> Ambient pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
        real(kind=WP) :: PA = ZERO
        !> Initial sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
        real(kind=WP) :: XMI = ZERO
        !> Problem end time, \f$\si{\hour}\f$
        real(kind=WP) :: XMAX = ZERO
        !> Emissivity * view factor, sodium pool surface to cell walls
        real(kind=WP) :: AF = ZERO
        !> Surface area of sodium pool, \f$\us{\square\foot}\f$
        real(kind=WP) :: A1 = ZERO
        !> Exposed cell wall surface area, \f$\us{\square\foot}\f$
        real(kind=WP) :: A2 = ZERO
        !> Wetted area of sodium pool, \f$\us{\square\foot}\f$
        real(kind=WP) :: A5 = ZERO
        !> Ambient oxygen concentration, volume-percent
        real(kind=WP) :: CO = ZERO
        !> Stoichiometric sodium combustion ratio \f$\us{\lbm\-\ce{Na}\per\lbm-\ce{O2}}\f$
        real(kind=WP) :: ANA = ZERO
        !> Heat of combustion, \f$\us{\BTU\per\lbm}\f$
        real(kind=WP) :: QC = ZERO
        !> Cell free volume, \f$\us{\cubic\foot}\f$
        real(kind=WP) :: VOLAC = ZERO
        !> Radiation exchange, floor liner to floor node 2, dimensionless
        !! \todo Should this actually be documented as "liner to wall/floor node emissivity and view factor"
        real(kind=WP) :: AF2 = ZERO
        !> Radiation exchange, sodium pool to gas, dimensionless
        real(kind=WP) :: AF3 = ZERO
        !> Mass of sodium spilled, \f$\us{\lbm}\f$
        real(kind=WP) :: SOD = ZERO
        !> Timesteps between transient output print during time interval \f$t \leq \SI{1}{\hour}\f$
        real(kind=WP) :: PRT1 = ZERO
        !> Timesteps between transient output print during time interval \f$\SI{1}{\hour} < t \leq \SI{3}{\hour}\f$
        real(kind=WP) :: PRT2 = ZERO
        !> Timesteps between transient output print during time interval \f$t > \SI{3}{\hour}\f$
        real(kind=WP) :: PRT3 = ZERO
        !> Cell exhaust volumetric flow rate, \f$\us{\cubic\foot\per\hour}\f$
        real(kind=WP) :: F3 = ZERO
        !> Pressure leakage factor, \f$\us{\cubic\foot\per\hour}\ \text{per} \sqrt{\us{\inch}\ \text{(water column)}}\f$
        real(kind=WP) :: CON = ZERO
        !> Time step stabilizer, dimensionless
        real(kind=WP) :: X = ZERO
        !> Sodium removal rate, sodium node 1, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S1 = ZERO
        !> Sodium removal rate, sodium node 2, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S2 = ZERO
        !> Sodium removal rate, sodium node 3, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S3 = ZERO
        !> Sodium removal rate, sodium node 4, \f$\us{\lbm\per\hour}\f$
        real(kind=WP) :: S4 = ZERO
        !> Sodium surface layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS = ZERO
        !> Sodium node 1 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS1 = ZERO
        !> Sodium node 2 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS2 = ZERO
        !> Sodium node 3 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS3 = ZERO
        !> Sodium node 4 layer thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZNAS4 = ZERO
        !> Pan (floor node 1) thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF1 = ZERO
        !> Floor node 2 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF2 = ZERO
        !> Floor node 3 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF3 = ZERO
        !> Floor node 4 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: ZF4 = ZERO
        !> Wall liner (node 1) thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC1 = ZERO
        !> Wall node 2 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC2 = ZERO
        !> Wall node 3 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC3 = ZERO
        !> Wall node 4 thickness, \f$\us{\foot}\f$
        real(kind=WP) :: XWC4 = ZERO
        !> Specific heat of air, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPAC = ZERO
        !> Specific heat of sodium, \f$\us{\BTU\per\lbm\per\fahrenheit}\f$
        real(kind=WP) :: CPS = ZERO
        !> Specific heat of burn pan (floor node 1), \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF1 = ZERO
        !> Specific heat of floor node 2, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF2 = ZERO
        !> Specific heat of floor node 3, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF3 = ZERO
        !> Specific heat of floor node 4, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPF4 = ZERO
        !> Specific heat of wall liner (node 1), \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC1 = ZERO
        !> Specific heat of wall node 2, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC2 = ZERO
        !> Specific heat of wall node 3, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC3 = ZERO
        !> Specific heat of wall node 4, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: CPWC4 = ZERO
        !> Thermal conductivity of sodium, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKS = ZERO
        !> Thermal conductivity of pan (floor node 1), \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF1 = ZERO
        !> Thermal conductivity of air gap between floor nodes 1 and 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF12 = ZERO
        !> Thermal conductivity of floor node 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF2 = ZERO
        !> Thermal conductivity of floor node 3, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF3 = ZERO
        !> Thermal conductivity of floor node 4, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKF4 = ZERO
        !> Thermal conductivity of wall node 1, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC1 = ZERO
        !> Thermal conductivity of air gap between wall nodes 1 and 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC12 = ZERO
        !> Thermal conductivity of wall node 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC2 = ZERO
        !> Thermal conductivity of wall node 3, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC3 = ZERO
        !> Thermal conductivity of wall node 4, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: AKWC4 = ZERO
        !> Cell gas density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHOA = ZERO
        !> Sodium density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHS = ZERO
        !> Burn pan (floor node 1) density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF1 = ZERO
        !> Floor node 2 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF2 = ZERO
        !> Floor node 3 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF3 = ZERO
        !> Floor node 4 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHF4 = ZERO
        !> Wall liner (node 1) density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC1 = ZERO
        !> Wall node 2 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC2 = ZERO
        !> Wall node 3 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC3 = ZERO
        !> Wall node 4 density, \f$\us{\lbm\per\cubic\foot}\f$
        real(kind=WP) :: RHWC4 = ZERO
        !> Gap width between burn pan (floor node 1) and floor node 2, \f$\us{foot}\f$
        real(kind=WP) :: ZF12 = ZERO
        !> Gap width between cell liner (wall node 1) and wall node 2, \f$\us{foot}\f$
        real(kind=WP) :: XWC12 = ZERO
    contains
        !> Initialize scenario data
        procedure :: init => scenario_dataframe_init
        !> Populate scenario attributes from array
        procedure :: set_from_array => scenario_dataframe_set_from_array
        !> Populate heatsinks and heat conduction paths from scenario attributes
        procedure :: populate_heatnet => scenario_dataframe_populate_heatnet
        !> Return number of timesteps to calculate before printing output based
        !! on current problem time
        procedure :: set_legacy_print_interval => scenario_dataframe_set_legacy_print_interval
    end type scenario_dataframe

    !> Structure for setting and tracking local and global timestep information
    type, public :: timestep_dataframe
        !> Limiting timestep for cell wall liner (node 1), \f$\us{\hour}\f$
        real(kind=WP) :: DELC1 = BIG1P9
        !> Limiting timestep for cell wall node 2, \f$\us{\hour}\f$
        real(kind=WP) :: DELC2 = BIG1P9
        !> Limiting timestep for cell wall node 3, \f$\us{\hour}\f$
        real(kind=WP) :: DELC3 = BIG1P9
        !> Limiting timestep for cell wall node 4, \f$\us{\hour}\f$
        real(kind=WP) :: DELC4 = BIG1P9
        !> Limiting timestep for cell floor liner (node 1), \f$\us{\hour}\f$
        real(kind=WP) :: DELF1 = BIG1P9
        !> Limiting timestep for cell floor node 2, \f$\us{\hour}\f$
        real(kind=WP) :: DELF2 = BIG1P9
        !> Limiting timestep for cell floor node 3, \f$\us{\hour}\f$
        real(kind=WP) :: DELF3 = BIG1P9
        !> Limiting timestep for cell floor node 4, \f$\us{\hour}\f$
        real(kind=WP) :: DELF4 = BIG1P9
        !> Limiting timestep for cell gas, \f$\us{\hour}\f$
        real(kind=WP) :: DELGC = BIG1P9
        !> Limiting timestep for sodium surface, \f$\us{\hour}\f$
        real(kind=WP) :: DELS  = BIG1P9
        !> Limiting timestep for sodium pool node 1, \f$\us{\hour}\f$
        real(kind=WP) :: DELS1 = BIG1P9
        !> Limiting timestep for sodium pool node 2, \f$\us{\hour}\f$
        real(kind=WP) :: DELS2 = BIG1P9
        !> Limiting timestep for sodium pool node 3, \f$\us{\hour}\f$
        real(kind=WP) :: DELS3 = BIG1P9
        !> Limiting timestep for sodium pool node 4, \f$\us{\hour}\f$
        real(kind=WP) :: DELS4 = BIG1P9
        !> User-defined minimum allowable timestep, \f$\us{\hour}\f$
        real(kind=WP) :: dtlimit = TINY1M6
    contains
        !> Initialize timestep object attributes
        procedure :: init => timestep_dataframe_init
        !> Update minimum allowable timesteps for each element or phenomenon
        procedure :: update => timestep_dataframe_update
        !> Retrieve global minimum allowable timestep
        procedure :: get_dt => timestep_dataframe_get_dt
    end type timestep_dataframe

    !> One-dimensional rectilinear slab heat sink with constant properties
    type, public :: heatsink_dataframe
        !> Heat sink identifying name
        character(len=32) :: name = 'DEFAULT HEAT SINK              '
        !> Heat transfer face area, \f$\us{\square\foot}\f$
        real(kind=WP) :: ahx = ZERO
        !> Slab thickness, \f$\us{\foot}\f$
        real(kind=WP) :: thickness = ZERO
        !> Mass, \f$\us{\lbm}\f$
        real(kind=WP) :: mass = ZERO
        !> Specific heat \f$C_{p}\f$, \f$\us{\BTU\per\lbm\per\rankine}\f$
        real(kind=WP) :: cp = ZERO
        !> Thermal conductivity \f$k\f$, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
        real(kind=WP) :: k = ZERO
        !> Initial temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: T0 = ZERO

    contains
        !> Initialize heatsink object attributes
        procedure :: init => heatsink_dataframe_init
        !> Add or remove mass from heatsink; added mass enters with same
        !! properties as heatsink (isothermal)
        procedure :: add_isothermal_mass => heatsink_dataframe_add_mass
        !> Calculate heatsink volume, \f${\us{\cubic\foot}}\f$
        procedure :: volume => heatsink_dataframe_volume
        !> Calculate heatsink density, \f${\us{\lbm\per\cubic\foot}}\f$
        procedure :: density => heatsink_dataframe_density
        !> Calculate heatsink full slab thermal conductance
        !! \f$\frac{k A}{x}\f$, \f${\us{\BTU\per\rankine}}\f$
        procedure :: th_conductance => heatsink_dataframe_thermal_conductance
        !> Calculate heatsink half-slab thermal resistance
        !! \f$\frac{x}{2 k A}\f$, \f${\us{\rankine\per\BTU}}\f$
        procedure :: half_resistance => heatsink_dataframe_half_resistance
        !> Calculate heatsink thermal capacity
        !! \f$m c_{p}\f$, \f$\us{\BTU\per\rankine}\f$
        procedure :: th_capacity => heatsink_dataframe_thermal_capacity
    end type heatsink_dataframe

    !> Centroid-to-centroid heat conduction path between two heatsinks
    !! with optional gap conductance.
    !!
    !! Gap conductance is only calculated if any all gap characteristics
    !! are positive (non-zero). If only one heat sink is specified,
    !! conduction path is centroid-to-edge of the specified heat sink.
    !! If no heat sinks are specified, zero conductance is returned;
    !! this may be viewed as an error condition.
    type, public :: qcond_dataframe
        !> Heat sink identifying name
        character(len=32) :: name = 'UNINITIALIZED CONDUCTION PATH   '
        !> Source heatsink
        type(heatsink_dataframe), pointer :: hs1 => NULL()
        !> Destination heatsink
        type(heatsink_dataframe), pointer :: hs2 => NULL()
        !> Gap heat transfer area, \f$\us{\square\foot}\f$
        real(kind=WP) :: ahxgap = ZERO
        !> Gap width, \f$\us{\foot}\f$
        real(kind=WP) :: xgap = ZERO
        !> Gap thermal conductivity, \f$\us{\BTU\per\foot\per\rankine}\f$
        real(kind=WP) :: kgap = ZERO
    contains
        !> Initialize heat conduction path attributes
        procedure :: init => qcond_dataframe_init
        !> Return gap resistance, \f$\us{\rankine\per\BTU}\f$
        procedure :: gap_resistance => qcond_dataframe_gap_resistance
        !> Return thermal conductance composed of source and destination half-resistance
        !! and gap resistance (if any), \f$\us{\BTU\per\rankine}\f$
        procedure :: conductance => qcond_dataframe_conductance
    end type qcond_dataframe

    !> Heat convection path between a heatsink and a gas volume
    type, public :: qconv_dataframe
        !> Heat sink identifying name
        character(len=32) :: name = 'UNINITIALIZED CONVECTION PATH   '
        !> Correlation coefficient
        real(kind=WP) :: coeff
        !> Heat transfer surface area, \f$\us{\square\foot}\f$
        real(kind=WP) :: ahx
    contains
        !> Initialize heat convection path attributes
        procedure :: init => qconv_dataframe_init
        !> Calculate convective heat transfer
        procedure :: qconvection => qconv_dataframe_qconvection
    end type qconv_dataframe

    !> Radiation heat transfer path between two heatsinks
    type, public :: qrad_dataframe
        !> Heat transfer path identifying name
        character(len=32) :: name = 'UNINITIALIZED RADIATION PATH    '
        !> Thermal emissivity, dimensionless
        real(kind=WP) :: emissivity = ZERO
        !> Radiating area, \f$\us{\square\foot}\f$
        real(kind=WP) :: ahx = ZERO
    contains
        !> Initialize heat radiation path attributes
        procedure :: init => qrad_dataframe_init
        !> Radiative heat transferred from emitter to receiver
        procedure :: qradiation => qrad_dataframe_qrad
    end type qrad_dataframe

contains

!> Initialize all result dataframe elements to zero
subroutine result_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this

    continue

    this%T = ZERO
    this%OXR = ZERO
    this%PGASC = ZERO
    this%TF1 = ZERO
    this%TF2 = ZERO
    this%TF3 = ZERO
    this%TF4 = ZERO
    this%TGASC = ZERO
    this%TS = ZERO
    this%TS1 = ZERO
    this%TS2 = ZERO
    this%TS3 = ZERO
    this%TS4 = ZERO
    this%TWC1 = ZERO
    this%TWC2 = ZERO
    this%TWC3 = ZERO
    this%TWC4 = ZERO
    this%XM = ZERO
    this%QCONV1 = ZERO
    this%QCONV2 = ZERO
    this%QRAD1 = ZERO
    this%QRAD2 = ZERO
    this%QRAD3 = ZERO
    this%QROD = ZERO
    this%OX = ZERO
    this%OXLB = ZERO
    this%RHOC = ZERO
    this%SUM1 = ZERO
    this%SUM2 = ZERO
    this%W = ZERO
    this%F40 = ZERO
    this%C = ZERO
    this%W1 = ZERO
    this%W2 = ZERO
    this%DTMIN1 = ZERO
    this%DTMIN2 = ZERO
    this%DT = ZERO
    this%DTMIN = ZERO

    this%DTF1 = ZERO
    this%DTF2 = ZERO
    this%DTF3 = ZERO
    this%DTF4 = ZERO
    this%DTGASC = ZERO
    this%DTS = ZERO
    this%DTS1 = ZERO
    this%DTS2 = ZERO
    this%DTS3 = ZERO
    this%DTS4 = ZERO
    this%DTWC1 = ZERO
    this%DTWC2 = ZERO
    this%DTWC3 = ZERO
    this%DTWC4 = ZERO

    this%YCONV1 = ZERO
    this%YCONV2 = ZERO

    this%YRAD1 = ZERO
    this%YRAD2 = ZERO
    this%YRAD3 = ZERO
    this%YROD = ZERO

    this%ST = ZERO
    this%SPGASC = ZERO
    this%STF1 = ZERO
    this%STF2 = ZERO
    this%STGASC = ZERO
    this%STS1 = ZERO
    this%STWC1 = ZERO
    this%STWC2 = ZERO
    this%SXM = ZERO

    return
end subroutine result_dataframe_init

!> Initialize result temperatures from heat sinks and scenario
subroutine result_dataframe_init_temperatures(this, scenario,           &
    hs_na_crust, hs_na_pool, hs_floor, hs_wall)
    use sofire2_param, only: n_hs_na_pool, n_hs_floor, n_hs_wall
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this

    !> Scenario object
    type(scenario_dataframe), intent(in) :: scenario
    !> Sodium surface heatsink
    type(heatsink_dataframe), intent(in) :: hs_na_crust
    !> List of sodium pool heatsinks
    type(heatsink_dataframe), dimension(n_hs_na_pool), intent(in) ::    &
        hs_na_pool
    !> List of sodium pool heatsinks
    type(heatsink_dataframe), dimension(n_hs_floor), intent(in) ::      &
        hs_floor
    !> List of sodium pool heatsinks
    type(heatsink_dataframe), dimension(n_hs_wall), intent(in) ::       &
        hs_wall

    continue

    this%TGASC = scenario%TGASCI

    this%TS = hs_na_crust%T0

    this%TS1 = hs_na_pool(1)%T0
    this%TS2 = hs_na_pool(2)%T0
    this%TS3 = hs_na_pool(3)%T0
    this%TS4 = hs_na_pool(4)%T0

    this%TF1 = hs_floor(1)%T0
    this%TF2 = hs_floor(2)%T0
    this%TF3 = hs_floor(3)%T0
    this%TF4 = hs_floor(4)%T0

    this%TWC1 = hs_wall(1)%T0
    this%TWC2 = hs_wall(2)%T0
    this%TWC3 = hs_wall(3)%T0
    this%TWC4 = hs_wall(4)%T0

    return
end subroutine result_dataframe_init_temperatures

!> Update element temperature differences and temperatures
subroutine result_dataframe_update_temperatures(this, scenario, prev,   &
    hs_na_crust, hs_na_pool, hs_floor, hs_wall, qcond_path,             &
    TGASC_adjusted)
    use sofire2_param, only: n_hs_na_pool, n_hs_floor, n_hs_wall,       &
        n_qcond_path
!    use m_constant, only: T0R, IN_FT
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this

    !> Scenario object
    type(scenario_dataframe), intent(in) :: scenario
    !> Results object for previous timestep
    type(result_dataframe), intent(in) :: prev
    !> Sodium surface heatsink
    type(heatsink_dataframe), intent(in) :: hs_na_crust
    !> List of sodium pool heatsinks
    type(heatsink_dataframe), dimension(n_hs_na_pool), intent(in) ::    &
        hs_na_pool
    !> List of sodium pool heatsinks
    type(heatsink_dataframe), dimension(n_hs_floor), intent(in) ::      &
        hs_floor
    !> List of sodium pool heatsinks
    type(heatsink_dataframe), dimension(n_hs_wall), intent(in) ::       &
        hs_wall
    !> List of heat conduction paths
    type(qcond_dataframe), dimension(n_qcond_path), intent(in) ::       &
        qcond_path
    !> Previous timestep cell gas temperature adjusted for leakage and
    !! forced flows
    real(kind=WP), intent(in) :: TGASC_adjusted

    continue

    this%DTGASC = this%DTMIN * (this%QCONV1 + this%QRAD3 - this%QCONV2) &
        / (this%RHOC * scenario%CPAC * scenario%VOLAC)

    this%DTS = this%DTMIN / hs_na_crust%th_capacity()                   &
        * (this%XM * scenario%A1 * scenario%QC                          &
        - this%QCONV1 - this%QROD - this%QRAD3                          &
        - qcond_path(10)%conductance() * (prev%TS - prev%TS1))

    this%DTS1 = this%DTMIN / hs_na_pool(1)%th_capacity()                &
        * (qcond_path(10)%conductance() * (prev%TS - prev%TS1)          &
           - qcond_path(11)%conductance() * (prev%TS1 - prev%TS2))

    this%DTS2 = this%DTMIN / hs_na_pool(2)%th_capacity()                &
        * (qcond_path(11)%conductance() * (prev%TS1 - prev%TS2)         &
           - qcond_path(12)%conductance() * (prev%TS2 - prev%TS3))

    this%DTS3 = this%DTMIN / hs_na_pool(3)%th_capacity()                &
        * (qcond_path(12)%conductance() * (prev%TS2 - prev%TS3)         &
           - qcond_path(13)%conductance() * (prev%TS3 - prev%TS4))

    this%DTS4 = this%DTMIN / hs_na_pool(4)%th_capacity()                &
        * (qcond_path(13)%conductance() * (prev%TS3 - prev%TS4)         &
           - qcond_path(1)%conductance() * (prev%TS4 - prev%TF1))

    this%DTF1 = this%DTMIN / hs_floor(1)%th_capacity()                  &
        * (qcond_path(1)%conductance() * (prev%TS4 - prev%TF1)          &
           - qcond_path(2)%conductance() * (prev%TF1 - prev%TF2)        &
           - this%QRAD1)

    this%DTF2 = this%DTMIN / hs_floor(2)%th_capacity()                  &
        * (qcond_path(2)%conductance() * (prev%TF1 - prev%TF2)          &
        - qcond_path(3)%conductance() * (prev%TF2 - prev%TF3)           &
        + this%QRAD1)

    this%DTF3 = this%DTMIN / hs_floor(3)%th_capacity()                  &
        * (qcond_path(3)%conductance() * (prev%TF2 - prev%TF3)          &
           - qcond_path(4)%conductance() * (prev%TF3 - prev%TF4))

    this%DTF4 = this%DTMIN / hs_floor(4)%th_capacity()                  &
        * (qcond_path(4)%conductance() * (prev%TF3 - prev%TF4)          &
           - qcond_path(5)%conductance() * (prev%TF4 - scenario%TA))

    this%DTWC1 = this%DTMIN / hs_wall(1)%th_capacity()                  &
        * (this%QCONV2 + this%QROD - this%QRAD2                         &
        - qcond_path(6)%conductance() * (prev%TWC1 - prev%TWC2))

    this%DTWC2 = this%DTMIN / hs_wall(2)%th_capacity()                  &
        * (qcond_path(6)%conductance() * (prev%TWC1 - prev%TWC2)        &
           - qcond_path(7)%conductance() * (prev%TWC2 - prev%TWC3)      &
           + this%QRAD2)

    this%DTWC3 = this%DTMIN / hs_wall(3)%th_capacity()                  &
        * (qcond_path(7)%conductance() * (prev%TWC2 - prev%TWC3)        &
           - qcond_path(8)%conductance() * (prev%TWC3 - prev%TWC4))

    this%DTWC4 = this%DTMIN / hs_wall(4)%th_capacity()                  &
        * (qcond_path(8)%conductance() * (prev%TWC3 - prev%TWC4)        &
           - qcond_path(9)%conductance() * (prev%TWC4 - scenario%TA))

    this%TGASC = TGASC_adjusted + this%DTGASC
    this%TS = prev%TS + this%DTS
    this%TS1 = prev%TS1 + this%DTS1
    this%TS2 = prev%TS2 + this%DTS2
    this%TS3 = prev%TS3 + this%DTS3
    this%TS4 = prev%TS4 + this%DTS4
    this%TF1 = prev%TF1 + this%DTF1
    this%TF2 = prev%TF2 + this%DTF2
    this%TF3 = prev%TF3 + this%DTF3
    this%TF4 = prev%TF4 + this%DTF4
    this%TWC1 = prev%TWC1 + this%DTWC1
    this%TWC2 = prev%TWC2 + this%DTWC2
    this%TWC3 = prev%TWC3 + this%DTWC3
    this%TWC4 = prev%TWC4 + this%DTWC4

    return
end subroutine result_dataframe_update_temperatures

!> Update convective heat transfer
subroutine result_dataframe_update_qconv(this, qconv_path,              &
    TS, TGASC, TWC1)
    use sofire2_param, only: n_qconv_path
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this

    !> List of convection path objects
    class(qconv_dataframe), dimension(n_qconv_path), intent(in) ::      &
        qconv_path

    !> Sodium surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS

    !> Gas temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TGASC

    !> Cell liner temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC1

    continue

    this%QCONV1 = qconv_path(1)%qconvection(TS, TGASC, this%RHOC)
    this%QCONV2 = qconv_path(2)%qconvection(TGASC, TWC1, this%RHOC)

    return
end subroutine result_dataframe_update_qconv

!> Update convective 'admittance'
subroutine result_dataframe_update_yconv(this)
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this
    continue

    this%YCONV1 = this%QCONV1 / (this%TS - this%TGASC)
    this%YCONV2 = this%QCONV2 / (this%TGASC - this%TWC1)

    return
end subroutine result_dataframe_update_yconv

!> Update radiative heat transfer
subroutine result_dataframe_update_qrad(this,                           &
    qrad_path, TF1, TF2, TWC1, TWC2, TS, TGASC)
    use sofire2_param, only: n_qrad_path
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this

    !> List of radiation path objects
    class(qrad_dataframe), dimension(n_qrad_path), intent(in) ::        &
        qrad_path

    !> Floor liner temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TF1

    !> Floor (node 2) temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TF2

    !> Cell liner temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC1

    !> Cell wall (node 2) temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC2

    !> Sodium surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS

    !> Gas temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TGASC

    continue

    this%QRAD1 = qrad_path(1)%qradiation(TF1, TF2)
    this%QRAD2 = qrad_path(2)%qradiation(TWC1, TWC2)
    this%QRAD3 = qrad_path(3)%qradiation(TS, TGASC)
    this%QROD = qrad_path(4)%qradiation(TS, TWC1)

    return
end subroutine result_dataframe_update_qrad

!> Update radiative 'admittance'
subroutine result_dataframe_update_yrad(this)
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this
    continue

    this%YRAD1 = this%QRAD1 / (this%TF1 - this%TF2)
    this%YRAD2 = this%QRAD2 / (this%TWC1 - this%TWC2)
    this%YRAD3 = this%QRAD3 / (this%TS - this%TGASC)
    this%YROD = this%QROD / (this%TS - this%TWC1)

    return
end subroutine result_dataframe_update_yrad

!> Update cell gas mass, density, oxygen concentration, aerosol mass,
!! and pressure
subroutine result_dataframe_update_final_gas_composition(this, scenario)
    use sofire2_param, only: faer_naox
    use m_constant, only: ZERO, RIN
    implicit none

    !> Current state
    class(result_dataframe), intent(inout) :: this

    !> Scenario object
    type(scenario_dataframe), intent(in) :: scenario

    continue

    if (this%XM /= ZERO) then
        this%RHOC = (this%W - this%OXLB) / scenario%VOLAC
        this%C = (this%OX - this%OXLB) / (this%RHOC * scenario%VOLAC)
        this%W = this%W - this%OXLB
    end if

    this%OXR = this%SUM2 * faer_naox

    this%PGASC = this%RHOC * RIN * this%TGASC

    return
end subroutine result_dataframe_update_final_gas_composition

!> Update transient results summary data
subroutine result_dataframe_update_summary_info(this, PA)
    use m_constant, only: T0R, IN_FT
    implicit none

    !> Object reference
    class(result_dataframe), intent(inout) :: this

    !> Ambient pressure in lbf/ft**2
    real(kind=WP), intent(in) :: PA

    continue

    this%STGASC = this%TGASC - T0R
    this%STWC1 = this%TWC1 - T0R
    this%SXM = this%XM
    this%SPGASC = (this%PGASC - PA) / IN_FT**2
    this%STF1 = this%TF1 - T0R
    this%STF2 = this%TF2 - T0R
    this%ST = this%T
    this%STWC2 = this%TWC2 - T0R
    this%STS1 = this%TS1 - T0R

    return
end subroutine result_dataframe_update_summary_info

!> Write initial rates in the original SOFIRE II tabular output format
subroutine result_dataframe_write_legacy_initial_rates(this, ounit,     &
    dt_manager)
    implicit none

    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

    !> Timestep manager object
    type(timestep_dataframe), intent(in) :: dt_manager

1   format('1')
2   format('0', 24X, 'TIME=', ES12.4, '     XM =', ES12.4, //,          &
        '  QCONV1=', ES12.5, '  QCONV2=', ES12.5, '     QROD=', ES12.5, &
        '   QRAD1=', ES12.5, '   QRAD2=', ES12.5, /,                    &
        '   QRAD3=', ES12.5, '   YRAD1=', ES12.5, '   YRAD2=', ES12.5,  &
        '   YRAD3=', ES12.5, '  YCONV1=', ES12.5, /,                    &
        '  YCONV2= ', ES12.5, '    YROD=', ES12.5, '   DELF1=', ES12.5, &
        '   DELF2=', ES12.5, '   DELF3=', ES12.5, /,                    &
        '   DELF4=', ES12.5, '   DELS1=', ES12.5, '    DELS=', ES12.5,  &
        '   DELGC=', ES12.5, '   DELC1=', ES12.5, /,                    &
        '   DELC2=', ES12.5, '   DELC3=', ES12.5, '   DELC4=', ES12.5,  &
        '   DELS2=', ES12.5, '   DELS3=', ES12.5, /,                    &
        '   DELS4=', ES12.5, /)

    continue

    write(unit=ounit, fmt=1)
    write(unit=ounit, fmt=2)                                            &
        this%T, this%XM, this%QCONV1, this%QCONV2, this%QROD,           &
        this%QRAD1, this%QRAD2, this%QRAD3, this%YRAD1, this%YRAD2,     &
        this%YRAD3, this%YCONV1, this%YCONV2, this%YROD,                &
        dt_manager%DELF1, dt_manager%DELF2, dt_manager%DELF3,           &
        dt_manager%DELF4, dt_manager%DELS1, dt_manager%DELS,            &
        dt_manager%DELGC, dt_manager%DELC1, dt_manager%DELC2,           &
        dt_manager%DELC3, dt_manager%DELC4, dt_manager%DELS2,           &
        dt_manager%DELS3, dt_manager%DELS4

    return
end subroutine result_dataframe_write_legacy_initial_rates

!> Write transient results in the original SOFIRE II tabular output
!! format
subroutine result_dataframe_write_legacy_output(this, ounit)
    implicit none

    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format('0', 24X, 'TIME =', ES12.4, '     XM =', ES12.4, //,         &
        '     TS =', ES12.5, '   TGASC=', ES12.5, '    TF1 =', ES12.5,  &
        '    TF2 =', ES12.5, '    TF3 =', ES12.5, /,                    &
        '    TF4 =', ES12.5, '   TWC1 =', ES12.5, '   TWC2 =', ES12.5,  &
        '   TWC3 =', ES12.5, '   TWC4 =', ES12.5, /,                    &
        '  QCONV1=', ES12.5, '  QCONV2=', ES12.5, ' PGASC =', ES12.5,   &
        '   QROD =', ES12.5, '  QRAD3 =', ES12.5, /,                    &
        '   TS1 =', ES12.5, '   SUM2 =', ES12.5, '    OX  =', ES12.5,   &
        '   RHOC =', ES12.5, '    SUM1=', ES12.5, /,                    &
        '     W  =', ES12.5, '    F40 =', ES12.5, '     C  =', ES12.5,  &
        '    W1  =', ES12.5, '    W2  =', ES12.5, /,                    &
        '   QRAD1=', ES12.5, '   QRAD2=', ES12.5, '    OXR =', ES12.5,  &
        '  DTMIN1=', ES12.5, '  DTMIN2=', ES12.5, /,                    &
        '    DT =', ES12.5, '   DTMIN=', ES12.5, '    TS2 =', ES12.5,   &
        '    TS3 =', ES12.5, '    TS4 =', ES12.5 /)

    continue

    write(unit=ounit, fmt=1)                                            &
        this%T, this%XM,                                                &
        this%TS, this%TGASC, this%TF1, this%TF2, this%TF3,              &
        this%TF4, this%TWC1, this%TWC2, this%TWC3, this%TWC4,           &
        this%QCONV1, this%QCONV2, this%PGASC, this%QROD, this%QRAD3,    &
        this%TS1, this%SUM2, this%OX, this%RHOC, this%SUM1,             &
        this%W, this%F40, this%C, this%W1, this%W2,                     &
        this%QRAD1, this%QRAD2, this%OXR, this%DTMIN1, this%DTMIN2,     &
        this%DT, this%DTMIN, this%TS2, this%TS3, this%TS4

    return
end subroutine result_dataframe_write_legacy_output

!> Write transient results header information in CSV format
!!
!! @note Object reference `this` is not used and was removed to avoid
!! compiler warnings. See `nopass` in the `result_dataframe` class
!! definition
subroutine result_dataframe_write_csv_header(ounit)
    implicit none

    ! !> Object reference - unused; removed to avoid compiler warning.
    ! !! See "nopass" in type definition
    ! class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

! 1   format('"T","OXR","PGASC","TF1","TF2",',                            &
!            '"TF3","TF4","TGASC","TS","TS1",',                           &
!            '"TS2","TS3","TS4","TWC1","TWC2",',                          &
!            '"TWC3","TWC4","XM","QCONV1","QCONV2",',                     &
!            '"QRAD1","QRAD2","QRAD3","QROD","OX",',                      &
!            '"RHOC","SUM1","SUM2","W","F40",',                           &
!            '"C","W1","W2","DTMIN1","DTMIN2",',                          &
!            '"DT","DTMIN"')

1  format('"T", "XM", "TS", "TGASC", "TF1", "TF2", "TF3", "TF4", ',     &
           '"TWC1", "TWC2", "TWC3", "TWC4", "QCONV1", "QCONV2", ',      &
           '"PGASC", "QROD", "QRAD3", "TS1", "SUM2", "OX", "RHOC", ',   &
           '"SUM1", "W", "F40", "C", "W1", "W2", "QRAD1", "QRAD2", ',   &
           '"OXR", "DTMIN1", "DTMIN2", "DT", "DTMIN", "TS2", "TS3",',   &
           ' "TS4"')
    continue

    write(unit=ounit, fmt=1)

    return
end subroutine result_dataframe_write_csv_header

!> Write transient results data in CSV format
subroutine result_dataframe_write_csv_data(this, ounit)
    implicit none

    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format(ES12.5, 36(",", ES12.5))

    continue

    ! write(unit=ounit, fmt=1)                                            &
    !     this%T, this%OXR, this%PGASC, this%TF1, this%TF2,               &
    !     this%TF3, this%TF4, this%TGASC, this%TS, this%TS1,              &
    !     this%TS2, this%TS3, this%TS4, this%TWC1, this%TWC2,             &
    !     this%TWC3, this%TWC4, this%XM, this%QCONV1, this%QCONV2,        &
    !     this%QRAD1, this%QRAD2, this%QRAD3, this%QROD, this%OX,         &
    !     this%RHOC, this%SUM1, this%SUM2, this%W, this%F40,              &
    !     this%C, this%W1, this%W2, this%DTMIN1, this%DTMIN2,             &
    !     this%DT, this%DTMIN

    write(unit=ounit, fmt=1)                                            &
        this%T, this%XM, this%TS, this%TGASC, this%TF1, this%TF2,       &
        this%TF3, this%TF4, this%TWC1, this%TWC2, this%TWC3, this%TWC4, &
        this%QCONV1, this%QCONV2, this%PGASC, this%QROD, this%QRAD3,    &
        this%TS1, this%SUM2, this%OX, this%RHOC, this%SUM1, this%W,     &
        this%F40, this%C, this%W1, this%W2, this%QRAD1, this%QRAD2,     &
        this%OXR, this%DTMIN1, this%DTMIN2, this%DT, this%DTMIN,        &
        this%TS2, this%TS3, this%TS4

    return
end subroutine result_dataframe_write_csv_data

!> Write summary results header information in CSV format
!!
!! @note Object reference `this` is not used and was removed to avoid
!! compiler warnings. See `nopass` in the `result_dataframe` class
!! definition
subroutine result_dataframe_write_summary_csv_header(ounit)
    implicit none

    ! !> Object reference - unused; removed to avoid compiler warning.
    ! !! See "nopass" in type definition
    ! class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format('"ST","SPGASC","STF1","STF2","STGASC","STS1","STWC1",',      &
           '"STWC2","SXM"')
    continue

    write(unit=ounit, fmt=1)

    return
end subroutine result_dataframe_write_summary_csv_header

!> Write summary results data in CSV format
subroutine result_dataframe_write_summary_csv_data(this, ounit)
    implicit none

    !> Object reference
    class(result_dataframe), intent(in) :: this

    !> Output file unit ID
    integer, intent(in) :: ounit

1   format(ES12.5, 8(",", ES12.5))

    continue

    write(unit=ounit, fmt=1) this%ST, this%SPGASC, this%STF1,           &
        this%STF2, this%STGASC, this%STS1, this%STWC1, this%STWC2,      &
        this%SXM

    return
end subroutine result_dataframe_write_summary_csv_data

!> Calculate passive pressure-driven leakage and forced outflow and
!! update the resulting cell mass, density, O2 mass and concentration,
!! and beginning-of-timestep gas pressure and temperature
subroutine result_dataframe_update_gas_flows(this, scenario,            &
    result_prev, PGASC_adjusted, TGASC_adjusted)
    use m_constant, only: RIN, MIN_HR, INWC_PSF
    implicit none

    !> System state, current timestep
    class(result_dataframe), intent(inout) :: this

    !> Scenario object
    type(scenario_dataframe), intent(in) :: scenario

    !> System state, previous timestep
    type(result_dataframe), intent(in) :: result_prev

    !> Previous-timestep gas pressure, adjusted for leakage and
    !! forced exhaust flow
    real(kind=WP), intent(out) :: PGASC_adjusted

    !> Previous-timestep gas temperature, adjusted for leakage and
    !! forced exhaust flow
    real(kind=WP), intent(out) :: TGASC_adjusted

    ! Difference between cell pressure and ambient pressure,
    ! in inches(water column)
    real(kind=WP) :: dp_leak

    ! End-of-timestep cell mass, lbm
    real(kind=WP) :: W_new

    continue

    ! Forced flow - F3 is positive for outflow;
    ! inflow is not supported
    this%W1 = scenario%F3 * MIN_HR * result_prev%RHOC * this%DTMIN

    TGASC_adjusted = result_prev%TGASC
    dp_leak = (result_prev%PGASC - scenario%PA) * INWC_PSF

    this%F40 = scenario%CON * sqrt(abs(dp_leak))
    if (dp_leak > ZERO) then
        ! Outleakage
        this%C = result_prev%C
        TGASC_adjusted = result_prev%TGASC
        this%W2 = this%F40  * MIN_HR * result_prev%RHOC * this%DTMIN
        this%W = result_prev%W - this%W1 - this%W2
    else
        ! Inleakage
        this%W2 = this%F40 * MIN_HR * scenario%RHOA * this%DTMIN
        W_new = result_prev%W - this%W1 + this%W2
        this%C = ((result_prev%W - this%W1) * result_prev%C             &
            + this%W2 * scenario%CO) / W_new
        TGASC_adjusted = ((result_prev%W - this%W1) * result_prev%TGASC &
            + this%W2 * scenario%TA) / W_new
        this%W = W_new
    end if

    this%RHOC = this%W / scenario%VOLAC

    PGASC_adjusted = this%RHOC * RIN * TGASC_adjusted

    this%OX = scenario%VOLAC * this%C * this%RHOC

    return
end subroutine result_dataframe_update_gas_flows

!> Update burning rate and mass of oxygen and sodium consumed
subroutine result_dataframe_update_reactant_mass(this, PGASC_adjusted,  &
    TGASC_adjusted, scenario, result_prev, prtct, errmsg, EXX_error)
    use sofire2_param, only: min_sodium_mass
    use m_constant, only: ZERO, THIRD, HALF, ONE, GIN
    use m_material, only: kgair, muair

    implicit none

    !> Current state
    class(result_dataframe), intent(inout) :: this

    !> Adjusted previous timestep gas pressure (for diagnostics)
    real(kind=WP), intent(in) :: PGASC_adjusted

    !> Adjusted previous timestep gas temperature
    real(kind=WP), intent(in) :: TGASC_adjusted

    !> Scenario object
    type(scenario_dataframe), intent(in) :: scenario

    !> Previous state
    type(result_dataframe), intent(in) :: result_prev

    !> Iteration ID (for diagnostics). May be safely set to zero if not
    !! known/needed
    integer, intent(in) :: prtct

    !> Error message
    character(len=400), intent(out) :: errmsg

    !> Error condition
    logical, intent(out) :: EXX_error

    ! Locals for turbulent free convection calcs and combustion

    ! Temperature of gas film of gas near sodium pool surface
    ! (for diagnostics)
    real(kind=WP) :: T1

    ! Coefficient of thermal expansion (beta) of gas near sodium pool
    ! surface (for diagnostics)
    real(kind=WP) :: B1

    ! TBD (for diagnostics)
    real(kind=WP) :: D1

    ! TBD (for diagnostics)
    real(kind=WP) :: EXX

    ! Thermal conductivity of air (gas) in boundary layer between
    ! sodium pool and bulk gas
    real(kind=WP) :: AK1

    !> Mass of sodium available for burning
    real(kind=WP) :: M_SODIUM

    !> Oxygen concentration in \f$\us{\lbm\per\cubic\foot}-\ce{O2}\f$
    real(kind=WP) :: RHO_OX
    real(kind=WP) :: HF
    real(kind=WP) :: DIFF

    ! For diagnostics; parallels B1, D1, T1 above
    real(kind=WP) :: B2
    real(kind=WP) :: D2
    real(kind=WP) :: T2

1   format('0', '      I=', I4, ' DTMIN1=', ES12.4, ' DTMIN2=', ES12.4, &
    '  DTMIN=', ES12.4, '      W=', ES12.4, /,                          &
    '   RHOC=', ES12.4, '  PGASC=', ES12.4, '     T1=', ES12.4,         &
    '     T2=', ES12.4, '     B1=', ES12.4, /,                          &
    '     B2=', ES12.4, '     D1=', ES12.4, '     D2=', ES12.4,         &
    '    GIN=', ES12.4, '    EXX=', ES12.4, /,                          &
    ' TGASC=', ES12.4, ' W1=', ES12.4, ' W2=', ES12.4,                  &
    ' TS=', ES12.4 /)

    continue

    T1 = HALF * (TGASC_adjusted + result_prev%TS)
    B1 = ONE / T1
    D1 = (muair(T1) / this%RHOC)**2
    AK1 = kgair(T1)
    EXX = GIN * B1 / D1 * abs(result_prev%TS - TGASC_adjusted)

    EXX_error = (EXX < ZERO)
    if (.not. EXX_error) then
        errmsg = 'OK'
        M_SODIUM = scenario%SOD - result_prev%SUM2
        RHO_OX = this%RHOC * this%C

        ! If more than 0.1 lbm unburned sodium remains
        if (M_SODIUM > min_sodium_mass) then
            DIFF = 241.57_WP / (132.0_WP + T1 / 1.8_WP)                 &
                * (T1 / 493.2_WP)**2.5_WP
            HF = 0.075_WP * DIFF * EXX**THIRD
            this%XM = scenario%ANA * HF * RHO_OX
            this%OXLB = this%XM * scenario%A1 * this%DTMIN              &
                / scenario%ANA
        else
            this%XM = ZERO
            this%OXLB = ZERO
        end if

        this%SUM1 = this%OXLB + result_prev%SUM1
        this%SUM2 = this%SUM1 * scenario%ANA

        if ((scenario%SOD - this%SUM2) <= min_sodium_mass) then
            this%XM = ZERO
        end if
    else
        ! Report error and exit current scenario
        T2 = HALF * (TGASC_adjusted + result_prev%TWC1)
        B2 = ONE / T2
        D2 = (muair(T2) / this%RHOC)**2
        write(errmsg, 1) prtct, this%DTMIN1, this%DTMIN2, this%DTMIN,   &
            this%W, this%RHOC, PGASC_adjusted, T1, T2, B1, B2, D1, D2,  &
            GIN, EXX, TGASC_adjusted, this%W1, this%W2, result_prev%TS
    end if

    return
end subroutine result_dataframe_update_reactant_mass

!> Initialize all scenario dataframe elements
subroutine scenario_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none

    !> Object reference
    class(scenario_dataframe), intent(inout) :: this

    continue

    this%TITLE = ''

    this%TSI = ZERO
    this%TS1I = ZERO
    this%TS2I = ZERO
    this%TS3I = ZERO
    this%TS4I = ZERO
    this%TF1I = ZERO
    this%TF2I = ZERO
    this%TF3I = ZERO
    this%TF4I = ZERO
    this%TWC1I = ZERO
    this%TWC2I = ZERO
    this%TWC3I = ZERO
    this%TWC4I = ZERO
    this%TGASCI = ZERO
    this%PGASCI = ZERO
    this%TI = ZERO
    this%TA = ZERO
    this%PA = ZERO
    this%XMI = ZERO
    this%XMAX = ZERO
    this%AF = ZERO
    this%A1 = ZERO
    this%A2 = ZERO
    this%A5 = ZERO
    this%CO = ZERO
    this%ANA = ZERO
    this%QC = ZERO
    this%VOLAC = ZERO
    this%AF2 = ZERO
    this%AF3 = ZERO
    this%SOD = ZERO
    this%PRT1 = ZERO
    this%PRT2 = ZERO
    this%PRT3 = ZERO
    this%F3 = ZERO
    this%CON = ZERO
    this%X = ZERO
    this%S1 = ZERO
    this%S2 = ZERO
    this%S3 = ZERO
    this%S4 = ZERO
    this%ZNAS = ZERO
    this%ZNAS1 = ZERO
    this%ZNAS2 = ZERO
    this%ZNAS3 = ZERO
    this%ZNAS4 = ZERO
    this%ZF1 = ZERO
    this%ZF2 = ZERO
    this%ZF3 = ZERO
    this%ZF4 = ZERO
    this%XWC1 = ZERO
    this%XWC2 = ZERO
    this%XWC3 = ZERO
    this%XWC4 = ZERO
    this%CPAC = ZERO
    this%CPS = ZERO
    this%CPF1 = ZERO
    this%CPF2 = ZERO
    this%CPF3 = ZERO
    this%CPF4 = ZERO
    this%CPWC1 = ZERO
    this%CPWC2 = ZERO
    this%CPWC3 = ZERO
    this%CPWC4 = ZERO
    this%AKS = ZERO
    this%AKF1 = ZERO
    this%AKF12 = ZERO
    this%AKF2 = ZERO
    this%AKF3 = ZERO
    this%AKF4 = ZERO
    this%AKWC1 = ZERO
    this%AKWC12 = ZERO
    this%AKWC2 = ZERO
    this%AKWC3 = ZERO
    this%AKWC4 = ZERO
    this%RHOA = ZERO
    this%RHS = ZERO
    this%RHF1 = ZERO
    this%RHF2 = ZERO
    this%RHF3 = ZERO
    this%RHF4 = ZERO
    this%RHWC1 = ZERO
    this%RHWC2 = ZERO
    this%RHWC3 = ZERO
    this%RHWC4 = ZERO
    this%ZF12 = ZERO
    this%XWC12 = ZERO

    return
end subroutine scenario_dataframe_init

!> Set numerical elements from array
subroutine scenario_dataframe_set_from_array(this, PUTINS)
    use iso_fortran_env, only: stderr => error_unit
    use m_constant, only: ZERO
    implicit none

! Arguments

    !> Object reference
    class(scenario_dataframe), intent(inout) :: this

    !> Array of input values
    real(kind=WP), dimension(:), intent(in) :: PUTINS

! Local variables

    integer :: NINPUTS

! Formats

500 format('ERROR: Cannot completely specify scenario with short ',     &
        'input array; ', I3, ' elements found, ', I3, ' are ',          &
        'required. Clearing input.')

    continue

    NINPUTS = size(PUTINS, dim=1)
    if (NINPUTS >= this%NPARAMS) then
        this%TSI = PUTINS(1)
        this%TS1I = PUTINS(2)
        this%TS2I = PUTINS(3)
        this%TS3I = PUTINS(4)
        this%TS4I = PUTINS(5)

        this%TF1I = PUTINS(6)
        this%TF2I = PUTINS(7)
        this%TF3I = PUTINS(8)
        this%TF4I = PUTINS(9)
        this%TWC1I = PUTINS(10)

        this%TWC2I = PUTINS(11)
        this%TWC3I = PUTINS(12)
        this%TWC4I = PUTINS(13)
        this%TGASCI = PUTINS(14)
        this%PGASCI = PUTINS(15)

        this%TI = PUTINS(16)
        this%TA = PUTINS(17)
        this%PA = PUTINS(18)
        this%XMI = PUTINS(19)
        this%XMAX = PUTINS(20)

        this%AF = PUTINS(21)
        this%A1 = PUTINS(22)
        this%A2 = PUTINS(23)
        this%A5 = PUTINS(24)
        this%CO = PUTINS(25)

        this%ANA = PUTINS(26)
        this%QC = PUTINS(27)
        this%VOLAC = PUTINS(28)
        this%AF2 = PUTINS(29)
        this%AF3 = PUTINS(30)

        this%SOD = PUTINS(31)
        this%PRT1 = PUTINS(32)
        this%PRT2 = PUTINS(33)
        this%PRT3 = PUTINS(34)
        this%F3 = PUTINS(35)

        this%CON = PUTINS(36)
        this%X = PUTINS(37)
        this%S1 = PUTINS(38)
        this%S2 = PUTINS(39)
        this%S3 = PUTINS(40)

        this%S4 = PUTINS(41)
        this%ZNAS = PUTINS(42)
        this%ZNAS1 = PUTINS(43)
        this%ZNAS2 = PUTINS(44)
        this%ZNAS3 = PUTINS(45)

        this%ZNAS4 = PUTINS(46)
        this%ZF1 = PUTINS(47)
        this%ZF2 = PUTINS(48)
        this%ZF3 = PUTINS(49)
        this%ZF4 = PUTINS(50)

        this%XWC1 = PUTINS(51)
        this%XWC2 = PUTINS(52)
        this%XWC3 = PUTINS(53)
        this%XWC4 = PUTINS(54)
        this%CPAC = PUTINS(55)

        this%CPS = PUTINS(56)
        this%CPF1 = PUTINS(57)
        this%CPF2 = PUTINS(58)
        this%CPF3 = PUTINS(59)
        this%CPF4 = PUTINS(60)

        this%CPWC1 = PUTINS(61)
        this%CPWC2 = PUTINS(62)
        this%CPWC3 = PUTINS(63)
        this%CPWC4 = PUTINS(64)
        this%AKS = PUTINS(65)

        this%AKF1 = PUTINS(66)
        this%AKF12 = PUTINS(67)
        this%AKF2 = PUTINS(68)
        this%AKF3 = PUTINS(69)
        this%AKF4 = PUTINS(70)

        this%AKWC1 = PUTINS(71)
        this%AKWC12 = PUTINS(72)
        this%AKWC2 = PUTINS(73)
        this%AKWC3 = PUTINS(74)
        this%AKWC4 = PUTINS(75)

        this%RHOA = PUTINS(76)
        this%RHS = PUTINS(77)
        this%RHF1 = PUTINS(78)
        this%RHF2 = PUTINS(79)
        this%RHF3 = PUTINS(80)

        this%RHF4 = PUTINS(81)
        this%RHWC1 = PUTINS(82)
        this%RHWC2 = PUTINS(83)
        this%RHWC3 = PUTINS(84)
        this%RHWC4 = PUTINS(85)

        this%ZF12 = PUTINS(86)
        this%XWC12 = PUTINS(87)
    else
        write(unit=stderr, fmt=500) NINPUTS, this%NPARAMS
        call this%init()
    end if

    return
end subroutine scenario_dataframe_set_from_array

!> Populate heat sink objects with input data
subroutine scenario_dataframe_populate_heatnet(this, hs_na_crust,       &
    hs_na_pool, hs_floor, hs_wall, qcond_path, qconv_path, qrad_path)
    use sofire2_param, only: n_hs_na_pool, n_hs_floor, n_hs_wall,       &
        n_qcond_path, n_qconv_path, n_qrad_path
    implicit none

! Arguments

    !> Object reference
    class(scenario_dataframe), intent(inout) :: this
    !> Sodium surface heatsink object
    type(heatsink_dataframe), target, intent(inout) :: hs_na_crust
    !> List of sodium pool heatsink objects
    type(heatsink_dataframe), dimension(n_hs_na_pool), target,          &
        intent(inout) :: hs_na_pool
    !> List of cell floor heatsink objects
    type(heatsink_dataframe), dimension(n_hs_floor), target,            &
        intent(inout) :: hs_floor
    !> List of cell wall heatsink objects
    type(heatsink_dataframe), dimension(n_hs_wall), target,             &
        intent(inout) :: hs_wall
    !> List of heat conduction paths
    type(qcond_dataframe), dimension(n_qcond_path), intent(inout) ::    &
        qcond_path
    !> List of heat convection paths
    type(qconv_dataframe), dimension(n_qconv_path), intent(inout) ::    &
        qconv_path
    !> List of heat convection paths
    type(qrad_dataframe), dimension(n_qrad_path), intent(inout) ::      &
        qrad_path

! Local variables

! Formats

    continue

    ! Set heat sink properties from scenario input
    hs_na_crust = heatsink_dataframe('Sodium surface',                  &
        this%A1, this%ZNAS, this%A1 * this%ZNAS * this%RHS,             &
        this%CPS, this%AKS, this%TSI)

    hs_na_pool(1) = heatsink_dataframe('Sodium pool node 1',            &
        this%A1, this%ZNAS1, this%A1 * this%ZNAS1 * this%RHS,           &
        this%CPS, this%AKS, this%TS1I)

    hs_na_pool(2) = heatsink_dataframe('Sodium pool node 2',            &
        this%A1, this%ZNAS2, this%A1 * this%ZNAS2 * this%RHS,           &
        this%CPS, this%AKS, this%TS2I)

    hs_na_pool(3) = heatsink_dataframe('Sodium pool node 3',            &
        this%A1, this%ZNAS3, this%A1 * this%ZNAS3 * this%RHS,           &
        this%CPS, this%AKS, this%TS3I)

    hs_na_pool(4) = heatsink_dataframe('Sodium pool node 4',            &
        this%A1, this%ZNAS4, this%A1 * this%ZNAS4 * this%RHS,           &
        this%CPS, this%AKS, this%TS4I)

    hs_floor(1) = heatsink_dataframe('Cell floor liner',                &
        this%A5, this%ZF1, this%A5 * this%ZF1 * this%RHF1,              &
        this%CPF1, this%AKF1, this%TF1I)

    hs_floor(2) = heatsink_dataframe('Cell floor node 2',               &
        this%A5, this%ZF2, this%A5 * this%ZF2 * this%RHF2,              &
        this%CPF2, this%AKF2, this%TF2I)

    hs_floor(3) = heatsink_dataframe('Cell floor node 3',               &
        this%A5, this%ZF3, this%A5 * this%ZF3 * this%RHF3,              &
        this%CPF3, this%AKF3, this%TF3I)

    hs_floor(4) = heatsink_dataframe('Cell floor node 4',               &
        this%A5, this%ZF4, this%A5 * this%ZF4 * this%RHF4,              &
        this%CPF4, this%AKF4, this%TF4I)

    hs_wall(1) = heatsink_dataframe('Cell wall liner',                  &
        this%A2, this%XWC1, this%A2 * this%XWC1 * this%RHWC1,           &
        this%CPWC1, this%AKWC1, this%TWC1I)

    hs_wall(2) = heatsink_dataframe('Cell wall node 2',                 &
        this%A2, this%XWC2, this%A2 * this%XWC2 * this%RHWC2,           &
        this%CPWC2, this%AKWC2, this%TWC2I)

    hs_wall(3) = heatsink_dataframe('Cell wall node 3',                 &
        this%A2, this%XWC3, this%A2 * this%XWC3 * this%RHWC3,           &
        this%CPWC3, this%AKWC3, this%TWC3I)

    hs_wall(4) = heatsink_dataframe('Cell wall node 4',                 &
        this%A2, this%XWC4, this%A2 * this%XWC4 * this%RHWC4,           &
        this%CPWC4, this%AKWC4, this%TWC4I)

    ! Define heat conduction paths
    qcond_path(1) = qcond_dataframe(                                    &
        'POOL 4 TO FLOOR LINER 1',                                      &
        hs_na_pool(4), hs_floor(1),                                     &
        ZERO, ZERO, ZERO)
    qcond_path(2) = qcond_dataframe(                                    &
        'FLOOR LINER 1 TO FLOOR 2',                                     &
        hs_floor(1), hs_floor(2),                                       &
        this%A5, this%ZF12, this%AKF12)
    qcond_path(3) = qcond_dataframe(                                    &
        'FLOOR 2 TO FLOOR 3',                                           &
        hs_floor(2), hs_floor(3),                                       &
        ZERO, ZERO, ZERO)
    qcond_path(4) = qcond_dataframe(                                    &
        'FLOOR 3 TO FLOOR 4',                                           &
        hs_floor(3), hs_floor(4),                                       &
        ZERO, ZERO, ZERO)
    qcond_path(5) = qcond_dataframe(                                    &
        'FLOOR 4 TO AMBIENT',                                           &
        hs_floor(4), null(),                                            &
        ZERO, ZERO, ZERO)

    qcond_path(6) = qcond_dataframe(                                    &
        'WALL LINER 1 TO WALL 2',                                       &
        hs_wall(1), hs_wall(2),                                         &
        this%A2, this%XWC12, this%AKWC12)
    qcond_path(7) = qcond_dataframe(                                    &
        'WALL 2 TO WALL 3',                                             &
        hs_wall(2), hs_wall(3),                                         &
        ZERO, ZERO, ZERO)
    qcond_path(8) = qcond_dataframe(                                    &
        'WALL 3 TO WALL 4',                                             &
        hs_wall(3), hs_wall(4),                                         &
        ZERO, ZERO, ZERO)
    qcond_path(9) = qcond_dataframe(                                    &
        'WALL 4 TO AMBIENT',                                            &
        hs_wall(4), null(),                                             &
        ZERO, ZERO, ZERO)

    qcond_path(10) = qcond_dataframe(                                   &
        'SODIUM SURFACE TO POOL 1',                                     &
        hs_na_crust, hs_na_pool(1),                                     &
        ZERO, ZERO, ZERO)
    qcond_path(11) = qcond_dataframe(                                   &
        'POOL 1 TO POOL 2',                                             &
        hs_na_pool(1), hs_na_pool(2),                                   &
        ZERO, ZERO, ZERO)
    qcond_path(12) = qcond_dataframe(                                   &
        'POOL 2 TO POOL 3',                                             &
        hs_na_pool(2), hs_na_pool(3),                                   &
        ZERO, ZERO, ZERO)
    qcond_path(13) = qcond_dataframe(                                   &
        'POOL 3 TO POOL 4',                                             &
        hs_na_pool(3), hs_na_pool(4),                                   &
        ZERO, ZERO, ZERO)

    ! Convection paths
    qconv_path(1) = qconv_dataframe('SODIUM SURFACE TO GAS',            &
        0.14_WP, this%A1)
    qconv_path(2) = qconv_dataframe('GAS TO WALL LINER',                &
        0.27_WP, this%A2)

    ! Radiation paths
    qrad_path(1) = qrad_dataframe('QRAD1: FLOOR LINER TO FLOOR 2',      &
        this%AF2, this%A5)
    qrad_path(2) = qrad_dataframe('QRAD2: WALL LINER TO WALL 2',        &
        this%AF2, this%A2)
    qrad_path(3) = qrad_dataframe('QRAD3: SODIUM CRUST TO GAS',         &
        this%AF3, this%A1)
    qrad_path(4) = qrad_dataframe('QROD: SODIUM CRUST TO WALL LINER',   &
        this%AF, this%A1)

    return
end subroutine scenario_dataframe_populate_heatnet

!> Return number of timesteps to calculate before printing output based
!! on current problem time
pure integer function scenario_dataframe_set_legacy_print_interval(     &
    this, T) result(prtlimit)
    use m_constant, only: ONE, THREE
    implicit none

    !> Object reference
    class(scenario_dataframe), intent(in) :: this

    !> Current problem time, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: T

    continue

    if (T <= ONE) then
        ! Short term (T <= 1hr) steps between output
        prtlimit = int(this%PRT1)
    else if (T <= THREE) then
        ! Intermediate term (1 hr < T <= 3hr) steps between output
        prtlimit = int(this%PRT2)
    else
        ! Long term (T > 3hr) steps between output
        prtlimit = int(this%PRT3)
    end if

    return
end function scenario_dataframe_set_legacy_print_interval

!> Initialize all dynamic elements in dataframe
subroutine timestep_dataframe_init(this)
    use m_constant, only: BIG1P9
    implicit none

! Arguments

    !> Object reference
    class(timestep_dataframe), intent(inout) :: this

    continue

    this%DELC1 = BIG1P9
    this%DELC2 = BIG1P9
    this%DELC3 = BIG1P9
    this%DELC4 = BIG1P9
    this%DELF1 = BIG1P9
    this%DELF2 = BIG1P9
    this%DELF3 = BIG1P9
    this%DELF4 = BIG1P9
    this%DELGC = BIG1P9
    this%DELS = BIG1P9
    this%DELS1 = BIG1P9
    this%DELS2 = BIG1P9
    this%DELS3 = BIG1P9
    this%DELS4 = BIG1P9

    return
end subroutine timestep_dataframe_init

!> Calculate timesteps based on sensitivity to heat transfer
subroutine timestep_dataframe_update(                                   &
    this, scenario, RHOC,                                               &
    hs_wall, hs_floor, hs_na_crust, hs_na_pool,                         &
    qcond_path,                                                         &
    YCONV1, YCONV2, YROD, YRAD1, YRAD2, YRAD3)
    use sofire2_param, only: n_hs_wall, n_hs_floor, n_hs_na_pool,       &
        n_qcond_path
    implicit none

! Arguments

    !> Object reference
    class(timestep_dataframe), intent(inout) :: this

    !> Scenario dataframe
    type(scenario_dataframe), intent(in) :: scenario

    !> Cell gas density, \f$\us{\lbm\per\cubic\foot}\f$
    real(kind=WP), intent(in) :: RHOC

    !> Array of cell wall heat sink objects
    class(heatsink_dataframe), dimension(n_hs_wall), intent(in) ::      &
        hs_wall

    !> Array of cell floor heat sink objects
    class(heatsink_dataframe), dimension(n_hs_floor), intent(in) ::     &
        hs_floor

    !> Sodium pool surface heat sink object
    class(heatsink_dataframe), intent(in) :: hs_na_crust

    !> Array of sodium pool heat sink objects
    class(heatsink_dataframe), dimension(n_hs_na_pool), intent(in) ::   &
        hs_na_pool

    !> Array of heat conduction paths
    class(qcond_dataframe), dimension(n_qcond_path), intent(in) ::      &
        qcond_path

    !> Convective admittance, TBD \f$\us{\BTU\per\hour\per\rankine}\f$
    real(kind=WP), intent(in) :: YCONV1

    !> Convective admittance, TBD \f$\us{\BTU\per\hour\per\rankine}\f$
    real(kind=WP), intent(in) :: YCONV2

    !> Radiative admittance, TBD \f$\us{\BTU\per\hour\per\rankine}\f$
    real(kind=WP), intent(in) :: YROD

    !> Radiative admittance, TBD \f$\us{\BTU\per\hour\per\rankine}\f$
    real(kind=WP), intent(in) :: YRAD1

    !> Radiative admittance, TBD \f$\us{\BTU\per\hour\per\rankine}\f$
    real(kind=WP), intent(in) :: YRAD2

    !> Radiative admittance, TBD \f$\us{\BTU\per\hour\per\rankine}\f$
    real(kind=WP), intent(in) :: YRAD3

    integer :: i
    real(kind=WP), dimension(n_qcond_path) :: YCOND

    continue

    ! YCOND(1:n_qcond_path) = qcond_path(1:n_qcond_path)%conductance()
    do i = 1, n_qcond_path
        YCOND(i) = qcond_path(i)%conductance()
    end do

    this%DELF1 = hs_floor(1)%th_capacity()                              &
        / (YCOND(1) + YCOND(2) + YRAD1)
    this%DELF2 = hs_floor(2)%th_capacity()                              &
        / (YCOND(3) + YCOND(2) + YRAD1)
    this%DELF3 = hs_floor(3)%th_capacity() / (YCOND(3) + YCOND(4))
    this%DELF4 = hs_floor(4)%th_capacity() / (YCOND(4) + YCOND(5))

    this%DELS1 = hs_na_pool(1)%th_capacity() / (YCOND(10) + YCOND(11))
    this%DELS2 = hs_na_pool(2)%th_capacity() / (YCOND(11) + YCOND(12))
    this%DELS3 = hs_na_pool(3)%th_capacity() / (YCOND(12) + YCOND(13))
    this%DELS4 = hs_na_pool(4)%th_capacity() / (YCOND(13) + YCOND(1))

    this%DELS = hs_na_crust%th_capacity()                               &
        / (YCOND(10) + YRAD3 + YCONV1 + YROD)

    this%DELGC = RHOC * scenario%CPAC * scenario%VOLAC                  &
        / (YCONV2 + YRAD3 + YCONV1)

    this%DELC1 = hs_wall(1)%th_capacity()                               &
        / (YRAD2 + YCOND(6) + YCONV2 + YROD)
    this%DELC2 = hs_wall(2)%th_capacity()                               &
        / (YRAD2 + YCOND(6) + YCOND(7))
    this%DELC3 = hs_wall(3)%th_capacity() / (YCOND(7) + YCOND(8))
    this%DELC4 = hs_wall(4)%th_capacity() / (YCOND(8) + YCOND(9))

    return
end subroutine timestep_dataframe_update

!> Return limiting (minimum) timestep, \f$\us{\hour}\f$. Limit timestep
!! to a minimum of dtlimit which defaults to \f$\US{1E-6}{\hour}\f$
pure real(kind=WP) function timestep_dataframe_get_dt(this) result(dt)
    implicit none

! Arguments

    !> Object reference
    class(timestep_dataframe), intent(in) :: this

    continue

    dt = min(this%DELS, this%DELGC,                                     &
        this%DELF1, this%DELF2, this%DELF3, this%DELF4,                 &
        this%DELS1, this%DELS2, this%DELS3, this%DELS4,                 &
        this%DELC1, this%DELC2, this%DELC3, this%DELC4)
    dt = max(dt, this%dtlimit)

    return
end function timestep_dataframe_get_dt

!> Set heatsink attributes to default values
subroutine heatsink_dataframe_init(this)
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(inout) :: this
    continue
    this%name = 'DEFAULT HEAT SINK              '
    this%ahx = ZERO
    this%thickness = ZERO
    this%mass = ZERO
    this%cp = ZERO
    this%k = ZERO
    this%T0 = ZERO
    return
end subroutine heatsink_dataframe_init

!> @brief Add mass to heatsink. Adjust thickness to maintain consistent
!! density.
!!
!! `dmass` may be positive or negative, allowing both addition and
!! removal of mass. Heat sink thickness will not be reduced to less
!! than \f$\US{0.1}{\inch}\f$. Mass added or removed is assumed to have
!! the same properties as the heat sink (temperature, density, thermal
!! conductivity, specific heat, etc.)
subroutine heatsink_dataframe_add_mass(this, dmass)
    use m_constant, only: TENTH, IN_FT
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(inout) :: this

    !> Change in heatsink mass, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: dmass

    real(kind=WP) :: new_mass
    real(kind=WP) :: new_thickness

    continue
    new_mass = this%mass + dmass
    new_thickness = new_mass / (this%density() * this%ahx)

    if (new_thickness >= TENTH / IN_FT) then
        this%mass = new_mass
        this%thickness = new_thickness
    end if

    return
end subroutine heatsink_dataframe_add_mass

!> Calculate heatsink volume, \f$\us{\cubic\foot}\f$
elemental real(kind=WP) function heatsink_dataframe_volume(this)        &
    result(v)
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(in) :: this
    continue
    v = this%ahx * this%thickness
    return
end function heatsink_dataframe_volume

!> Calculate heatsink density, \f$\us{\lbm\per\cubic\foot}\f$
elemental real(kind=WP) function heatsink_dataframe_density(this)       &
    result(rho)
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(in) :: this
    continue
    rho = this%mass / this%volume()
    return
end function heatsink_dataframe_density

!> Calculate heatsink thermal conductance, \f$\us{\BTU\per\rankine}\f$
elemental real(kind=WP) function                                        &
    heatsink_dataframe_thermal_conductance(this) result(TCOND)
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(in) :: this
    continue
    TCOND = this%k * this%ahx / this%thickness
    return
end function heatsink_dataframe_thermal_conductance

!> Calculate thermal resistance of slab half-thickness,
!! \f$\us{\rankine\per\BTU}\f$
elemental real(kind=WP) function                                        &
    heatsink_dataframe_half_resistance(this) result(THRESIST)
    use m_constant, only: HALF
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(in) :: this
    continue
    THRESIST = HALF * this%thickness / (this%k * this%ahx)
    return
end function heatsink_dataframe_half_resistance

!> Calculate heatsink thermal capacity, \f$\us{\BTU\per\rankine}\f$
elemental real(kind=WP) function                                        &
    heatsink_dataframe_thermal_capacity(this) result(TCAP)
    implicit none
    !> Object reference
    class(heatsink_dataframe), intent(in) :: this
    continue
    TCAP = this%mass * this%cp
    return
end function heatsink_dataframe_thermal_capacity

!> Initialize heat conduction attributes and nullify pointers
subroutine qcond_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none
    !> Object reference
    class(qcond_dataframe), intent(inout) :: this
    continue

    this%name = 'UNINITIALIZED CONDUCTION PATH   '
    nullify(this%hs1)
    nullify(this%hs2)
    this%ahxgap = ZERO
    this%xgap = ZERO
    this%kgap = ZERO

    return
end subroutine qcond_dataframe_init

!> Return gap resistance, \f$\us{\rankine\per\BTU}\f$. If any gap
!! characteristic is not positive, zero is returned.
pure real(kind=WP) function qcond_dataframe_gap_resistance(this)        &
    result(rgap)
    use m_constant, only: ZERO
    implicit none
    !> Object reference
    class(qcond_dataframe), intent(in) :: this
    continue

    if (min(this%ahxgap, this%xgap, this%kgap) > ZERO) then
        rgap = max(ZERO, this%xgap / (this%ahxgap * this%kgap))
    else
        rgap = ZERO
    end if

    return
end function qcond_dataframe_gap_resistance

!> Return thermal conductance composed of source and destination
!! half-resistance and gap resistance (if any),
!! \f$\us{\BTU\per\rankine}\f$
pure real(kind=WP) function qcond_dataframe_conductance(this)           &
    result(cond)
    use m_constant, only: ZERO, ONE, TWO
    implicit none
    !> Object reference
    class(qcond_dataframe), intent(in) :: this
    continue

    if (associated(this%hs1) .and. associated(this%hs2)) then
        ! If both associated, calculate conductivity between two heat
        ! sinks plus any gap
        cond = ONE / (this%hs1%half_resistance()                        &
                    + this%gap_resistance()                             &
                    + this%hs2%half_resistance())
    else if (associated(this%hs1)) then
        ! If only hs1 associated, calculate conductivity of hs1
        ! half-slab
        cond = TWO * this%hs1%th_conductance()
    else if (associated(this%hs2)) then
        ! If only hs2 associated, calculate conductivity of hs2
        ! half-slab
        cond = TWO * this%hs2%th_conductance()
    else
        ! Neither heat sink is associated; return zero (no conductivity)
        ! NOTE: This is likely an error which should be handled upstream
        cond = ZERO
    end if

    return
end function qcond_dataframe_conductance

!> Initialize heat convection path attributes
subroutine qconv_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none
    !> Object reference
    class(qconv_dataframe), intent(inout) :: this
    continue

    this%name = 'UNINITIALIZED CONVECTION PATH   '

    this%coeff = ZERO
    this%ahx = ZERO

    return
end subroutine qconv_dataframe_init

!> Calculate convective heat transfer
elemental real(kind=WP) function qconv_dataframe_qconvection(this,      &
    tsurface, tgas, rhogas) result(qconv)
    use m_constant, only: ZERO, THIRD, HALF, ONE, GIN
    use m_material, only: kgair, muair
    implicit none
    !> Object reference
    class(qconv_dataframe), intent(in) :: this
    !> Surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: tsurface
    !> Gas temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: tgas
    !> Gas density, \f$\us{\lbm\per\cubic\foot}\f$
    real(kind=WP), intent(in) :: rhogas

    real(kind=WP) :: DT
    real(kind=WP) :: tfilm
    real(kind=WP) :: beta
    real(kind=WP) :: D
    real(kind=WP) :: EX

    continue

    dt = tsurface - tgas
    tfilm = HALF * (tsurface + tgas)
    beta = ONE / tfilm
    D = (muair(tfilm) / rhogas)**2
    EX = (GIN * beta / D * abs(dt))**THIRD
    qconv = this%coeff * this%ahx * kgair(tfilm) * dt * EX

    return
end function qconv_dataframe_qconvection

!> Initialize heat radiation path attributes
subroutine qrad_dataframe_init(this)
    use m_constant, only: ZERO
    implicit none
    !> Object reference
    class(qrad_dataframe), intent(inout) :: this
    continue

    this%name = 'UNINITIALIZED RADIATION PATH    '

    this%emissivity = ZERO
    this%ahx = ZERO

    return
end subroutine qrad_dataframe_init

!> Radiative heat transferred from emitter to receiver in
!! units of \f$\us{\BTU\per\hour}\f$. The value is positive
!! for an emitter which is hotter than the receiver.
elemental real(kind=WP) function qrad_dataframe_qrad(this, tsrc, tdest) &
    result(qrad)
    use m_constant, only: SIGMA_SB_US
    implicit none

    !> Object reference
    class(qrad_dataframe), intent(in) :: this

    !> Emitter surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: tsrc

    !> Receiver surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: tdest

    continue

    qrad = this%ahx * this%emissivity * SIGMA_SB_US                     &
        * (tsrc**4 - tdest**4)

    return
end function qrad_dataframe_qrad

end module m_dataframe
