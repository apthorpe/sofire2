\chapter{Theory}

\section{SOFIRE~II -- One-Cell}

The original one-cell model diagram has been modified to more clearly
illustrate the arrangement of heat sinks and emphasize the air gap
between the burn pan/liner and the cell floor/wall heat sinks. Emphasis
has also been placed on the distinction between the sodium surface node
where oxidation reactions take place and the nodes representing the
sodium pool. The current one-cell model diagram is shown in
Figure~\ref{fig:c1_geometry}.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c1_geometry}
    \end{center}
    \caption{Revised one-cell model geometry}
    \label{fig:c1_geometry}
\end{figure}

\subsection{Sodium Burning Rate}
\label{s:theory_c1_burn_rate}

During a number of pool fire tests, it was determined that the sodium
burning rate was proportional to the oxygen concentration and was controlled
by the diffusion of oxygen to the sodium surface. Therefore, the burning rate
can be computed by

\begin{equation} \label{eq:t1}
    \left(\frac{dm}{dt} \frac{1}{A_{s}}\right) = H_{G} C \rho_{G} S
\end{equation}
\nomenclature{$A_{s}$}{Surface area of sodium pool}%
\nomenclature{$m$}{Mass of sodium burned}%
\nomenclature{$t$}{Time of burn}%
\nomenclature{$H_{G}$}{Gas transport coefficient}%
\nomenclature{$C$}{Mass fraction of oxygen}%
\nomenclature{$\rho_{G}$}{Density of gas}%
\nomenclature{$S$}{Stoichiometric combustion ratio}%

The gas transport coefficient, $H_{G}$, is derived from the free convection
equation for the heat transfer coefficient invoking the analogy between heat and
mass transfer. Thermal conductivity is replaced by the diffusion coefficient \cite{Perry_3e}
and the Prandtl number is replaced by the Schmidt number,
\nomenclature{$K$}{Thermal conductivity}%
\nomenclature{$\text{Pr}$}{Prandtl number}%
\nomenclature{$\text{Sc}$}{Schmidt number}%

\begin{equation} \label{eq:t2}
    H_{G} = 0.14 D \left[ g \text{Sc} \frac{\beta}{\nu^{2}} \left(T_{ss} - T_{G}\right)\right]^{\frac{1}{3}}
\end{equation}
\nomenclature{$D$}{Diffusion coefficient}%
\nomenclature{$g$}{Gravitational acceleration}%
\nomenclature{$\beta$}{Coefficient of gas expansion?}%
\nomenclature{$\nu$}{Kinematic viscosity}%
\nomenclature{$T_{ss}$}{Temperature of sodium surface}%
\nomenclature{$T_{G}$}{Temperature of gas}%

The description of the one-cell geometry is shown in Figure~\ref{fig:c1_geometry}.

\label{loc:combustion_ratio}
In the above mentioned pool fire tests, it was determined that the reaction
products consisted of a varying proportion of sodium monoxide to sodium peroxdide.
In calculating burning rates, the stoichiometric combustion ratio for
sodium monoxide is 2.88~lbm-sodium / lbm-oxygen consumed, and the ratio for
sodium peroxide is 1.44~lbm-sodium / lbm-oxygen consumed.

\begin{align}
    f_{s,\ce{Na2O}} & = \US{2.88}{\lbm_{\ce{Na}}\per\lbm_{\ce{O}}} \\
    f_{s,\ce{Na2O2}} & = \US{1.44}{\lbm_{\ce{Na}}\per\lbm_{\ce{O}}} \\
\end{align}
\nomenclature{$f_{s}$}{Stoichiometric combustion ratio, mass of reactant per mass of oxygen for a given product}%
\nomenclature{$f_{s,\ce{Na2O}}$}{Stoichiometric combustion ratio for \ce{Na2O}, mass of \ce{Na} per mass \ce{O} = 2.88}%
\nomenclature{$f_{s,\ce{Na2O2}}$}{Stoichiometric combustion ratio for \ce{Na2O2}, mass of \ce{Na} per mass \ce{O} = 1.44}%

\label{loc:hform}
The net energy added to the system by the combustion of sodium is the heat
of formation, which is the difference between the heat of combustion for each
oxide and the heat of vaporization for the sodium. In the use of the model, an
appropriate value for sodium monoxide is 3900~BTU/lbm-sodium and for sodium
peroxide 4500~BTU/lbm-sodium.

\begin{align}
    \Delta H_{f,\ce{Na2O}} & = \US{3900}{\BTU\per\lbm_{\ce{Na}}} \\
    \Delta H_{f,\ce{Na2O2}} & = \US{4500}{\BTU\per\lbm_{\ce{Na}}} \\
\end{align}
\nomenclature{$\Delta H_{f,\ce{Na2O}}$}{Heat of formation of \ce{Na2O}}%
\nomenclature{$\Delta H_{f,\ce{Na2O2}}$}{Heat of formation of \ce{Na2O2}}%

Since the model is written for constant values of the stoichiometric combustion
ratio, $S$, and heat of formation, $\Delta H$, these values can be computed for
a mixture of oxides by

\begin{equation}
    S = 2.88 k + 1.44 (1 - k)
\end{equation}
\nomenclature{$k$}{Fraction of total consumed oxygen used to form sodium monoxide}%

\begin{align}
    \Delta H & = f_{s,\ce{Na2O}} k \Delta H_{f,\ce{Na2O}} + \frac{f_{s,\ce{Na2O2}} (1 - k) \Delta H_{f,\ce{Na2O2}}}{S} \\
             & = 2.88 k (\US{3900}{\BTU\per\lbm_{\ce{Na}}}) + \frac{1.44 (1 - k) (\US{4500}{\BTU\per\lbm_{\ce{Na}}})}{S}, \us{\BTU\per\lbm_{\ce{Na}}} \\
\end{align}
\nomenclature{$\Delta H$}{Heat of formation per unit-mass of sodium consumed}%

where $k$ is the fraction of total consumed oxygen used to form sodium monoxide.

Upper and lower limits can be calculated by using the values of $k$ for pure
sodium monoxide and pure sodium peroxide, respectively. Sodium monoxide\footnote{The
use of 100\% sodium monoxide results in the prediction of higher system pressures
than will actually occur.} formation produces more energy than does sodium peroxide,
because of the higher stoichiometric combustion ratio. The product of $A \Delta H$
and Eq.~\ref{eq:t1} is now defined as $Q_{ox}$, and the total energy production rate is
in BTU/hr:

\begin{equation}
    Q_{ox} = H_{G} C \rho_{G} S A_{s} \Delta H
\end{equation}
\nomenclature{$Q_{ox}$}{total energy production rate from combusion}%

\subsection{Heat Transfer Rates}

As the sodium reacts with oxygen transferred to the surface, the energy produced is
in turn transferred to the floor of the cell by conduction through the bulk of the sodium;
from the sodium surface to the gas by convection and radiation; and by radiation to
the cell walls. The heat transferred to the gas is transferred by convection to the
walls which in turn is transferred to the insulation on the walls. The heat sink
available to all the boundary surfaces is the atmosphere.

The sodium pool is divided into five nodes, the surface and four thicker nodes.
The conduction rate between any two nodes $A$ and $B$ is,

\begin{equation}
    Q_{cond} = \frac{K_{s}A_{s}}{x} \left(T_{A} - T_{B}\right)
\end{equation}
\nomenclature{$Q_{cond}$}{Heat conduction rate}%
\nomenclature{$K_{s}$}{Thermal conductivity of liquid sodium}%
\nomenclature{$x$}{Linear distance between centroids of heat sink nodes `A' and `B'}%
\nomenclature{$T_{A}$}{Temperature of sodium heat sink node `A'}%
\nomenclature{$T_{B}$}{Temperature of sodium heat sink node `B'}%

In this manner, heat is transferred to the first node in the floor, which is normally
the cell liner.

The convective heat transfer coefficient, $h_{c}$, is defined by the equation

\begin{equation}
    \label{eq:h_conv_floor}
    h_{c} = 0.14 K_{G} \left( g \text{Pr} \frac{\beta}{\nu^{2}} \left(T_{ss} - T_{G}\right)\right)^{\frac{1}{3}}
\end{equation}
\nomenclature{$h_{c}$}{Convective heat transfer coefficient from sodium to gas}%
\nomenclature{$K_{G}$}{Thermal conductivity of gas}%

The portion of the heat which is transferred to the gas from the sodium by
natural convection can be described by

\begin{align}
    \label{eq:q_conv_floor}
    Q_{conv} & = h_{c} A_{s} \left(T_{ss} - T_{G}\right) \\
      & = 0.14 A_{s} \left(T_{ss} - T_{G}\right) K_{G} \left[ g \text{Pr} \frac{\beta}{\nu^{2}} \left(T_{ss} - T_{G}\right)\right]^{\frac{1}{3}}
\end{align}
\nomenclature{$Q_{conv}$}{Heat convection rate from sodium pool to gas}%

The energy transferred from the sodium to the aerosol particles in the gas
phase by radiative heat transfer is

\begin{equation} \label{eq:t5}
    Q_{a} = \sigma A_{s} \epsilon \left(T_{ss}^{4} - T_{G}^{4}\right)
\end{equation}
\nomenclature{$Q_{a}$}{Heat radiation rate to aerosols}%
\nomenclature{$\sigma$}{Stefan-Boltzmann constant}%
\nomenclature{$\epsilon$}{Emissivity of a surface for radiative heat transport}%

The emissivity, $\epsilon$, is a function of the amount of sodium oxide
aerosol which is in the gas phase. At low concentrations, $\epsilon$
approaches zero while at high concentrations $\epsilon$ is 1.0. The value
of $\epsilon$ used in the model calculations is based on the anticipated
aerosol concentration.

Additionally, there is some direct heat radiated from the pool surface to
the walls of the cell which can be described by an equation similar to Equation~\ref{eq:t5}:

\begin{equation}
    Q_{w} = \sigma A_{s} \Lscript \left(T_{ss}^{4} - T_{w}^{4}\right)
\end{equation}
\nomenclature{$Q_{w}$}{Heat radiation rate to cell walls}%
\nomenclature{$\Lscript$}{Radiation interchange factor}%
\nomenclature{$T_{w}$}{Wall surface temperature}%

The value of the radiation interchange factor, $\Lscript$, is a function of the aerosol
concentration. At high aerosol concentrations, the heat radiated from the
sodium is captured by the aerosol particles so that no heat can be absorbed by
the walls and therefore $\Lscript$ is zero. At very low concentrations, no heat is radiated
to the particles and $\Lscript$ is 1.0. At intermediate concentrations, $\Lscript$ will lie
between these limiting values.

The heat which had been transferred to the gas by convection and radiation
is lost to the walls by convection:

\begin{equation}
    Q_{wc} = h_{cw} A_{w} \left(T_{G} - T_{w}\right)
\end{equation}
\nomenclature{$Q_{wc}$}{Heat convection rate from gas to cell walls}%
\nomenclature{$h_{cw}$}{Convective heat transfer rate from gas to cell walls}%
\nomenclature{$A_{w}$}{Wall (surface) heat transfer area}%

The convective heat transfer coefficient, $h_{cw}$ in the gas space, is defined by

\begin{equation}
    \label{eq:h_conv_wall}
    h_{cw} = 0.27 K_{G} \left[ g \text{Pr} \frac{\beta}{\nu^{2}} \left(T_{G} - T_{w}\right)\right]^{\frac{1}{3}}
\end{equation}

Usually, the liner in a cell is separated from the concrete by a gas space so
that heat in the liner is transferred to the floor or wall node by radiation, as
defined in Equation~\ref{eq:t5}, except that the emissivity is the effective emissivity
of the liner and wall or floor material. The heat reaching the first node in the
concrete is then lost to the atmosphere by conduction through the concrete.

The heat balance equations which are used to describe the transient thermal
behavior in a physical system are as follows:

\subsubsection{Gas Heat Balance}

\begin{align}
    \rho_{G}(C_{V})_{G} \int_{V_{G}} \frac{dT_{G}}{dt} dV_{G} = & A_{s} h_{c} \left(T_{s} - T_{G}\right) \\
    & - A_{s} \sigma \Lscript \left(T_{s}^{4} - T_{G}^{4}\right) \\
    & - A_{w} h_{cw} \left(T_{G} - T_{w}\right)
\end{align}
%(C_{V})_{G}
%V_{G}
\nomenclature{$(C_{V})_{G}$}{Specific heat of gas at constant volume}%
\nomenclature{$V_{G}$}{Cell gas volume}%

\subsubsection{Cell Wall Heat Balance}

\begin{equation}
    \rho_{w} \int_{V_{w}} (C_{p})_{w} \frac{dT_{w}}{dt} dV_{w} + \rho_{in} \int_{V_{in}} (C_{p})_{in} \frac{dT_{in}}{dt} dV_{in} = h_{cw} \A_{w} \left(T_{G} - T_{w}\right)
\end{equation}
\nomenclature{$\rho_{w}$}{Wall density}%
\nomenclature{$V_{w}$}{Wall volume}%
\nomenclature{$(C_{p})_{w}$}{Specific heat of wall material}%
\nomenclature{$\rho_{in}$}{Insulation density}%
\nomenclature{$V_{in}$}{Insulation volume}%
\nomenclature{$(C_{p})_{in}$}{Specific heat of insulation material}%

\subsubsection{Burn Pan Heat Balance}

\begin{equation}
    \begin{split}
        \rho_{Na}(C_{P})_{Na} \int_{V_{Na}} \frac{dT_{Na}}{dt} dV_{Na} & {} \\
        + \rho_{p}(C_{P})_{p} \int_{V_{p}} \frac{dT_{p}}{dt} dV_{p} & {} \\
        + \rho_{in}(C_{P})_{in} \int_{V_{in}} \frac{dT_{in}}{dt} dV_{in}
        & = Q_{ox} - A_{s} h_{c} \left(T_{s} - T_{G}\right) \\
        {} & - \epsilon A_{s} \Lscript \sigma \left(T_{s}^{4} - T_{G}^{4}\right)
    \end{split}
\end{equation}
\nomenclature{$\rho_{Na}$}{Sodium density}%
\nomenclature{$(C_{P})_{Na}$}{Specific heat of sodium}%
\nomenclature{$V_{Na}$}{Sodium volume}%
\nomenclature{$\rho_{p}$}{Burn pan density}%
\nomenclature{$(C_{P})_{p}$}{Specific heat of burn pan material}%
\nomenclature{$V_{p}$}{Burn pan volume}%

Since the program uses the finite difference method to solve these equations,
the temperature of each node at each time step is approximated by

\begin{equation}
T_{t + \mathtt{DTMIN}} = T_{t} + \frac{\mathtt{DTMIN}}{\mathtt{CAP}}\left[Q_{ox} + \sum Y\left(T_{i} - T_{o}\right)\right]
\end{equation}

where

\begin{itemize}
    \item $\mathtt{DTMIN}$ is the timestep,
    \item $\mathtt{CAP}$ is the heat capacitance of the node, \us{\BTU\per\rankine}%BTU/$^{\circ}$R
    \item $T_{o}$ is the temperature of the node being iterated at time $t + \mathtt{DTMIN}$
    \item $T_{i}$ is the temperature of the node connected to node 0
\end{itemize}

The timestep, \texttt{DTMIN}, must be small or the calculation procedure will cause some
of the temperatures to diverge and the calculations to ``blow up.'' The code is programmed
to select the lowest value of the time steps calculated for each node by dividing the heat
flow path admittences to each node into the heat capacity of each node:

\begin{equation}
    \mathtt{DT} = \frac{\mathtt{CAP}_{o}}{\sum Y}
\end{equation}

Additionally, to ensure that the time step is small, the minimum step calculated
as above is multiplied by a time step reduction factor, $\underbar{X}$, which is an input
parameter $\le 1.0$. If the calculation with the chosen value of $\underbar{X}$ does not proceed,
it should be rerun with a lower value of $\underbar{X}$. In general, $\underbar{X}$ is close to 1.0 for
small sodium spills or low temperature spills and $\underbar{X}$ is close to 0.1 for large
sodium spills.
\nomenclature{$\underbar{X}$}{time step reduction factor}

\clearpage

\section{SOFIRE II -- Two-Cell}

\subsection{Mass Transfer}

In the two-cell version of SOFIRE~II, the sodium burning occurs in the
lower compartment and the hot gas which results rises through an opening into
the upper compartment because of density differences between the gas in the
two cells. Cold gas, with the average oxygen content of the upper compartment,
flows down the same opening into the cell for the same reason. Heat is transferred
from the combustion cell to the second compartment in the hot gas.
This hot gas mixes with the cold gas around it and the resultant mixture loses
heat to the walls of the second compartment by natural convection. The heat
transfer equations for the two cells is added to the SOFIRE~II one-cell model and the new
version is designated SOFIRE~II - Two-Cell. See Figure~\ref{fig:c2_geometry} for a diagram of the
two-cell geometry.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c2_geometry}
    \end{center}
    \caption{Revised two-cell model geometry}
    \label{fig:c2_geometry}
\end{figure}

The relationship used to describe the mass flow from the upper volume to
the lower is based on the work of Brown \cite{Brown1962-2} who studied the natural convection
through openings in partitions. His work led to an empirical relationship which was fitted
by experimental values for Grashof numbers from \num{1E4} to \num{5E7}.
The corresponding Nusselt number is

\begin{equation} \label{eq:t12}
    \text{Nu}_{X} = 0.0546 \text{Gr}_{X}^{0.55} \text{Pr}\left(\frac{L}{X}\right)^{\frac{1}{3}}
\end{equation}
\nomenclature{$\text{Gr}$}{Grashof number}%
\nomenclature{$\text{Nu}$}{Nusselt number}%

where the Nusselt number is defined as

\begin{equation}
    \text{Nu}_{X} = \frac{h_{T}X}{K_{G}}
\end{equation}
\nomenclature{$h_{T}$}{Heat transfer coefficient through the opening between upper and lower cells}%

Substituting for $h_{T}$, the convective form $\frac{\bar{\rho}C_{p}v}{2}$
(where $\bar{\rho}$ is the average density, $C_{p}$ the specific heat of
the fluid, and $v$ the velocity in the opening) and a Prandtl number of
\num{0.71}, which would be representative of the gas properties,
Equation~\ref{eq:t12} becomes

\begin{equation}
    v = \frac{0.07753}{X} \left(\frac{L}{X}\right)^{\frac{1}{3}}
        \alpha\left[X^{3}g\frac{\beta}{v^{2}}\left(T_{c} - T_{s}\right)\right]^{0.55}
\end{equation}
\nomenclature{$L$}{Width of opening between upper and lower cells}%
\nomenclature{$X$}{Thickness of partition between upper and lower cells}%
\nomenclature{$\bar{\rho}$}{Mean density of fluid, $\frac{\rho_{c} - \rho{s}}{2}$}%
\nomenclature{$v$}{Velocity of the gas flow in the opening between upper and lower cells}%

where the thermal diffusivity is given as

\begin{equation}
    \alpha = \frac{K_{G}}{\bar{\rho} C_{p}}
\end{equation}
\nomenclature{$\alpha$}{Thermal diffusivity}%
\nomenclature{$C_{p}$}{Specific heat}%

The mass transfer downward is

\begin{equation}
    W_{D} = \rho_{s} A_{X} v \mathtt{DTMIN}
\end{equation}
\nomenclature{$W_{D}$}{Mass of gas transferred from upper volume to lower volume}%
\nomenclature{$A_{X}$}{One half flow area between upper and lower volumes}%

The flow from the lower volume to the upper is dependent on the pressure
and temperature of the gas in the lower volume. The lower volume pressure is
a function of the flow into the lower volume and the static head over the opening.
Solving the Bernoulli Equation for the pressure in the cell,

\begin{equation}
    P_{c} = P_{s} - 1.5 \rho_{s} \frac{v^{2}}{2g} + \rho_{s} X
\end{equation}
\nomenclature{$P_{c}$}{Absolute pressure of gas in lower volume}%
\nomenclature{$P_{s}$}{Absolute pressure of gas in upper volume}%

The term $1.5 \rho_{s} \frac{v^{2}}{2g}$ includes the entrance loss equivalent to one-half the
kinetic energy. During the integration time step (\texttt{DTMIN}), the mass flowing
from the lower volume to the upper is

\begin{equation}
    W_{U} = \frac{\rho_{c} V_{c} T_{c}}{R}
\end{equation}
\nomenclature{$W_{U}$}{Mass of gas transferred from lower volume to upper volume}%
\nomenclature{$V_{c}$}{Volume of lower cell}%

\subsection{Pressure Rise Calculations}

At the end of each integration time step, in addition to nodal temperatures,
the pressure, $P$, in each gas volume, $V$, is calculated from the equation

\begin{align}
    & P_{i}V_{i} = n_{i}RT_{i}\text{, or} \\
    & P_{i} = \rho_{i}RT_{i}
\end{align}

The density, $\rho_{i}$ in each cell is computed from the weight of gas in the cell and
the volume of the cell. The weight of gas includes the reduction in weight resulting
from oxygen consumption, the gain or loss of weight resulting from flow to
or from the outer cell, the gain in weight resulting from leakage from the atmosphere,
and/or the loss in weight resulting from an exhaust system being operated.
