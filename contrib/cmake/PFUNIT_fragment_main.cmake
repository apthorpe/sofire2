# After building and installing pFUnit, add the following fragments to the
# appropriate sections of the main CMakeLists.txt file, then copy
# <root>/contrib/cmake/FindPFUNIT.cmake to <root>/cmake/FindPFUNIT.cmake

###############################################################################
## Options ####################################################################
###############################################################################

# Set path to root of pFUnit installation, e.g.
# cmake -D "PFUNIT_ROOT:PATH=C:/Users/zorak/dev/pFUnit/PFUNIT-4.1"
# where pFUnit was built with
# `cmake -D -DCMAKE_INSTALL_PREFIX=C:\Users\zorak\dev\pFUnit [...]`
# and installed (via `ninja install`, `make install`, etc.)
set(PFUNIT_ROOT CACHE PATH "")
mark_as_advanced(PFUNIT_ROOT)

# set(PFUNIT_PATH "C:/Users/apthorpe/dev/pFUnit/PFUNIT-4.1")

###############################################################################
## Dependencies and CMake Modules  ############################################
###############################################################################

if(PFUNIT_ROOT)
    find_package(PFUNIT)
endif()
