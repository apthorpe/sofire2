!> @file m_format.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Common format strings (edit specifiers)
module m_format
    implicit none

    private

!> General 80-column text outout format
    character(len=*), parameter, public :: FMT_A80 = '(A80)'

!> General text outout format
    character(len=*), parameter, public :: FMT_A = '(A)'

end module m_format
