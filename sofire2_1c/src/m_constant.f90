!> @file m_constant.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains numerical and physical constants used by SOFIRE2
!!
!! @note All members are considered public
module m_constant
    use sofire2_param, only: WP
    implicit none

    ! *** Numerical constants ***

    !> Numerical constant - zero
    real(kind=WP), parameter :: ZERO = 0.0_WP

    !> Numerical constant - one
    real(kind=WP), parameter :: ONE = 1.0_WP

    !> Numerical constant - two
    real(kind=WP), parameter :: TWO = 2.0_WP

    !> Numerical constant - three
    real(kind=WP), parameter :: THREE = 3.0_WP

    !> Numerical constant - four
    real(kind=WP), parameter :: FOUR = 4.0_WP

    !> Numerical constant - five
    real(kind=WP), parameter :: FIVE = 5.0_WP

    !> Numerical constant - ten
    real(kind=WP), parameter :: TEN = 10.0_WP

    !> Numerical constant - thousand \f$\num{1000}\f$
    real(kind=WP), parameter :: THOUSAND = 1000.0_WP

    !> Numerical constant - tenth \f$\left(\frac{1}{10}\right)\f$
    real(kind=WP), parameter :: TENTH = ONE / TEN

    !> Numerical constant - fifth \f$\left(\frac{1}{5}\right)\f$
    real(kind=WP), parameter :: FIFTH = ONE / FIVE

    !> Numerical constant - quarter \f$\left(\frac{1}{4}\right)\f$
    real(kind=WP), parameter :: QUARTER = ONE / FOUR

    !> Numerical constant - third \f$\left(\frac{1}{3}\right)\f$
    real(kind=WP), parameter :: THIRD = ONE / THREE

    !> Numerical constant - half \f$\left(\frac{1}{2}\right)\f$
    real(kind=WP), parameter :: HALF = ONE / TWO

    !> Numerical constant - \f$\num{1.0E9}\f$
    real(kind=WP), parameter :: BIG1P9 = 1.0E9_WP

    !> Numerical constant - \f$\num{1.0E-6}\f$
    real(kind=WP), parameter :: TINY1M6 = 1.0E-6_WP

    !> Numerical constant - \f$\num{1.0E-24}\f$
    real(kind=WP), parameter :: TINY1M24 = 1.0E-24_WP

    ! *** Unit conversion factors ***

    !> Zero Celsius, \f$\SI{273.15}{\kelvin}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: T0K = 273.15_WP

    !> Zero Fahrenheit, \f$\US{459.67}{\rankine}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: T0R = 459.67_WP

    !> Kelvin to Rankine conversion, \f$\SI{1.8}{\rankine\per\kelvin}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: R_K = 1.8_WP

    !> Inches per foot, \f$\US{12}{\inch\per\foot}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: IN_FT = 12.0_WP

    !> Conversion of \f$\US{1}{\BTU\per\hour}\f$ to \f$\si{\watt}\f$, \f$\SI{0.29307107}{\watt\hour\per\BTU}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: W_BTUH = 0.29307107_WP

    !> Conversion of \f$\US{1}{\foot}\f$ to \f$\si{\meter}\f$, \f$\SI{0.3048}{\meter\per\foot}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: M_FT = 0.3048_WP

    !> Seconds per minute, \f$\SI{60}{\second\per\minute}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: SEC_MIN = 60.0_WP

    !> Minutes per hour, \f$\SI{60}{\minute\per\hour}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: MIN_HR = 60.0_WP

    !> Seconds per hour, \f$\SI{3600}{\second\per\hour}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: SEC_HR = SEC_MIN * MIN_HR

    !> Pounds-mass per gram (US customary), \f$\US{0.0022046226}{\lbm\per\gram}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: LBM_G = 0.0022046226_WP
!    real(kind=WP), parameter :: LBM_G = 0.00220462262_WP

    !> Pounds-mass per kilogram (US customary), \f$\US{2.2046226}{\lbm\per\kilo\gram}\f$,
    !! scaled from `LBM_G`
    real(kind=WP), parameter :: LBM_KG = THOUSAND * LBM_G

    !> Inches (water column) per (pound-force per square foot),
    !! \f$\approx \US{0.1926}{\inch (w.c.) \square\foot\per\lbf}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: INWC_PSF = 0.0160185_WP * IN_FT

    !> Foot-pound(force) per BTU, \f$\US{778.169}{\foot\lbf\per\BTU}\f$,
    !! from @cite CRC_Handbook_66
    real(kind=WP), parameter :: FTLBF_BTU = 778.169_WP
!    real(kind=WP), parameter :: FTLBF_BTU = 778.169262_WP

    !> Joule per BTU, \f$\SI{1055.05585}{\joule\per\BTU}\f$
    !! Scaled from W_BTUH
    real(kind=WP), parameter :: JOULE_BTU = W_BTUH * SEC_HR

    ! *** Physical constants - SI ***

    !> Molecular (formula) weight of air at sea level, \f$\SI{28.964425278794}{\gram\per\mol}\f$
    !! or \f$\SI{28.964425278794}{\lbm\per\lbmol}\f$
    !! from @cite CRC_Handbook_66 Source: "U.S. Standard Atmosphere, 1976", NOAA/NASA/USAF
    real(kind=WP), parameter :: MW_AIR = 28.964425278794_WP
!    real(kind=WP), parameter :: MW_AIR = 28.9647_WP

    !> Standard gravitational acceleration, \f$\SI{9.80665}{\meter\per\square\second}\f$
    !! CODATA 2018 data taken from https://physics.nist.gov/cuu/Constants/Table/allascii.txt
    real(kind=WP), parameter :: GRAV = 9.80665_WP

    !> Molar (universal) gas constant, \f$R = k N_{A} = \SI{8.31446261815324}{\joule\per\mol\per\kelvin}\f$
    !! Approximation of CODATA 2018 data taken from
    !! https://physics.nist.gov/cuu/Constants/Table/allascii.txt
    real(kind=WP), parameter :: RGAS = 8.31446261815324_WP

    !> Stefan-Boltzmann constant, \f$\SI{5.670374419E-8}{\watt\per\square\meter\per\kelvin^{4}}\f$
    !! Approximation of CODATA 2018 data taken from
    !! https://physics.nist.gov/cuu/Constants/Table/allascii.txt
    real(kind=WP), parameter :: SIGMA_SB = 5.670374419E-8_WP

    ! *** Physical constants - US (typically derived) ***

    !> Molar (universal) gas constant, \f$\approx \US{1.985875279009}{\BTU\per\lbmol\per\rankine}\f$
    !! Scaled from CODATA 2018 value
    real(kind=WP), parameter :: RGAS_US = RGAS / (R_K * LBM_G * JOULE_BTU)

    !> Standard gravitational acceleration, \f$\approx \US{32.174}{\foot\per\square\second}\f$
    !! Scaled from CODATA 2018 value
    real(kind=WP), parameter :: GIN = GRAV / M_FT

    !> Specific gas constant for air, \f$\approx \US{53.3}{\foot\lbf\per\rankine\per\lbm}\f$ of air
    !! Scaled from CODATA 2018 value and derived from `MW_AIR`
    real(kind=WP), parameter :: RIN = RGAS_US * FTLBF_BTU / MW_AIR

    !> Stefan-Boltzmann constant, \f$\approx \US{1.7123E-09}{\BTU\per\hour\per\square\foot\per\rankine^{4}}\f$.
    !! Scaled from CODATA 2018 value
    real(kind=WP), parameter :: SIGMA_SB_US = SIGMA_SB * M_FT**2 / (W_BTUH  * R_K**4)

    ! *** Other constants ***

! contains
end module m_constant
