\chapter{Comparison of Original and Modernized One-Cell SOFIRE II Code}
\label{c:c1_comparison}

\section{Overview}

The modernized version of the one-cell SOFIRE~II code was compiled as both
single- and double precision by changing the value of the parameter \texttt{WP}
in \texttt{sofire2\_param.f90}. Both builds of the modernized code were run
against the reference input (listed in \ref{input:c1}) and the results plotted
against the original 1973 results as published in \cite{Beiriger1973}.

\section{Test Results and Discussion}

Agreement is generally good except for two cases. The temperature of wall node 2
(exposed concrete facing the test chamber liner) is greater in the modernized code;
see Figure~\ref{fig:c1_tf2}. The original code's performance is rather poor due
to roundoff error as described in \cite{Sienicki2012}. The effect of roundoff in
the modernized single-precision code was less pronounced but ``clipping'' is
still observed. The modernized double precision code shows no sign of clipping and
the temperature response of the concrete cell wall is smoother and more consistent
with our expectations of a physical system.

After \num{9.5}~hours of problem time errors are seen in the original tabular output,
leading one to view results beyond that point with some suspicion. Transient results
are only plotted for the first eight hours of the scenario in the original report;
extended results are plotted here to illustrate that the modernized code is
well-behaved late in the analysis.

Results produced with the current modernized version of SOFIRE~II are consistent
with Argonne's 2012 results with an independently modernized and corrected version
of the code as presented in \cite{Sienicki2012}.

\section{Conclusion}

Overall, the modernized version of SOFIRE~II produces results consistent with the
original code. Deviations are consistent with improved numerical performance and
changes in internal storage to avoid problems experienced with the original code
such as insufficient static memory allocation and file read errors. The double
precision modernized version of SOFIRE~II provides more phyically consistent
results than the original code or the single precision modernized version of
SOFIRE~II. Consistent with the recommendation in \cite{Sienicki2012}, it is
recommended that one use the double precision modernized version of SOFIRE~II
in preference to the single precision version.

\section{Transient Results}

The transient results plots from pages C-10 through C-17 in \cite{Beiriger1973}
are recreated in Figures~\ref{fig:c1_tgasc} through \ref{fig:c1_ts}. The scales
of the original plots have been retained except for Figure~\ref{fig:c1_tf2} where
results from the modernized code exceeded the original plot's range.

Note that \US{0}{\fahrenheit} is taken as \US{459.67}{\rankine} rather than
\US{460}{\rankine} which accounts for slightly increased temperatures
in the current plots. Temperature data for all cases is maintained in absolute units
so any differences between the current and historical plots resulting from unit
conversions are a result of consistently applying the same unit conversions
to all temperature data.

Another significant difference is that results are plotted for the entire scenario
duration of \SI{10}{\hour}. The original plots show results truncated at \SI{8}{\hour} while
the tabular output for scenario provides results for the full scenario duration.
As described previously, errors were noted in the original tabular output occuring
at approximately \SI{9.5}{\hour}. To show a complete comparison of the three codes,
the entire \SI{10}{\hour} data set was plotted rather than truncating data from the
current code.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_1}
    \end{center}
    \caption{Gas temperature}
    \label{fig:c1_tgasc}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_2}
    \end{center}
    \caption{Wall surface temperature}
    \label{fig:c1_twc1}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_3}
    \end{center}
    \caption{Sodium burning rate}
    \label{fig:c1_xm}
\end{figure}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_4}
    \end{center}
    \caption{Gas pressure}
    \label{fig:c1_pgasc}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_5}
    \end{center}
    \caption{Floor temperature}
    \label{fig:c1_tf1}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_6}
    \end{center}
    \caption{Floor temperature, concrete (node 2)}
    \label{fig:c1_tf2}
\end{figure}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_7}
    \end{center}
    \caption{Wall temperature, concrete (node 2)}
    \label{fig:c1_twc2}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_comparison_8}
    \end{center}
    \caption{Sodium surface temperature}
    \label{fig:c1_ts}
\end{figure}

\clearpage

\section{Input File}

\label{input:c1}
\VerbatimInput[numbers=left, numbersep=3pt, stepnumber=5,
    fontsize=\small, frame=single, labelposition=topline, label=C1.INP]{C1.INP}
