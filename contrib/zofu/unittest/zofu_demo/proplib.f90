module proplib
    use sofire2_param, only: WP
    use m_constant
    use m_textio
    use m_material

    type(material_database), public :: mdb

    public :: populate_mdb

contains

subroutine populate_mdb()
    continue
    call mdb%populate()
    return
end subroutine populate_mdb

end module proplib