!> @file c1_input.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_input module.
program c1_input
    use sofire2_param, only: WP
    use m_constant, only: ZERO!, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_input
    use toast
    implicit none

    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_input")

    !!! Test exposed functions

    ! call read_labels(UNIT, ND, LABEL)

    ! call subroutine read_values(UNIT, ND, LABEL, PUTINS, SAVINS, TITLE)

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_input.json")

    call test%checkfailure()
end program c1_input
