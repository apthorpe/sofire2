!> @file m_inventory.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Classes and routines pertaining to material inventory and
!! mass/mole distribution
module m_inventory
    use sofire2_param, only: WP
    use m_constant, only: ZERO
    use m_format, only: fmt_a
    use m_material, only: phase_NULL, mid_NULL
    implicit none

    private

    public :: new_id_mass
    public :: new_gas_parcel
    public :: new_aerosol_parcel
    public :: new_gas_mf
    public :: new_aerosol_mf
    public :: new_gas_inventory
    public :: new_aerosol_inventory
    public :: as_nf
    public :: as_mf
    public :: get_nf_dry_air

    !> Individual species entry for a list of species in a volume
    type, public :: id_mass
        !> Phase ID; determines whether id is a gas ID (gid) or
        !! aerosol ID (aid)
        integer :: phase_id = phase_NULL
        !> Material ID
        integer :: id = mid_NULL
        !> Mass, \f$\us{\lbm}\f$
        real(kind=WP) :: mass = ZERO
    contains
        !> Initialize species attributes
        procedure :: init => id_mass_init
        !> Returns .true. if material is a gas
        procedure :: is_gas => id_mass_is_gas
        !> Returns .true. if material is an aerosol
        procedure :: is_aerosol => id_mass_is_aerosol
        !> Moles, \f$\us{\lbmol}\f$
        procedure :: moles => id_mass_to_moles
        !> Print internal contents and diagnostics
        procedure :: dump => id_mass_dump
    end type id_mass

    !> Simple(-ish) array of species
    type, public :: parcel
        !> Material name
        character(len=32) :: name = 'UNINITIALIZED PARCEL            '
        !> Phase ID
        integer :: phase_id = phase_NULL
        !> Inventory of materials
        type(id_mass), dimension(:), allocatable :: species
    contains
        !> Initialize species attributes
        procedure :: init => parcel_init
        !> Returns a new parcel object initialized with the current
        !! object's attributes
        procedure :: clone_parcel => parcel_clone
        !> Add the mass in a parcel to the current parcel
        procedure :: add => parcel_add
        !> Remove the mass in a parcel from the current parcel
        procedure :: subtract => parcel_subtract
        !> Returns .true. if material is a gas
        procedure :: is_gas => parcel_is_gas
        !> Returns .true. if material is an aerosol
        procedure :: is_aerosol => parcel_is_aerosol
        !> Aggregate mass, \f$\us{\lbm}\f$
        procedure :: mass => parcel_mass
        !> Aggregate moles, \f$\us{\lbmol}\f$
        procedure :: moles => parcel_moles
        !> Aggregate formula weight, \f$\us{\lbm\per\lbmol}\f$
        procedure :: mw => parcel_mw
        !> Energy contained in parcel at temperature \f$T\f$,
        !! \f$\us{\BTU}\f$
        ! procedure :: energy_us => parcel_energy_us
        ! !> Energy (in units of \f$\si{\joule}\f$) contained in parcel at
        ! !! temperature \f$T\f$ (in units of \f$\si{\kelvin}\f$)
        procedure :: energy_si => parcel_energy_si
        !> Print internal contents and diagnostics
        procedure :: dump => parcel_dump
    end type parcel

    !> Fraction of single species
    type, public :: id_fraction
        !> Phase ID; determines whether id is a gas ID (gid) or
        !! aerosol ID (aid)
        integer :: phase_id = phase_NULL
        !> Material ID
        integer :: id = mid_NULL
        !> Material ID
        real(kind=WP) :: f = ZERO
    contains
        !> Initialize fraction attributes
        procedure :: init => id_fraction_init
        !> Returns .true. if material is a gas
        procedure :: is_gas => id_fraction_is_gas
        !> Returns .true. if material is an aerosol
        procedure :: is_aerosol => id_fraction_is_aerosol
        !> Print internal contents and diagnostics
        procedure :: dump => id_fraction_dump
    end type id_fraction

    !> Fractional distribution (by mole, by mass) of material
    type, public :: material_dist
        !> Distribution name
        character(len=32) :: name = 'UNINITIALIZED MATERIAL DIST     '
        !> Distribution type: `m` for mass, `n` for mole
        character(len=1) :: dist_type = 'm'
        !> Phase ID; determines object contains gas or aerosol species
        integer :: phase_id = phase_NULL
        !> Material distribution
        type(id_fraction), dimension(:), allocatable :: dist
    contains
        !> Initialize distribution
        procedure :: init => material_dist_init
        !> Normalize fractions in distribution
        procedure :: normalize => material_dist_normalize
        !> Specify distribution
        ! procedure :: specify => material_dist_specify
        !> Scale distribution to set one element to a given fraction
        procedure :: scale_to_fraction =>                               &
            material_dist_scale_to_fraction
        !> Returns .true. if object represents a gas
        procedure :: is_gas => material_dist_is_gas
        !> Returns .true. if object represents an aerosol
        procedure :: is_aerosol => material_dist_is_aerosol
        !> Return `.true.` if object represents a mass distribution
        procedure :: is_mass_fraction =>                                &
            material_dist_is_mass_fraction
        !> Return `.true.` if object represents a molar distribution
        procedure :: is_mole_fraction =>                                &
            material_dist_is_mole_fraction
        !> Return molar distribution based on current distribution
        procedure :: as_nf
        !> Return mass distribution based on current distribution
        procedure :: as_mf
        !> Aggregate formula weight of distribution, \f$\us{\lbm\per\lbmol}\f$
        procedure :: mw => material_dist_mw
        !> Print internal contents and diagnostics
        procedure :: dump => material_dist_dump
    end type material_dist

    !> Individual species entry for a list of species in a volume
    type, extends(parcel), public :: inventory
        ! !> Material name -- INHERITED
        ! character(len=32) :: name = 'UNINITIALIZED INVENTORY         '
        ! !> Phase ID - INHERITED
        ! integer :: phase_id = phase_NULL
        ! !> Inventory of materials -- INHERITED
        ! type(id_mass), dimension(:), allocatable :: species
        !> Mass distribution
        type(material_dist) :: mf
        !> Molar distribution
        type(material_dist) :: nf
    contains
        !> Initialize species attributes (overrides `parcel_init`)
        procedure :: init => inventory_init
        !> Returns a new inventory object initialized with the current
        !! object's attributes
        procedure :: clone_inventory => inventory_clone

        ! !> Returns .true. if material is a gas -- INHERITED
        ! procedure :: is_gas => parcel_is_gas
        ! !> Returns .true. if material is an aerosol -- INHERITED
        ! procedure :: is_aerosol => parcel_is_aerosol
        ! !> Mass, \f$\us{\lbm}\f$ -- INHERITED
        ! procedure :: mass => parcel_mass
        ! !> Moles, \f$\us{\lbmol}\f$ -- INHERITED
        ! procedure :: moles => parcel_moles
        ! !> Aggregate formula weight, \f$\us{\lbm\per\lbmol}\f$ -- INHERITED
        ! procedure :: mw => parcel_mw
        ! !> Energy contained in parcel at temperature \f$T\f$,
        ! !! \f$\us{\BTU}\f$ -- INHERITED
        ! procedure :: energy_us => parcel_energy_us
        ! !> Energy contained in parcel at temperature \f$T\f$
        !! (\f$\si{\kelvin}\f$), \f$\si{\joule}\f$ -- INHERITED
        ! procedure :: energy_si => parcel_energy_si

        !> Add the mass in a parcel/inventory to the current inventory
        procedure :: add => inventory_add
        !> Remove the mass in a parcel/inventory from the current
        !! inventory
        procedure :: subtract => inventory_subtract
        !> Update mass and mole distributions of inventory
        procedure :: update_distributions =>                            &
            inventory_update_distributions
        !> Return aggregate formula weight of inventory
        procedure :: mw => inventory_mw
        !> Return mass distribution of inventory
        procedure :: as_mf => inventory_as_mf
        !> Return molar distribution of inventory
        procedure :: as_nf => inventory_as_nf
        !> Add mass of component `id` to mixture, \f$\us{\lbm}\f$
        procedure :: add_component_mass =>                              &
            inventory_add_component_mass
        !> Add mass, moles, or fraction of component `id` to mixture
        procedure :: add_component => inventory_add_component
        !> Remove mass, moles, or fraction of component `id` from
        !! mixture
        procedure :: remove_component => inventory_remove_component
        !> Extract mass of mixture, \f$\us{\lbm}\f$
        procedure :: extract_mixture => inventory_extract_mixture
        !> Print internal contents and diagnostics
        procedure :: dump => inventory_dump
    end type inventory

contains

! integer, parameter, public :: gid_NULL   = 0

! character(len=6), dimension(*), parameter :: gas_name = (/          &
!     'Ar    ', 'CH4   ', 'CO    ', 'CO2   ', 'H2    ', 'H2O   ',     &
!     'He    ', 'Kr    ', 'N2    ', 'Na    ', 'Ne    ', 'O2    ',     &
!     'Xe    ' /)

! integer, parameter :: n_gas = size(gas_name)

! integer, dimension(n_gas), parameter, public :: gas_id = (/         &
! gid_Ar, gid_CH4, gid_CO, gid_CO2, gid_H2, gid_H2O, gid_He, gid_Kr,  &
! gid_Na, gid_N2, gid_Ne, gid_O2, gid_Xe /)

! ! ID for each tracked aerosol species

! integer, parameter, public :: aid_NULL   = 0

! character(len=6), dimension(*), parameter, public :: aer_name = (/  &
!     'H2O   ', 'Na    ', 'Na2CO3', 'Na2O  ', 'Na2O2 ', 'NaO2  ',     &
!     'NaOH  ' /)
! integer, parameter, public :: n_aer = size(aer_name)
! integer, dimension(n_aer), parameter, public :: aer_id = (/         &
!     aid_H2O, aid_Na, aid_Na2CO3, aid_Na2O, aid_Na2O2, aid_NaO2,     &
!     aid_NaOH /)

!> Initialize attributes
subroutine id_mass_init(this)
    implicit none
    !> Object reference
    class(id_mass), intent(inout) :: this
    continue

    this%phase_id = phase_NULL
    this%id = mid_NULL
    this%mass = ZERO

    return
end subroutine id_mass_init

!> Returns .true. if material is a gas
pure logical function id_mass_is_gas(this) result(is_gas)
    use m_material, only: phase_GAS
    implicit none
    !> Object reference
    class(id_mass), intent(in) :: this
    continue
    is_gas = (this%phase_id == phase_GAS)
    return
end function id_mass_is_gas

!> Returns .true. if material is an aerosol
pure logical function id_mass_is_aerosol(this) result(is_aerosol)
    implicit none
    !> Object reference
    class(id_mass), intent(in) :: this
    continue
    is_aerosol = (this%phase_id > 0)
    return
end function id_mass_is_aerosol

!> Return moles of material based on mase, phase, and ID
pure real(kind=WP) function id_mass_to_moles(this) result(moles)
    use m_material, only: material_db, is_gas_id, is_aerosol_id
    implicit none
    !> Object reference
    class(id_mass), intent(in) :: this
    continue

    moles = ZERO
    if (this%is_gas()) then
        if (is_gas_id(this%id)) then
            moles = this%mass / material_db%gas(this%id)%mw
        end if
    else if (this%is_aerosol()) then
        if (is_aerosol_id(this%id)) then
            moles = this%mass / material_db%aerosol(this%id)%mw
        end if
    end if

    return
end function id_mass_to_moles

!> Display internal contents and diagnostics
subroutine id_mass_dump(this, i)
    use iso_fortran_env, only: stderr => error_unit
    implicit none
    !> Object reference
    class(id_mass), intent(in) :: this

    !> Array position, optional
    integer, intent(in), optional :: i

10  format(I2, ' - id: ', I2, ', phase_id: ', I2, ', mass: ', ES16.5,   &
    ', moles: ', ES16.5)
20  format('id: ', I2, ', phase_id: ', I2, ', mass: ', ES16.5,          &
    ', moles: ', ES16.5)

    continue

    if (present(i)) then
        write(unit=stderr, fmt=10) i, this%id, this%phase_id,           &
            this%mass, this%moles()
    else
        write(unit=stderr, fmt=20) this%id, this%phase_id, this%mass,   &
            this%moles()
    end if

    return
end subroutine id_mass_dump

!> Return initialized id_mass object. `phase_id` is mandatory; `id` and
!! `mass` arguments are optional. `mass` must be not be negative and is
!! ignored if `id` is not specified or does not correspond to a known
!! material for the given phase.
!!
!! Impure because of idm%init()
type(id_mass) function new_id_mass(phase_id, id, mass) result (idm)
    use m_material, only: gid_NULL, aid_NULL, gas_id, aer_id
    implicit none

    !> Phase ID; 0 is gas, 1 is condensed (aerosol)
    integer, intent(in) :: phase_id

    !> Material ID. Corresponds to gas ID (gid) if phase_id is 0,
    !! aerosol ID (aid) if phase_id is greater than 0
    integer, intent(in), optional :: id

    !> Mass of material, \f$\us{\lbm}\f$
    real(kind=WP), intent(in), optional :: mass

    ! real(kind=WP) :: init_mass

    continue

    call idm%init()
    idm%phase_id = phase_id

    idm%id = mid_NULL
    if (present(id)) then
        if (idm%is_gas()) then
            ! gas
            if (any(gas_id == id)) then
                idm%id = id
            end if
        else if (idm%is_aerosol()) then
            ! aerosol
            if (any(aer_id == id)) then
                idm%id = id
            end if
        end if
    end if

    idm%mass = ZERO
    if (present(mass)) then
        if (idm%id /= mid_NULL) then
            if (idm%is_gas() .or. idm%is_aerosol()) then
                idm%mass = max(ZERO, mass)
            end if
        end if
    end if

    return
end function new_id_mass

!> Initialize parcel attributes
subroutine parcel_init(this)
    implicit none
    !> Object reference
    class(parcel), intent(inout) :: this
    continue

    this%name = 'UNINITIALIZED PARCEL            '
    !> Phase ID
    this%phase_id = phase_NULL
    !> Inventory of materials
    if (allocated(this%species)) then
        deallocate(this%species)
    end if

    return
end subroutine parcel_init

!> Create a new parcel object initialized with the attributes of the
!! current parcel
type(parcel) function parcel_clone(this) result(clone)
    use m_constant, only: ZERO, ONE
    implicit none

    !> Object reference
    class(parcel), intent(in) :: this

    continue

    clone%name = this%name
    clone%phase_id = this%phase_id
    if (allocated(clone%species)) then
        deallocate(clone%species)
    end if
    allocate(clone%species, source=this%species)

    return
end function parcel_clone

!> Add the mass in a parcel to the current parcel
subroutine parcel_add(this, that)
    use m_constant, only: ZERO, ONE
    implicit none

    !> Object reference
    class(parcel), intent(inout) :: this

    !> Incoming parcel
    class(parcel), intent(in) :: that

    integer :: n_this
    integer :: n_that

    continue

    if (allocated(this%species) .and. allocated(that%species)) then
        n_this = size(this%species)
        n_that = size(that%species)
        if (n_this == n_that .and. n_this > 0) then
            if (all(that%species(:)%mass >= ZERO)) then
                this%species(:)%mass = this%species(:)%mass             &
                    + that%species(:)%mass
            end if
        end if
    end if

    return
end subroutine parcel_add

!> Remove the mass in a parcel from the current parcel
subroutine parcel_subtract(this, that)
    implicit none
    !> Object reference
    class(parcel), intent(inout) :: this

    !> Outgoing parcel
    class(parcel), intent(in) :: that

    integer :: n_this
    integer :: n_that

    continue

    if (allocated(this%species) .and. allocated(that%species)) then
        n_this = size(this%species)
        n_that = size(that%species)
        if (n_this == n_that .and. n_this > 0) then
            if (all(that%species(:)%mass >= ZERO) .and.                 &
                all(this%species(:)%mass >= this%species(:)%mass)) then
                this%species(:)%mass = this%species(:)%mass             &
                    - that%species(:)%mass
            end if
        end if
    end if

    return
end subroutine parcel_subtract

!> Returns .true. if parcel represents gases
pure logical function parcel_is_gas(this) result(is_gas)
    use m_material, only: phase_GAS
    implicit none
    !> Object reference
    class(parcel), intent(in) :: this
    continue
    is_gas = (this%phase_id == phase_GAS)
    return
end function parcel_is_gas

!> Returns .true. if parcel represents aerosols
pure logical function parcel_is_aerosol(this) result(is_aerosol)
    implicit none
    !> Object reference
    class(parcel), intent(in) :: this
    continue
    is_aerosol = (this%phase_id > 0)
    return
end function parcel_is_aerosol

!> Return aggregate mass of parcel constituents
pure real(kind=WP) function parcel_mass(this) result(mass)
    implicit none
    !> Object reference
    class(parcel), intent(in) :: this
    continue

    if (allocated(this%species)) then
        mass = sum(this%species(:)%mass)
    else
        mass = ZERO
    end if

    return
end function parcel_mass

!> Return aggregate moles of parcel constituents
pure real(kind=WP) function parcel_moles(this) result(moles)
    implicit none
    !> Object reference
    class(parcel), intent(in) :: this
    integer :: i
    continue

    moles = ZERO
    if (allocated(this%species)) then
        do i = 1, size(this%species)
            moles = moles + this%species(i)%moles()
        end do
    end if

    return
end function parcel_moles

!> Return aggregate formula weight of parcel constituents
pure real(kind=WP) function parcel_mw(this) result(mw)
    use m_constant, only: ZERO
    implicit none
    !> Object reference
    class(parcel), intent(in) :: this

    real(kind=WP) :: moles

    continue

    mw = ZERO
    if (allocated(this%species)) then
        moles = this%moles()
        if (moles > ZERO) then
            mw = this%mass() / moles
        end if
    end if

    return
end function parcel_mw

!> Return aggregate energy of parcel constituents (SI units)
pure real(kind=WP) function parcel_energy_si(this, T) result(U)
    use m_constant, only: ONE, RGAS
    use m_material, only: material_db
    implicit none
    !> Object reference
    class(parcel), intent(in) :: this
    !> Temperature, \f$\si{\kelvin}\f$
    real(kind=WP), intent(in) :: T

    integer :: i
    integer :: id
    real(kind=WP) :: u_i
    continue

    U = ZERO
    if (allocated(this%species)) then
        if (this%is_gas()) then
            ! Assume constant volume:
            ! U = sum(u_i) = sum(m_i cv_i(T) T)
            ! m_i is stored in this%species(i)%mass
            ! T is given
            ! For an ideal gas, cv_i(T) = material_db%gas(i)%cp(T)
            !                           - RGAS / material_db%gas(i)%mw
            ! u_i = this%species(i)%mass * T * (material_db%gas(i)%cp(T)
            !       - RGAS / material_db%gas(i)%mw)
            do i = 1, size(this%species)
                id = this%species(i)%id
                u_i = this%species(i)%moles() * T                       &
                    * (material_db%gas(id)%cp(T) - ONE) * RGAS
                U = U + u_i
            end do
        else if (this%is_aerosol()) then
            ! Assume constant pressure:
            ! U = sum(u_i) = sum(m_i cp_i(T) T) =? sum(m_i h_i(T))
            ! m_i is stored in this%species(i)%mass
            ! T is given
            ! cp_i is stored in material_db%aerosol(i)%cp(T)
            ! u_i = this%species(i)%mass * T * material_db%aerosol(i)%cp(T)
            !     =? this%species(i)%mass * material_db%aerosol(i)%h(T)
            do i = 1, size(this%species)
                id = this%species(i)%id
                u_i = this%species(i)%moles() * T                       &
                    * material_db%aerosol(id)%cp(T) * RGAS
                U = U + u_i
            end do
        end if
    end if

    return
end function parcel_energy_si

! parcel_energy_us() requires review

! !> Return aggregate energy of parcel constituents (US customary units)
! pure real(kind=WP) function parcel_energy_us(this, T) result(U)
!     use m_constant, only: THOUSAND, JOULE_BTU, LBM_KG, R_K, RGAS
!     use m_material, only: material_db
!     implicit none
!     !> Object reference
!     class(parcel), intent(in) :: this
!     !> Temperature, \f$\us{\rankine}\f$
!     real(kind=WP), intent(in) :: T

!     integer :: i
!     integer :: id
!     real(kind=WP) :: u_i
!     real(kind=WP) :: Tkelvin
!     continue

!     U = ZERO
!     if (allocated(this%species)) then
!         Tkelvin = T / R_K
!         if (this%is_gas()) then
!             ! Assume constant volume:
!             ! U = sum(u_i) = sum(m_i cv_i(T) T)
!             ! m_i is stored in this%species(i)%mass
!             ! T is given
!             ! For an ideal gas, cv_i(T) = material_db%gas(i)%cp(T)
!             !                           - RGAS / material_db%gas(i)%mw
!             ! u_i = this%species(i)%mass * T * (material_db%gas(i)%cp(T)
!             !       - RGAS / material_db%gas(i)%mw)
!             do i = 1, size(this%species)
!                 id = this%species(i)%id
!                 u_i = this%species(i)%mass / LBM_KG * Tkelvin           &
!                     * (material_db%gas(id)%cp(Tkelvin)                  &
!                     - RGAS * THOUSAND / material_db%gas(id)%mw)
!                 U = U + u_i
!             end do
!         else if (this%is_aerosol()) then
!             ! Assume constant pressure:
!             ! U = sum(u_i) = sum(m_i cp_i(T) T) =? sum(m_i h_i(T))
!             ! m_i is stored in this%species(i)%mass
!             ! T is given
!             ! cp_i is stored in material_db%aerosol(i)%cp(T)
!             ! u_i = this%species(i)%mass * T * material_db%aerosol(i)%cp(T)
!             !     =? this%species(i)%mass * material_db%aerosol(i)%h(T)
!             do i = 1, size(this%species)
!                 id = this%species(i)%id
!                 u_i = this%species(i)%mass / LBM_KG * Tkelvin           &
!                     * material_db%aerosol(id)%cp(Tkelvin) * RGAS        &
!                     / material_db%aerosol(id)%mw
!                 U = U + u_i
!             end do
!         end if
!         ! Convert units to stupid US customary nonsense (BTU)
!         U = U / JOULE_BTU
!     end if

!     return
! end function parcel_energy_us

!> @brief Display parcel object internal contents and diagnostics
subroutine parcel_dump(this)
    use iso_fortran_env, only: stderr => error_unit
    implicit none

    !> Parcel object reference
    class(parcel), intent(in) :: this

    integer :: i
    integer :: n_species

10  format(A, A)
20  format(A, I0)

    continue

    write(unit=stderr, fmt=10) 'name      ', this%name
    write(unit=stderr, fmt=20) 'phase_id  ', this%phase_id
    if (allocated(this%species)) then
        n_species = size(this%species)
        write(unit=stderr, fmt=20) 'size(species)', n_species

        write(unit=stderr, fmt=10) 'species contents', ''
        if (n_species > 0) then
            do i = 1, n_species
                call this%species(i)%dump(i)
            end do
        end if
    else
        write(unit=stderr, fmt=10) 'species is not allocated', ''
    end if

    return
end subroutine parcel_dump

!> Return parcel object initialized with an empty gas inventory.
!! Species masses may be initialized by specifying an aggregate mass
!! and a mass distribution or an aggregate number of moles and a molar
!! distribution.
!!
!! Impure because of gparcel%init() and allocate(gparcel%species())
type(parcel) function new_gas_parcel(name, mass, mf, moles, nf)         &
    result (gparcel)
    use m_textio, only: munch
    use m_material, only: material_db, n_gas, gas_id, phase_GAS
    implicit none

    !> Gas parcel name, optional
    character(len=32), intent(in), optional :: name

    !> Total parcel mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: mass

    !> Mass distribution, optional
    type(material_dist), intent(in), optional :: mf

    !> Total parcel mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: moles

    !> Molar distribution, optional
    type(material_dist), intent(in), optional :: nf

    integer :: i

    logical :: set_from_mass
    logical :: set_from_moles

    continue

    call gparcel%init()
    gparcel%phase_id = phase_GAS
    allocate(gparcel%species(n_gas))
    gparcel%species(:)%mass = ZERO
    gparcel%species(:)%phase_id = gparcel%phase_id

    if (present(name)) then
        gparcel%name = munch(name)
    end if

    set_from_mass = (present(mass) .and. present(mf))
    if (set_from_mass) then
        set_from_mass = (mf%is_gas() .and. (mass > ZERO))
    end if

    set_from_moles = (present(moles) .and. present(nf))
    if (set_from_moles) then
        set_from_moles = (nf%is_gas() .and. (moles > ZERO))
    end if

    do i = 1, n_gas
        gparcel%species(i)%id = gas_id(i)
        ! ASSERT (gparcel%species(i)%id == i)
        if (set_from_mass) then
            gparcel%species(i)%mass = mf%dist(i)%f * mass
        else if (set_from_moles) then
            gparcel%species(i)%mass = nf%dist(i)%f * moles              &
                * material_db%gas(i)%mw
        end if
    end do

    return
end function new_gas_parcel

!> Return parcel object initialized with an empty aerosol inventory.
!! Species masses may be initialized by specifying an aggregate mass
!! and a mass distribution or an aggregate number of moles and a molar
!! distribution.
!!
!! Impure because of aparcel%init() and allocate(aparcel%species())
type(parcel) function new_aerosol_parcel(name, mass, mf, moles, nf)     &
    result (aparcel)
    use m_textio, only: munch
    use m_material, only: material_db, n_aer, aer_id, phase_CONDENSED
    implicit none

    !> Gas parcel name, optional
    character(len=32), intent(in), optional :: name

    !> Total parcel mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: mass

    !> Mass distribution, optional
    type(material_dist), intent(in), optional :: mf

    !> Total parcel mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: moles

    !> Molar distribution, optional
    type(material_dist), intent(in), optional :: nf

    integer :: i

    logical :: set_from_mass
    logical :: set_from_moles

    continue

    call aparcel%init()
    aparcel%phase_id = phase_CONDENSED
    allocate(aparcel%species(n_aer))
    aparcel%species(:)%mass = ZERO
    aparcel%species(:)%phase_id = aparcel%phase_id

    if (present(name)) then
        aparcel%name = munch(name)
    end if

    set_from_mass = (present(mass) .and. present(mf))
    if (set_from_mass) then
        set_from_mass = (mf%is_aerosol() .and. (mass > ZERO))
    end if

    set_from_moles = (present(moles) .and. present(nf))
    if (set_from_moles) then
        set_from_moles = (nf%is_aerosol() .and. (moles > ZERO))
    end if

    do i = 1, n_aer
        aparcel%species(i)%id = aer_id(i)
        ! ASSERT (aparcel%species(i)%id == i)
        if (set_from_mass) then
            aparcel%species(i)%mass = mf%dist(i)%f * mass
        else if (set_from_moles) then
            aparcel%species(i)%mass = nf%dist(i)%f * moles              &
                * material_db%aerosol(i)%mw
        end if
    end do

    return
end function new_aerosol_parcel

!> Initialize fraction attributes
subroutine id_fraction_init(this)
    implicit none
    !> Object reference
    class(id_fraction), intent(inout) :: this
    continue

    this%phase_id = phase_NULL
    this%id = mid_NULL
    this%f = ZERO

    return
end subroutine id_fraction_init

!> Returns .true. if material is a gas
pure logical function id_fraction_is_gas(this) result(is_gas)
    use m_material, only: phase_GAS
    implicit none
    !> Object reference
    class(id_fraction), intent(in) :: this
    continue
    is_gas = (this%phase_id == phase_GAS)
    return
end function id_fraction_is_gas

!> Returns .true. if material is an aerosol
pure logical function id_fraction_is_aerosol(this) result(is_aerosol)
    implicit none
    !> Object reference
    class(id_fraction), intent(in) :: this
    continue
    is_aerosol = (this%phase_id > 0)
    return
end function id_fraction_is_aerosol

!> Display internal contents and diagnostics
subroutine id_fraction_dump(this, i)
    use iso_fortran_env, only: stderr => error_unit
    implicit none
    !> Object reference
    class(id_fraction), intent(in) :: this

    !> Array position, optional
    integer, intent(in), optional :: i

10  format(I2, ' - id: ', I2, ', phase_id: ', I2, ', f:', ES16.5)
20  format('id: ', I2, ', phase_id: ', I2, ', f:', ES16.5)

    continue

    if (present(i)) then
        write(unit=stderr, fmt=10) i, this%id, this%phase_id, this%f
    else
        write(unit=stderr, fmt=20) this%id, this%phase_id, this%f
    end if

    return
end subroutine id_fraction_dump

!> Initialize distribution attributes
subroutine material_dist_init(this)
    implicit none
    !> Object reference
    class(material_dist), intent(inout) :: this
    continue

    this%name = 'UNINITIALIZED MATERIAL DIST     '
    this%dist_type = 'm'
    this%phase_id = phase_NULL
    if (allocated(this%dist)) then
        deallocate(this%dist)
    end if

    return
end subroutine material_dist_init

!> Normalize material distribution fractions
subroutine material_dist_normalize(this)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    use m_format, only: fmt_a
    use m_constant, only: ZERO, ONE, TEN
    implicit none
    !> Object reference
    class(material_dist), intent(inout) :: this

    real(kind=WP) :: fsum
    integer :: n_constituents
    integer :: n_present

    continue

    if (allocated(this%dist)) then
        fsum = ZERO
        n_constituents = size(this%dist)
        n_present = 0
        if (n_constituents > 0) then
            n_present = count(this%dist(1:n_constituents)%f > ZERO)
        end if
        ! write(unit=stderr, fmt="(A, I0, A, I0, A)") 'Found ',           &
        !     n_constituents, ' elements total, ', n_present, ' present'
        ! if (n_constituents > 1) then
        if (n_present > 1) then
            fsum = sum(this%dist(1:n_constituents)%f)
            ! Only normalize if the sum is more than a few epsilons away
            ! from 1.0
            if (fsum > ZERO) then
                if (abs(ONE - fsum) > TEN * TINY(ONE)) then
                    this%dist(1:n_constituents)%f =                     &
                        this%dist(1:n_constituents)%f / fsum
                end if
            else
                ! This is possibly an error
                this%dist(1:n_constituents)%f = ZERO
            end if
        ! else if (n_constituents == 1) then
        else if (n_present == 1) then
            ! By definition
            this%dist(1)%f = ONE
        else
            ! Warning - No species to normalize
        end if
    else
        ! Warning - this%dist not allocated
        write(unit=stderr, fmt=fmt_a) 'Warning: Trying to normalize '   &
            // 'an unallocated dist() in [' // munch(this%name)         &
            // '], dist_type ' // this%dist_type
    end if

    return
end subroutine material_dist_normalize

!> Scale distribution to set a given fraction of one component
subroutine material_dist_scale_to_fraction(this, id, fraction)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    use m_format, only: fmt_a
    use m_constant, only: ONE
    implicit none
    !> Object reference
    class(material_dist), intent(inout) :: this
    !> Material ID
    integer, intent(in) :: id
    !> Mole fraction
    real(kind=WP), intent(in) :: fraction
    continue

    if (.not. allocated(this%dist)) then
        ! Warn on unallocated dist
        write(unit=stderr, fmt=fmt_a)                                   &
            "Warning: Attempting to scale_to_fraction() on unallocated" &
            // " dist() in [" // munch(this%name) // "] "
        return
    end if

    ! Scale fraction so that this%dist(id)%f = fraction
    ! after normalization. Scale factor = (1 - f_old) / (1 - f_new)
    ! or (1 - this%dist(id)%f) / (1 - fraction)
    if (fraction >= ZERO .and. fraction < ONE) then
        if (any(this%dist(:)%id == id)                                  &
            .and. id > 0                                                &
            .and. size(this%dist) >= id) then
            ! ASSERT(this%dist(id)%id == id)
            if (this%dist(id)%id == id) then
                if (all(this%dist(:)%f == ZERO)) then
                    this%dist(id)%f = ONE
                else if (sum(this%dist(:)%f) == this%dist(id)%f) then
                    this%dist(id)%f = ONE
                else
                    this%dist(id)%f = fraction                          &
                        * (ONE - this%dist(id)%f) / (ONE - fraction)
                    call this%normalize()
                end if
            else
                ! Warn on id mismatch
                write(unit=stderr, fmt='(A,I0)')                        &
                    "Warning: Cannot match ID ", id
                call this%dump()
            end if
        else
            ! Error on bad id
            write(unit=stderr, fmt='(A,I0)')                            &
                "Warning: Bad ID ", id
            call this%dump()
        end if
    else
        ! Warn on malformed fraction
        write(unit=stderr, fmt='(A,ES12.5)')                            &
            "Warning: Bad fraction ", fraction
    end if

    return
end subroutine material_dist_scale_to_fraction

!> Returns .true. if material_dist contains mass fractions
pure logical function material_dist_is_mass_fraction(this)              &
    result(is_mass_fraction)
    implicit none
    !> Object reference
    class(material_dist), intent(in) :: this
    continue
    is_mass_fraction = (this%dist_type == 'm')
    return
end function material_dist_is_mass_fraction

!> Returns .true. if material_dist contains mole fractions
pure logical function material_dist_is_mole_fraction(this)              &
    result(is_mole_fraction)
    implicit none
    !> Object reference
    class(material_dist), intent(in) :: this
    continue
    is_mole_fraction = (this%dist_type == 'n')
    return
end function material_dist_is_mole_fraction

!> Returns .true. if material_dist represents gases
pure logical function material_dist_is_gas(this) result(is_gas)
    use m_material, only: phase_GAS
    implicit none
    !> Object reference
    class(material_dist), intent(in) :: this
    continue
    is_gas = (this%phase_id == phase_GAS)
    return
end function material_dist_is_gas

!> Returns .true. if material_dist represents aerosols
pure logical function material_dist_is_aerosol(this) result(is_aerosol)
    implicit none
    !> Object reference
    class(material_dist), intent(in) :: this
    continue
    is_aerosol = (this%phase_id > 0)
    return
end function material_dist_is_aerosol

!> Return aggregate formula weight of material distribution. Returns
!! zero if phase or mass/mole distribution type are unknown or if all
!! distribution fractions are zero (empty/uninitialized distribution)
pure real(kind=WP) function material_dist_mw(this) result(mw)
    use m_constant, only: ZERO
    use m_material, only: material_db
    implicit none
    !> Object reference
    class(material_dist), intent(in) :: this

    integer :: i
    integer :: id
    real(kind=WP) :: mass
    real(kind=WP) :: moles

    continue

    mw = ZERO

    if (.not. allocated(this%dist)) then
        ! ERROR: Uninitialized distribution
        return
    end if

    if (all(this%dist(:)%f <= ZERO)) then
        ! ERROR: Null distribution
        return
    end if

    if (this%is_mole_fraction()) then
        if (this%is_gas()) then
            ! mw = sum(this%dist(:)%f                                 &
            !     * material_db%gas(this%dist(:)%id)%mw)
            do i = 1, size(this%dist)
                id = this%dist(i)%id
                mw = mw + this%dist(i)%f * material_db%gas(id)%mw
            end do
        else if (this%is_aerosol()) then
            ! mw = sum(this%dist(:)%f                                 &
            !     * material_db%aerosol(this%dist(:)%id)%mw)
            do i = 1, size(this%dist)
                id = this%dist(i)%id
                mw = mw + this%dist(i)%f * material_db%aerosol(id)%mw
            end do
        else
            ! ERROR: Unknown phase
        end if
    else if (this%is_mass_fraction()) then
        moles = ZERO
        if (this%is_gas()) then
            mass = sum(this%dist(:)%f)
            do i = 1, size(this%dist)
                id = this%dist(i)%id
                moles = moles + this%dist(i)%f / material_db%gas(id)%mw
            end do
            mw = mass / moles
        else if (this%is_aerosol()) then
            mass = sum(this%dist(:)%f)
            do i = 1, size(this%dist)
                id = this%dist(i)%id
                moles = moles + this%dist(i)%f                          &
                    / material_db%aerosol(id)%mw
            end do
            mw = mass / moles
        else
            ! ERROR: Unknown phase
        end if
    else
        ! ERROR: Unknown distribution type
    end if

    return
end function material_dist_mw

!> Display internal contents and diagnostics
subroutine material_dist_dump(this)
    use iso_fortran_env, only: stderr => error_unit
    implicit none
    !> Object reference
    class(material_dist), intent(in) :: this

    integer :: i
    integer :: n_dist

10  format(A, ': ', A)
20  format(A, ': ', I0)

    continue

    write(unit=stderr, fmt=10) 'name      ', this%name
    write(unit=stderr, fmt=10) 'dist_type ', this%dist_type
    write(unit=stderr, fmt=20) 'phase_id  ', this%phase_id
    if (allocated(this%dist)) then
        n_dist = size(this%dist)
        write(unit=stderr, fmt=20) 'size(dist)', n_dist

        write(unit=stderr, fmt=10) 'dist contents', ''
        if (n_dist > 0) then
            do i = 1, n_dist
                call this%dist(i)%dump(i)
            end do
        end if
    else
        write(unit=stderr, fmt=10) 'dist is not allocated', ''
    end if

    return
end subroutine material_dist_dump

!> Return material_dist object initialized as an empty gas mass
!! distribution. If supplied with a non-empty gas parcel, the
!! returned object will be initialized with the parcel's mass
!! distribution
!!
!! Impure because of gmf%init() and allocate(gmf%dist())
type(material_dist) function new_gas_mf(name, gparcel) result (gmf)
!    use iso_fortran_env, only: stderr => error_unit
    use m_constant, only: ZERO
    use m_textio, only: munch
    use m_material, only: n_gas, gas_id, phase_GAS
    implicit none

    !> Distribution name, optional
    character(len=32), intent(in), optional :: name

    !> Gas parcel from which mass fractions will be derived, optional
    class(parcel), intent(in), optional :: gparcel

    integer :: i
    integer :: id

    logical :: seed_from_parcel
    real(kind=WP) :: total_mass

    continue

!    write(unit=stderr, fmt="('Initializing gmf')")
    call gmf%init()
    gmf%phase_id = phase_GAS
    gmf%dist_type = 'm'

!    write(unit=stderr, fmt="('Allocating gmf%dist(n_gas)')")
    allocate(gmf%dist(n_gas))

    if (present(name)) then
        gmf%name = munch(name)
    end if

    total_mass = ZERO
    seed_from_parcel = present(gparcel)
    if (seed_from_parcel) then
        total_mass = gparcel%mass()
        seed_from_parcel = (gparcel%is_gas() .and. total_mass > ZERO)
    end if

    gmf%dist(:)%f = ZERO
    gmf%dist(:)%phase_id = gmf%phase_id
    do i = 1, n_gas
        id = gas_id(i)
        ! ASSERT(i = id)
        gmf%dist(i)%id = id
        if (seed_from_parcel) then
            gmf%dist(i)%f = gparcel%species(id)%mass / total_mass
        end if
    end do

!    call gmf%dump()

    return
end function new_gas_mf

!> Return material_dist object initialized as an empty aerosol mass
!! distribution. If supplied with a non-empty aerosol parcel, the
!! returned object will be initialized with the parcel's mass
!! distribution
!!
!! Impure because of gmf%init() and allocate(gmf%dist())
type(material_dist) function new_aerosol_mf(name, aparcel) result (amf)
    use m_textio, only: munch
    use m_material, only: n_aer, aer_id, phase_CONDENSED
    implicit none

    !> Distribution name, optional
    character(len=32), intent(in), optional :: name

    !> Aerosol parcel from which mass fractions will be derived, optional
    class(parcel), intent(in), optional :: aparcel

    integer :: i
    integer :: id

    logical :: seed_from_parcel
    real(kind=WP) :: total_mass

    continue

    call amf%init()
    amf%phase_id = phase_CONDENSED
    amf%dist_type = 'm'
    allocate(amf%dist(n_aer))

    if (present(name)) then
        amf%name = munch(name)
    end if

    total_mass = ZERO
    seed_from_parcel = present(aparcel)
    if (seed_from_parcel) then
        total_mass = aparcel%mass()
        seed_from_parcel = (aparcel%is_aerosol()                        &
            .and. total_mass > ZERO)
    end if

    amf%dist(:)%f = ZERO
    amf%dist(:)%phase_id = amf%phase_id
    do i = 1, n_aer
        id = aer_id(i)
        ! ASSERT(i = id)
        amf%dist(i)%id = id
        if (seed_from_parcel) then
            amf%dist(i)%f = aparcel%species(id)%mass / total_mass
        end if
    end do

    return
end function new_aerosol_mf

!> Return material_dist object initialized as an empty gas molar
!! distribution. If supplied with a non-empty gas parcel, the
!! returned object will be initialized with the parcel's mass
!! distribution
!!
!! Impure because of gnf%init() and allocate(gnf%dist())
type(material_dist) function new_gas_nf(name, gparcel) result (gnf)
!    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    use m_constant, only: ZERO
    use m_material, only: n_gas, gas_id, phase_GAS
    implicit none

    !> Distribution name, optional
    character(len=32), intent(in), optional :: name

    !> Gas parcel from which mass fractions will be derived, optional
    class(parcel), intent(in), optional :: gparcel

    integer :: i
    integer :: id

    logical :: seed_from_parcel
    real(kind=WP) :: total_moles

    continue

!    write(unit=stderr, fmt="('Initializing gnf')")
    call gnf%init()
    gnf%phase_id = phase_GAS
    gnf%dist_type = 'n'

!    write(unit=stderr, fmt="('Allocating gnf%dist(n_gas)')")
    allocate(gnf%dist(n_gas))

    if (present(name)) then
        gnf%name = munch(name)
    end if

    total_moles = ZERO
    seed_from_parcel = present(gparcel)
    if (seed_from_parcel) then
        total_moles = gparcel%moles()
        seed_from_parcel = (gparcel%is_gas() .and. total_moles > ZERO)
    end if

    gnf%dist(:)%f = ZERO
    gnf%dist(:)%phase_id = gnf%phase_id
    do i = 1, n_gas
        id = gas_id(i)
        ! ASSERT(i = id)
        gnf%dist(i)%id = id
        if (seed_from_parcel) then
            gnf%dist(i)%f = gparcel%species(id)%moles() / total_moles
        end if
    end do

!    call gnf%dump()

    return
end function new_gas_nf

!> Return material_dist object initialized as an empty aerosol mass
!! distribution. If supplied with a non-empty aerosol parcel, the
!! returned object will be initialized with the parcel's mass
!! distribution
!!
!! Impure because of gnf%init() and allocate(gnf%dist())
type(material_dist) function new_aerosol_nf(name, aparcel) result (anf)
    use m_textio, only: munch
    use m_material, only: n_aer, aer_id, phase_CONDENSED
    implicit none

    !> Distribution name, optional
    character(len=32), intent(in), optional :: name

    !> Aerosol parcel from which mass fractions will be derived, optional
    class(parcel), intent(in), optional :: aparcel

    integer :: i
    integer :: id

    logical :: seed_from_parcel
    real(kind=WP) :: total_moles

    continue

    call anf%init()
    anf%phase_id = phase_CONDENSED
    anf%dist_type = 'n'
    allocate(anf%dist(n_aer))

    if (present(name)) then
        anf%name = munch(name)
    end if

    total_moles = ZERO
    seed_from_parcel = present(aparcel)
    if (seed_from_parcel) then
        total_moles = aparcel%moles()
        seed_from_parcel = (aparcel%is_aerosol()                        &
            .and. total_moles > ZERO)
    end if

    anf%dist(:)%f = ZERO
    anf%dist(:)%phase_id = anf%phase_id
    do i = 1, n_aer
        id = aer_id(i)
        ! ASSERT(i = id)
        anf%dist(i)%id = id
        if (seed_from_parcel) then
            anf%dist(i)%f = aparcel%species(id)%moles() / total_moles
        end if
    end do

    return
end function new_aerosol_nf

!> Return molar distribution from mass distribution
!!
!! Impure because of write() and nf%normalize()
type(material_dist) function as_nf(mf) result(nf)
    use m_material, only: material_db, n_gas, gas_id, is_gas_id,        &
        n_aer, aer_id, is_aerosol_id
    use iso_fortran_env, only: stderr => error_unit
    implicit none

    !> Mass distribution
    class(material_dist), intent(in) :: mf

    integer :: n_constituents
    integer :: i
    integer :: id
    logical :: process_as_gas

1   format("as_nf: when setting element ", I0, ", no ", A,              &
        " matched id = ", I0)
2   format("as_nf: index/id mismatch, id = ", I0, ", i = ", I0)

    continue

    nf%name = mf%name
    nf%phase_id = mf%phase_id
    nf%dist_type = "n"

    if (mf%is_mole_fraction()) then
        ! Source is already a mole fraction
        nf = mf
        return
    end if

    ! Not sure how mf can be neither a mole fraction nor a mass
    ! fraction but whatever it is, we don't know how to proceed so just
    ! return the blank nf object
    if (.not. mf%is_mass_fraction()) then
        return
    end if

    ! write(unit=stderr, fmt=fmt_a) 'Incoming mf:'
    ! call mf%dump()

    process_as_gas = mf%is_gas()
    if (process_as_gas) then
        n_constituents = n_gas
        nf = new_gas_nf()

        ! write(unit=stderr, fmt=fmt_a) 'Outgoing gas nf:'
    else if (mf%is_aerosol()) then
        n_constituents = n_aer
        nf = new_aerosol_nf()

        ! write(unit=stderr, fmt=fmt_a) 'Outgoing aerosol nf:'
    else
        ! Not sure how mf can be neither a gas nor an aerosol but
        ! whatever it is, we don't know how to proceed so just return
        ! the blank nf object with dist_type set to "n"
        return
    end if
    ! call nf%dump()

    ! allocate(nf%dist(n_constituents))

    nf%dist(:)%id = mf%dist(:)%id
    nf%dist(:)%phase_id = nf%phase_id
    do i = 1, n_constituents
        id = mf%dist(i)%id
        if (process_as_gas) then
            if (is_gas_id(id)) then
                nf%dist(id)%f = mf%dist(i)%f / material_db%gas(id)%mw
            else
                write(unit=stderr, fmt=1) i, 'gas', id
            end if
        else
            if (is_aerosol_id(id)) then
                nf%dist(id)%f = mf%dist(i)%f / material_db%aerosol(id)%mw
            else
                write(unit=stderr, fmt=2) i, 'aerosol', id
            end if
        end if

        if (id /= i) then
            write(unit=stderr, fmt=2) id, i
        end if
    end do

    call nf%normalize()

    return
end function as_nf

!> Create mass distribution from molar distribution
!!
!! Impure because of write() and mf%normalize()
type(material_dist) function as_mf(nf) result(mf)
    use m_material, only: material_db, n_gas, gas_id, is_gas_id,        &
        n_aer, aer_id, is_aerosol_id
    use iso_fortran_env, only: stderr => error_unit
    implicit none

    !> Molar distribution
    class(material_dist), intent(in) :: nf

    integer :: n_constituents
    integer :: i
    integer :: id
    logical :: process_as_gas

1   format("as_mf: when setting element ", I0, ", no ", A,              &
        " matched id = ", I0)
2   format("as_mf: index/id mismatch, id = ", I0, ", i = ", I0)

    continue

    mf%name = nf%name
    mf%phase_id = nf%phase_id
    mf%dist_type = 'm'

    if (nf%is_mass_fraction()) then
        ! Source is already a mass fraction
        mf = nf
        return
    end if

    ! Not sure how nf can be neither a mole fraction nor a mass
    ! fraction but whatever it is, we don't know how to proceed so just
    ! return the blank mf object
    if (.not. nf%is_mole_fraction()) then
        return
    end if

    process_as_gas = nf%is_gas()
    if (process_as_gas) then
        n_constituents = n_gas
    else if (mf%is_aerosol()) then
        n_constituents = n_aer
    else
        ! Not sure how nf can be neither a gas nor an aerosol but
        ! whatever it is, we don't know how to proceed so just return
        ! the blank mf object with mf%dist_type set to "m"
        return
    end if

    allocate(mf%dist(n_constituents))

    mf%dist(:)%id = nf%dist(:)%id
    mf%dist(:)%phase_id = mf%phase_id
    do i = 1, n_constituents
        id = nf%dist(i)%id
        if (process_as_gas) then
            if (is_gas_id(id)) then
                mf%dist(id)%f = nf%dist(i)%f * material_db%gas(id)%mw
            else
                write(unit=stderr, fmt=1) i, 'gas', id
            end if
        else
            if (is_aerosol_id(id)) then
                mf%dist(id)%f = nf%dist(i)%f * material_db%aerosol(id)%mw
            else
                write(unit=stderr, fmt=2) i, 'aerosol', id
            end if
        end if

        if (id /= i) then
            write(unit=stderr, fmt=2) id, i
        end if
    end do

    call mf%normalize()

    return
end function as_mf

!> Initialize inventory attributes
subroutine inventory_init(this)

    implicit none
    !> Object reference
    class(inventory), intent(inout) :: this
    continue

    ! Initialize from base class (parcel)
    call this%parcel%init()

    this%name = 'UNINITIALIZED INVENTORY         '

    call this%mf%init()
    call this%nf%init()

    return
end subroutine inventory_init

!> Update mass and mole distributions of inventory
subroutine inventory_update_distributions(this)
    use iso_fortran_env, only: stderr => error_unit
    use m_constant, only: ONE, TEN
    implicit none

    real(kind=WP), parameter :: min_mass = TEN * TINY(ONE)

    !> Object reference
    class(inventory), intent(inout) :: this

    integer :: i
    integer :: id
    real(kind=WP) :: total_mass

    continue

    ! write(unit=stderr, fmt=fmt_a) '***** Inside update_distributions()'

    if (allocated(this%species)) then
        ! write(unit=stderr, fmt=fmt_a) '***** this%species is allocated'
        if (this%is_gas()) then
            ! write(unit=stderr, fmt=fmt_a) '***** this is a gas; this%mf = new_gas_mf()'
            this%mf = new_gas_mf()
        else if (this%is_aerosol()) then
            ! write(unit=stderr, fmt=fmt_a) '***** this is an aerosol; this%mf = new_aerosol_mf()'
            this%mf = new_aerosol_mf()
        else
            ! Something is deeply wrong here
            write(unit=stderr, fmt=fmt_a) '***** no idea wtf is in this inventory - bailing'
            return
        end if

        ! write(unit=stderr, fmt=fmt_a) '***** Dump this new mf to see its phase...'
        ! call this%mf%dump()

        total_mass = this%mass()
        if (total_mass > min_mass) then
            do i = 1, size(this%species)
                id = this%species(i)%id
                this%mf%dist(id)%f = this%species(i)%mass / total_mass
            end do
        else
            this%mf%dist(:)%f = ZERO
        end if
!        call this%mf%normalize()
        this%nf = this%mf%as_nf()
    else
        write(unit=stderr, fmt=fmt_a) '***** this%species is not allocated - bailing'
    end if

    return
end subroutine inventory_update_distributions

!> Return aggregate formula weight of inventory
pure real(kind=WP) function inventory_mw(this) result(mw)
    implicit none

    !> Object reference
    class(inventory), intent(in) :: this

    continue

    mw = this%mass() / this%moles()

    return
end function inventory_mw

!> Returns a new inventory object initialized with the current
!! object's attributes. Overrides `parcel_clone()`.
type(inventory) function inventory_clone(this) result(clone)
    use m_constant, only: ZERO, ONE
    implicit none

    !> Object reference
    class(inventory), intent(in) :: this

    continue

    clone%name = this%name
    clone%phase_id = this%phase_id
    clone%mf = this%mf
    clone%nf = this%nf

    if (allocated(clone%species)) then
        deallocate(clone%species)
    end if

    if (allocated(this%species)) then
        allocate(clone%species, source=this%species)
    end if

    return
end function inventory_clone

!> Removed mass, moles, or fraction of mixture from this inventory and
!! return an inventory containing the removed mass. `dmass` represents
!! mass lost by leakage or forced outflow.
!!
!! @note Do not use this for inflow to a cell.
type(inventory) function inventory_extract_mixture(this, dmass, dmoles, &
    dfraction) result(extracted)
    use m_constant, only: ZERO, ONE
    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    !> Mass of material to remove (value should be non-negative),
    !! \f$\us{\lbm}\f$
    real(kind=WP), intent(in), optional :: dmass

    !> Mass of material to remove (value should be non-negative),
    !! \f$\us{\lbm}\f$
    real(kind=WP), intent(in), optional :: dmoles

    !> Mass of material to remove (value should be non-negative),
    !! \f$\us{\lbm}\f$
    real(kind=WP), intent(in), optional :: dfraction

    real(kind=WP) :: mass_removed
    real(kind=WP) :: mass_current
    real(kind=WP) :: mass_new
    real(kind=WP) :: f_reduce

    continue

    if (.not. allocated(this%species)) then
        extracted%name = 'Failed extraction               '
        return
    end if

    mass_removed = ZERO
    if (present(dmass)) then
        mass_removed = dmass
    end if

    if (mass_removed == ZERO .and. present(dmoles)) then
        mass_removed = dmoles * this%mw()
    end if

    if (mass_removed == ZERO .and. present(dfraction)) then
        mass_removed = dfraction * this%mass()
    end if

    extracted = this%clone_inventory()
    extracted%name = 'Extracted mass                  '

    mass_current = this%mass()
    mass_new = mass_current - mass_removed
    if (mass_new >= ZERO) then
        if (mass_current > ZERO) then
            ! Scale species masses by
            ! (mass_current - dmass) / mass_current
            f_reduce = mass_new / mass_current
            extracted%species(:)%mass = (ONE - f_reduce) * extracted%species(:)%mass
            this%species(:)%mass = f_reduce * this%species(:)%mass
!            this%species(:)%mass = this%species(:)%mass - extracted%species(:)mass
            call this%update_distributions()
        else
            if (dmass > ZERO) then
                ! Set masses of extracted species from dmass and mf
                extracted%species(:)%mass = dmass * extracted%mf%dist(:)%f
            else
                ! Error: inventory mass must be positive - this should
                ! be trapped earlier
                extracted%species(:)%mass = ZERO
            end if
        end if
    else
        ! Error: inventory mass must be positive
        extracted%species(:)%mass = ZERO
    end if

    call extracted%update_distributions()

    return
end function inventory_extract_mixture

!> Return mass distribution of inventory
type(material_dist) function inventory_as_mf(this) result(mf)
    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    continue

    if (this%is_gas()) then
        mf = new_gas_mf(name=this%name, gparcel=this)
    else if (this%is_aerosol()) then
        mf = new_aerosol_mf(name=this%name, aparcel=this)
    else
        ! Something has gone awry; just return the default empty mf
    end if

    return
end function inventory_as_mf

!> Return molar distribution of inventory
type(material_dist) function inventory_as_nf(this) result(nf)
    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    continue

    nf = as_nf(this%as_mf())

    return
end function inventory_as_nf

!> Add the mass in an inventory/parcel to the current inventory
subroutine inventory_add(this, that)
    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    !> Incoming parcel
    class(parcel), intent(in) :: that

    continue

    call this%parcel%add(that)
    call this%update_distributions()

    return
end subroutine inventory_add

!> Subtract the mass in an inventory/parcel from the current inventory
subroutine inventory_subtract(this, that)
    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    !> Outgoing parcel
    class(parcel), intent(in) :: that

    continue

    call this%parcel%subtract(that)
    call this%update_distributions()

    return
end subroutine inventory_subtract

!> Add mass of component `id` to mixture, \f$\us{\lbm}\f$
subroutine inventory_add_component_mass(this, id, dmass)
    use m_constant, only: MIN_MASS => TINY1M24
    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    !> Species ID
    integer, intent(in) :: id

    !> Mass of material to add (may be negative for mass removal),
    !! \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: dmass

    real(kind=WP) :: tmp_mass

    continue

    if (allocated(this%species)) then
        if (any(this%species(:)%id == id)                               &
            .and. id > 0 .and. id <= size(this%species)) then
            if (dmass /= ZERO) then
                ! Adjust mass of species gid, dropping ephemeral amounts
                ! (be careful here)
                tmp_mass = dmass + this%species(id)%mass
                if (tmp_mass >= MIN_MASS) then
                    this%species(id)%mass = tmp_mass
                else
                    this%species(id)%mass = ZERO
                end if
                ! Adjust mf and nf
                this%mf = this%as_mf()
                this%nf = this%mf%as_nf()
            else
                ! No change; do nothing
            end if
        else
            ! Warn on invalid species ID
        end if
    else
        ! Warn on uninitialized inventory
    end if

    return
end subroutine inventory_add_component_mass

!> @brief Add mass, moles, or fraction of component `id` from
!! mixture.
!!
!! `dmass`, `dmoles`, or `dfrac` may be negative for mass removal.
!! One and only one of `dmass`, `dmoles`, or `dfrac` should be
!! specified. In ambiguous cases, the first argument which exists and
!! is non-zero will be applied. If none are supplied or all are zero,
!! no change will be made.
subroutine inventory_add_component(this, id, dmass, dmoles, dfraction)
    use m_material, only: material_db

    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    !> Species ID
    integer, intent(in) :: id

    !> Mass of material to add, \f$\us{\lbm}\f$
    real(kind=WP), intent(in), optional :: dmass

    !> Moles of material to add
    real(kind=WP), intent(in), optional :: dmoles

    !> Fraction of material to add
    real(kind=WP), intent(in), optional :: dfraction

    ! Mass to add
    real(kind=WP) :: mass_added

    continue

    mass_added = ZERO

    if (allocated(this%species)) then

        if (present(dmass)) then
            mass_added = dmass
        end if

        if (mass_added == ZERO .and. present(dmoles)) then
            if (this%is_gas()) then
                mass_added = dmoles * material_db%gas(id)%mw
            else if (this%is_aerosol()) then
                mass_added = dmoles * material_db%aerosol(id)%mw
!            else
!                mass_added = ZERO
            end if
        end if

        if (mass_added == ZERO .and. present(dfraction)) then
            mass_added = dfraction * this%species(id)%mass
!        else
!            mass_added = ZERO
        end if

        if (mass_added /= ZERO) then
            call this%add_component_mass(id, mass_added)
        end if
    end if

    return
end subroutine inventory_add_component

!> @brief Remove mass, moles, or fraction of component `id` from
!! mixture.
!!
!! `dmass`, `dmoles`, or `dfrac` may be negative for mass addition.
!! One and only one of `dmass`, `dmoles`, or `dfrac` should be
!! specified. In ambiguous cases, the first argument which exists and
!! is non-zero will be applied. If none are supplied or all are zero,
!! no change will be made.
subroutine inventory_remove_component(this, id, dmass, dmoles,          &
    dfraction)
    use m_material, only: material_db

    implicit none

    !> Object reference
    class(inventory), intent(inout) :: this

    !> Species ID
    integer, intent(in) :: id

    !> Mass of material to remove, \f$\us{\lbm}\f$
    real(kind=WP), intent(in), optional :: dmass

    !> Moles of material to remove
    real(kind=WP), intent(in), optional :: dmoles

    !> Fraction of material to remove
    real(kind=WP), intent(in), optional :: dfraction

    ! Mass to add
    real(kind=WP) :: mass_added

    continue

    mass_added = ZERO

    if (allocated(this%species)) then

        if (present(dmass)) then
            mass_added = -dmass
        end if

        if (mass_added == ZERO .and. present(dmoles)) then
            if (this%is_gas()) then
                mass_added = -dmoles * material_db%gas(id)%mw
            else if (this%is_aerosol()) then
                mass_added = -dmoles * material_db%aerosol(id)%mw
!            else
!                mass_added = ZERO
            end if
        end if

        if (mass_added == ZERO .and. present(dfraction)) then
            mass_added = -dfraction * this%species(id)%mass
!        else
!            mass_added = ZERO
        end if

        if (mass_added /= ZERO) then
            call this%add_component_mass(id, mass_added)
        end if
    end if

    return
end subroutine inventory_remove_component

!> @brief Display inventory object internal contents and diagnostics
subroutine inventory_dump(this)
    use iso_fortran_env, only: stderr => error_unit
    implicit none

    !> Inventory object reference
    class(inventory), intent(in) :: this

10  format(A)

    continue

    call this%parcel%dump()
    write(unit=stderr, fmt=10) 'Mass distribution:'
    call this%mf%dump()
    write(unit=stderr, fmt=10) 'Molar distribution:'
    call this%nf%dump()

    return
end subroutine inventory_dump

!> Return inventory object initialized with an empty gas inventory.
!! Species masses may be initialized by specifying an aggregate mass
!! and a mass distribution or an aggregate number of moles and a molar
!! distribution.
!!
!! Impure because of ginventory%init() and allocate(ginventory%species())
type(inventory) function new_gas_inventory(name, mass, mf, moles, nf)   &
    result (ginventory)
    use m_textio, only: munch
    use m_material, only: material_db, n_gas, gas_id, phase_GAS
    implicit none

    !> Gas inventory name, optional
    character(len=32), intent(in), optional :: name

    !> Total inventory mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: mass

    !> Mass distribution, optional
    type(material_dist), intent(in), optional :: mf

    !> Total inventory mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: moles

    !> Molar distribution, optional
    type(material_dist), intent(in), optional :: nf

    integer :: i

    logical :: set_from_mass
    logical :: set_from_moles

    continue

    call ginventory%init()
    ginventory%phase_id = phase_GAS
    allocate(ginventory%species(n_gas))
    ginventory%species(:)%mass = ZERO
    ginventory%species(:)%phase_id = ginventory%phase_id

    if (present(name)) then
        ginventory%name = munch(name)
    end if

    set_from_mass = (present(mass) .and. present(mf))
    if (set_from_mass) then
        set_from_mass = (mf%is_gas() .and. mass > ZERO)
    end if

    set_from_moles = (present(moles) .and. present(nf))
    if (set_from_moles) then
        set_from_moles = (nf%is_gas() .and. moles > ZERO)
    end if

    do i = 1, n_gas
        ginventory%species(i)%id = gas_id(i)
        ! ASSERT (ginventory%species(i)%id == i)
        if (set_from_mass) then
            ginventory%species(i)%mass = mf%dist(i)%f * mass
        else if (set_from_moles) then
            ginventory%species(i)%mass = nf%dist(i)%f * moles           &
                * material_db%gas(i)%mw
        end if
    end do

    call ginventory%update_distributions()

    return
end function new_gas_inventory

!> Return inventory object initialized with an empty aerosol inventory.
!! Species masses may be initialized by specifying an aggregate mass
!! and a mass distribution or an aggregate number of moles and a molar
!! distribution.
!!
!! Impure because of ainventory%init() and allocate(ainventory%species())
type(inventory) function new_aerosol_inventory(name, mass, mf, moles,   &
    nf) result (ainventory)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    use m_material, only: material_db, n_aer, aer_id, phase_CONDENSED
    implicit none

    !> Gas inventory name, optional
    character(len=32), intent(in), optional :: name

    !> Total inventory mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: mass

    !> Mass distribution, optional
    type(material_dist), intent(in), optional :: mf

    !> Total inventory mass (\f$\us{\lbm}\f$), optional
    real(kind=WP), intent(in), optional :: moles

    !> Molar distribution, optional
    type(material_dist), intent(in), optional :: nf

    integer :: i

    logical :: set_from_mass
    logical :: set_from_moles

    continue

    ! write(unit=stderr, fmt=fmt_a) '***** Inside new_aerosol_inventory()'

    call ainventory%init()
    ainventory%phase_id = phase_CONDENSED
    allocate(ainventory%species(n_aer))
    ainventory%species(:)%mass = ZERO
    ainventory%species(:)%phase_id = ainventory%phase_id

    if (present(name)) then
        ainventory%name = munch(name)
    end if

    set_from_mass = (present(mass) .and. present(mf))
    if (set_from_mass) then
        set_from_mass = (mf%is_aerosol() .and. mass > ZERO)
    end if

    set_from_moles = (present(moles) .and. present(nf))
    if (set_from_moles) then
        set_from_moles = (nf%is_aerosol() .and. moles > ZERO)
    end if

    do i = 1, n_aer
        ainventory%species(i)%id = aer_id(i)
        ! ASSERT (ainventory%species(i)%id == i)
        if (set_from_mass) then
            ainventory%species(i)%mass = mf%dist(i)%f * mass
        else if (set_from_moles) then
            ainventory%species(i)%mass = nf%dist(i)%f * moles           &
                * material_db%aerosol(i)%mw
        end if
    end do

    ! write(unit=stderr, fmt=fmt_a) '***** Before ainventory%update_distributions()'
    call ainventory%update_distributions()
    ! write(unit=stderr, fmt=fmt_a) '***** After ainventory%update_distributions()'

    return
end function new_aerosol_inventory

!> Define mole fraction distribution of dry air. Data taken from
!! @cite USStdAtmos1976
type(material_dist) function get_nf_dry_air() result(nf_dry_air)
!    use iso_fortran_env, only: stderr => error_unit
    use m_material, only: gid_N2, gid_O2, gid_Ar, gid_CO2, gid_Ne,      &
        gid_He, gid_Kr, gid_Xe, gid_CH4, gid_H2!, n_gas
    implicit none
    integer, parameter :: n_air_species = 10;
    ! Constituent IDs
    integer, dimension(n_air_species) :: gid = (/                       &
        gid_N2, gid_O2, gid_Ar, gid_CO2, gid_Ne,                        &
        gid_He, gid_Kr, gid_Xe, gid_CH4, gid_H2 /)
    ! Constituent mole fractions, taken from @cite USStdAtmos1976
    real(kind=WP), dimension(n_air_species) :: f = (/                   &
        7.8084E-1_WP, 2.09476E-1_WP, 9.34E-3_WP, 3.14E-4_WP,            &
        1.818E-05_WP, 5.24E-06_WP, 1.14E-06_WP, 8.70E-08_WP,            &
        2.00E-06_WP, 5.00E-07_WP /)

    ! type(material_dist) :: tmp_mf
    continue

    ! write(unit=stderr, fmt="('Setting tmp_mf')")
    ! tmp_mf = new_gas_mf()
    ! write(unit=stderr, fmt="('Converting tmp_mf to nf_dry_air')")
    ! nf_dry_air = tmp_mf%as_nf()
    nf_dry_air = as_nf(new_gas_mf())

!    call nf_dry_air%dump()

    ! write(unit=stderr, fmt="('Setting nf_dry_air%name')")
    nf_dry_air%name = 'Dry air                         '
    ! write(unit=stderr, fmt="('Array-setting nf_dry_air%dist%f')")
    nf_dry_air%dist(gid(1:n_air_species))%f = f(1:n_air_species)

!    call nf_dry_air%dump()

    return
end function get_nf_dry_air

end module m_inventory