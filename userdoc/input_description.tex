\chapter{Input Data Instructions}

\section{SOFIRE~II, One-Cell}

A list of input quantities, their definitions and card formats are given in
Table~\ref{tbl:c1_input}. The input deck consists of 18 alphanumeric variable
title cards followed by the title card and 18 input parameter cards. Subsequent
cases require only a new title card and those cards with new data fields.

A full example file is shown in Section~\ref{input:c1}. Figure~\ref{fig:c1_sample_input} shows sample input for cards 19--37 (case title and input parameters; 
input parameter labels on cards 1--18 are omitted).

\begin{table}[htbp]
    \begin{center}
        \caption{One-cell input description\label{tbl:c1_input}}%
        \begin{tabularx}{\textwidth}{cclXc}
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
1-18, 20-37 & 1       & \texttt{N}             & Termination character; '\texttt{1}' indicates end of scenario & - \\
1-18, 20-37 & 2-6     & \texttt{LOC}           & Array index of first input parameter on card; first case must start with 1, last case must start with 86 & - \\
1-18, 20-37 & 7-12    & \texttt{LOC1}          & TBD                   & - \\
\midrule
1-18        & 13-24   & \texttt{LABEL(LOC)}    & Input parameter label & - \\
1-18        & 25-36   & \texttt{LABEL(LOC+1)}  & Input parameter label & - \\
1-18        & 37-48   & \texttt{LABEL(LOC+2)}  & Input parameter label & - \\
1-18        & 49-60   & \texttt{LABEL(LOC+3)}  & Input parameter label & - \\
1-18        & 61-72   & \texttt{LABEL(LOC+4)}  & Input parameter label & - \\
\midrule
19          & 1-72    & \texttt{TITLE}         & Title of case         & - \\
\midrule
20          & 2-6     & \texttt{LOC} = 1       & Index of first input parameter on card & - \\
20          & 13-24   & \texttt{TSI}           & Initial temperature, sodium surface node & \us{\rankine} \\
20          & 25-36   & \texttt{TS1I}          & Initial temperature, sodium pool node 1  & \us{\rankine} \\
20          & 37-48   & \texttt{TS2I}          & Initial temperature, sodium pool node 2  & \us{\rankine} \\
20          & 49-60   & \texttt{TS3I}          & Initial temperature, sodium pool node 3  & \us{\rankine} \\
20          & 61-72   & \texttt{TS4I}          & Initial temperature, sodium pool node 4  & \us{\rankine} \\
\midrule
21          & 2-6     & \texttt{LOC} = 6       & Index of first input parameter on card & - \\
21          & 13-24   & \texttt{TF1I}          & Initial temperature, cell floor node 1   & \us{\rankine} \\
21          & 25-36   & \texttt{TF2I}          & Initial temperature, cell floor node 2   & \us{\rankine} \\
21          & 37-48   & \texttt{TF3I}          & Initial temperature, cell floor node 3   & \us{\rankine} \\
21          & 49-60   & \texttt{TF4I}          & Initial temperature, cell floor node 4   & \us{\rankine} \\
21          & 61-72   & \texttt{TWC1I}         & Initial temperature, cell wall node 1    & \us{\rankine} \\
\midrule
22          & 2-6     & \texttt{LOC} = 11      & Index of first input parameter on card & - \\
22          & 13-24   & \texttt{TWC2I}         & Initial temperature, cell wall node 2    & \us{\rankine} \\
22          & 25-36   & \texttt{TWC3I}         & Initial temperature, cell wall node 3    & \us{\rankine} \\
22          & 37-48   & \texttt{TWC4I}         & Initial temperature, cell wall node 4    & \us{\rankine} \\
22          & 49-60   & \texttt{TGASCI}        & Initial gas temperature                  & \us{\rankine} \\
22          & 61-72   & \texttt{PGASCI}        & Initial gas pressure                     & \us{\psf} absolute \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c1_input} One-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule

23          & 2-6     & \texttt{LOC} = 16      & Index of first input parameter on card & - \\
23          & 13-24   & \texttt{TI}            & Initial problem time                     & \us{\hour} \\
23          & 25-36   & \texttt{TA}            & Ambient gas temperature                  & \us{\rankine} \\
23          & 37-48   & \texttt{PA}            & Ambient gas pressure                     & \us{\psf} absolute \\
23          & 49-60   & \texttt{XMI}           & Initial sodium burning rate              & \us{\lbm\per\hour\square\foot} \\
23          & 61-72   & \texttt{XMAX}          & Maximum problem time                     & \si{\hour} \\
\midrule
24          & 2-6     & \texttt{LOC} = 21      & Index of first input parameter on card & - \\
24          & 13-24   & \texttt{AF}            & Product of emissivity and view factor, sodium pool to walls & dimensionless \\
24          & 25-36   & \texttt{A1}            & Surface area of sodium pool              & \us{\square\foot} \\
24          & 37-48   & \texttt{A2}            & Cell wall area (exposed)                 & \us{\square\foot} \\
24          & 49-60   & \texttt{A5}            & Wetted area of sodium pool               & \us{\square\foot} \\
24          & 61-72   & \texttt{CO}            & Initial oxygen concentration             & weight fraction \\
\midrule
25          & 2-6     & \texttt{LOC} = 26      & Index of first input parameter on card   & - \\
25          & 13-24   & \texttt{ANA}           & \ce{Na} / \ce{O2} burning ratio          & dimensionless \\
25          & 25-36   & \texttt{QC}            & Heat of combustion                       & \us{\BTU\per\lbm} \\
25          & 37-48   & \texttt{VOLAC}         & Cell volume                              & \us{\cubic\foot} \\
25          & 49-60   & \texttt{AF2}           & Radiation exchange, floor liner to floor node 2 & dimensionless \\
25          & 61-72   & \texttt{AF3}           & Radiation exchange, sodium pool to gas   & dimensionless \\
\midrule
26          & 2-6     & \texttt{LOC} = 31      & Index of first input parameter on card   & - \\
26          & 13-24   & \texttt{SOD}           & Initial sodium spill mass                & \us{\lbm} \\
26          & 25-36   & \texttt{PRT1}          & Print interval, $0.0 \leq t \leq 1.0$    & ? \\
26          & 37-48   & \texttt{PRT2}          & Print interval, $1.0 < t \leq 3.0$       & ? \\
26          & 49-60   & \texttt{PRT3}          & Print interval, $t > 3.0$                & ? \\
26          & 61-72   & \texttt{F3}            & Cell exhaust flowrate   & \us{\cubic\foot\per\hour} \\
\midrule
27          & 2-6     & \texttt{LOC} = 36      & Index of first input parameter on card   & - \\
27          & 13-24   & \texttt{CON}           & Pressure leakage factor                  & - \\
27          & 25-36   & \texttt{X}             & Timestep reduction factor                & - \\
27          & 37-48   & \texttt{S1}            & Sodium removal rate, node 1              & \us{\lbm\per\hour} \\
27          & 49-60   & \texttt{S2}            & Sodium removal rate, node 2              & \us{\lbm\per\hour} \\
27          & 61-72   & \texttt{S3}            & Sodium removal rate, node 3              & \us{\lbm\per\hour} \\
\midrule
28          & 2-6     & \texttt{LOC} = 41      & Index of first input parameter on card   & - \\
28          & 13-24   & \texttt{S4}            & Sodium removal rate, node 4              & \us{\lbm\per\hour} \\
28          & 25-36   & \texttt{ZNAS}          & Sodium surface node thickness            & \us{\foot} \\
28          & 37-48   & \texttt{ZNAS1}         & Sodium thickness, node 1                 & \us{\foot} \\
28          & 49-60   & \texttt{ZNAS2}         & Sodium thickness, node 2                 & \us{\foot} \\
28          & 61-72   & \texttt{ZNAS3}         & Sodium thickness, node 3                 & \us{\foot} \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c1_input} One-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
29          & 2-6     & \texttt{LOC} = 46      & Index of first input parameter on card & - \\
29          & 13-24   & \texttt{ZNAS4}         & Sodium thickness, node 4                 & \us{\foot} \\
29          & 25-36   & \texttt{ZF1}           & Cell floor thickness, node 1             & \us{\foot} \\
29          & 37-48   & \texttt{ZF2}           & Cell floor thickness, node 2             & \us{\foot} \\
29          & 49-60   & \texttt{ZF3}           & Cell floor thickness, node 3             & \us{\foot} \\
29          & 61-72   & \texttt{ZF4}           & Cell floor thickness, node 4             & \us{\foot} \\
\midrule
30          & 2-6     & \texttt{LOC} = 51      & Index of first input parameter on card & - \\
30          & 13-24   & \texttt{XWC1}          & Cell floor thickness, node 1             & \us{\foot} \\
30          & 25-36   & \texttt{XWC2}          & Cell floor thickness, node 2             & \us{\foot} \\
30          & 37-48   & \texttt{XWC3}          & Cell floor thickness, node 3             & \us{\foot} \\
30          & 49-60   & \texttt{XWC4}          & Cell floor thickness, node 4             & \us{\foot} \\
30          & 61-72   & \texttt{CPAC}          & Specific heat of cell gas                & \us{\BTU\per\lbm\per\fahrenheit} \\
\midrule
31          & 2-6     & \texttt{LOC} = 56      & Index of first input parameter on card & - \\
31          & 13-24   & \texttt{CPS}           & Specific heat of sodium                  & \us{\BTU\per\lbm\per\fahrenheit} \\
31          & 25-36   & \texttt{CPF1}          & Specific heat of cell floor, node 1      & \us{\BTU\per\lbm\per\fahrenheit} \\
31          & 37-48   & \texttt{CPF2}          & Specific heat of cell floor, node 2      & \us{\BTU\per\lbm\per\fahrenheit} \\
31          & 49-60   & \texttt{CPF3}          & Specific heat of cell floor, node 3      & \us{\BTU\per\lbm\per\fahrenheit} \\
31          & 61-72   & \texttt{CPF4}          & Specific heat of cell floor, node 4      & \us{\BTU\per\lbm\per\fahrenheit} \\
\midrule
32          & 2-6     & \texttt{LOC} = 61      & Index of first input parameter on card & - \\
32          & 13-24   & \texttt{CPWC1}         & Specific heat of cell floor, node 1      & \us{\BTU\per\lbm\per\fahrenheit} \\
32          & 25-36   & \texttt{CPWC2}         & Specific heat of cell floor, node 2      & \us{\BTU\per\lbm\per\fahrenheit} \\
32          & 37-48   & \texttt{CPWC3}         & Specific heat of cell floor, node 3      & \us{\BTU\per\lbm\per\fahrenheit} \\
32          & 49-60   & \texttt{CPWC4}         & Specific heat of cell floor, node 4      & \us{\BTU\per\lbm\per\fahrenheit} \\
32          & 61-72   & \texttt{AKS}           & Thermal conductivity of sodium           & \us{\BTU\per\hour\per\foot\per\rankine} \\
\midrule
33          & 2-6     & \texttt{LOC} = 66      & Index of first input parameter on card   & - \\
33          & 13-24   & \texttt{AKF1}          & Thermal conductivity of floor, node 1    & \us{\BTU\per\hour\per\foot\per\rankine} \\
33          & 25-36   & \texttt{AKF12}         & Thermal conductivity of floor, air gap   & \us{\BTU\per\hour\per\foot\per\rankine} \\
33          & 37-48   & \texttt{AKF2}          & Thermal conductivity of floor, node 2    & \us{\BTU\per\hour\per\foot\per\rankine} \\
33          & 49-60   & \texttt{AKF3}          & Thermal conductivity of floor, node 3    & \us{\BTU\per\hour\per\foot\per\rankine} \\
33          & 61-72   & \texttt{AKF4}          & Thermal conductivity of floor, node 4    & \us{\BTU\per\hour\per\foot\per\rankine} \\
\midrule
34          & 2-6     & \texttt{LOC} = 71      & Index of first input parameter on card   & - \\
34          & 13-24   & \texttt{AKWC1}         & Thermal conductivity of wall, node 1     & \us{\BTU\per\hour\per\foot\per\rankine} \\
34          & 25-36   & \texttt{AKWC12}        & Thermal conductivity of wall, air gap    & \us{\BTU\per\hour\per\foot\per\rankine} \\
34          & 37-48   & \texttt{AKWC2}         & Thermal conductivity of wall, node 2     & \us{\BTU\per\hour\per\foot\per\rankine} \\
34          & 49-60   & \texttt{AKWC3}         & Thermal conductivity of wall, node 3     & \us{\BTU\per\hour\per\foot\per\rankine} \\
34          & 61-72   & \texttt{AKWC4}         & Thermal conductivity of wall, node 4     & \us{\BTU\per\hour\per\foot\per\rankine} \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c1_input} One-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
35          & 2-6     & \texttt{LOC} = 76      & Index of first input parameter on card   & - \\
35          & 13-24   & \texttt{RHOA}          & Gas density                              & \us{\lbm\per\cubic\foot} \\
35          & 25-36   & \texttt{RHS}           & Sodium density                           & \us{\lbm\per\cubic\foot} \\
35          & 37-48   & \texttt{RHF1}          & Density of floor, node 1                 & \us{\lbm\per\cubic\foot} \\
35          & 49-60   & \texttt{RHF2}          & Density of floor, node 2                 & \us{\lbm\per\cubic\foot} \\
35          & 61-72   & \texttt{RHF3}          & Density of floor, node 3                 & \us{\lbm\per\cubic\foot} \\
\midrule
36          & 2-6     & \texttt{LOC} = 81      & Index of first input parameter on card   & - \\
36          & 13-24   & \texttt{RHF4}          & Density of floor, node 4                 & \us{\lbm\per\cubic\foot} \\
36          & 25-36   & \texttt{RHWC1}         & Density of wall, node 1                  & \us{\lbm\per\cubic\foot} \\
36          & 37-48   & \texttt{RHWC2}         & Density of wall, node 2                  & \us{\lbm\per\cubic\foot} \\
36          & 49-60   & \texttt{RHWC3}         & Density of wall, node 3                  & \us{\lbm\per\cubic\foot} \\
36          & 61-72   & \texttt{RHWC4}         & Density of wall, node 4                  & \us{\lbm\per\cubic\foot} \\
\midrule
37          & 1       & \texttt{N} = 1         & Termination character; '\texttt{1}' indicates end of scenario & - \\
37          & 2-6     & \texttt{LOC} = 86      & Index of first input parameter on card   & - \\
37          & 13-24   & \texttt{ZF12}          & Thickness of floor-liner air gap         & \us{\foot} \\
37          & 25-36   & \texttt{ZWC12}         & Thickness of wall-liner air gap          & \us{\foot} \\
\bottomrule
\end{tabularx}
\end{center}
\end{table}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c1_input_data_card}
    \end{center}
    \caption{Example one-cell input}
    \label{fig:c1_sample_input}
\end{figure}

\section{SOFIRE~II, Two-Cell}

A list of input quantities, their definitions, and card formats are given in
the following tabulation. The input deck consists of 24 alphanumeric variable
title cards followed by the title card and 24 input parameter cards. Subsequent
cases only need 3 new title cards and input cards with new input parameters;
only fields with new data need be entered.

\begin{table}[htbp]
    \begin{center}
        \caption{Two-cell input description\label{tbl:c2_input}}%
        \begin{tabularx}{\textwidth}{cclXc}
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
1-24, 25-48 & 1       & \texttt{N}             & Termination character; '\texttt{1}' indicates end of scenario & - \\
1-24, 25-48 & 2-6     & \texttt{LOC}           & Array index of first input parameter on card; first case must start with 1, last case must start with 116 & - \\
1-24, 25-48 & 7-12    & \texttt{LOC1}          & TBD                   & - \\
\midrule
1-24        & 13-24   & \texttt{LABEL(LOC)}    & Input parameter label & - \\
1-24        & 25-36   & \texttt{LABEL(LOC+1)}  & Input parameter label & - \\
1-24        & 37-48   & \texttt{LABEL(LOC+2)}  & Input parameter label & - \\
1-24        & 49-60   & \texttt{LABEL(LOC+3)}  & Input parameter label & - \\
1-24        & 61-72   & \texttt{LABEL(LOC+4)}  & Input parameter label & - \\
\midrule
25          & 1-72    & \texttt{TITLE}         & Title of case         & - \\
\midrule
26          & 2-6     & \texttt{LOC} = 1       & Index of first input parameter on card & - \\
26          & 13-24   & \texttt{TSI}           & Initial temperature, sodium surface node & \us{\rankine} \\
26          & 25-36   & \texttt{TS1I}          & Initial temperature, sodium pool node 1  & \us{\rankine} \\
26          & 37-48   & \texttt{TS2I}          & Initial temperature, sodium pool node 2  & \us{\rankine} \\
26          & 49-60   & \texttt{TS3I}          & Initial temperature, sodium pool node 3  & \us{\rankine} \\
26          & 61-72   & \texttt{TS4I}          & Initial temperature, sodium pool node 4  & \us{\rankine} \\
\midrule
27          & 2-6     & \texttt{LOC} = 6       & Index of first input parameter on card & - \\
27          & 13-24   & \texttt{TFC1I}         & Initial temperature, primary cell floor node 1   & \us{\rankine} \\
27          & 25-36   & \texttt{TFC2I}         & Initial temperature, primary cell floor node 2   & \us{\rankine} \\
27          & 37-48   & \texttt{TFC3I}         & Initial temperature, primary cell floor node 3   & \us{\rankine} \\
27          & 49-60   & \texttt{TFC4I}         & Initial temperature, primary cell floor node 4   & \us{\rankine} \\
27          & 61-72   & \texttt{TWC1I}         & Initial temperature, primary cell wall node 1    & \us{\rankine} \\
\midrule
28          & 2-6     & \texttt{LOC} = 11      & Index of first input parameter on card & - \\
28          & 13-24   & \texttt{TWC2I}         & Initial temperature, primary cell wall node 2    & \us{\rankine} \\
28          & 25-36   & \texttt{TWC3I}         & Initial temperature, primary cell wall node 3    & \us{\rankine} \\
28          & 37-48   & \texttt{TWC4I}         & Initial temperature, primary cell wall node 4    & \us{\rankine} \\
28          & 49-60   & \texttt{TWS1I}         & Initial temperature, secondary cell wall node 1  & \us{\rankine} \\
28          & 61-72   & \texttt{TWS2I}         & Initial temperature, secondary cell wall node 2  & \us{\rankine} \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c2_input} Two-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
29          & 2-6     & \texttt{LOC} = 16      & Index of first input parameter on card & - \\
29          & 13-24   & \texttt{TWS3I}         & Initial temperature, secondary cell wall node 3  & \us{\rankine} \\
29          & 25-36   & \texttt{TWS4I}         & Initial temperature, secondary cell wall node 4  & \us{\rankine} \\
29          & 37-48   & \texttt{TGASCI}        & Initial primary gas temperature                          & \us{\rankine} \\
29          & 49-60   & \texttt{PGASCI}        & Initial primary gas pressure                             & \us{\rankine} \\
29          & 61-72   & \texttt{CO}            & Initial oxygen concentration                     & weight fraction \\
\midrule
30          & 2-6     & \texttt{LOC} = 21      & Index of first input parameter on card   & - \\
30          & 13-24   & \texttt{TGASSI}        & Initial secondary gas temperature        & \us{\rankine} \\
30          & 25-36   & \texttt{PGASSI}        & Initial secondary gas pressure           & \us{\rankine} \\
30          & 37-48   & \texttt{TI}            & Initial problem time                     & \us{\hour} \\
30          & 49-60   & \texttt{TA}            & Ambient gas temperature                  & \us{\rankine} \\
30          & 61-72   & \texttt{PA}            & Ambient gas pressure                     & \us{\psf} absolute \\
\midrule
31          & 2-6     & \texttt{LOC} = 26      & Index of first input parameter on card & - \\
31          & 13-24   & \texttt{XMAX}          & Maximum problem time                     & \si{\hour} \\
31          & 25-36   & \texttt{ANA}           & \ce{Na} / \ce{O2} burning ratio          & dimensionless \\
31          & 37-48   & \texttt{QC}            & Heat of \ce{Na} combustion               & \us{\BTU\per\lbm} \\
31          & 49-60   & \texttt{QO}            & Primary floor liner cooler heat removal rate & \us{\BTU\per\hour} \\
31          & 61-72   & \texttt{Q1}            & Primary wall liner cooler heat removal rate  & \us{\BTU\per\hour} \\
\midrule
32          & 2-6     & \texttt{LOC} = 31      & Index of first input parameter on card   & - \\
32          & 13-24   & \texttt{A1}            & ??? Surface area of sodium pool          & \us{\square\foot} \\
32          & 25-36   & \texttt{A2}            & Primary cell wall area (exposed)         & \us{\square\foot} \\
32          & 37-48   & \texttt{A3}            & ??? Wetted area of sodium pool           & \us{\square\foot} \\
32          & 49-60   & \texttt{A4}            & One half throat opening                  & \us{\square\foot} \\
32          & 61-72   & \texttt{A6}            & Secondary cell wall area (exposed)       & \us{\square\foot} \\
\midrule
33          & 2-6     & \texttt{LOC} = 36      & Index of first input parameter on card   & - \\
33          & 13-24   & \texttt{VOLAC}         & Primary cell volume                      & \us{\cubic\foot} \\
33          & 25-36   & \texttt{VOLAS}         & Secondary cell volume                    & \us{\cubic\foot} \\
33          & 37-48   & \texttt{AF}            & Radiation exchange coefficient, sodium to primary wall & dimensionless \\
33          & 49-60   & \texttt{AF}            & Radiation exchange coefficient, sodium to primary gas  & dimensionless \\
33          & 61-72   & \texttt{AWC}           & ??? Area of wetted primary floor       & \us{\square\foot} \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c2_input} Two-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
34          & 2-6     & \texttt{LOC} = 41      & Index of first input parameter on card   & - \\
34          & 13-24   & \texttt{F1}            & Radiation exchange coefficient, primary floor liner to floor node 2 & dimensionless \\
34          & 25-36   & \texttt{F4}            & Radiation exchange coefficient, secondary floor liner to floor node 2 & dimensionless \\
34          & 37-48   & \texttt{XL}            & Diameter of throat opening               & \us{\foot} \\
34          & 49-60   & \texttt{HIN}           & Height of throat opening                 & \us{\foot} \\
34          & 61-72   & \texttt{X}             & Timestep reduction factor                & dimensionless \\
\midrule
35          & 2-6     & \texttt{LOC} = 46      & Index of first input parameter on card   & - \\
35          & 13-24   & \texttt{S}             & Thermal heating or cooling in sodium pool & \us{\BTU\per\hour\per\fahrenheit} \\
35          & 25-36   & \texttt{SOD}           & Initial sodium spill mass                & \us{\lbm} \\
35          & 37-48   & \texttt{PRT1}          & Print interval, $0.0 \leq t \leq 1.0$    & ? \\
35          & 49-60   & \texttt{PRT2}          & Print interval, $1.0 < t \leq 3.0$       & ? \\
35          & 61-72   & \texttt{PRT3}          & Print interval, $t > 3.0$                & ? \\
\midrule
36          & 2-6     & \texttt{LOC} = 51      & Index of first input parameter on card   & - \\
36          & 13-24   & \texttt{ZNAS}          & Sodium surface node thickness            & \us{\foot} \\
36          & 25-36   & \texttt{ZNAS1}         & Sodium thickness, node 1                 & \us{\foot} \\
36          & 37-48   & \texttt{ZNAS2}         & Sodium thickness, node 2                 & \us{\foot} \\
36          & 49-60   & \texttt{ZNAS3}         & Sodium thickness, node 3                 & \us{\foot} \\
36          & 61-72   & \texttt{ZNAS4}         & Sodium thickness, node 4                 & \us{\foot} \\
\midrule
37          & 2-6     & \texttt{LOC} = 56      & Index of first input parameter on card & - \\
37          & 13-24   & \texttt{ZF1}           & Primary cell floor thickness, node 1             & \us{\foot} \\
37          & 25-36   & \texttt{ZF12}          & Air gap thickness between primary cell floor nodes 1 and 2 & \us{\foot} \\
37          & 37-48   & \texttt{ZF2}           & Primary cell floor thickness, node 2             & \us{\foot} \\
37          & 49-60   & \texttt{ZF3}           & Primary cell floor thickness, node 3             & \us{\foot} \\
37          & 61-72   & \texttt{ZF4}           & Primary cell floor thickness, node 4             & \us{\foot} \\
\midrule
38          & 2-6     & \texttt{LOC} = 61      & Index of first input parameter on card & - \\
38          & 13-24   & \texttt{XWC1}          & Primary cell wall thickness, node 1             & \us{\foot} \\
38          & 25-36   & \texttt{XWC12}         & Air gap thickness between primary cell wall nodes 1 and 2 & \us{\foot} \\
38          & 37-48   & \texttt{XWC2}          & Primary cell wall thickness, node 2             & \us{\foot} \\
38          & 49-60   & \texttt{XWC3}          & Primary cell wall thickness, node 3             & \us{\foot} \\
38          & 61-72   & \texttt{XWC4}          & Primary cell wall thickness, node 4             & \us{\foot} \\
\midrule
39          & 2-6     & \texttt{LOC} = 66      & Index of first input parameter on card          & - \\
39          & 13-24   & \texttt{XWs1}          & Secondary cell wall thickness, node 1           & \us{\foot} \\
39          & 25-36   & \texttt{XWs12}         & Air gap thickness between secondary cell wall nodes 1 and 2 & \us{\foot} \\
39          & 37-48   & \texttt{XWs2}          & Secondary cell wall thickness, node 2           & \us{\foot} \\
39          & 49-60   & \texttt{XWs3}          & Secondary cell wall thickness, node 3           & \us{\foot} \\
39          & 61-72   & \texttt{XWs4}          & Secondary cell wall thickness, node 4           & \us{\foot} \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c2_input} Two-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
40          & 2-6     & \texttt{LOC} = 71      & Index of first input parameter on card          & - \\
40          & 13-24   & \texttt{CPAS}          & Specific heat of air at constant volume         & \us{\BTU\per\lbm\per\fahrenheit} \\
40          & 25-36   & \texttt{CPS}           & Specific heat of sodium                         & \us{\BTU\per\lbm\per\fahrenheit} \\
40          & 37-48   & \texttt{CPF1}          & Specific heat of primary cell floor, node 1     & \us{\BTU\per\lbm\per\fahrenheit} \\
40          & 49-60   & \texttt{CPF2}          & Specific heat of primary cell floor, node 2     & \us{\BTU\per\lbm\per\fahrenheit} \\
40          & 61-72   & \texttt{CPF3}          & Specific heat of primary cell floor, node 3     & \us{\BTU\per\lbm\per\fahrenheit} \\
\midrule
41          & 2-6     & \texttt{LOC} = 76      & Index of first input parameter on card          & - \\
41          & 13-24   & \texttt{CPF4}          & Specific heat of primary cell floor, node 4     & \us{\BTU\per\lbm\per\fahrenheit} \\
41          & 25-36   & \texttt{CPWC1}         & Specific heat of primary cell wall, node 1      & \us{\BTU\per\lbm\per\fahrenheit} \\
41          & 37-48   & \texttt{CPWC2}         & Specific heat of primary cell wall, node 2      & \us{\BTU\per\lbm\per\fahrenheit} \\
41          & 49-60   & \texttt{CPWC3}         & Specific heat of primary cell wall, node 3      & \us{\BTU\per\lbm\per\fahrenheit} \\
41          & 61-72   & \texttt{CPWC4}         & Specific heat of primary cell wall, node 4      & \us{\BTU\per\lbm\per\fahrenheit} \\
\midrule
42          & 2-6     & \texttt{LOC} = 81      & Index of first input parameter on card          & - \\
42          & 13-24   & \texttt{CPWS1}         & Specific heat of secondary cell wall, node 1    & \us{\BTU\per\lbm\per\fahrenheit} \\
42          & 25-36   & \texttt{CPWS2}         & Specific heat of secondary cell wall, node 2    & \us{\BTU\per\lbm\per\fahrenheit} \\
42          & 37-48   & \texttt{CPWS3}         & Specific heat of secondary cell wall, node 3    & \us{\BTU\per\lbm\per\fahrenheit} \\
42          & 49-60   & \texttt{CPWS4}         & Specific heat of secondary cell wall, node 4    & \us{\BTU\per\lbm\per\fahrenheit} \\
42          & 61-72   & \texttt{AKS}           & Thermal conductivity of sodium                  & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
\midrule
43          & 2-6     & \texttt{LOC} = 86      & Index of first input parameter on card          & - \\
43          & 13-24   & \texttt{AKF1}          & Thermal conductivity of primary cell floor, node 1 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
43          & 25-36   & \texttt{AKF12}         & Thermal conductivity of air gap between primary cell floor node 1 and node 2 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
43          & 37-48   & \texttt{AKF2}          & Thermal conductivity of primary cell floor, node 2 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
43          & 49-60   & \texttt{AKF3}          & Thermal conductivity of primary cell floor, node 3 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
43          & 61-72   & \texttt{AKF4}          & Thermal conductivity of primary cell floor, node 4 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
\midrule
\multicolumn{5}{c}{\textit{Continued on next page}} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\begin{table}[htbp]
    \begin{center}
        \begin{tabularx}{\textwidth}{cclXc}
\multicolumn{5}{c}{\textbf{Table~\ref{tbl:c2_input} Two-cell input description -- \textit{Continued from previous page}}} \\
\toprule
Card No.    & Columns & Variable               & Description           & Unit \\
\midrule
44          & 2-6     & \texttt{LOC} = 91      & Index of first input parameter on card          & - \\
44          & 13-24   & \texttt{AKWC1}         & Thermal conductivity of primary cell wall, node 1 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
44          & 25-36   & \texttt{AKWC12}        & Thermal conductivity of air gap between primary cell wall node 1 and node 2 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
44          & 37-48   & \texttt{AKWC2}         & Thermal conductivity of primary cell wall, node 2 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
44          & 49-60   & \texttt{AKWC3}         & Thermal conductivity of primary cell wall, node 3 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
44          & 61-72   & \texttt{AKWC4}         & Thermal conductivity of primary cell wall, node 4 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
\midrule
45          & 2-6     & \texttt{LOC} = 96      & Index of first input parameter on card          & - \\
45          & 13-24   & \texttt{AKWS1}         & Thermal conductivity of secondary cell wall, node 1 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
45          & 25-36   & \texttt{AKWS12}        & Thermal conductivity of air gap between secondary cell wall node 1 and node 2 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
45          & 37-48   & \texttt{AKWS2}         & Thermal conductivity of secondary cell wall, node 2 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
45          & 49-60   & \texttt{AKWS3}         & Thermal conductivity of secondary cell wall, node 3 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
45          & 61-72   & \texttt{AKWS4}         & Thermal conductivity of secondary cell wall, node 4 & \us{\BTU\per\lbm\per\foot\per\fahrenheit} \\
\midrule
46          & 2-6     & \texttt{LOC} = 101     & Index of first input parameter on card   & - \\
46          & 13-24   & \texttt{RHOA}          & Gas density                              & \us{\lbm\per\cubic\foot} \\
46          & 25-36   & \texttt{RHS}           & Sodium density                           & \us{\lbm\per\cubic\foot} \\
46          & 37-48   & \texttt{RHF1}          & Density of primary cell floor, node 1    & \us{\lbm\per\cubic\foot} \\
46          & 49-60   & \texttt{RHF2}          & Density of primary cell floor, node 2    & \us{\lbm\per\cubic\foot} \\
46          & 61-72   & \texttt{RHF3}          & Density of primary cell floor, node 3    & \us{\lbm\per\cubic\foot} \\
\midrule
47          & 2-6     & \texttt{LOC} = 106     & Index of first input parameter on card   & - \\
47          & 13-24   & \texttt{RHF4}          & Density of primary cell floor, node 4    & \us{\lbm\per\cubic\foot} \\
47          & 25-36   & \texttt{RHWC1}         & Density of primary cell wall, node 1     & \us{\lbm\per\cubic\foot} \\
47          & 37-48   & \texttt{RHWC2}         & Density of primary cell wall, node 2     & \us{\lbm\per\cubic\foot} \\
47          & 49-60   & \texttt{RHWC3}         & Density of primary cell wall, node 3     & \us{\lbm\per\cubic\foot} \\
47          & 61-72   & \texttt{RHWC4}         & Density of primary cell wall, node 4     & \us{\lbm\per\cubic\foot} \\
\midrule
48          & 2-6     & \texttt{LOC} = 111     & Index of first input parameter on card   & - \\
48          & 13-24   & \texttt{RHWS1}         & Density of secondary cell wall, node 1   & \us{\lbm\per\cubic\foot} \\
48          & 25-36   & \texttt{RHWS2}         & Density of secondary cell wall, node 2   & \us{\lbm\per\cubic\foot} \\
48          & 37-48   & \texttt{RHWS3}         & Density of secondary cell wall, node 3   & \us{\lbm\per\cubic\foot} \\
48          & 49-60   & \texttt{RHWS4}         & Density of secondary cell wall, node 4   & \us{\lbm\per\cubic\foot} \\
48          & 61-72   & \texttt{PDLEAK}        & Design leak rate                         & \us{\cubic\foot\per\minute} \\
\midrule
49          & 2-6     & \texttt{LOC} = 116     & Index of first input parameter on card   & - \\
49          & 13-24   & \texttt{BLDGDP}        & Design pressure for leak rate            & \us{\lbf\per\square\foot} \\
49          & 25-36   & \texttt{PATCON}        & Atmospheric pressure change over \SI{24}{\hour} & \us{\lbf\per\square\foot\per\hour} \\
\bottomrule
\end{tabularx}
    \end{center}
\end{table}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{c2_input_data_card}
    \end{center}
    \caption{Example two-cell input}
    \label{fig:c2_sample_input}
\end{figure}
