!> @file m_textio.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Common text processing functions
module m_textio
    use iso_fortran_env, only: REAL32, REAL64, REAL128,                 &
        INT8, INT16, INT32, INT64!, stderr => ERROR_UNIT

    implicit none

    private

    !> Generic interface to integer-type-specific string conversion
    !! functions
    interface itoa
        procedure i8toa
        procedure i16toa
        procedure i32toa
        procedure i64toa
    end interface itoa

    !> Generic interface to real-type-specific string conversion functions
    interface ftoa
        procedure r32toa
        procedure r64toa
        procedure r128toa
    end interface ftoa

    public :: ucase
    public :: lcase
    public :: munch
    public :: itoa
    public :: i8toa
    public :: i16toa
    public :: i32toa
    public :: i64toa
    public :: ftoa
    public :: r32toa
    public :: r64toa
    public :: r128toa
    public :: ltoa
    public :: timestamp
    public :: basename

contains

!> Function to set lower case ASCII characters to upper case
subroutine ucase(str)

!> String to modify
    character(len=*), intent(inout) :: str

    integer :: i
    continue

    do i = 1, len_trim(str)
        select case(str(i:i))
        case("a":"z")
            str(i:i) = achar(iachar(str(i:i)) - 32)
        end select
    end do

    return
end subroutine ucase

!> Function to set upper case ASCII characters to lower case
subroutine lcase(str)

!> String to modify
    character(len=*), intent(inout) :: str

    integer :: i
    continue

    do i = 1, len_trim(str)
        select case(str(i:i))
        case("A":"Z")
            str(i:i) = achar(iachar(str(i:i)) + 32)
        end select
    end do

    return
end subroutine lcase

!> Removes leading and trailing whitespace from a string, i.e. what
!! one expects trim() to do instead of behaving like ltrim()
pure function munch(str)
    implicit none

    character (len=:), allocatable :: munch

!> String to modify
    character (len=*), intent(in) :: str

    continue

    munch = trim(adjustl(str))

    return
end function munch

!> Produces a text representation of an integer value. Arbitrarily
!! limited to 16 characters.
function i8toa(i)
    implicit none

    character (len=:), allocatable :: i8toa

!> Integer to represent as text
    integer(kind=INT8), intent(in) :: i

    character (len=5) :: str

    continue

    write(str, '(I5)') i
    i8toa = trim(adjustl(str))

    return
end function i8toa

!> Produces a text representation of an integer value. Arbitrarily
!! limited to 16 characters.
function i16toa(i)
    implicit none

    character (len=:), allocatable :: i16toa

!> Integer to represent as text
    integer(kind=INT16), intent(in) :: i

    character (len=7) :: str

    continue

    write(str, '(I7)') i
    i16toa = trim(adjustl(str))

    return
end function i16toa

!> Produces a text representation of an integer value. Arbitrarily
!! limited to 16 characters.
function i32toa(i)
    implicit none

    character (len=:), allocatable :: i32toa

!> Integer to represent as text
    integer(kind=INT32), intent(in) :: i

    character (len=12) :: str

    continue

    write(str, '(I12)') i
    i32toa = trim(adjustl(str))

    return
end function i32toa

!> Produces a text representation of an integer value. Arbitrarily
!! limited to 22 characters.
function i64toa(i)
    implicit none

    character (len=:), allocatable :: i64toa

!> Integer to represent as text
    integer(kind=INT64), intent(in) :: i

    character (len=22) :: str

    continue

    write(str, '(I22)') i
    i64toa = trim(adjustl(str))

    return
end function i64toa

!> Produces a text representation of an floating point value. Defaults
!! to a format specifier of ES11.4E2 but mode, length, and significant
!! figures are each optionally adjustable.
function r32toa(f, ef, l, sl, el)
    implicit none

    character (len=:), allocatable :: r32toa

!> Real value to format
    real(kind=REAL32), intent(in) :: f

!> Format specifier (D, E, ES, F, G)
    character (len=*), intent(in), optional :: ef

!> Total width of formatted number
    integer, intent(in), optional :: l

!> Number of decimal digits to display
    integer, intent(in), optional :: sl

!> Number of exponent digits to display; default = 2
    integer, intent(in), optional :: el

    character (len=:), allocatable :: ef_a
    integer :: l_a
    integer :: sl_a
    integer :: el_a

    character (len=:), allocatable :: ffmt
    character (len=32) :: str

    continue

    if (present(ef)) then
        ef_a = munch(ef)
        if (len(ef_a) < 1) then
            ef_a = 'ES'
        end if
    else
        ef_a = 'ES'
    end if

    if (present(l)) then
        l_a = l
    else
        l_a = 11
    end if

    if (present(sl)) then
        sl_a = max(0, min(sl, 8))
    else
       sl_a = 4
    end if

    if (present(el)) then
        el_a = max(1, min(el, 4))
    else
        el_a = 2
    end if

    ! Expand l_a if it is too short to hold significand sign, integral
    ! digit, radix, exponent indicator, and exponent sign
    ! (5 characters) plus the widths of the decimal fraction (sl_a) and
    ! exponent (el_a)
    l_a = max(5 + sl_a + el_a, l_a)

    ffmt = '(' // ef_a // itoa(l_a) // '.' // itoa(sl_a) // 'E'         &
        // itoa(el_a) // ')'

    ! write(unit=stderr, fmt='(A)') '>> rr32 format is ' // ffmt

    write(str, ffmt) f
    r32toa = trim(adjustl(str))

    return
end function r32toa

!> Produces a text representation of an floating point value. Defaults
!! to a format specifier of ES13.4E3 but mode, length, and significant
!! figures are each optionally adjustable.
function r64toa(f, ef, l, sl, el)
    implicit none

    character (len=:), allocatable :: r64toa

!> Real value to format
    real(kind=REAL64), intent(in) :: f

!> Format specifier (D, E, ES, F, G)
    character (len=*), intent(in), optional :: ef

!> Total width of formatted number
    integer, intent(in), optional :: l

!> Number of decimal digits to display
    integer, intent(in), optional :: sl

!> Number of exponent digits to display, default = 3
    integer, intent(in), optional :: el

    character (len=:), allocatable :: ef_a
    integer :: l_a
    integer :: sl_a
    integer :: el_a

    character (len=:), allocatable :: ffmt
    character (len=32) :: str

    continue

    if (present(ef)) then
        ef_a = munch(ef)
        if (len_trim(adjustl(ef_a)) < 1) then
            ef_a = 'ES'
        end if
    else
        ef_a = 'ES'
    end if

    if (present(l)) then
        l_a = l
    else
        l_a = 13
    end if

    if (present(sl)) then
       sl_a = max(0, min(sl, 16))
    else
       sl_a = 4
    end if

    if (present(el)) then
        el_a = max(1, min(el, 4))
    else
        el_a = 3
    end if

    ! Expand l_a if it is too short to hold significand sign, integral
    ! digit, radix, exponent indicator, and exponent sign
    ! (5 characters) plus the widths of the decimal fraction (sl_a) and
    ! exponent (el_a)
    l_a = max(5 + sl_a + el_a, l_a)

    ffmt = '(' // ef_a // itoa(l_a) // '.' // itoa(sl_a) // 'E'         &
        // itoa(el_a) // ')'

    ! write(unit=stderr, fmt='(A)') '>> rr64 format is ' // ffmt

    write(str, ffmt) f
    r64toa = trim(adjustl(str))

    return
end function r64toa

!> Produces a text representation of an floating point value. Defaults
!! to a format specifier of ES16.4E4 but mode, length, and significant
!! figures are each optionally adjustable.
function r128toa(f, ef, l, sl, el)
    implicit none

    character (len=:), allocatable :: r128toa

!> Real value to format
    real(kind=REAL128), intent(in) :: f

!> Format specifier (D, E, ES, F, G)
    character (len=*), intent(in), optional :: ef

!> Total width of formatted number
    integer, intent(in), optional :: l

!> Number of decimal digits to display
    integer, intent(in), optional :: sl

!> Number of exponent digits to display, default = 4
    integer, intent(in), optional :: el

    character (len=:), allocatable :: ef_a
    integer :: l_a
    integer :: sl_a
    integer :: el_a

    character (len=:), allocatable :: ffmt
    character (len=48) :: str

    continue

    if (present(ef)) then
        ef_a = munch(ef)
        if (len(ef_a) < 1) then
            ef_a = 'ES'
        end if
    else
        ef_a = 'ES'
    end if

    if (present(l)) then
        l_a = l
    else
        l_a = 16
    end if

    if (present(sl)) then
       sl_a = max(0, min(sl, 35))
    else
       sl_a = 4
    end if

    if (present(el)) then
        el_a = max(1, min(el, 4))
    else
        el_a = 4
    end if

    ! Expand l_a if it is too short to hold significand sign, integral
    ! digit, radix, exponent indicator, and exponent sign
    ! (5 characters) plus the widths of the decimal fraction (sl_a) and
    ! exponent (el_a)
    l_a = max(5 + sl_a + el_a, l_a)

    ffmt = '(' // ef_a // itoa(l_a) // '.' // itoa(sl_a) // 'E'         &
        // itoa(el_a) // ')'

    ! write(unit=stderr, fmt='(A)') '>> rr128 format is ' // ffmt

    write(str, ffmt) f
    r128toa = trim(adjustl(str))

    return
end function r128toa

!> Produces a text representation of a logical value.
function ltoa(lvar)
    implicit none

    character (len=:), allocatable :: ltoa

!> Logical value
    logical, intent(in) :: lvar

    continue

    if (lvar) then
        ltoa = '.true.'
    else
        ltoa = '.false.'
    end if

    return
end function ltoa

!> @brief Return timestamp in the format YYYYMMDDhhmmss based on the
!! current time
character(len=14) function timestamp() result (ts)
    implicit none

    character(len=8) :: dtag
    character(len=10) :: ttag

    continue

    call date_and_time(date=dtag, time=ttag)
    ts = dtag // ttag(1:6)

    return
end function timestamp

!> @brief Return the base name of a file
function basename(file)
    implicit none

    ! Return value
    character(len=:), allocatable :: basename

    character(len=*), intent(in) :: file

    integer :: dot_pos
    integer :: dirsep_pos
    integer :: left_mark
    integer :: right_mark

    continue

    ! Strip directory, if any. Treat '/' and '\' as directory
    ! separators.
    left_mark = 1
    dirsep_pos = scan(string=file, set='/\', back=.true.)
    if (dirsep_pos > 0) then
        left_mark = dirsep_pos + 1
    end if

    ! Strip directory, if any. Treat '/' and '\' as directory
    ! separators.
    right_mark = len_trim(file)
    dot_pos = scan(string=file, set='.', back=.true.)
    if (dot_pos > left_mark) then
        right_mark = dot_pos - 1
    end if

    if (left_mark > right_mark) then
        ! Something is wrong; use something distinctive so user
        ! can trap/diagnose this
        basename = '__BASENAME__'
    else
        basename = munch(file(left_mark:right_mark))
    end if

    return
end function basename

end module m_textio
